// auth 
export { default as Auth_Login } from '../componets/auth/Login';
export { default as Auth_Register } from '../componets/auth/Register';

export { default as Edit_Profile } from '../componets/auth/passwords/Edit_Profile';
export { default as Forgotten_Password } from '../componets/auth/passwords/Forgotten_Password';
export { default as Reset_Password } from '../componets/auth/passwords/Reset_Password';

// frontend Export
export { default as Frontend } from '../componets/frontend';
// before_afters
export { default as Frontend_BeforeAfter } from '../componets/frontend/before_afters/Before_after';
export { default as Frontend_BeforeAfters } from '../componets/frontend/before_afters/Before_afters';
// consultings
export { default as Frontend_Consulting } from '../componets/frontend/consultings/Consulting';
export { default as Frontend_Consultings } from '../componets/frontend/consultings/Consultings';
export { default as Frontend_Consultings_Create } from '../componets/frontend/consultings/Create';
export { default as Frontend_Consultings_Edit } from '../componets/frontend/consultings/Edit';
export { default as Frontend_Consultings_Answer } from '../componets/frontend/consultings/Answer';
// events
export { default as Frontend_Event } from '../componets/frontend/events/Event';
export { default as Frontend_Events } from '../componets/frontend/events/Events';
// includes
export { default as Frontend_Header } from '../componets/frontend/includes/Header';
export { default as Frontend_Footer } from '../componets/frontend/includes/Footer';
export { default as Frontend_RouterError } from '../componets/frontend/includes/router_error/RouterError';
// notices
export { default as Frontend_Notice } from '../componets/frontend/notices/Notice';
export { default as Frontend_Notices } from '../componets/frontend/notices/Notices';
// reservations
export { default as Frontend_Reservation } from '../componets/frontend/reservations/Reservation';
export { default as Frontend_Reservations } from '../componets/frontend/reservations/Reservations';
export { default as Frontend_Reservations_Create } from '../componets/frontend/reservations/Create';
export { default as Frontend_Reservations_Edit } from '../componets/frontend/reservations/Edit';

// intro
export { default as Infor } from '../componets/frontend/intro/Infor';
export { default as Doctor_infor } from '../componets/frontend/intro/Doctor_infor';
export { default as Look_around } from '../componets/frontend/intro/Look_around';
export { default as Medical_treatment_guide } from '../componets/frontend/intro/Medical_treatment_guide';
export { default as Location } from '../componets/frontend/intro/Location';

// conditions
export { default as Email_collection } from '../componets/frontend/condition/Email_collection';
export { default as Personal_information } from '../componets/frontend/condition/Personal_information';
export { default as Provision } from '../componets/frontend/condition/Provision';

// contents
export { default as Dynamic_Component } from '../componets/frontend/contents/Dynamic_Component';


// backend Export
export { default as Backend } from '../componets/backend';
// before_afters
export { default as Backend_BeforeAfter } from '../componets/backend/before_afters/Before_after';
export { default as Backend_BeforeAfters } from '../componets/backend/before_afters/Before_afters';
export { default as Backend_BeforeAfter_Create } from '../componets/backend/before_afters/Create';
export { default as Backend_BeforeAfter_Edit } from '../componets/backend/before_afters/Edit';
// // consultings
export { default as Backend_Consulting_Answer } from '../componets/backend/consultings/Answer';
export { default as Backend_Consultings } from '../componets/backend/consultings/Consultings';
// // events
export { default as Backend_Event } from '../componets/backend/events/Event';
export { default as Backend_Events } from '../componets/backend/events/Events';
export { default as Backend_Event_Create } from '../componets/backend/events/Create';
export { default as Backend_Event_Edit } from '../componets/backend/events/Edit';

// expense_guides
export { default as Backend_ExpenseGuide } from '../componets/backend/expense_guides/ExpenseGuide';
export { default as Backend_ExpenseGuides } from '../componets/backend/expense_guides/ExpenseGuides';

// includes
export { default as Backend_Header } from '../componets/backend/includes/Header';
export { default as Backend_Footer } from '../componets/backend/includes/Footer';

// notices
export { default as Backend_Notice } from '../componets/backend/notices/Notice';
export { default as Backend_Notices } from '../componets/backend/notices/Notices';
export { default as Backend_Notice_Create } from '../componets/backend/notices/Create';
export { default as Backend_Notice_Edit } from '../componets/backend/notices/Edit';

// popups
export { default as Backend_Popup } from '../componets/backend/popups/Popup';
export { default as Backend_Popups } from '../componets/backend/popups/Popups';
export { default as Backend_Popup_Create } from '../componets/backend/popups/Create';
export { default as Backend_Popup_Edit } from '../componets/backend/popups/Edit';

// // reservations
export { default as Backend_Reservation } from '../componets/backend/reservations/Reservation';
export { default as Backend_Reservations } from '../componets/backend/reservations/Reservations';
// export { default as Backend_Reservations_Edit } from '../componets/backend/reservations/Edit';
// // Users
// export { default as Backend_Users } from '../componets/backend/users/Users';
// // Visiters
// export { default as Backend_Users } from '../componets/backend/visitors/Visiters';