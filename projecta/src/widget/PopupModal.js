import React, { Fragment } from 'react';
import { currentUsesDomain } from 'constants/index';
import './Popup.css';

const imgUrl = currentUsesDomain.imgUrl;

export default function PopupModal ({layer, mobile, closePopupModal}) {
    const { layer_width, layer_height, layer_x, layer_y, photo_file, re_pop_up_time, id } = layer;
    let deviceTop = layer_y;

    if (typeof layer_y === 'number') {
        deviceTop = mobile ? layer_y + 48 : layer_y + 115;
    }
    return (
        <Fragment>
            {
                photo_file && (
                    <div className="popup-container" style={{left: layer_x, top: deviceTop}}>
                        <div className="popup-container-wrap">
                            <img src={imgUrl + photo_file} style={{maxWidth: layer_width, maxHeight: layer_height, objectFit: 'cover'}} />
                            <div className="popup-closed" onClick={() => closePopupModal(id)}> 
                                <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="#3E2626" fillRule="evenodd" clipRule="evenodd"><path d="M12 11.293l10.293-10.293.707.707-10.293 10.293 10.293 10.293-.707.707-10.293-10.293-10.293 10.293-.707-.707 10.293-10.293-10.293-10.293.707-.707 10.293 10.293z"/></svg>
                            </div>
                            <div className="popup-closed-text" onClick={() => closePopupModal(id, true)}>{re_pop_up_time + '시간 동안 창을 열지 않습니다. [ 닫기 ]'}</div>
                        </div>
                    </div>
                    
                )
            }
        </Fragment>
    )
}