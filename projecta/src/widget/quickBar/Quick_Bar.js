import React from 'react';
import { Link } from 'react-router-dom';
import './styleQuick.css';

export default function QuickBar({ isLowerThanHeader }) {
    const style = {
        zIndex: 99,
        position: 'fixed',
        top: '200px',
        borderRadius: '20px',
        backgroundColor: '#fff',
        transition: '0.3s',
        boxShadow: '0px 2px 14px 5px rgba(0,0,0,0.1)',
        color:'#605043',
    };

    const lowerHeaderStyle = {
        ...style,
        top: '100px'
    }



    return (
        <ul className="quick-bar quick-bar-content" style={isLowerThanHeader ? lowerHeaderStyle : style}>
            <li className="quick-bar-top">
                <Link to={`/events`} className="quick-menu">
                <div className="quick-icon-01"/>
                    <span className="quick-title">이벤트</span>
                </Link>
            </li>
            <li>
                <Link to={`/consultings`} className="quick-menu">
                <div className="quick-icon-02"/>
                    <span className="quick-title"> 온라인 상담</span>
                </Link>
            </li>
            <li>
                <Link to={`/before_afters`} className="quick-menu">
                <div className="quick-icon-03"/>
                    <span className="quick-title">전후사진</span>
                </Link>
            </li>
            <li>
                <Link to={`/location`} className="quick-menu">
                <div className="quick-icon-04"/>
                <span className="quick-title">오시는 길</span>
                </Link>
            </li>
            <li className="quick-kakao">
                <a href="http://pf.kakao.com/_jXxmxaxb" className="quick-menu-kakao">
                <div className="quick-icon-05"/>
                <span className="quick-title">카톡상담</span>
                <span className="quick-kakao-title">미밸런스</span>
                </a>
            </li>
            <li className="quick-number">
                <Link to={`/`} className="quick-menu-number">
                전화상담<span className="quick-number-text">031.<br/>962.<br/>7781</span>
                </Link>
            </li>
        </ul>
    )
}