import React, { Component, Fragment } from 'react';

class boardTopTextbox extends React.Component {
    render(){
      return (
      <Fragment>
          <div className="board-top-style">
              <ul className="board-title-box">
                  <li className="board-title">공지사항</li>
                  <li className="board-title-box-line"></li>
                  <li className="board-text">MIBALANCE NOTICE</li>
              </ul>
            </div>
      </Fragment>
    );
  };
}
export default boardTopTextbox;