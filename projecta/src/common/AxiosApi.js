import axios from 'axios';

class axiosApi {
    constructor() {

    }

    getApi(url) {
        return axios.get(url);
    }

    postApi(url, payload) {
        return axios.post(url, payload);
    }

    deleteApi(url) {
        return axios.get(url);
    }

    deleteAllApi(url, payload) {
        return axios.post(url, payload);
    }


}

export default axiosApi;