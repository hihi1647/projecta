import myCookie from './MyCookie';
import { ROLES } from 'constants/index';

// need to be improve(copy things.)
class User {
    constructor() {
        this.name = '';
        this.email = '';
        this.role = '';

        /* this.email_verified_at = rawData.email_verified_at;
        this.status = rawData.status;
        this.created_by = rawData.created_by;
        this.updated_by = rawData.updated_by;
        this.created_at = rawData.created_at;
        this.updated_at = rawData.updated_at; */

        this.isAdmin = false;
        this.isNurse = false;
        this.isUser = false;
    }

    static fromCookie() {
        const user = myCookie.getUser();
        if (user) {
            if (typeof user === 'string') {
                return User.fromJSON(user);   
            } else {
                return User.fromObject(user);
            }
        }

        return new User();
    }

    static fromObject(raw) {
        const user = new User();

        user.assignUserDatas(raw);
        user.setRoles();
        return user;
    }

    static fromJSON(raw) {
        const user = new User();

        user.assignUserDatas(JSON.parse(raw));
        user.setRoles();
        return user;
    }

    copy(raw) {
        this.assignUserDatas(raw);
        this.setRoles();
        this.updateCookie(this);
        return this;
    }

    assignUserDatas(rawUser) {
        const newUser = {};

        Object.keys(rawUser).forEach(key => {
            if (key !== 'access_token' || key !== 'token') {
                newUser[key] = rawUser[key]
            }
        });

        Object.assign(this, newUser);
    }

    updateCookie(newUser) {
        let userUpdate = newUser;
        if (!userUpdate) {
            userUpdate = this.getTiny();   
        }
        myCookie.setUser(userUpdate);
    }

    getTiny() {
        const tinyUser = {};

        Object.keys(this).forEach(key => {
            tinyUser[key] = this[key];
        });

        return tinyUser;
    }

    setRoles() {
        this.isAdmin = ROLES.admin.roles.includes(this.role.role_id);
        this.isNurse = ROLES.nurse.roles.includes(this.role.role_id);
        this.isUser = ROLES.user.roles.includes(this.role.role_id); 
    }

    getRole() {
        return this.role;
    }

    deepCopy() {
        return JSON.parse(JSON.stringify(this));
    }

}

export default User;