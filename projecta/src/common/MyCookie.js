import * as Cookies from 'js-cookie';

class MyCookie {
    constructor() {
        this.cookies = Cookies;
        this.POPUP = 'mibalance_popup';
        this.USER = 'mibalance_user';
        this.AUTH_TOKEN = 'mibalance_auth';
        this.REFRESH_TOKEN = 'mibalance_auth_ref';

        this.domain = window.location.hostname;
    }

    set(key, val) {
        this.checkCookieSize(val);

        const expires = 60 * 60 * 1000;
        const inOneHour = new Date(new Date().getTime() + expires);

        this.cookies.set(key, val, {
            domain: this.domain,
            expires: inOneHour
        })
    }

    setInfinitely(key, val) {
        this.checkCookieSize(val);

        this.cookies.set(key, val);
    }

    checkCookieSize(val) {
        let stringfied = val;
        if (typeof val !== 'string') {
            stringfied = JSON.stringify(val);
        }

        if (stringfied.length > 4000) {
            console.error('Cookie size is almost reached at 4KB');
            return false;
        }
        return true;
    }

    get(key) {
        return this.cookies.get(key);
    }

    getJSON(key) {
        return this.cookies.getJSON(key);
    }

    remove(key) {
        this.cookies.remove(key, {
            domain: this.domain
        });
    }

    clear() {
        this.remove(this.USER);
        this.remove(this.POPUP);

        this.remove(this.AUTH_TOKEN);
        this.remove(this.REFRESH_TOKEN);
    }

    clearUser() {
        this.remove(this.USER);
    }

    clearPopup() {
        this.remove(this.POPUP);
    }

    setTokens(token) {
        this.setTokens(token.accessToken);
        this.setRefreshToken(token.refreshToken);
    }

    setToken(token) {
        this.set(this.AUTH_TOKEN, token);
    }

    getToken() {
        return this.get(this.AUTH_TOKEN);
    }

    setRefreshToken(refreshToken) {
        this.set(this.REFRESH_TOKEN, refreshToken);
    }

    getRefreshToken() {
        return this.get(this.REFRESH_TOKEN);
    }

    setUser(user) {
        this.set(this.USER, user);
    }

    getUser() {
        return this.getJSON(this.USER);
    }

    setPopup(popup) {
        this.set(this.POPUP, popup);
    }

    getPopup() {
        return this.getJSON(this.POPUP);
    }
}

const myCookie = new MyCookie();
window.myCookie = myCookie;

export default myCookie;