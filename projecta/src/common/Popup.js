import myCookie from './MyCookie';

// need to be improve(copy things.)
class Popup {
    constructor() {
    }

    static fromCookie() {
        const popup = myCookie.getPopup();
        if (popup) {
            if (typeof popup === 'string') {
                return Popup.fromJSON(popup);   
            } else {
                return Popup.fromObject(popup);
            }
        }

        return new Popup();
    }

    static fromObject(raw) {
        const popup = new Popup();

        popup.assignPopupDatas(raw);

        return popup;
    }

    static fromJSON(raw) {
        const popup = new Popup();

        popup.assignPopupDatas(JSON.parse(raw));

        return popup;
    }

    copy(raw) {
        this.assignPopupDatas(raw);

        this.updateCookie(this);
        return this;
    }

    assignPopupDatas(rawPopup) {
        const newPopup = {};

        Object.keys(rawPopup).forEach(key => {
            if (key !== 'access_token' || key !== 'token') {
                newPopup[key] = rawPopup[key]
            }
        });

        Object.assign(this, newPopup);
    }

    updateCookie(newPopup) {
        let popupUpdate = newPopup;
        if (!popupUpdate) {
            popupUpdate = this.getTiny();   
        }
        myCookie.setPopup(popupUpdate);
    }

    getTiny() {
        const tiny = {};

        Object.keys(this).forEach(key => {
            tiny[key] = this[key];
        });

        return tiny;
    }

    deepCopy() {
        return JSON.parse(JSON.stringify(this));
    }

}

export default Popup;