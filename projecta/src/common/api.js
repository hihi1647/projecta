import axiosApi from './AxiosApi';
import { currentUsesDomain }  from 'constants/index'

const currentUsesDomainApi = currentUsesDomain.api;

class api extends axiosApi {
    constructor() {
        super();
    }

    getUser() {

    }

    signIn(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/oauth/token`, payload);
    }

    checkEmailDuplicated(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/email_double_check`, payload);
    }

    registerUser(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/register`, payload);
    }

    sendResetPasswordEmail(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/send_reset_password_email`, payload);
    }

    checkEmailLinkIsValid(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/check_is_there_code`, payload);
    }

    resetPassword(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/reset_password `, payload);
    }

    getUserData(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/user/edit`, payload);
    }

    updateUser(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/user/update`, payload);
    }

    deleteUser(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/user/delete_account`, payload);
    }
    

    getFrontendLinks(isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/frontend`);
    }

    getFrontendDetailPage(pageId, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/menu_details/${pageId}`);
    }

    uploadImage(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/images/upload`, payload);
    }

    // mainPage

    getMainPage(isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/mainpage`);
    }

    // expenseguide
    getExpenseGuideCategories(isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/expenseguides/setup`);
    }

    createExpenseGuide(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/expenseguides/store`, payload);
    }

    getSearchExpenseGuide(payload, page, isBackend) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/expenseguides/search${page ? '?page=' + page : ''}`, payload);
    }

    getExpenseGuides(page, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/expenseguides${page ? '?page=' + page : ''}`);
    }

    getExpenseGuide(id, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/expenseguides/expenseguide/${id}`);
    }

    getUpdateExpenseGuide(id, isBackend = false) {
        return this.getApi(`${isBackend ? '../../' : ''}../../${currentUsesDomainApi}/expenseguides/expenseguide/${id}/edit`);
    }

    updateExpenseGuide(id, payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../../' : ''}../../${currentUsesDomainApi}/expenseguides/expenseguide/${id}/update`, payload);
    }

    deleteExpenseGuide(id, isBackend = false) {
        return this.deleteApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/expenseguides/expenseguide/${id}/destroy`);
    }
    
    deleteExpenseGuidesAll(payload, isBackend = false) {
        return this.deleteAllApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/expenseguides/destroyall`, payload);
    }



    // consultings
    checkUserPassword(id, payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/consultings/consulting/${id}/checkpassword`, payload);
    }
    
    getConsultingCategories(isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/consultings/setup`);
    }

    createConsulting(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/consultings/store`, payload);
    }
    
    getSearchConsulting(payload, page, isBackend) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/consultings/search${page ? '?page=' + page : ''}`, payload);
    }

    getConsultings(page, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/consultings${page ? '?page=' + page : ''}`);
    }

    getConsulting(id, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/consultings/consulting/${id}`);
    }

    getUpdateConsulting(id, isBackend = false) {
        return this.getApi(`${isBackend ? '../../' : ''}../../${currentUsesDomainApi}/consultings/consulting/${id}/edit`);
    }

    updateConsulting(id, payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../../' : ''}../../${currentUsesDomainApi}/consultings/consulting/${id}/update`, payload);
    }

    deleteConsulting(id, isBackend = false) {
        return this.deleteApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/consultings/consulting/${id}/destroy`);
    }
    
    deleteConsultingsAll(payload, isBackend = false) {
        return this.deleteAllApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/consultings/destroyall`, payload);
    }




    // notice
    getNoticeCategories(isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/notices/setup`);
    }

    createNotice(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/notices/store`, payload);
    }
    
    getSearchNotice(payload, page, isBackend) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/notices/search${page ? '?page=' + page : ''}`, payload);
    }

    getNotices(page, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/notices${page ? '?page=' + page : ''}`);
    }

    getNotice(id, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/notices/notice/${id}`);
    }

    getUpdateNotice(id, isBackend = false) {
        return this.getApi(`${isBackend ? '../../' : ''}../../${currentUsesDomainApi}/notices/notice/${id}/edit`);
    }

    updateNotice(id, payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../../' : ''}../../${currentUsesDomainApi}/notices/notice/${id}/update`, payload);
    }

    deleteNotice(id, isBackend = false) {
        return this.deleteApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/notices/notice/${id}/destroy`);
    }
    
    deleteNoticesAll(payload, isBackend = false) {
        return this.deleteAllApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/notices/destroyall`, payload);
    }




    // event
    getEventCategories(isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/events/setup`);
    }

    createEvent(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/events/store`, payload);
    }
    
    getSearchEvent(payload, page, isBackend) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/events/search${page ? '?page=' + page : ''}`, payload);
    }

    getEvents(page, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/events${page ? '?page=' + page : ''}`);
    }

    getEvent(id, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/events/event/${id}`);
    }

    getUpdateEvent(id, isBackend = false) {
        return this.getApi(`${isBackend ? '../../' : ''}../../${currentUsesDomainApi}/events/event/${id}/edit`);
    }

    updateEvent(id, payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../../' : ''}../../${currentUsesDomainApi}/events/event/${id}/update`, payload);
    }

    deleteEvent(id, isBackend = false) {
        return this.deleteApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/events/event/${id}/destroy`);
    }
    
    deleteEventsAll(payload, isBackend = false) {
        return this.deleteAllApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/events/destroyall`, payload);
    }


    


    // beforeafters
    getBeforeAftersCategories(isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/beforeafters/setup`);
    }

    createBeforeAfter(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/beforeafters/store`, payload);
    }

    getFilteredBeforeAfter(categ_id, isBackend) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/beforeafters/category/${categ_id}`);
    }
    
    getSearchBeforeAfter(payload, page, isBackend) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/beforeafters/search${page ? '?page=' + page : ''}`, payload);
    }

    getBeforeAfters(page, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/beforeafters${page ? '?page=' + page : ''}`);
    }

    getBeforeAfter(id, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/beforeafters/beforeafter/${id}`);
    }

    getUpdateBeforeAfter(id, isBackend = false) {
        return this.getApi(`${isBackend ? '../../' : ''}../../${currentUsesDomainApi}/beforeafters/beforeafter/${id}/edit`);
    }

    updateBeforeAfter(id, payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../../' : ''}../../${currentUsesDomainApi}/beforeafters/beforeafter/${id}/update`, payload);
    }

    deleteBeforeAfter(id, isBackend = false) {
        return this.deleteApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/beforeafters/beforeafter/${id}/destroy`);
    }
    
    deleteBeforeAftersAll(payload, isBackend = false) {
        return this.deleteAllApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/beforeafters/destroyall`, payload);
    }



    // popups
    getPopupsCategories(isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/popups/setup`);
    }

    getPopupsCurrent(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/popups/now`, payload);
    }

    createPopup(payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/popups/store`, payload);
    }
    
    getSearchPopup(payload, page, isBackend) {
        return this.postApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/popups/search${page ? '?page=' + page : ''}`, payload);
    }

    getPopups(page, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/popups${page ? '?page=' + page : ''}`);
    }

    getPopup(id, isBackend = false) {
        return this.getApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/popups/popup/${id}`);
    }

    getUpdatePopup(id, isBackend = false) {
        return this.getApi(`${isBackend ? '../../' : ''}../../${currentUsesDomainApi}/popups/popup/${id}/edit`);
    }

    updatePopup(id, payload, isBackend = false) {
        return this.postApi(`${isBackend ? '../../' : ''}../../${currentUsesDomainApi}/popups/popup/${id}/update`, payload);
    }

    deletePopup(id, isBackend = false) {
        return this.deleteApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/popups/popup/${id}/destroy`);
    }
    
    deletePopupsAll(payload, isBackend = false) {
        return this.deleteAllApi(`${isBackend ? '../' : ''}../../${currentUsesDomainApi}/popups/destroyall`, payload);
    }
}



export default new api();