import myCookie from './MyCookie';

// https://stackoverflow.com/questions/49819183/react-what-is-the-best-way-to-handle-login-and-authentication
// https://stackoverflow.com/users/652669/gg

// export const getAccessToken = () => Cookies.get('access_token');
// export const getRefreshToken = () => Cookies.get('refresh_token');
// export const isAuthenticated = () => !!getAccessToken();

// export const authenticate = async () => {
//     if (getRefreshToken()) {
//         try {
//             const tokens = await refreshTokens() // call an API

//             const expires = (tokens.expires_in || 60 * 60) * 1000;
//             const inOneHour = new Date(new Date().getTime() + expires);

//             Cookies.set('access_token', tokens.access_token, { expires: inOneHour });
//             Cookies.set('refresh_token', tokens.refresh_token);

//             return true;
//         } catch (err) {
//             redirectToLogin();
//             return false;
//         }
//     }

//     redirectToLogin();
//     return false;
// }

const redirectToLogin = () => {
    // window.location.replace(`${getConfig().LOGIN_URL}?next=${window.location.href}`)
    // history.push('/login');
}

export const getUserCookie = () => myCookie.getUser();
export const isAuthenticated = () => !!getUserCookie();
