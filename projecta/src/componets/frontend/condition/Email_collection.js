import React, { Fragment } from 'react';

class Email_collection extends React.Component { 
  render() {
    return (
        <Fragment>
        <div className="board-top-style">
          <ul className="board-title-box">
              <li className="board-title">이메일주소 무단수집 거부 안내</li>
              <li className="board-title-box-line"></li>
              <li className="board-text">MIBALANCE</li>
          </ul>
        </div>
        <div className="border-container">
          <div className="board-list">
              <div style={{borderTop: '3px solid #594544'}}/>
              <div className="condition-box">
              <span className="condition-text">
                본 사이트(미밸런스)에 게시되어 있는 이메일 주소가 "이메일 주소 수집 프로그램"이나, 그 밖의 기술적 장치를 사용하여 무단으로 수집되는 것을 거부하며, 위반시 정보통신망법에 의해 형사 처벌됨을 유념하시기 바랍니다.
              </span>
            </div>
          </div>
        </div>
        </Fragment>
    )
  } 
}
export default Email_collection;