import React, { Component, Fragment } from 'react';
import api from 'common/api';

import { Select, Pagination } from 'element-react';

// https://material-ui.com/components/tables/

const columns = [
  { prop: 'notice_category_name', label: '구분'},
  { prop: 'title', label: '제목'},
  { prop: 'created_at', label: '작성일'},
  { prop: 'views', label: '조회수' }
];

const mColumns = [
  { prop: 'title', label: '제목'},
  { prop: 'notice_category_name', label: '구분'},
  { prop: 'created_at', label: '작성일'},
];

let rows = [];

function getIndexedRows(datas) {
  let numFixedIndex = 0;
  let numNotFixedIndex = 0;
  let rows = [];

  datas.forEach(data => {
    // 고정된 노티스일 때
    if (data.is_fix) {
      const noticeArr = data.notices.map(fixedNotice => {

        let created_at = fixedNotice.created_at;
        created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;

        return {
          ...fixedNotice,
          created_at,
          is_fix: true
        };
      });
      rows = [...rows, ...noticeArr];

    } else {
      // 고정된 노티스가 아닐 때
      // index값은 페이지당 고정되지 않은 노티스 개수 * 현재 페이지 + 고정된 노티스부터 시작 
      numNotFixedIndex = data.notices.per_page * (data.notices.current_page - 1) + numFixedIndex;

      const noticeArr = data.notices.data.map(notice => {

        let created_at = notice.created_at;
        created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;

        numNotFixedIndex += 1;
        return {
          numIndex: numNotFixedIndex,
          ...notice,
          created_at,
          is_fix: false
        };
      });
      rows = [...rows, ...noticeArr];
    }
  });

  return rows;
}

function getSearchedIndexedRows(datas) {
  let numNotFixedIndex = 0;

  numNotFixedIndex = datas.per_page * (datas.current_page - 1);

  return datas.data.map(data => {
    let created_at = data.created_at;
    created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;

    return {
      ...data,
      numIndex: ++numNotFixedIndex,
      created_at
    }
  })

}

class Frontend_Notices extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      total: 0,
      perPage: 0,
      checkboxObj: [],
      searchData: {
        categ: 0,
        searchOption: 0,
        query: ''
      },
      isSearched: false
    }
  }

  componentDidMount() {
    this.getDatas();
  }

  getDatas = async (page) => {
    await api.getNotices(page)
      .then(res => {
        rows = getIndexedRows(res.data.data);

        const newRow = rows.map(cur => ({
          ...cur,
          checkbox: false
        }));

        const notFixedNotice = res.data.data.filter(cur => !cur.is_fix)[0].notices;

        this.setState({
          page: notFixedNotice.current_page,
          total: notFixedNotice.last_page,
          perPage: notFixedNotice.per_page,
          checkboxObj: newRow,
          isSearched: false
        });
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  getSearchedDatas = async (page) => {
    const { searchData } = this.state;
    const payload = {
      search_word: searchData.query,
      notice_category_id: searchData.categ,
      search_option_id: searchData.searchOption
    };
    await api.getSearchNotice(payload, page)
      .then(res => {
        const resData = res.data.data;
        rows = getSearchedIndexedRows(resData);
        this.setState({
          checkboxObj: rows,
          total: resData.last_page,
          page: resData.current_page,
          isSearched: true
        })
        console.log('successfully searched..', res, rows);
      })
      .catch(error => {
        console.log(error);
      })
  }

  handleChangePage = (selectedPage) => {
    const { isSearched } = this.state;
    if (isSearched) {
      this.getSearchedDatas(selectedPage);
    } else {
      this.getDatas(selectedPage);
    }
  };

  searchClickHanlder = async () => {
    console.log('clicking...', this.state.searchData);
    const { searchData, isSearched } = this.state;

    if (searchData.searchOption === 0 && searchData.categ === 0 && searchData.query === '') {
      if (isSearched) {
        this.getDatas();
      }
    } else {
      this.getSearchedDatas();
    }
  }

  categChangeHandler = (value) => {
    this.searchChangeHandler(value,'categ');
  }
  searchOptionChangeHandler = (value) => {
    this.searchChangeHandler(value,'searchOption');
  }

  searchChangeHandler = (target, type) => {
    let newSearchData = {
      ...this.state.searchData
    };
    if (type === 'query') {
      newSearchData = {
        ...newSearchData,
        [type]: target.target.value
      }
    } else {
      newSearchData = {
        ...newSearchData,
        [type]: target
      };
    }

    this.setState({
      searchData: newSearchData
    });
  }

  searchClear = () => {
    this.setState({
      searchData: {
        categ: 0,
        searchOption: 0,
        query: ''
      }
    })
  }

  handleLink = (id) => {
    this.props.history.push(`${this.props.match.url}/notice/${id}`);
  }


  render() {
    console.log('this is front notices', this.state, this.props);

    const { searchData, checkboxObj } = this.state;
    const { categ, searchOption, query } = searchData;
    const noticeCategories = this.props.noticeCategories;
    return (
      <Fragment>
      <div className="board-top-style">
        <ul className="board-title-box">
            <li className="board-title">공지사항</li>
            <li className="board-title-box-line"></li>
            <li className="board-text">MIBALANCE NOTICE</li>
        </ul>
      </div>
        <div className="border-container">
          <div className="board-list">
            <table summary="이 표는 공지사항 목록이 번호, 구분, 제목, 작성자, 등록일, 조회로 구성되어 있습니다.">
              <colgroup>
              <col className="notice-col"/>
              <col/>
              <col className="notice-col"/>
              <col className="notice-col"/>
              </colgroup>
              <thead>
                <tr>
                  {
                    columns.map((column, index) => (
                      <td key={index} >
                        {column.label}
                      </td>
                    ))
                  }
                </tr>
              </thead>
              <tbody>
              {
                checkboxObj.map((row, index) => (
                  <tr key={index} >
                    {
                      columns.map((column, colIndex) => {
                        return column.prop === 'checkbox' ? (
                          <td key={colIndex}>
                            <input type="checkbox" checked={row[column.prop]} onChange={(e) => this.handleCheckBox(e, row.id)} />
                          </td>
                        ) : (
                          column.prop === 'title' ? (
                            <td 
                              style={{cursor:'pointer',maxWidth:'280px', whiteSpace: 'nowrap',textOverflow: 'ellipsis', overflow: 'hidden', textAlign: 'left'}}  key={colIndex} onClick={() => this.handleLink(row.id)}>
                              {
                                row.is_fix ? (
                                  <span>
                                    <span className="icon-notices"></span>
                                    <span style={{color: '#E38A88', fontWeight:700}} className={row.is_fix ? "fixed": "noFixed"}>{row[column.prop]}</span>
                                  </span>
                                ) : <span className={row.is_fix ? "fixed": "noFixed"}>{row[column.prop]}</span>
                              }
                            </td>
                          ) : (
                            <td style={{cursor:'pointer',maxWidth:'280px', whiteSpace: 'nowrap',textOverflow: 'ellipsis', overflow: 'hidden'}}  key={colIndex} onClick={() => this.handleLink(row.id)}>
                              {row[column.prop]}
                            </td>
                          )
                        )
                      })
                    }
                  </tr>
                ))
              }
              </tbody>
            </table>
            <br />
           
            
            <ul className="m-board m-notices">
                  { checkboxObj.map((row, index) => (
                    <li className="m-board-list" key={index} >
                      {
                        mColumns.map((mColumns, colIndex) => {
                          return mColumns.prop === 'checkbox' ? (
                            <span key={colIndex}>
                              <input type="checkbox" checked={row[mColumns.prop]} onChange={(e) => this.handleCheckBox(e, row.id)} />
                            </span>
                          ) : (
                            mColumns.prop === 'title' ? (
                              <div 
                                style={{cursor:'pointer', whiteSpace: 'nowrap',textOverflow: 'ellipsis', overflow: 'hidden', maxWidth: '100%'}}  key={colIndex} onClick={() => this.handleLink(row.id)}>
                                {
                                  row.is_fix ? (
                                   
                                      <span style={{color: '#E38A88', fontWeight:'700', display: 'block', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}} className={row.is_fix ? "fixed": "noFixed"}>{row[mColumns.prop]}</span>
                             
                                  ) : <span className={row.is_fix ? "fixed": "noFixed"}>{row[mColumns.prop]}</span>
                                }
                              </div>
                            ) : (
                            <span key={colIndex} onClick={() => this.handleLink(row.id)}>
                              {row[mColumns.prop]}
                            </span>
                            )
                          )
                        })
                      }
                    </li>
                    ))
                  }
            </ul>
            <Pagination 
              style={{display:'flex',justifyContent:'center'}} 
              layout="prev, pager, next" 
              currentPage={this.state.page} 
              pageCount={this.state.total} 
              pageSize={this.state.perPage} 
              small={true}
              onCurrentChange={this.handleChangePage}
            />
            <div className="board-search">
              <div className="search-box">
                <Select value={searchData.categ} placeholder="구분없음" onChange={this.categChangeHandler}>
                  {noticeCategories.notice_categories ? noticeCategories.notice_categories.map(el => {
                    return <Select.Option key={el.id} label={el.notice_category_name} value={el.id} />
                  }) : null}
                </Select>
              </div>
              <div className="search-box">
                <Select value={searchData.searchOption} placeholder="글제목" onChange={this.searchOptionChangeHandler}>
                  {noticeCategories.search_options ? noticeCategories.search_options.map(el => {
                    return <Select.Option key={el.id} label={el.search_option_name} value={el.id} />
                  }) : null}
                </Select>
              </div>
              <span>
                <input className="board-input search-box" type="text" value={query} onChange={(e) => this.searchChangeHandler(e, 'query')} />
                {
                  (categ || searchOption || query) && <i className="el-icon-circle-cross" onClick={this.searchClear}></i> 
                }
              </span>
              <a className="btn-frontend btn-search search-box" onClick={this.searchClickHanlder}>검색</a>
            </div>
          </div>
      </div>
      </Fragment>
    )
  }
}

export default Frontend_Notices;