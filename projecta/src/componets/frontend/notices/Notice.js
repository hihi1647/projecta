import React, { Fragment } from 'react';
import api from 'common/api';
import { Link } from 'react-router-dom';
import parse from 'html-react-parser';
import boardTopTextbox from '../../../widget/board-top-textbox/boardTopTextbox'

const columnsObj = {
  'title': '제목',
  'writer': '작성자',
  'created_at': '작성일',
  'views': '조회수'
}

const columns = [
  'title',
  'writer',
  'created_at',
  'views'
];

class Frontend_Notice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notice: {}
    }

    this.history = this.props.history;
    this.match = this.props.match;
    this.currentNoticeID = this.match.params.id;
  }

  componentDidMount() {
    this.getNotice(this.currentNoticeID);
  }

  getNotice = async (currentNoticeID) => {
    await api.getNotice(currentNoticeID)
      .then(res => {
        const resData = res.data.data;

        if (resData) {
          let created_at = resData.created_at;
          created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;
          
          const notice = {
            ...resData,
            created_at
          }

          this.setState({
            notice
          });
        }
        
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  goBack = () => {
    this.history.push('/notices');
  }

  handleLink = (noticeID) => {
    if (noticeID) {
      const newURL = this.match.url.replace(':id', noticeID);
      this.history.push(newURL);
    }

    console.log('clicking Links...', noticeID);
  }

  render() {
    const notice = this.state.notice;
    console.log('this is front notice', notice, this.props);
    return (
      <div>
        {
          notice.id ? (
            <Fragment>
              <div className="board-top-style">
                <ul className="board-title-box">
                  <li className="board-title">공지사항</li>
                  <li className="board-title-box-line"></li>
                  <li className="board-text">MIBALANCE NOTICE</li>
                </ul>
              </div>
              <div className="border-container">
                <div className="board-list">
                <div className="border-header">
                  <span className="border-header-section">{notice.notice_category_name}</span>
                  <span className="notice-border-header-title">{notice[columns[0]]}</span>
                </div>
                <div className="border-header-bottom">
                {
                  columns.slice(1).map(column => (
                    <Fragment key={column} >
                        <span className="border-view-name">{columnsObj[column]}</span>
                        <span className="border-view-body">{notice[column]}</span>
                    </Fragment>
                  ))
                }
                </div>
                <div className="border-body">{parse(notice.details)}</div>
                <div className="border-btn-wrap"><a className="btn-frontend btn-borderList" onClick={this.goBack}>목록보기</a></div>
                <div className="prev-contianer">
                  {
                    notice.previous ? (
                      <Fragment>
                        <div className="prev-wrap prev-wrap-boder">
                          <span className="border-arrow">
                            <Link to={`${this.match.path.replace(':id', notice.previous.id)}`}>
                              <svg  className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><path d="M0 16.67l2.829 2.83 9.175-9.339 9.167 9.339 2.829-2.83-11.996-12.17z" fill="##615454"/></svg>
                              이전글
                            </Link>
                          </span> 
                          <span className="border-prev-body">{notice.previous.title}</span>
                        </div>
                      </Fragment>
                    ) : null
                  }
                  {
                    notice.next ? (
                      <Fragment>
                        <div className="prev-wrap">
                          <div className="border-arrow">
                            <Link to={`${this.match.path.replace(':id', notice.next.id)}`}>
                              <svg className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z" fill="##615454"/></svg>
                              다음글
                      </Link>
                          </div>
                          <div className="border-prev-body">{notice.next.title}</div>
                        </div>
                      </Fragment>
                    ) : null
                  }
                </div>
                </div>
              </div>
            </Fragment>
          ) : (
              <div>
                something went wrong..
              </div>
            )
        }

      </div>
    )
  }
}

export default Frontend_Notice;