import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Notice from './Notice';
import Notices from './Notices';
import api from 'common/api';

class Notice_Container extends React.Component {
    state = {
        noticeCategories: {}
    };
    

    componentDidMount() {
        this.getDatas();
    }

    getDatas = async () => {
        await api.getNoticeCategories()
          .then(res => {
              console.log('this is category datas', res);
              this.setState({
                  noticeCategories: res.data.data
              })
          })
          .catch(error => {
              console.log('something went wrong with category', error);
          })
    }

    render() {
        const { noticeCategories } = this.state
        const { path } = this.props.match;
        return (
            <Switch>
                <Route path={`${path}/notice/:id`} render={(props) => <Notice {...props} key={props.match.params.id} />} />
                <Route path={path} render={(props) => <Notices {...props} noticeCategories={noticeCategories} />} /> 
            </Switch>
        )
    }
}

export default Notice_Container;