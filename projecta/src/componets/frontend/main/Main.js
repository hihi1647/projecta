import React, { Component, Fragment } from 'react';
import api from 'common/api';
import { Link } from 'react-router-dom';
import { editingConsultingName, isDeviceMobile } from 'common/Utils';
import Popup from 'common/Popup';
import PopupModal from 'widget/PopupModal';

function getIndexedRows(datas) {
    let rows = [];

    datas.forEach(data => {
        const notices = data.notices;
        if (notices) {
            rows = [
                ...rows,
                ...notices
            ];
        }
    })
  
    return rows;
}

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            consultings: {},
            notices: {},
            popup: {},
            activePopupIDs: []
        }

        this.isMobile = isDeviceMobile();
    }

    componentDidMount() {
        this.getDatas();
    }

    getDatas = async () => {
        await api.getMainPage()
            .then(res => {
                const resData = res.data.data;

                if (resData) {
                    let { consultings, notices } = resData;
                    notices = getIndexedRows(notices);
                    this.setState({
                        consultings,
                        notices
                    })
                } 
            })
            .catch(error => {
                console.log('something went wrong with main page', error);
            })

        const currentTime = new Date();
        const currentTimeJSON = currentTime.toString();
        await api.getPopupsCurrent({now: currentTime, mobile: this.isMobile})
            .then(res => {
                const resData = res.data.data;
                const an_Hour = 60 * 60 * 1000;
    
                if (resData) {
                const popupCookie = Popup.fromCookie();
                // popupCookie = {"1": {id: 1, last_visited_time: '2020-05-08T15:41:04.079Z'}};
                let activePopupIDs = [];
    
                if (popupCookie && Object.keys(popupCookie).length > 0) {
    
                    resData.forEach(popup => {
    
                    if (popupCookie[popup.id]) {
                        const prev_time = new Date(popupCookie[popup.id].last_visited_time).getTime();
                        const current_time = currentTime.getTime();
    
                        if (prev_time < current_time && prev_time + popup.re_pop_up_time*an_Hour < current_time) {
    
                        activePopupIDs = [
                            ...activePopupIDs,
                            popup.id
                        ];
                        }
                    } else {
                        activePopupIDs = [
                        ...activePopupIDs,
                        popup.id
                        ];
                    }
                    })
                } else {
                    activePopupIDs = resData.map(popup => popup.id);
                }

                    this.setState({
                        popup: resData,
                        activePopupIDs
                    })
                }
            })
            .catch(error => {
                console.log('something went wrong with popup data', error);
            })
    }

    closeBtn = (id, isSetCookie) => {
    
        if (isSetCookie) {
          const popupCookie = Popup.fromCookie();
          const currentTimeJSON = new Date().toString();
          let newPopupCookie = {
            ...popupCookie,
            [id]: {
              id,
              last_visited_time: currentTimeJSON
            }
          }
    
          const newPopup = new Popup();
          newPopup.copy(newPopupCookie);
        }
        
        const activePopupIDs = this.state.activePopupIDs.filter(curID => curID !== id);
    
        this.setState({
          activePopupIDs
        })
    }

    handleLink = (toWhere, id) => {
        if (toWhere === 'notice') {
            this.props.history.push(`/notices/${toWhere}/${id}`);
        } else {
            this.props.history.push(`/${toWhere}`);
        }
    }


    render() {
        var BannerImg = require('../../../assets/images/banner_02.jpg');
        var BannerText = require('../../../assets/images/banner_text.png');
        var program_01 = require('../../../assets/images/program_01.jpg');
        var program_02 = require('../../../assets/images/program_02.jpg');
        var program_03 = require('../../../assets/images/program_03.png');
        var program_04 = require('../../../assets/images/program_04.png');
        var program_05 = require('../../../assets/images/program_05.png');
        var program_06 = require('../../../assets/images/program_06.png');
        console.log('this is from main page', this.state, this.props);
        const { consultings, notices, popup, activePopupIDs } = this.state;
        return(
            <div className="container main">
                <section className="banner">
                    <ul>
                        <li className="banner-img" style= {{backgroundImage: "url("+BannerImg+")"}}>
                            <h2 className="ir">이벤트 베너</h2>
                            <div className="banner-container">
                                <div className="banner-text" style= {{backgroundImage: "url("+BannerText+")"}}></div>
                                <Link to={`/infor`} className="btn-banner">
                                    ABOUT MI BALANCE
                                    <svg className="icon-arrow" xmlns="http://www.w3.org/2000/svg" width="17.007" height="4.804" viewBox="0 0 17.007 4.804">
                                        <path className="banner-arrow" d="M5216.949-13199.223h15.019l-4.759-3.6" transform="translate(-5216.449 13203.526)" fill="none" stroke="#fff" strokeLinecap="round" strokeWidth="1"/>
                                    </svg>
                                </Link>
                            </div>
                        </li>
                    </ul>
                    <div className="info-background"></div>
                    <div className="footer-sns-background"></div>
                    <div className="footer-counseling-background"></div>
                    <div className="footer-background"></div>
                </section>
                {/* banner */}


                <ul className="main-title-box">
                    <li><h3 className="main-title">미밸런스<span className="pink"> 프로그램</span> 소개</h3></li>
                    <li className="main-title-box-line"></li>
                    <li className="main-text">MIBALANCE SKIN PROGRAM</li>
                </ul>
                <article className="program">
                    <div className="main-program">
                        <ul className="main-program-contents">
                            <li className="program-cardList card-01" style= {{backgroundImage: "url("+program_01+")"}}>
                                <a href="#" className="program-link">
                                    <p className="program-card-text">MIBALANCE SKIN PROGRAM</p>
                                    <Link to={`/skin_care/skin_care`}>
                                    <ul className="btn-card-desc">
                                        <li className="card-text">여드름&nbsp;·&nbsp;수분&nbsp;·&nbsp;재생&nbsp;·&nbsp;톤업</li>
                                        <li><h4 className="card-title">피부관리</h4></li>
                                        <li className="btn-card">보러가기&nbsp;&nbsp;></li>   
                                    </ul>
                                    </Link>
                                </a>
                            </li>
                            <li className="program-cardList card-01" style= {{backgroundImage: "url("+program_02+")"}}>
                            
                                <a href="#" className="program-link">
                                    <p className="program-card-text">MIBALANCE SKIN PROGRAM</p>
                                    <Link to={`/lifting/shrink`}>
                                    <ul className="btn-card-desc">
                                        <li className="card-text">V라인&nbsp;·&nbsp;잔주름&nbsp;·&nbsp;목주름&nbsp;·&nbsp;체형관리</li>
                                        <li><h4 className="card-title">슈링크</h4></li>
                                        <li className="btn-card">보러가기&nbsp;&nbsp;></li>   
                                    </ul>
                                    </Link>
                                </a>
                            </li>
                        </ul>

                        <ul className="main-program-contents">
                            <li className="main-program-cardList-size card-02" style= {{backgroundImage: "url("+program_03+")", backgroundColor:'#f0f0ee'}}>
                                
                                <a href="#" className="program-link">
                                    <p className="program-card-text">MIBALANCE SKIN PROGRAM</p>
                                    <Link to={`/petit/botox`}>
                                    <ul className="btn-card-desc">
                                        <li className="card-text">V라인&nbsp;·&nbsp;잔주름&nbsp;·&nbsp;종아리&nbsp;·&nbsp;다한증</li>
                                        <li><h4 className="card-title">보톡스</h4></li>
                                        <li className="btn-card">보러가기&nbsp;&nbsp;></li>   
                                    </ul>
                                    </Link>
                                </a>
                            </li>
                            <li className="main-program-cardList-size card-02" style= {{backgroundImage: "url("+program_04+")",  backgroundColor:'#F5C8C2' }}>
                                
                                <a href="#" className="program-link">
                                    <p className="program-card-text">MIBALANCE SKIN PROGRAM</p>
                                    <Link to={`/petit/filler`}>
                                    <ul className="btn-card-desc">
                                        <li className="card-text">이마&nbsp;·&nbsp;콧대&nbsp;·&nbsp;턱끝&nbsp;·&nbsp;입술</li>
                                        <li><h4 className="card-title">필러</h4></li>  
                                        <li className="btn-card">보러가기&nbsp;&nbsp;></li>   
                                    </ul>
                                    </Link>
                                </a>
                            </li>
                            <li className="main-program-cardList-size card-02" style= {{backgroundImage: "url("+program_05+")",  backgroundColor:'#CDD1D2'}}>
                                
                                <a href="#" className="program-link">
                                    <p className="program-card-text">MIBALANCE SKIN PROGRAM</p>
                                    <Link to={`/body/ice_waxing`}>
                                    <ul className="btn-card-desc">
                                        <li className="card-text">헤어라인&nbsp;·&nbsp;겨드랑이&nbsp;·&nbsp;브라질리언</li>
                                        <li><h4 className="card-title">제모</h4></li>
                                        <li className="btn-card">보러가기&nbsp;&nbsp;></li>   
                                    </ul>
                                    </Link>
                                </a>
                            </li>
                            <li className="main-program-cardList-size card-02" style= {{backgroundImage: "url("+program_06+")",  backgroundColor:'#E7E7E7'}}>
                                
                                <a href="#" className="program-link">
                                    <p className="program-card-text">MIBALANCE SKIN PROGRAM</p>
                                    <Link to={`/laser/athlete_foot`}>
                                    <ul className="btn-card-desc">
                                        <li className="card-text">손&nbsp;·&nbsp;발톱 부위 집중 무좀균 사멸</li>
                                        <li><h4 className="card-title">손발톱무좀</h4></li>
                                        <li className="btn-card">보러가기&nbsp;&nbsp;></li>   
                                    </ul>
                                    </Link>
                                </a>
                            </li>
                        </ul>
                    </div>
                </article>


                
                {/* main-program */}
                <ul className="main-title-box">
            <li><h3 className="main-title">미밸런스<span className="pink"> 의료진</span> 소개</h3></li>
            <li className="main-title-box-line"></li>
            <li className="main-text">MIBALANCE DOCTOR INFO</li>
        </ul>
        <article className="info">
            <div className="info-container">
                <div className="info-cardList cardlist-01">
                    <ul className="info-card-desc">
                        <li className="info-text-sub">피부&nbsp;/&nbsp;리프팅&nbsp;/&nbsp;쁘띠&nbsp;/&nbsp;비만</li>
                        <li>
                            <h4 className="info-name">
                                <span className="font-weight-medium">
                                    윤정후</span>&nbsp;대표원장
                                <svg className="icon-petal" xmlns="http://www.w3.org/2000/svg" width="23.513" height="24.909" viewBox="0 0 23.513 24.909">
                                    <g transform="translate(-3559.337 17839.749)">
                                        <path d="M526.563,1685.929a18.97,18.97,0,0,0-17.928,17.928A18.968,18.968,0,0,0,526.563,1685.929Z" transform="translate(3050.702 -19525.678)" fill="#e5c5b9" opacity="0.5"/>
                                        <path d="M17.928,0A18.97,18.97,0,0,0,0,17.928,18.968,18.968,0,0,0,17.928,0Z" transform="matrix(0.921, 0.391, -0.391, 0.921, 3566.347, -17838.348)" fill="#e5c5b9"/>
                                    </g>
                                </svg>
                            </h4>
                        </li>
                        <li className="info-name-en">Youn Jung hu</li>
                        <li className="info-text">수준 높은 진료와 체계적인 관리로 어제보다 아름다운 나를 만나게 해드리겠습니다.</li>
                        <li><Link to={`/doctor_infor`} className="btn-info">
                            More
                            <svg className="icon-arrow" xmlns="http://www.w3.org/2000/svg" width="17.007" height="4.804" viewBox="0 0 17.007 4.804">
                                <path className="info-arrow" d="M5216.949-13199.223h15.019l-4.759-3.6" transform="translate(-5216.449 13203.526)" fill="none" stroke="#605043" strokeLinecap="round" strokeWidth="1"/>
                            </svg>                                  
                        </Link></li>
                    </ul>
                    <img src={ require('../../../assets/images/profile_01.png') } alt="윤정후 대표원장 사진" className="info-img card-desc-02"/>
                </div>
                <div className="info-cardList cardlist-02">
                    <ul className="info-card-desc">
                        <li className="info-text-sub">피부&nbsp;/&nbsp;리프팅&nbsp;/&nbsp;쁘띠&nbsp;/&nbsp;비만&nbsp;/&nbsp;체형</li>
                        <li>
                            <h4 className="info-name">
                                <span className="font-weight-medium">
                                    이형진</span>&nbsp;대표원장
                                <svg className="icon-petal" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23.513 24.909">
                                    <g transform="translate(-3559.337 17839.749)">
                                        <path d="M526.563,1685.929a18.97,18.97,0,0,0-17.928,17.928A18.968,18.968,0,0,0,526.563,1685.929Z" transform="translate(3050.702 -19525.678)" fill="#e5c5b9" opacity="0.5"/>
                                        <path d="M17.928,0A18.97,18.97,0,0,0,0,17.928,18.968,18.968,0,0,0,17.928,0Z" transform="matrix(0.921, 0.391, -0.391, 0.921, 3566.347, -17838.348)" fill="#e5c5b9"/>
                                    </g>
                                </svg>
                            </h4>
                        </li>
                        <li className="info-name-en">Lee Hyeong Jin</li>
                        <li className="info-text">정밀한 진단과 정교한 진료로 어떤 순간에도 무너지지 않는 아름다움을 선사해 드리겠습니다.</li>
                        <li>
                        <Link to={`/doctor_infor`} className="btn-info">
                                More
                                <svg className="icon-arrow" xmlns="http://www.w3.org/2000/svg" width="17.007" height="4.804" viewBox="0 0 17.007 4.804">
                                    <path className="info-arrow" d="M5216.949-13199.223h15.019l-4.759-3.6" transform="translate(-5216.449 13203.526)" fill="none" stroke="#605043" strokeLinecap="round" strokeWidth="1"/>
                                </svg>                                  
                            </Link>
                        </li>
                    </ul>
                    <img src={ require('../../../assets/images/profile_02.png') } alt="윤정후 대표원장 사진" className="info-img"/>  
                </div>
            </div>
        </article>
        {/* main-info */}
        <article className="notice">
                <div className="main-title-box">
                    <h3 className="main-title">미밸런스 <span className="pink">공지사항</span> 보기</h3>
                    <span className="main-title-box-line"></span>
                    <p className="main-text">MIBALANCE NOTICE</p>
                </div>
                <div className="notice-container">
                    <div className="notice-wrap notice-01"> 
                        <h4 className="onlin-title">온라인 상담</h4>
                        <a onClick={() => this.handleLink('consultings')} className="btn-notice">+</a>
                        <table className="notice-online-table">     
                            <tbody>
                                {
                                    (consultings && consultings.length > 0 ) && consultings.map((consulting, index) => (
                                        <tr key={consulting.main_category_name + index}>
                                            <td className="table-type">{consulting.main_category_name}</td>
                                            <td className="notice-space">
                                                <svg className="icon-lock" xmlns="http://www.w3.org/2000/svg" width="19.5" height="14" viewBox="0 0 13.5 18">
                                                    <path d="M14.25,7.5v-3a4.5,4.5,0,0,0-9,0v3H3V18H16.5V7.5Zm-7.5,0v-3a3,3,0,0,1,6,0v3Z" transform="translate(-3)" fill="#3e2626"/>
                                                </svg>
                                                {editingConsultingName(consulting.writer) + '님의 상담입니다.'}
                                            </td>
                                            <td>{consulting.created_at.slice(0, 10)}</td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                    </div>
                    <div className="notice-wrap notice-02"> 
                        <h4 className="onlin-title">공지사항</h4>
                        <a onClick={() => this.handleLink('notices')} className="btn-notice">+</a>
                        <table className="notice-online-table">
                            <tbody>
                                {
                                    (notices && notices.length > 0) && notices.map((notice, index) => (
                                        <tr key={index + notice.notice_category_name}>
                                            <td className="font-color-blue font-weight-medium">{notice.notice_category_name}</td>
                                            <td className="notice-space" onClick={() => this.handleLink('notice', notice.id)}>
                                                {notice.title}
                                            </td>
                                            <td>{notice.created_at.slice(0, 10)}</td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
        </article>
        {/* main-notice */}

                

            {/* 팝업 */}
            {
                ( activePopupIDs && activePopupIDs.length > 0 ) ? (
                    popup.map((cur, ind) => {
                        if (activePopupIDs.includes(cur.id)) {
                        return (
                            <PopupModal key={ind} layer={cur} mobile={this.isMobile} closePopupModal={this.closeBtn} />
                        )
                        }
                    })
                ) : null
            }
            </div>
        );
    }
}

export default Main;