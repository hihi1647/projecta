import React, { Fragment } from 'react';
import api from 'common/api';
import { Select, Message } from 'element-react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import dateFnsFormat from 'date-fns/format';
import { Link } from 'react-router-dom';

import { parseDate, formatDate, formatDateMySQL } from 'common/Utils';
import { DATE_FORMAT } from 'constants/index';

import Custom_Image_Font_Quill from 'componets/quill/Custom_Image_Font_Quill';
const columnsObj = {
  'title': '제목',
  'phone': '휴대폰',
  'writer': '작성자',
  'created_at': '작성일',
  'views': '조회수',
  'email': '이메일',
  'details': '상담내용',
  'password': '비밀번호'
}

const columns = [
  'title',
  'writer',
  'created_at',
  'views'
];

const required = ['isConditionChecked', 'writer', 'phone', 'title', 'main_category_id', 'sub_category_id', 'details', 'password', 'picPassword'];
const validationCheck = {
    password: {
        regex: 'required',
        msg: '비밀번호는 필수 입력항목입니다.',
        required: true
    },
    picPassword: {
        regex: 'picPassword',
        msg: '사진 비밀번호와 다릅니다.',
        required: true
    },
    writer: {
        regex: 'required',
        msg: '이름은 필수 입력항목입니다.',
        required: true
    },
    phone: {
        regex: /^\d{3}-\d{3,4}-\d{4}$/,
        msg: '휴대폰 번호는 필수 입력항목입니다. (000-0000-0000)',
        required: true
    },
    title: {
      regex: 'required',
      msg: '제목은 필수 입력항목입니다.',
      required: true
    },
    details: {
      regex: 'required',
      msg: '상담내용은 필수 입력항목입니다.',
      required: true
    },
    isConditionChecked: {
        regex: 'required',
        msg: '약관에 동의해 주세요.',
        required: true
    },
    main_category_id: {
      regex: 'required',
      msg: '상담과목 선택은 필수 항목입니다.',
      required: true
    },
    sub_category_id: {
      regex: 'required',
      msg: '상담과목 선택은 필수 항목입니다.',
      required: true
    }
}


class Frontend_Consultings_Create extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      consulting: {
        title: '',
        phone: '',
        email: '',
        writer: '',
        created_at: new Date(),
        details: '',
        main_category_id: '',
        sub_category_id: '',
        email_domain_id: 'default',
        password: '',
        picPassword: '',
        isConditionChecked: false
      },
      subCategories: []
    }

    this.history = this.props.history;
    this.match = this.props.match;
    this.$user = this.props.$user;
  }

  createConsulting = async () => {

    // input validation form check
    let validationCheck = true;

    const validationCheckMsg = this.validationCheck();

    if (validationCheckMsg !== 'success') {
        Message({
            message: validationCheckMsg,
            type: 'error'
        });
        return ;
    }

    const {
      writer,
      phone,
      created_at,
      title,
      main_category_id,
      sub_category_id,
      details,
      password,
      email_domain_id
    } = this.state.consulting;

    let email = this.state.consulting.email;

    if (email_domain_id !== 'default') {
      const givenDomains = this.props.consultingCategories.email_domains;
      const selectedDomain = givenDomains.filter(domain => domain.id === email_domain_id)[0];

      if (selectedDomain) {
        email = email + '@' + selectedDomain.email_domain_name;
      } 
    }

    let payload = {
      writer,
      phone,
      created_at,
      email,
      title,
      main_category_id,
      sub_category_id,
      details,
      password
    }

    if ( !payload.created_at ) {
      payload.created_at = formatDateMySQL(new Date());
    } else {
      payload.created_at = formatDateMySQL(payload.created_at);
    }

    if (this.$user && this.$user.id) {
      payload.user_id = this.$user.id;
    }

    if ( validationCheck ) {
      await api.createConsulting(payload, true)
      .then(res => {
        Message({
          message: '성공적으로 생성되었습니다.',
          type: 'success'
        });
        console.log('successfully created', res);
      })
      .then(() => {
        this.goBack();
      })
      .catch(error => {
        console.log('something went wrong', error);
      })
    }
  }


  cancel = () => {
  }

  goBack = () => {
    this.history.push('/consultings');
  }

  handleConditionCheck = (e) => {
    const newConsulting = {
      ...this.state.consulting,
      isConditionChecked: e.target.checked
    }
    
    this.setState({
      consulting: newConsulting
    })
  }

  handleChange = (event, input) => {

    let newConsulting = {
      ...this.state.consulting
    };

    let subCategories = '';

    if (input === 'created_at' || input === 'sub_category_id' || input === 'email_domain_id' || input === 'details') {
      newConsulting = {
        ...newConsulting,
        [input]: event
      }
    } else if (input === 'main_category_id') {
      subCategories = this.props.consultingCategories.categories.filter(categ => categ.id === event)[0].sub_categories;

      newConsulting = {
        ...newConsulting,
        [input]: event,
        sub_category_id: ''
      }
    } else {
      newConsulting = {
        ...newConsulting,
        [input]: event.target.value
      };
    }

    if (subCategories) {
      this.setState({
        consulting: newConsulting,
        subCategories
      })
    } else {
      this.setState({
        consulting: newConsulting
      })
    }
  }

  isRequiredFilled = () => {
    return required.length === required.filter(cur => this.state.consulting[cur]).length
  }

  validationCheck = () => {

      for (let i = 0; i < required.length; i += 1) {
          const current = required[i];
          const regex = validationCheck[current].regex;
          let isValid = true;
          if (regex === 'required') {
              isValid = this.state.consulting[current];
          } else if (regex === 'picPassword') {
              isValid = this.state.consulting.picPassword == 584250;
              
          } else {
              isValid = regex.test(this.state.consulting[current]);
          }

          if (!isValid) {
              return validationCheck[current].msg;
          }
      }

      return 'success';
  }

  render() {
    const { consulting, subCategories } = this.state;
    const consultingCategories = this.props.consultingCategories;
    console.log('this is create consulting', this.state, this.props, consulting);
    const disabled = !this.isRequiredFilled();

    return (
      <Fragment>
        <div className="board-top-style">
          <ul className="board-title-box">
            <li className="board-title">온라인 상담</li>
            <li className="board-title-box-line"></li>
            <li className="board-text">MIBALANCE COUNSELING</li>
          </ul>
        </div>
        <div className="border-container">
          <div className="board-list">
            <div className="personal-information">
              <span className="personal-information-wrap">
                <p className="personal-information-title">개인정보취급방침 동의</p>
                <p className="personal-information-textarea">
                  "미밸런스의원"은 &#40;이하 "미밸런스"라고 합니다.&#41; 이용자의 개인 정보를 중요시하며, "정보통신망 이용 촉진 및 정보보호 등에 관한 법률" 등 개인 정보와 관련된 법령 상의 개인 정보보호 규정 및 방송통신위원회가 제정한 "개인정보보호지침"을 준수하고자 노력하고 있습니다. 미밸런스는 개인정보취급방침을 통해 이용자가 제공한 개인 정보가 어떠한 용도와 방식으로 이용되며, 개인 정보보호를 위해 어떠한 방법을 통해 관리되고 있는지에 대해 알려드립니다.<br/><br/>본 방침은 2011년 6월 15일부터 시행됩니다. &#40;개인정보취급방침 버전 번호 : 1.0&#41;<br/><br/>1. 수집하는 개인 정보 항목 미밸런스는 회원가입 &#40;추후 서비스 예정&#41;, 원활한 고객상담, 각종 서비스 &#40;추후 예정&#41; 제공을 위한 필수 정보와 고객 맞춤 서비스 제공을 위한 선택 정보로 구분하여 아래와 같은 개인 정보를 수집하고 있습니다. <br/>1&#41; 수집항목 : 성명, 열람/수정 비밀번호, 휴대전화 번호, 이메일, 성별, 나이 <br/>2&#41; 개인 정보 수집 방법 : 온라인 게시판의 글 등록, 비회원 개별 상담 문의<br/><br/>2. 개인 정보의 수집 및 이용목적 미밸런스는 수집한 개인 정보를 다음의 목적을 위해 활용합니다. <br/>1&#41; 정확한 병명 및 환자 상태 확인과 이를 통한 정확한 상담 답변 <br/>2&#41; 본원의 등록된 개별 고객의 신상관리 <br/>3&#41; 마케팅 및 광고에 활용 : 신규 서비스 개발과 이벤트 행사에 따른 정보 전달 및 개인별 맞춤 의료 서비스 제공<br/><br/>3. 개인 정보의 보유 및 이용 기간 미밸런스는 상담 서비스를 이용하는 동안 개인 정보를 보유 및 이용하며, 상담이 완료된 후 지속적인 관리를 위해 해당 정보를 보관하나 서면, 전화, 메일 요청 시 열람/수정/삭제가 가능합니다. 단 미밸런스에서 주관하는 이벤트 참여시 이벤트 진행이 종료될 때까지 데이터가 보관될 수 있습니다.<br/><br/>4. 개인 정보의 파기절차 및 방법 미밸런스는 원칙적으로 개인 정보 수집 및 이용목적이 달성된 후에는 해당 정보를 지체 없이 파기합니다. 파기절차 및 방법은 다음과 같습니다. <br/>1&#41; 파기 절차 : 서면, 전화, 이메일로 사이트 내 정보가 입력된 주소를 알려주시면 지체 없이 파기를 진행합니다. 사이트 주소 &#40;URL&#41;을 모를 경우엔 개인 정보를 알려주시면 해당부분을 찾아 확인후 삭제 해드리겠습니다. 삭제 요청 시 미밸런스에서 지정한 서류 &#40;주민등록증 사본&#41;를 반드시 제시하셔야 합니다. <br/>2&#41; 파기방법 : 전자적 파일 형태로 저장된 개인 정보는 기록을 재생할 수 없는 기술적 방법을 사용하여 삭제합니다. 또한 종이에 출력된 개인 정보는 분쇄기로 분쇄하거나 소각을 통하여 파기합니다.<br/><br/>5. 개인 정보 제공 미밸런스는 이용자들의 개인 정보를 "2. 개인 정보의 수집 목적 및 이용목적"에서 고지한 범위 내에서 사용하며, 이용자의 사전 동의 없이는 동 범위를 초과하여 이용하거나 원칙적으로 이용자의 개인 정보를 외부에 제공하지 않습니다. 다만, 아래의 경우에는 주의를 기울여 개인정보를 이용 및 제공할 수 있습니다. <br/>1&#41; 이용자들이 사전에 공개에 동의한 경우 <br/>2&#41; 서비스의 제공에 관한 계약의 이행을 위하여 필요한 개인 정보로서 경제적/기술적인 사유로 통상의 동의를 받는 것이 현저히 곤란한 경우 <br/>3&#41; 법령의 규정에 의거하거나, 수사목적으로 법령에 정해진 절차와 방법에 따라 수사기관의 요구가 있는 경우<br/><br/>6. 이용자 및 법정대리인의 권리와 그 행사방법 이용자 및 법정 대리인은 언제든지 등록되어 있는 자신 혹은 당해 만 14세 미만 아동의 개인 정보를 조회하거나 수정할 수 있으며 가입해지를 요청할 수도 있습니다. 귀하가 개인 정보의 오류에 대한 정정을 요청하신 경우에는 정정을 완료하기 전까지 개인 정보를 이용 또는 제공하지 않습니다. 또한 잘못된 개인 정보를 제3자에게 이미 제공한 경우에는 정정 처리결과를 제3자에게 지체 없이 통지하여 정정이 이루어지도록 하겠습니다. 미밸런스는 이용자 혹은 법정 대리인의 요청에 의해 해지 또는 삭제된 개인 정보는 "미밸런스가 수집하는 개인정보의 보유 및 이용 기간"에 명시된 바에 따라 처리하고 그 외의 용도로 열람 또는 이용할 수 없도록 처리하고 있습니다. <br/>1&#41; 사이트에 기록한 개인 정보가 있고 작성한 게시물에 대한 비밀번호를 알고 있다면 해당 게시글을 열람, 수정, 삭제를 스스로 할 수 있습니다. <br/>2&#41; 만약 비밀번호 및 열람정보를 잊어버렸다면 개인 정보관리 책임자 &#40;하단 기재&#41;에게 연락하여 도움을 청할 수 있습니다.<br/><br/>7. 개인 정보 자동수집 장치의 설치, 운영 및 그 거부에 관한 사항 미밸런스는 이용자에게 특화된 맞춤 서비스를 제공하기 위해서 이용자들의 정보를 수시로 저장하고 찾아내는 '쿠키&#40;coo-kie&#41;'를 운용합니다. 쿠키란 웹사이트를 운영하는데 이용되는 서버가 이용자의 브라우저에 보내는 아주 작은 텍스트 파일로서 이용자의 컴퓨터 하드디스크에 저장되기도 합니다. 미밸런스는 다음과 같은 목적을 위해 쿠키를 사용합니다. <br/>■ 쿠키 사용목적 : 회원과 비회원의 접속 빈도나 방문 시간 등을 분석, 이용자의 방문 경로 파악, 각종 이벤트 참여 정도 및 방문 횟수 파악 등을 통한 타깃 마케팅 및 개인 맞춤 서비스 제공. 이용자는 쿠키 설치에 대한 선택권을 가지고 있습니다. 따라서, 이용자는 웹 브라우저에서 옵션을 설정함으로써 모든 쿠키를 허용하거나, 쿠키가 저장될 때마다 확인을 거치거나, 아니면 모든 쿠키의 저장을 거부할 수도 있습니다. <br/>■ 쿠키 설정 거부 방법 : 예&#41; 쿠키 설정을 거부하는 방법으로는 이용자가 사용하는 웹 브라우저의 옵션을 선택함으로써 모든 쿠키를 허용하거나 쿠키를 저장할 때 마다 확인을 거치거나, 모든 쿠키의 저장을 거부할 수 있습니다. 설정방법 예 &#40;인터넷 익스플로어의 경우&#41; : 웹 브라우저 상단의 도구 > 인터넷 옵션 > 개인정보 단, 쿠키설치를 거부하였을 경우 로그인 &#40;추후 서비스 예정&#41;이 필요한 일부 서비스 이용에 어려움이 있을 수 있습니다.<br/><br/>8. 개인 정보의 기술적, 관리적 보호 대책 미밸런스는 이용자의 개인 정보를 취급함에 있어 개인 정보가 분실, 도난, 누출, 변조 또는 훼손되지 않도록 안정성 확보를 위하여 기술적, 관리적 대책 수립에 노력하고 있습니다.<br/><br/>9. 개인 정보에 관한 민원서비스 미밸런스는 고객의 개인 정보를 보호하고 개인 정보와 관련한 불만을 처리하기 위해서 전문 컨설턴트와 대표원장에게 개인 정보관리 책임을 맡기고 있습니다. <br/>개인정보관리 책임자 성명 : 윤정후대표원장, 이형진대표원장
                  전화번호 : 031-962-7781 &#40;AM 10:30 ~ PM 08:30&#41; 이메일 : mi-balance@naver.com <br/><br/>기타 개인 정보 침해에 대한 신고나 상담이 필요하신 경우에는 아래 기관에 문의하시기 바랍니다. <br/>1. 개인분쟁조정위원회 www.1336.or.kr / 1336 <br/>2. 정보보호마크인증위원회 www.eprivacy.or.kr / 02-580-0533~4 <br/>3. 대검찰청 인터넷범죄수사센터 http://icic.sppo.go.kr / 02-3480-3600 <br/>4. 경찰청 사이버테러대응센터 www.ctrc.go.kr / 02-392-0330<br/><br/>10. 부칙 이 개인정보취급방침은 2011년 6월 1일부터 적용되며, 법령, 정책 또는 보안 기술의 변경에 따라 내용의 추가, 삭체 및 수정이 있을 시에는 변경사항의 시행일의 7일 전부터 미밸런스 사이트의 공지사항을 통하여 고지할 것입니다.<br/><br/>■ 개인정보취급방침 버전번호 : 1.0<br/>■ 개인정보취급방침 시행일자 : 2020년 04월 01일<br/><br/>개인정보취급방침에 동의 합니다.
                </p>
                <label>
                  <input style={{verticalAlign: 'middle'}} type="checkbox" value={consulting.isConditionChecked} onChange={this.handleConditionCheck}></input>
                  개인정보취급방침에 동의합니다.
                </label>
              </span>
            </div>
            <div className="consulting-create-container">
              <ul className="consulting-create-wrap">
                <li>
                  <span className="consulting-create-title"><span style={{color: 'red'}}>*</span>{columnsObj.writer}</span>
                  <input className="consulting-create-input create-input-text" type="text" value={consulting.writer} onChange={(e) => this.handleChange(e, 'writer')} placeholder="이름을 입력해주세요." />
                </li>
                <li>
                  <span className="consulting-create-title"><span style={{color: 'red'}}>*</span>{columnsObj.phone}</span>
                  <input className="consulting-create-input create-input-text" type="text" value={consulting.phone} onChange={(e) => this.handleChange(e, 'phone')} placeholder="휴대폰 번호를 입력해주세요. (000-0000-0000)" />
                </li>
                <li>
                  <span className="consulting-create-title">{columnsObj.email}</span>
                  <input className="consulting-create-input create-input-email" type="text" value={consulting.email} onChange={(e) => this.handleChange(e, 'email')} placeholder="이메일을 입력해주세요."/>
                  <span style={{padding: '10px'}}>@</span>
                  <Select value={consulting.email_domain_id} placeholder="이메일 도메인" onChange={(e) => this.handleChange(e, 'email_domain_id')}>
                    {( consultingCategories && consultingCategories.email_domains && consultingCategories.email_domains.length > 0) ? consultingCategories.email_domains.map(el => (
                      <Select.Option key={el.id} label={el.email_domain_name} value={el.id} />
                    )) : null}
                  </Select>
                </li>
                <li>
                  <span className="consulting-create-title"><span style={{color: 'red'}}>*</span>{columnsObj.title}</span>
                  <input className="consulting-create-input create-input-text" type="text" value={consulting.title} onChange={(e) => this.handleChange(e, 'title')} placeholder="제목을 입력해주세요."/>
                </li>
                <li>
                  <span className="consulting-create-title"><span style={{color: 'red'}}>*</span>상담과목</span>
                  <Select value={consulting.main_category_id} placeholder="1분류 상담 분야 선택" onChange={(e) => this.handleChange(e, 'main_category_id')}>
                    {(consultingCategories && consultingCategories.categories && consultingCategories.categories.length > 0) ? consultingCategories.categories.map(el => (
                      <Select.Option key={el.id} label={el.main_category_name} value={el.id} />
                    )) : null}
                  </Select>
                  <span style={{padding: '16px'}}></span>
                  <Select value={consulting.sub_category_id} placeholder="2분류 상담 분야 선택" onChange={(e) => this.handleChange(e, 'sub_category_id')}>
                    {(subCategories && subCategories.length > 0) ? subCategories.map(el => (
                      <Select.Option key={el.id} label={el.sub_category_name} value={el.id} />
                    )) : null}
                  </Select>
                </li>
                <li>
                  <span className="consulting-create-title"><span style={{color: 'red'}}>*</span>{columnsObj.details}</span>
                  <Custom_Image_Font_Quill setContents={consulting.details} onChange={(e) => this.handleChange(e, 'details')} />
                </li>
                <li>
                  <span className="consulting-create-title"><span style={{color: 'red'}}>*</span>{columnsObj.password}</span>
                  <input className="consulting-create-input create-input-email" type="password" value={consulting.password} onChange={(e) => this.handleChange(e, 'password')} placeholder="비밀번호를 입력해주세요."/>
                  <p className="consulting-text">기본 비밀글입니다.</p>
                </li>
                <li>
                  <span className="consulting-create-title num-img"><span style={{color: 'red'}}>*</span></span>
                  <input className="consulting-create-input create-input-email" type="password" value={consulting.picPassword} onChange={(e) => this.handleChange(e, 'picPassword')} placeholder="보안문자를 입력해주세요."/>
                  <p className="consulting-text">숫자를 입력하세요.</p>
                </li>
              </ul>
            </div>

            <ul className="border-create-btn-wrap">  
              <li><a className="btn btn-create" disabled={disabled} onClick={this.createConsulting}>글쓰기</a></li>
              <li><a className="btn btn-cancel" onClick={this.goBack}>취소</a></li>
            </ul>
          </div>
        </div>
      </Fragment>
    )
  }
}
export default Frontend_Consultings_Create;
