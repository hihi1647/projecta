import React, { Fragment } from 'react';
import api from 'common/api';
import parse from 'html-react-parser';
import { reservation_answer_status } from 'constants/index';

import { MessageBox, Message } from 'element-react';

const columnsObj = {
  'title': '제목',
  'writer': '작성자',
  'created_at': '작성일',
  'main_category_name': '상담과목(1분류)',
  'sub_category_name': '상담과목(2분류)'
}

const columns = [
  'title',
  'writer',
  'created_at',
  'main_category_name',
  'sub_category_name'
];

class Frontend_Consulting extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      consulting: {},
      hasPermission: false
    }

    this.history = this.props.history;
    this.match = this.props.match;
    this.$user = this.props.$user;
    this.currentConsultingID = this.match.params.id;
  }

  componentDidMount() {
    this.checkPermissionBeforeGetDatas();
  }

  checkPermissionBeforeGetDatas = () => {
    const $user = this.$user;
    const higherAccess =  $user && ($user.isAdmin || $user.isNurse);
    const consultingID = this.currentConsultingID;
    const location = this.history.location;

    console.log('checking....', location.state, higherAccess);

    if ((location.state && location.state.passwordSuccess) || higherAccess) {
      this.getDatas(consultingID);
    } else {
      MessageBox.prompt('비밀번호를 적어주세요.', '사용자 확인', {
        confirmButtonText: '확인',
        cancelButtonText: '취소',
      }).then(async ({ value }) => {
        const payload = {password: value};
        return await api.checkUserPassword(consultingID, payload, true);
      }).then((res)=>{
        if (res && res.data.data) {
          Message({
            type: 'success',
            message: '비밀번호가 일치합니다.'
          });
  
          this.getDatas(consultingID);
        } else {
          Message({
            type: 'info',
            message: "비밀번호를 잘못 입력하셨습니다. "
          });
        }
      }).catch(() => {
        Message({
          type: 'info',
          message: '죄송하지만 글을 볼 수 없습니다.'
        });
      });
    }
  }

  getDatas = async (currentConsultingID) => {
    await api.getConsulting(currentConsultingID)
      .then(res => {

        const resData = res.data.data;

        if (resData) {
          let created_at = resData.created_at;
          created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;
          
          const consulting = {
            ...resData,
            created_at
          }

          this.setState({
            consulting
          })
        }

      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  editLink = (id) => {
    this.props.history.push(`${this.match.url}/edit`, {passwordSuccess: true});
  }

  answerLink = (id) => {
    this.props.history.push(`${this.match.url}/answer`);
  }

  deleteConsulting = async () => {

    await api.deleteConsulting(this.currentConsultingID, true)
      .then((res)=> {
        Message({
          message: '성공적으로 삭제되었습니다.',
          type: 'success'
        });
      })
      .then(() => {
        this.goBack();
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  goBack = () => {
    this.history.push('/consultings');
  }

  render() {
    const consulting = this.state.consulting;
    console.log('this is front consulting', consulting, this.props);
    const $user = this.$user;
    const higherAccess =  $user && ($user.isAdmin || $user.isNurse);
    const isAnswered = consulting.answer && consulting.answer_status_id === reservation_answer_status.answer_done;
    return (
      <Fragment>
        <div className="board-top-style">
          <ul className="board-title-box">
            <li className="board-title">온라인 상담</li>
            <li className="board-title-box-line"></li>
            <li className="board-text">MIBALANCE CONSULTING</li>
          </ul>
        </div>
        <div className="border-container">
          <div className="board-list">
            <div className="border-header">
              <span className="border-header-section">{consulting.main_category_name + ' · ' + consulting.sub_category_name}</span>
              <span className="border-header-title">{consulting.title}</span>
            </div>
            <div className="border-header-bottom">
              {
                columns.slice(1).map(column => (
                  <Fragment key={column} >
                    <span className="border-view-name m-border-view-name">{columnsObj[column]}</span>
                    <span className="border-view-body m-border-view-body">{consulting[column]}</span>
                  </Fragment>
                ))
              }
            </div>
            <div className="border-body">{typeof consulting.details === 'string' ? parse(consulting.details) : consulting.details}</div>
            {/* {
              higherAccess ? (
                (consulting.answer || consulting.memo) && (
                  <div>
                    {
                      consulting.answer && (
                        <div>
                          <div className="haeinTitle"> 답변 </div>
                          <div className="haeinContent"> { consulting.answer } </div>
                        </div>
                      )
                    }
                    {
                      consulting.memo && (
                        <div>
                          <div className="haeinTitle"> 메모 </div>
                          <div className="haeinContent"> { consulting.memo } </div>
                        </div>
                      )

                    }
                  </div>
                ) 
              ) : null
            } */}
            {
              isAnswered && (
                <ul className="answer-wrap">
                  <li className="answer-title"> 답변 내용 </li>
                  <li className="answer-text"> { typeof consulting.answer === 'string' ? parse(consulting.answer) : consulting.answer } </li>
                </ul>
              )
            }
            {
              higherAccess && (
                <ul className="answer-wrap memo">
                  <li className="memo-title"> 메모 내용 </li>
                  <li className="memo-text"> { typeof consulting.memo === 'string' ? parse(consulting.memo) : consulting.memo } </li>
                </ul>
              )
            }
            <ul className="border-btn-wrap">
            {
              // need to be improve (담당자 이상일 때, 답변하기로 보이고, answer가 있다면 유저한테는 안보여야한다.)
              higherAccess ? (
              
                  <li><a className="btn-frontend btn-borderList" onClick={this.answerLink}>답변하기</a></li>
              
              ) : (
                // 기본 유저일 때 answer 체크
                isAnswered ? null : (
                  <li><a className="btn-borderList" onClick={this.editLink}>수정</a></li>
                )
              ) 
            }
                  <li><a className="btn-frontend  btn-delete" onClick={this.deleteConsulting}>삭제</a></li>
                  <li><a className="btn-frontend  btn-borderList" onClick={this.goBack}>목록</a></li>
            </ul>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default Frontend_Consulting;