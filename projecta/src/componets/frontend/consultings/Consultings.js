import React, { Component, Fragment } from 'react';
import api from 'common/api';

import { parseDate, formatDate, editingConsultingName } from 'common/Utils';

import { Select, Pagination, MessageBox, Message } from 'element-react';

// https://material-ui.com/components/tables/

const columns = [
  { prop: 'checkbox', label: '선택' },
  { prop: 'numIndex', label: '번호'},
  { prop: 'main_category_name', label: '구분'},
  { prop: 'title', label: '제목'},
  { prop: 'answer_status_name', label: '상담결과'},
  { prop: 'writer', label: '작성자'},
  { prop: 'created_at', label: '작성일'},
  { prop: 'answering', label: '답변' }
];

const userColumns = [
  { prop: 'numIndex', label: '번호'},
  { prop: 'main_category_name', label: '구분'},
  { prop: 'title', label: '제목'},
  { prop: 'answer_status_name', label: '상담결과'},
  { prop: 'writer', label: '작성자'},
  { prop: 'created_at', label: '작성일'}
]

const mColumns = [
  { prop: 'title', label: '제목'},
  { prop: 'main_category_name', label: '구분'},
  { prop: 'created_at', label: '작성일'},
  { prop: 'answering', label: '답변' },
  { prop: 'answer_status_name', label: '상담결과'},
];

let rows = [];

function getIndexedRows(datas) {
  let numIndex = datas.per_page * (datas.current_page - 1);  

  return datas.data.map(data => {
    numIndex = data.id;

    let created_at = data.created_at;
    created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;

    return {
      ...data,
      numIndex,
      checkbox: false,
      created_at,
      writer: editingConsultingName(data.writer)
    }
  });
}

class Frontend_Consultings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      total: 0,
      perPage: 0,
      checkboxObj: [],
      searchData: {
        categ: 0,
        searchOption: 0,
        query: ''
      },
      isSearched: false,
      checkAll: false
    }
  }

  componentDidMount() {
    this.getDatas();
  }

  getDatas = async (page) => {
    await api.getConsultings(page)
      .then(res => {
        const resData = res.data.data;

        if (resData) {
          rows = getIndexedRows(resData);

          this.setState({
            page: resData.current_page,
            total: resData.last_page,
            perPage: resData.per_page,
            checkboxObj: rows,
            isSearched: false
          });
        }
        
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  getSearchedDatas = async (page) => {
    const { searchData } = this.state;
    const payload = {
      search_word: searchData.query,
      main_category_id: searchData.categ,
      search_option_id: searchData.searchOption
    };

    await api.getSearchConsulting(payload, page)
      .then(res => {
        const resData = res.data.data;

        if (resData) {
          rows = getIndexedRows(resData);

          this.setState({
            page: resData.current_page,
            total: resData.last_page,
            perPage: resData.per_page,
            checkboxObj: rows,
            isSearched: true
          });
          console.log('successfully searched..', res, rows);
        }
        
      })
      .catch(error => {
        console.log(error);
      })
  }

  handleChangePage = (selectedPage) => {
    const { isSearched } = this.state;
    if (isSearched) {
      this.getSearchedDatas(selectedPage);
    } else {
      this.getDatas(selectedPage);
    }
  };

  handleCheckBox = (event, id) => {
    const ind = this.state.checkboxObj.findIndex(cur => cur.id === id);
    const newTarget = {
      ...this.state.checkboxObj[ind],
      checkbox: event.target.checked
    }
    let newCheckboxObj = [
      ...this.state.checkboxObj.slice(0, ind),
      newTarget,
      ...this.state.checkboxObj.slice(ind + 1),
    ]

    const checkAll = newCheckboxObj.length === newCheckboxObj.filter(cur => cur.checkbox).length;
    this.setState({
      checkboxObj: newCheckboxObj,
      checkAll
    })
  }

  handleSelectAll = () => {
    let checkAll = !this.state.checkAll;

    let newCheckboxObj = this.state.checkboxObj.map(cur => ({
      ...cur,
      checkbox: checkAll
    }));

    this.setState({
      checkboxObj: newCheckboxObj,
      checkAll
    })
  }

  createConsulting = () => {
    this.props.history.push(`${this.props.match.url}/consulting/create`);
  }

  deleteConsulting = async () => {

    const checkedArr = this.state.checkboxObj.filter(consulting => consulting.checkbox);
    const checkedIDs = checkedArr.map(consulting => consulting.id); 
    if (checkedIDs.length > 0) {
       await api.deleteConsultingsAll({ids: checkedIDs})
        .then(res => {
          Message({
            message: '성공적으로 삭제되었습니다.',
            type: 'success'
          });
          
        })
        .then(() => {
          this.getDatas();
        })
        .catch(err => {
          console.log('something went wrong', err);
        });
    }
  }

  searchClickHanlder = async () => {
    console.log('clicking...', this.state.searchData);

    const { searchData, isSearched } = this.state;

    if (searchData.searchOption === 0 && searchData.categ === 0 && searchData.query === '') {
      if (isSearched) {
        this.getDatas();
      }
    } else {
      this.getSearchedDatas();
    }
  }

  categChangeHandler = (value) => {
    this.searchChangeHandler(value,'categ');
  }
  searchOptionChangeHandler = (value) => {
    this.searchChangeHandler(value,'searchOption');
  }

  searchChangeHandler = (target, type) => {
    let newSearchData = {
      ...this.state.searchData
    };
    if (type === 'query') {
      newSearchData = {
        ...newSearchData,
        [type]: target.target.value
      }
    } else {
      newSearchData = {
        ...newSearchData,
        [type]: target
      };
    }

    this.setState({
      searchData: newSearchData
    });
  }

  searchClear = () => {
    this.setState({
      searchData: {
        categ: 0,
        searchOption: 0,
        query: ''
      }
    })
  }

  handleLink = (consulting) => {
    const $user = this.props.$user;
    const higherAccess =  $user && ($user.isAdmin || $user.isNurse);
    const consultingID = consulting.id;

    if ($user && ($user.id === consulting.user_id || higherAccess)) {
      this.props.history.push(`${this.props.match.url}/consulting/${consultingID}`, {passwordSuccess: true});
    } else {
      MessageBox.prompt('비밀번호를 입력해주세요.', '사용자 확인', {
        confirmButtonText: '확인',
        cancelButtonText: '취소',
      }).then(async ({ value }) => {
        const payload = {password: value};
        return await api.checkUserPassword(consultingID, payload, true);
      }).then((res)=>{
        if (res && res.data.data) {
          Message({
            type: 'success',
            message: '비밀번호가 일치합니다.'
          });
  
          this.props.history.push(`${this.props.match.url}/consulting/${consultingID}`, {passwordSuccess: res.data});
        } else {
          Message({
            type: 'info',
            message: "비밀번호를 잘못 입력하셨습니다. "
          });
        }
      }).catch(() => {
        Message({
          type: 'info',
          message: '죄송하지만 글을 볼 수 없습니다.'
        });
      });
    }

    // Email inputPattern: /[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?/,
  }

  answerLink = (id) => {
    this.props.history.push(`${this.props.match.url}/consulting/${id}/answer`);
  }

  render() {
    console.log('this is front consultings', this.state, this.props);
    const { searchData, checkboxObj } = this.state;
    const { categ, searchOption, query } = searchData;
    const consultingCategories = this.props.consultingCategories;
    const $user = this.props.$user;
    const higherAccess =  $user && ($user.isAdmin || $user.isNurse);
    const colClassName = higherAccess ? 'consulting-col-admin' : 'consulting-col';
    let count = 0;
 
    return (
      <Fragment>
        <div className="board-top-style">
          <ul className="board-title-box">
              <li className="board-title">온라인 상담</li>
              <li className="board-title-box-line"></li>
              <li className="board-text">MIBALANCE CONSULTINGS</li>
          </ul>
        </div>
          <div className="border-container">
            <div className="board-list">
              <div className="border-topBtn-wrap">
                {
                  higherAccess && (
                    <div>
                      <label className="btn-all"><input style= {{verticalAlign: "middle"}} type="checkbox" checked={this.state.checkAll} onChange={this.handleSelectAll}></input>전체선택</label>
                      <button className="btn-all" onClick={this.deleteConsulting}><svg style={{marginRight:"2px"}} xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24" fill="#666"><path d="M3 6v18h18v-18h-18zm5 14c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm5 0c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm5 0c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm4-18v2h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.315c0 .901.73 2 1.631 2h5.712z"/></svg>삭제</button>
                    </div>
                  )
                }
              </div>
              <table style={{marginBottom:'1rem'}}>
              <colgroup>
                {/* <col className> [관리자로 로그인했을때 .consulting-col-admin] [기본값 .consulting-col]  */}
                { higherAccess && <col className={colClassName}/> }
                <col className={colClassName}/>
                <col className={colClassName}/>
                <col/>
                <col className={colClassName}/>
                <col className={colClassName}/>
                <col className={colClassName}/>
                { higherAccess && <col className={colClassName}/> }
              </colgroup>
                <thead>
                  <tr>
                    {
                      higherAccess ? (
                        columns.map((column, index) => {
                          count += 1;
                          return (
                            <td key={count} >
                              {column.label}
                            </td>
                          )
                        })
                      ) : (
                        userColumns.map((column, index) => {
                          count += 1;
                          return (
                            <td key={count} >
                              {column.label}
                            </td>
                          )
                        })
                      )
                    }
                  </tr>
                </thead>
                <tbody>
                  {
                    checkboxObj.map((row, index) => {
                      count += 1;
                      return (
                        <tr key={count} >
                        {
                          higherAccess ? (
                            columns.map((column, colIndex) => {
                              count += 1;
                              return column.prop === 'checkbox' ? (
                                <td key={count} >
                                  <input type="checkbox" checked={row[column.prop]} onChange={(e) => this.handleCheckBox(e, row.id)} />
                                </td>
                              ) : (
                                column.prop === 'numIndex' ? (
                                  <td key={count} onClick={() => this.handleLink(row)}>
                                    {row[column.prop] ? row[column.prop] : 'ㅁ'}
                                  </td>
                                ) : (
                                  column.prop === 'answering' ? (
                                    <td key={count}>
                                      <a className="btn-frontend btn-answer" onClick={() => this.answerLink(row.id)}>답변하기</a>
                                    </td>
                                  ) : (
                                    column.prop === 'answer_status_name' ? (
                                      <td style={{cursor:'pointer'}} key={count} onClick={() => this.handleLink(row)} >
                                        {/* 답변 버튼 */}
                                        <div className={row.answer_status_id === 1 ? "notAnswer" : "answered"}>{row[column.prop]}</div>
                                      </td>
                                    ) : (
                                      column.prop === 'title' ? (
                                        <td style={{cursor:'pointer', textAlign: "left", maxWidth:'280px',padding: '0 2rem' ,whiteSpace: 'nowrap',textOverflow: 'ellipsis', overflow: 'hidden'}} key={count} onClick={() => this.handleLink(row)} >
                                          {/* 제목 아이콘 */}
                                          <span>
                                            <span className="icon-consulting"></span>
                                            {row.writer + '님의 상담입니다.'}
                                          </span>
                                        </td>
                                      ) : (
                                        <td style={{cursor:'pointer'}} key={count} onClick={() => this.handleLink(row)} >
                                          {row[column.prop]}
                                        </td>
                                      )
                                    )
                                  )
                                )
                              )
                            })
                          ) : (
                            userColumns.map((column, colIndex) => {
                              count += 1;
                              return (
                                column.prop === 'numIndex' ? (
                                  <td key={count} onClick={() => this.handleLink(row)}>
                                    {row[column.prop] ? row[column.prop] : 'ㅁ'}
                                  </td>
                                ) : (
                                  column.prop === 'title' ? (
                                    <td style={{cursor:'pointer',textAlign: "left", maxWidth:'280px', padding: '0 2rem', whiteSpace: 'nowrap',textOverflow: 'ellipsis', overflow: 'hidden'}} key={count} onClick={() => this.handleLink(row)} >
                                      {/* 제목 아이콘 */}
                                      <span>
                                        <span className="icon-consulting"></span>
                                        {row.writer + '님의 상담입니다.'}
                                      </span>
                                    </td>
                                  ) : (
                                    column.prop === 'answer_status_name' ? (
                                      <td style={{cursor:'pointer'}} key={count} onClick={() => this.handleLink(row)} >
                                        {/* 답변 버튼 */}
                                        <div className={row.answer_status_id === 1 ? "notAnswer" : "answered"}>{row[column.prop]}</div>
                                      </td>
                                    ) : (
                                      <td style={{cursor:'pointer'}} key={count} onClick={() => this.handleLink(row)} >
                                        {row[column.prop]}
                                      </td>
                                    )
                                  )                                
                                )
                              )
                            })
                          )
                        }
                        
                      </tr>

                      )
                    })
                  }
                </tbody>
              </table>
              <ul className="m-board">
                { checkboxObj.map((row, index) => {
                  count += 1;
                  return (
                    <li className="m-board-list" key={count}>
                        {
                          higherAccess && (
                            <span>
                              <input type="checkbox" checked={row.checkbox} onChange={(e) => this.handleCheckBox(e, row.id)} />
                            </span>
                          )
                        }
                        {/* 제목 아이콘 */}
                        <span className="m-consulting-text-wrap" onClick={() => this.handleLink(row)}>
                          <span className="icon-consulting"></span>
                          <p style={{fontSize:"14px",cursor:'pointer'}}>{row.writer + '님의 상담입니다.'}</p>
                        </span>

                        <span className="m-consulting-wrap">
                          <p style={{cursor:'pointer', color: '#757575', marginRight: '0.5em'}} onClick={() => this.handleLink(row)}>
                            {row.main_category_name}
                          </p>
                          <p style={{cursor:'pointer', color: '#757575', marginRight: '0.5em'}} onClick={() => this.handleLink(row)}>
                            {row.created_at}
                          </p>
                        </span>
                        
                        <div className="consulting-btn-wrap">
                        <span className="consulting-btn" style={{cursor:'pointer'}} onClick={() => this.handleLink(row)}>
                          {/* 답변 버튼 */}
                          <span className={row.answer_status_id === 1 ? "notAnswer" : "answered"}>{row.answer_status_name}</span>
                        </span>
                        {
                          higherAccess && (
                            <span>
                              <span className="btn-frontend btn-answer" onClick={() => this.answerLink(row.id)}>답변하기</span>
                            </span>
                          )
                        }    
                        </div>
                    </li>
                  )
                })
                }
              </ul>
              <button className="btn-frontend btn-writing" onClick={this.createConsulting}>글쓰기</button>
              <br/>
              <Pagination 
                style={{display:'flex',justifyContent:'center'}} 
                layout="prev, pager, next" 
                currentPage={this.state.page} 
                pageCount={this.state.total} 
                pageSize={this.state.perPage} 
                small={true}
                onCurrentChange={this.handleChangePage}
              />
              {/* <div className="board-search">
                <div className="search-box">
                  <Select value={searchData.categ} placeholder="구분없음" onChange={this.categChangeHandler}>
                    {consultingCategories.categories ? consultingCategories.categories.map(el => {
                      return <Select.Option key={el.id} label={el.main_category_name} value={el.id} />
                    }) : null}
                  </Select>
                </div>
                <div className="search-box">
                  <Select value={searchData.searchOption} placeholder="글제목" onChange={this.searchOptionChangeHandler}>
                    {consultingCategories.search_options ? consultingCategories.search_options.map(el => {
                      return <Select.Option key={el.id} label={el.search_option_name} value={el.id} />
                    }) : null}
                  </Select>
                </div>
                <span>
                  <input className="board-input search-box" type="text" value={query} onChange={(e) => this.searchChangeHandler(e, 'query')} />
                  {
                    (categ || searchOption || query) && <i className="el-icon-circle-cross" onClick={this.searchClear}></i> 
                  }
                </span>
                <a className="btn-frontend btn-search search-box" onClick={this.searchClickHanlder}>검색</a>
              </div> */}
            </div>
        </div>
      </Fragment>
    )

  }
}
export default Frontend_Consultings;