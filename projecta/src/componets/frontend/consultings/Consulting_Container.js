import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { AuthenticatedRoute } from 'componets/auth/AuthenticatedRoute';
import Consulting from './Consulting';
import Consultings from './Consultings';
import Consulting_Create from './Create';
import Consulting_Edit from './Edit';
import Consulting_Answer from './Answer';

import api from 'common/api';

class Consulting_Container extends React.Component {
    state = {
        consultingCategories: {},
    };
    

    componentDidMount() {
        this.getDatas();
    }

    getDatas = async () => {
        await api.getConsultingCategories()
            .then(res => {
                let consultingCategories = {
                    ...res.data.data
                };

                const email_domains =  [
                    ...[{id: 'default', email_domain_name: "직접입력"}],
                    ...consultingCategories.email_domains
                ];

                consultingCategories = {
                    ...consultingCategories,
                    email_domains
                }

                this.setState({
                    consultingCategories
                })
            })
            .catch(error => {
                console.log('something went wrong with consulting category', error);
            });
    }

    render() {
        const { consultingCategories } = this.state
        const { path } = this.props.match;

        const { $user, isAuthenticated, changeState } = this.props;
        return (
            <Switch>
                <AuthenticatedRoute path={`${path}/consulting/:id/answer`} authSuccess={$user && ($user.isAdmin || $user.isNurse)} $user={$user} component={Consulting_Answer} />
                <Route path={`${path}/consulting/:id/edit`} render={(props) => <Consulting_Edit {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={`${path}/consulting/create`} render={(props) => <Consulting_Create {...props} $user={$user} consultingCategories={consultingCategories} />} />
                <Route path={`${path}/consulting/:id`} render={(props) => <Consulting {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={path} render={(props) => <Consultings {...props} $user={$user} consultingCategories={consultingCategories} />} />
            </Switch>
        )
    }
}

export default Consulting_Container;