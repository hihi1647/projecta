import React, { Fragment } from 'react';
import api from 'common/api';
import { Select, MessageBox, Message } from 'element-react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import dateFnsFormat from 'date-fns/format';

import { parseDate, formatDate, formatDateMySQL } from 'common/Utils';
import { DATE_FORMAT, reservation_answer_status } from 'constants/index';

import Custom_Image_Font_Quill from 'componets/quill/Custom_Image_Font_Quill';

const columnsObj = {
  'title': '제목',
  'phone': '휴대폰',
  'writer': '작성자',
  'created_at': '작성일',
  'views': '조회수',
  'email': '이메일',
  'details': '내용',
  'password': '비밀번호'
}

const columns = [
  'title',
  'writer',
  'created_at',
  'views'
];

class Frontend_Consultings_Edit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      consulting: {},
      consultingCategories: [],
      subCategories: [],
      setUp: {},
      checkPermission: 'pending'
    }

    this.history = this.props.history;
    this.match = this.props.match;
    this.currentConsultingID = this.match.params.id;
  }

  componentDidMount() {
    this.checkPermissionBeforeGetDatas();
  }

  checkPermissionBeforeGetDatas = () => {
    const consultingID = this.currentConsultingID;
    const location = this.history.location;
    
    if (location.state && location.state.passwordSuccess) {
      this.getDatas(consultingID);
    } else {
      // have action here(admin )
      MessageBox.prompt('비밀번호를 적어주세요.', '사용자 확인', {
        confirmButtonText: '확인',
        cancelButtonText: '취소',
      }).then(async ({ value }) => {
        const payload = {password: value};
        return await api.checkUserPassword(consultingID, payload, true);
      }).then((res)=>{
        if (res && res.data.data) {
          Message({
            type: 'success',
            message: '비밀번호가 일치합니다.'
          });
  
          this.getDatas(consultingID);
        } else {
          Message({
            type: 'info',
            message: "비밀번호를 잘못 입력하셨습니다. "
          });
        }
      }).catch(() => {
        Message({
          type: 'info',
          message: '죄송하지만 글을 볼 수 없습니다.'
        });
      });
    }
  }

  getDatas = async (currentConsultingID) => {
    await api.getUpdateConsulting(currentConsultingID, true)
      .then(res => {
        const resData = res.data.data;

        if (resData) {
          let { created_at, details } = resData.consulting;
          if (typeof created_at === 'string') {
            created_at = new Date(created_at);
          } else if (!created_at) {
            created_at = new Date();
          }

          if (details === null) {
            details = '';
          }

          const consulting = {
            ...resData.consulting,
            created_at,
            email_domain_id: 'default',
            details
          }

          let setUp = resData.setUp;
          const email_domains =  [
            ...[{id: 'default', email_domain_name: "직접입력"}],
            ...setUp.email_domains
          ];

          setUp = {
            ...setUp,
            email_domains
          }

          const consultingCategories = setUp.categories;
          const subCategories = consultingCategories.filter(categ => categ.id === consulting.main_category_id)[0].sub_categories;

          this.setState({
            setUp,
            consultingCategories,
            subCategories,
            consulting,
            checkPermission: true
          })
        }
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }


  updateConsulting = async () => {

    // input validation form check
    let validationCheck = true;

    const {
        writer,
        phone,
        created_at,
        title,
        main_category_id,
        sub_category_id,
        details,
        email_domain_id
    } = this.state.consulting;

    let email = this.state.consulting.email;

    if (email_domain_id !== 'default') {
      const givenDomains = this.state.setUp.email_domains;
      const selectedDomain = givenDomains.filter(domain => domain.id === email_domain_id)[0];

      if (selectedDomain) {
        email = email + '@' + selectedDomain.email_domain_name;
      } 
    }

    let payload = {
        writer,
        phone,
        created_at,
        email,
        title,
        main_category_id,
        sub_category_id,
        details
    }

    if ( !payload.created_at ) {
      payload.created_at = formatDateMySQL(new Date());
    } else {
      payload.created_at = formatDateMySQL(payload.created_at);
    }

    if ( validationCheck ) {
      await api.updateConsulting(this.currentConsultingID, payload, true)
      .then(res => {
        Message({
          message: '성공적으로 수정되었습니다.',
          type: 'success'
        });
        console.log('successfully edit', res);
      })
      .then(() => {
        this.goBack();
      })
      .catch(error => {
        console.log('something went wrong', error);
      })
    }
  }

  deleteConsulting = async () => {

    await api.deleteConsulting(this.currentConsultingID, true)
      .then((res)=> {
        Message({
          message: '성공적으로 삭제되었습니다.',
          type: 'success'
        });
      })
      .then(() => {
        this.goBack();
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }


  cancel = () => {
  }

  goBack = () => {
    this.history.push('/consultings');
  }

  handleChange = (event, input) => {

    let newConsulting = {
      ...this.state.consulting
    };

    let subCategories = '';

    if (input === 'created_at' || input === 'sub_category_id' || input === 'email_domain_id' || input === 'details') {
      newConsulting = {
        ...newConsulting,
        [input]: event
      }
    } else if (input === 'main_category_id') {
      const consultingCategories = this.state.consultingCategories;
      subCategories = consultingCategories.filter(categ => categ.id === event)[0].sub_categories;

      newConsulting = {
        ...newConsulting,
        [input]: event,
        sub_category_id: ''
      }
    } else {
      newConsulting = {
        ...newConsulting,
        [input]: event.target.value
      };
    }

    if (subCategories) {
      this.setState({
        consulting: newConsulting,
        subCategories
      })
    } else {
      this.setState({
        consulting: newConsulting
      })
    }

    
  }

  render() {
    const consulting = this.state.consulting;
    const $user = this.props.$user;
    const { consultingCategories, subCategories, setUp, checkPermission } = this.state;
    const higherAccess =  $user && ($user.isAdmin || $user.isNurse);
    console.log('this is edit consulting', consulting, this.props);

    const isAnswered = consulting.answer && consulting.answer_status_id === reservation_answer_status.answer_done;

    return (
      <Fragment>
        <div className="board-top-style">
          <ul className="board-title-box">
            <li className="board-title">온라인 상담</li>
            <li className="board-title-box-line"></li>
            <li className="board-text">MIBALANCE COUNSELING</li>
          </ul>
        </div>
        <div className="border-container">
          <div className="board-list">
          
        {
          checkPermission === 'pending' || !isAnswered ? (
            <div className="answer-board-container">
            <ul className="answer-board-wrap">
              
                <li>
                  <div className="consulting-create-title">{columnsObj.writer}</div>
                  <input className="consulting-create-input create-input-text"  type="text" value={consulting.writer} onChange={(e) => this.handleChange(e, 'writer')} />
                </li>

                <li>
                  <div className="consulting-create-title">{columnsObj.phone}</div>
                  <input className="consulting-create-input create-input-text" type="text" value={consulting.phone} onChange={(e) => this.handleChange(e, 'phone')} />
                </li>

                <li>
                  <div className="consulting-create-title">{columnsObj.created_at}</div>
                  <div>
                    <DayPickerInput
                      formatDate={formatDate}
                      format={DATE_FORMAT}
                      parseDate={parseDate}
                      placeholder={(!consulting.created_at || typeof consulting.created_at === 'string') ? `${dateFnsFormat(new Date(), DATE_FORMAT)}` : `${dateFnsFormat(consulting.created_at, DATE_FORMAT)}`}
                      onDayChange={(e) => this.handleChange(e, 'created_at')}
                    />
                  </div>
                </li>

                <li>
                  <div className="consulting-create-title">{columnsObj.email}</div>
                  <input className="consulting-create-input create-input-email" type="text" value={consulting.email} onChange={(e) => this.handleChange(e, 'email')} />
                  <Select value={consulting.email_domain_id} placeholder="이메일 도메인" onChange={(e) => this.handleChange(e, 'email_domain_id')}>
                    {(setUp && setUp.email_domains && setUp.email_domains.length > 0) ? setUp.email_domains.map(el => (
                      <Select.Option key={el.id} label={el.email_domain_name} value={el.id} />
                    )) : null}
                  </Select>
                </li>

                <li>
                  <div className="consulting-create-title">{columnsObj.title}</div>
                    <input className="consulting-create-input create-input-text" type="text" value={consulting.title} onChange={(e) => this.handleChange(e, 'title')} />
                 
                </li>

                <li>
                  <div className="consulting-create-title">상담과목</div>
                  <div>
                    <Select style={{paddingRight:'1rem'}} value={consulting.main_category_id} placeholder="상담 분야를 선택해 주세요(1분류)" onChange={(e) => this.handleChange(e, 'main_category_id')}>
                      {(consultingCategories && consultingCategories.length > 0) ? consultingCategories.map(el => (
                        <Select.Option key={el.id} label={el.main_category_name} value={el.id} />
                      )) : null}
                    </Select>
                    <Select value={consulting.sub_category_id} placeholder="상담 분야를 선택해 주세요 (2분류)" onChange={(e) => this.handleChange(e, 'sub_category_id')}>
                      {(subCategories && subCategories.length > 0) ? subCategories.map(el => (
                        <Select.Option key={el.id} label={el.sub_category_name} value={el.id} />
                      )) : null}
                    </Select>
                  </div>
                </li>
                
                <li>
                  <div className="consulting-create-title">{columnsObj.details}</div>
                  <div>
                    <Custom_Image_Font_Quill setContents={consulting.details} onChange={(e) => this.handleChange(e, 'details')} />
                  </div>
           
                </li>
                </ul>
          
             
              <div className="border-btn-wrap">
               <button className="btn-borderList" onClick={this.updateConsulting}>확인</button>
                <button className="btn-delete" onClick={this.deleteConsulting}>삭제</button>
               <button className="btn-borderList" onClick={this.goBack}>목록</button>
              </div>
              
        
            </div>
          ) : <div>답변이 완료되어 수정할 수 없습니다.</div>
        }
         </div>
        </div>
      </Fragment>

    )
  }
}
export default Frontend_Consultings_Edit;
