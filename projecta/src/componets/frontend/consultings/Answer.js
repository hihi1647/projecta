import React, { Fragment } from 'react';
import api from 'common/api';
import { Select, Message } from 'element-react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import dateFnsFormat from 'date-fns/format';

import { parseDate, formatDate, formatDateMySQL } from 'common/Utils';
import { DATE_FORMAT } from 'constants/index';

import parse from 'html-react-parser';

import Custom_Image_Font_Quill from 'componets/quill/Custom_Image_Font_Quill';
import Custom_Image_Quill from 'componets/quill/Custom_Image_Quill';

const columnsObj = {
  'title': '제목',
  'phone': '휴대폰',
  'writer': '작성자',
  'created_at': '작성일',
  'views': '조회수',
  'email': '이메일',
  'details': '내용',
  'password': '비밀번호',
  'main_category_id': '상담과목(1분류)',
  'sub_category_id': '상담과목(2분류)',
  'answer_status_id': '상담결과',
  'answer_date': '답변일시',
  'answer': '답변',
  'manager_id': '담당자',
  'consulting_status_id': '2차 상담결과',
  'memo': '관리자 메모'
}

const columns = [
  'writer',
  'created_at',
  'phone',
  'email',
  'title',
  'main_category_id',
  'sub_category_id',
  'details'
];

class Frontend_Consultings_Edit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      consulting: {},
      setUp: {}
    }

    this.history = this.props.history;
    this.match = this.props.match;
    this.currentConsultingID = this.match.params.id;
  }

  componentDidMount() {
    this.getDatas(this.currentConsultingID);
  }

  getDatas = async (currentConsultingID) => {
    await api.getUpdateConsulting(currentConsultingID, true)
      .then(res => {
        const resData = res.data.data;

        if (resData) {
          let { created_at, answer_date, reservation_date, answer, memo } = resData.consulting;
          created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;
          if (answer_date) {
            answer_date = new Date(answer_date);
          } else {
            answer_date = new Date();
          }

          if (reservation_date) {
            reservation_date = new Date(reservation_date);
          } else {
            reservation_date = new Date()
          }

          if (answer === null) {
            answer = ''
          }

          if (memo === null) {
            memo = '';
          }

          const consulting = {
            ...resData.consulting,
            created_at,
            answer_date,
            reservation_date,
            answer,
            memo
          }

          this.setState({
            setUp: resData.setUp,
            consulting
          })
        }
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }


  editConsulting = async () => {

    // input validation form check
    let validationCheck = true;

    const {
      answer_status_id,
      answer_date,
      answer,
      manager_id,
      consulting_status_id,
      memo,
      reservation_date
    } = this.state.consulting;

    let payload = {
      answer_status_id,
      answer_date,
      answer,
      manager_id,
      consulting_status_id,
      memo,
      reservation_date
    }

    if ( !payload.answer_date ) {
      payload.answer_date = formatDateMySQL(new Date());
    } else {
      payload.answer_date = formatDateMySQL(payload.answer_date);
    }

    if ( !payload.reservation_date ) {
      payload.reservation_date = formatDateMySQL(new Date());
    } else {
      payload.reservation_date = formatDateMySQL(payload.reservation_date);
    }

    if ( validationCheck ) {
      await api.updateConsulting(this.currentConsultingID, payload, true)
      .then(res => {
        Message({
          message: '성공적으로 수정되었습니다.',
          type: 'success'
        });
        console.log('successfully created', res);
      })
      .then(() => {
        this.goBack();
      })
      .catch(error => {
        console.log('something went wrong', error);
      })
    }
  }

  deleteConsulting = async () => {

    await api.deleteConsulting(this.currentConsultingID, true)
      .then((res)=> {
        Message({
          message: '성공적으로 삭제되었습니다.',
          type: 'success'
        });
      })
      .then(() => {
        this.goBack();
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }
  
  goBack = () => {
    this.history.push('/consultings');
  }

  handleChange = (event, input) => {

    let newConsulting = {
      ...this.state.consulting
    };

    if (
      input === 'answer_status_id' || 
      input === 'answer_date' || 
      input === 'answer' || 
      input === 'manager_id' || 
      input === 'consulting_status_id' || 
      input === 'reservation_date' || 
      input === 'memo'
    ) {
      newConsulting = {
      ...newConsulting,
      [input]: event
      }
    } else {
      newConsulting = {
        ...newConsulting,
        [input]: event.target.value
      };
    }

    this.setState({
      consulting: newConsulting
    })
  }

  render() {
    const consulting = this.state.consulting;
    const { managers, consulting_statuses, answer_statuses } = this.state.setUp;
    const $user = this.props.$user;
    const higherAccess =  $user && ($user.isAdmin || $user.isNurse);
    console.log('this is answer consulting', consulting, this.props);

    return (
      <Fragment>
        <div className="board-top-style">
          <ul className="board-title-box">
            <li className="board-title">온라인 상담</li>
            <li className="board-title-box-line"></li>
            <li className="board-text">MIBALANCE COUNSELING</li>
          </ul>
        </div>
        <div className="border-container">
          <div className="board-list">
            <div className="answer-board-container">
              <ul className="answer-board-wrap">
              {
                columns.map((column, ind) => (
                  <Fragment key={ind}>
                    <li>
                        <span className="consulting-create-title">{columnsObj[column]}</span>
                        <span className="answer-board-text">{column === 'details' ? ( typeof consulting[column] === 'string' ? parse(consulting[column]) : consulting[column] ) : consulting[column]}</span>
                    
                    </li>
                  </Fragment>
                ))
              }
              <li>
                  <span className="consulting-create-title">{columnsObj.answer_status_id}</span>
                  <span>
                      <Select value={consulting.answer_status_id} placeholder="구분 선택" onChange={(e) => this.handleChange(e, 'answer_status_id')}>
                          {(answer_statuses && answer_statuses.length > 0) ? answer_statuses.map(el => (
                          <Select.Option key={el.id} label={el.answer_status_name} value={el.id} />
                          )) : null}
                      </Select>
                  </span>
              </li>
              
              <li>
                  <span className="consulting-create-title">{columnsObj.answer_date}</span>
                  <span>
                      <DayPickerInput
                        formatDate={formatDate}
                        format={DATE_FORMAT}
                        parseDate={parseDate}
                        placeholder={(!consulting.answer_date || typeof consulting.answer_date === 'string') ? `${dateFnsFormat(new Date(), DATE_FORMAT)}` : `${dateFnsFormat(consulting.answer_date, DATE_FORMAT)}`}
                        onDayChange={(e) => this.handleChange(e, 'answer_date')}
                      />
                  </span>
              </li>
              <li>
                  <span className="consulting-create-title">{columnsObj.answer}</span>
                  <span>
                    <Custom_Image_Quill setContents={consulting.answer} onChange={(e) => this.handleChange(e, 'answer')} />
                   {/*  <Custom_Image_Font_Quill setContents={consulting.answer} onChange={(e) => this.handleChange(e, 'answer')} /> */}
                    
                  </span>
              </li>
            </ul>
          </div>

          <div>
              <div> 관리자 메모 </div>
              <ul className="answer-board-wrap">
                  <li>
                      <span className="consulting-create-title">{columnsObj.manager_id}</span>
                      <span>
                          <Select value={consulting.manager_id} placeholder="담당자 선택" onChange={(e) => this.handleChange(e, 'manager_id')}>
                              {(managers && managers.length > 0) ? managers.map(el => (
                              <Select.Option key={el.id} label={el.manager_name} value={el.id} />
                              )) : null}
                          </Select>
                      </span>
                  </li>
                  <li>
                      <span className="consulting-create-title">{columnsObj.consulting_status_id}</span>
                      <span>
                          <Select value={consulting.consulting_status_id} placeholder="상담결과 선택" onChange={(e) => this.handleChange(e, 'consulting_status_id')}>
                              {(consulting_statuses && consulting_statuses.length > 0) ? consulting_statuses.map(el => (
                              <Select.Option key={el.id} label={el.consulting_status_name} value={el.id} />
                              )) : null}
                          </Select>
                      </span>
                  </li>
                  <li>
                    <span className="consulting-create-title">{columnsObj.reservation_date}날짜</span>
                    <span>
                          <DayPickerInput
                            formatDate={formatDate}
                            format={DATE_FORMAT}
                            parseDate={parseDate}
                            placeholder={(!consulting.reservation_date || typeof consulting.reservation_date === 'string') ? `${dateFnsFormat(new Date(), DATE_FORMAT)}` : `${dateFnsFormat(consulting.reservation_date, DATE_FORMAT)}`}
                            onDayChange={(e) => this.handleChange(e, 'reservation_date')}
                          />
                      </span>
                  </li>
                  <li>
                      <span className="consulting-create-title">{columnsObj.memo}</span>
                      <span>
                        <Custom_Image_Quill setContents={consulting.memo} onChange={(e) => this.handleChange(e, 'memo')} />
                        {/* <Custom_Image_Font_Quill setContents={consulting.memo} onChange={(e) => this.handleChange(e, 'memo')} /> */}
                      </span>
                  </li>
                </ul>

              <ul className="border-btn-wrap">
                <li><a className="btn-borderList" onClick={this.editConsulting}>답변하기</a></li>
                <li><a className="btn-delete" onClick={this.deleteConsulting}>삭제</a></li>
                <li><a className="btn-borderList" onClick={this.goBack}>목록</a></li>
              </ul>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }
}
export default Frontend_Consultings_Edit;
