import React, {Fragment} from 'react';
import './styleContents.css';
import api from 'common/api';

import { HAS_NUMS_DETAIL_PAGE_COMPS } from 'constants/index';
import MyComponent from './components/index';
import GhostPane from './components/GhostPane';
import Component_Inspection from './components/Component_Inspection';
import { addZeroStringIfLessThan10 } from 'common/Utils';

class Dynamic_Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {},
            componentsOrders: []
        }
    }

    componentDidMount() {
        this.getDatas();
    }

    getDatas = async () => {
        const url = this.props.match.url;
        const specific_links = this.props.specific_links;

        const targetID = specific_links.filter(sub => sub.link === url)[0].id;
        if (targetID) {
            console.log('this is targetID', targetID);
            await api.getFrontendDetailPage(targetID)
                .then(res => {
                    const resData = res.data.data;

                    if (resData && resData.length > 0) {
                        const componentsOrders = resData.map(comp => Object.keys(comp)[0]);

                        this.setState({
                            data: resData,
                            componentsOrders
                        })
                    } else {
                        this.setState({
                            data: null
                        })
                    }
                })
                .catch(error => {
                    console.log('something went wrong', error);
                });
        }
    }

    render () {
        console.log('this is frontend menu detail page', this.state);
        const { componentsOrders, data } = this.state;
        let compNums = 0;
        return (
            <Fragment>
                <section>
                    {
                        data ? (
                            componentsOrders.length > 0 ? (
                                componentsOrders.map((comp, ind) => {
                                    let currentCompNum;
                                    if (HAS_NUMS_DETAIL_PAGE_COMPS.includes(comp)) {
                                        compNums += 1;
                                        currentCompNum = addZeroStringIfLessThan10(compNums);
                                    }
        
                                    return (
                                        <MyComponent tag={comp} key={ind} datas={data[ind][comp]} num={currentCompNum} />
                                    )
                                })
                            ) : (
                                <GhostPane />
                            )
                        ) : (
                            <Component_Inspection />
                        )
                    }  
                </section>
            </Fragment>
        )
    }
}
export default Dynamic_Component;
