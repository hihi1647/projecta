import React from 'react';
import parse from 'html-react-parser';
import { currentUsesDomain }  from 'constants/index'

const imgUrl = currentUsesDomain.imgUrl;

const Component_D = ({ datas, num }) => {
    const { title, list, background_color, image, details } = datas;
    const backgroundImage = `url(${imgUrl + image})`
    return (
        <div className="introduction-background img_basics" style={background_color ? {backgroundColor: background_color} : {}}>
            <div className="contents-container">
                <div className="container-wrap">

                    <div className="introduction-content">
                        <p className="contents-number">{num}</p>
                        {/* this is for html parse */}
                        {/* <h2 className="contents-title"><strong>스킨 부스터</strong> 주사소개</h2> */}
                        <h2 className="contents-title">{ typeof title === 'string' ? parse(title) : title }</h2>
                        {
                            details && (
                                <p className="contnets-desc">
                                    { typeof details === 'string' ? parse(details) : details }
                                </p>
                            )
                        }
                        {
                            image && (
                                <figure className="introduction-card-height-img img_contain" style={image && { backgroundImage }} />
                            )
                        }
                        
                        <ul className="introduction-card">
                            {
                                list && list.length > 0 &&  list.map((cur, ind) => {
                                    const img = `url(${imgUrl + cur.image})`;
                                    const curDetails = cur.details;

                                    return (
                                        <li className="introduction-card-list" key={cur.title + ind}>
                                            <figure className="introduction-card-img img_basics" style={{backgroundImage: img}}></figure>
                                            <div className="introduction-card-desc"> 
                                                <p className="introduction-card-title">{cur.title}</p>
                                                <p className="introduction-card-sub-title" style={cur.text_color ? {color: cur.text_color} :{}}>{cur.sub_title}</p>
                                                <p className="introduction-card-text">{ typeof curDetails === 'string' ? parse(curDetails) : curDetails }</p>
                                            </div>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Component_D;