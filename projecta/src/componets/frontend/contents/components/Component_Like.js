import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import parse from 'html-react-parser';
import { currentUsesDomain }  from 'constants/index'

const imgUrl = currentUsesDomain.imgUrl;

export default function Component_Like ({ datas, num }) {
    const { background_image, list, title, details } = datas;
    const backgroundImage = `url(${imgUrl + background_image})`;
    return (
        <Fragment>
            {/* 함께하면 좋아요*/}
            <div className="like-background img_basics" style={background_image ? { backgroundImage }: {}}>
            <div className="contents-container">
                <div className="container-wrap">
                <h3 className="contents-title">{ typeof title === 'string' ? parse(title) : title }</h3>
                    <div>{ typeof details === 'string' ? parse(details) : details }</div>
                    <ul className="like-card">
                        {
                            list && list.length > 0 && list.map((cur, ind) => {
                                const backgroundImage = `url(${imgUrl + cur.image})`;
                                return (
                                    <li className="like-card-list" key={cur.title + ind}>
                                        <figure className="like-card-img img_contain" style={cur.image ? { backgroundImage } : {}}>
                                            <figcaption className="ir">{ cur.details }</figcaption>
                                        </figure>
                                        <div><p className="like-card-sub-title color-2B2749 ">{ cur.title }</p></div>
                                        <div><p className="like-card-title color-2B2749">{ cur.details }</p></div>
                                        <div><Link to={ cur.link } className="like-card-btn-2B2749 color-2B2749">자세히 보기</Link></div>
                                        <div><p className="like-deco-text">MI BALANCE</p></div>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
            </div>
        </div>
        </Fragment>
    )
}