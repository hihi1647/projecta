import React, { Fragment } from 'react';

// 시술부위 사진2개
import Image01 from 'assets/images/treatmentArea-two-01.jpg';
import Image02 from 'assets/images/treatmentArea-two-02.jpg';
export default function Component_TreatmentAreaTwo () {
    return (
        <Fragment>
            {/* 시술 부위 이미지 2개 */}
            <div className="contents-container color-background-DEEDF2">
                <div className="container-wrap">
                <h3 className="contents-title">윤곽주사 시술부위</h3>
                    <ul className="treatmentAreaTwo-card">
                        <li className="treatmentAreaTwo-card-list">
                        <figure className="treatmentAreaTwo-img img_contain" style={{backgroundImage: `url(${Image01})`}}>
                            <figcaption className="ir">face-옆광대,앞광대,코,볼살,v라인,이중턱</figcaption>
                        </figure>
                        </li>
                        <li className="treatmentAreaTwo-card-list">
                        <figure className="treatmentAreaTwo-img img_contain" style={{backgroundImage: `url(${Image02})`}}>
                            <figcaption className="ir">body-팔뚝,복부,허벅지,종아리,부유방,등,러브핸들</figcaption>
                        </figure>
                        </li>
                    </ul>
                </div>
          </div>
        </Fragment>
    )
}