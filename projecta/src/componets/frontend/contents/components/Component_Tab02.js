import React, { Fragment } from 'react';
import { Tabs } from 'element-react';

import img1 from 'assets/images/a-01.png';
import img2 from 'assets/images/a-02.png';
import img3 from 'assets/images/a-03.png';

export default function Component_Tab02 () {
    return (
        <Fragment>
            <div className="contents-container color-background-E1CBA9">
                <div className="container-wrap">
                    {/* 여기에 tab 작업 해주세요 :) */}
                    <Tabs type="border-card" activeName="1">
                         {/* Tabs.Pane이 각 탭 아이템, label은 탭 이름, name(중요)은 각 탭 아이디- 순차적으로 1,2,이렇게 :D */}
                        <Tabs.Pane label='바디 보톡스' name="1">
                            <p className="contents-number">02</p>
                            <h3 className="contents-title"><strong>바디 보톡스</strong> 소개</h3>
                            <p className="contnets-desc">
                                바디 보톡스는 보툴리눔 독소를 근육에 주사하는 시술입니다.<br/> 
                                보톡스의 주성분인 보튤리늄 성분은 근육의 움직임을 조절하는 신경 자극 물질을 막아<br/> 
                                울퉁불퉁 뭉쳐있는 근육을 부드럽게 풀어주고 과도하게 발달된 알, 근육을 축소시켜 줍니다.<br/>  
                                운동으로도 빠지지 않았던 알, 근육을 슬림하게 정리해줍니다.<br/> 
                            </p>
                        
                            <div className="effect-content">
                                <h3 className="contents-title">물방울 리프팅 효과</h3>
                                <ul className="effect-card"> 
                                    <li className="effect-card-item" style={{ backgroundImage:`url(${img1})` }}>
                                        <figure className="effect-card-img img_basics" ></figure>
                                        <p className="effect-card-text">
                                        자연 발생적 피부보습
                                        </p>
                                    </li>
                                    <li className="effect-card-item" style={{ backgroundImage:`url(${img1})` }}>
                                        <figure className="effect-card-img img_basics" ></figure>
                                        <p className="effect-card-text">
                                        여드름·홍조 피부진정
                                        </p>
                                    </li>
                                    <li className="effect-card-item" style={{ backgroundImage:`url(${img1})` }}>
                                        <figure className="effect-card-img img_basics" ></figure>
                                        <p className="effect-card-text">
                                        촉촉한 물광피부
                                        </p>
                                    </li>
                                    <li className="effect-card-item" style={{ backgroundImage:`url(${img1})` }}>
                                        <figure className="effect-card-img img_basics" ></figure>
                                        <p className="effect-card-text">
                                        피부탄력 증가
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </Tabs.Pane>
                        <Tabs.Pane label='바디 스킨보톡스' name="2">
                            <div>
                                여기에 탭 jsx
                            </div>
                        </Tabs.Pane>

                        <Tabs.Pane label='다한증 보톡스' name="3">
                            <div>
                                여기에 탭 jsx
                            </div>
                        </Tabs.Pane>
                    </Tabs>
                </div>
            </div>
        </Fragment>
    )
}