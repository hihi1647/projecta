import React from 'react';
import { currentUsesDomain }  from 'constants/index'

const imgUrl = currentUsesDomain.imgUrl;

const Component_G = ({ datas }) => {
    const { background_image, image } = datas;
    const backgroundImage = `url(${imgUrl + background_image})`;
    const frontImage = `url(${imgUrl + image })`;

    return (
        <div className="solution-img" style={{backgroundImage: backgroundImage}}>
            {/* this is for html parse */}
            <h3 className="ir">스킨 부스터 이런분들께 아주 좋아요 !</h3>
            <ol className="ir">
                <li>피부가 예민하고 민감해 레이저 시술을 받지 못하시는 분!</li>
                <li>피부 속 환경부터 건강하고 생기 있게 개선하고 싶으신 분!</li>
                <li>피부톤을 화사하게, 피부결을 매끄럽게 정리하고 싶으신 분!</li>
                <li>눈가, 입가, 목, 손 등의 잔주름을 개선하고 싶으신 분!</li>
                <li>촉촉한 수분감과 탱탱한 피부 탄력감을 느끼고 싶으신 분!</li>
                <li>피부 속부터 빛나는 자연스러운 광채감을 원하시는 분!</li>
            </ol>
            <div className="solution-container">
                <div className="solution-img-content img_contain" style={{backgroundImage: frontImage}}/>
            </div>
        </div>
    )
}

export default Component_G;