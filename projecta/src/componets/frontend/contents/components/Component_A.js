import React from 'react';
import { currentUsesDomain }  from 'constants/index'

const imgUrl = currentUsesDomain.imgUrl;

const Component_A = ({ datas }) => {
    const { pc, mobile } = datas;
    const backgroundImage = `url(${imgUrl + pc})`;
    const backgroundImageMobile = `url(${imgUrl + mobile})`;
    return (
        <div>
            {/* mobile-main-img */}
            <div className="contents-m-main-img img_basics" style={{backgroundImage: backgroundImageMobile}}>
                <h2 className="ir">스킨부스터</h2><p className="ir">지친 피부를 촉촉하고 화사하고 탄력있게</p>
            </div>
            {/* web-main-img */}
            <div className="contents-main-img img_basics" style={{backgroundImage: backgroundImage}}>
                {/* this is for html parse */}
                <h2 className="ir">스킨부스터</h2><p className="ir">지친 피부를 촉촉하고 화사하고 탄력있게</p>
            </div>
           
        </div>
    )
}

export default Component_A;