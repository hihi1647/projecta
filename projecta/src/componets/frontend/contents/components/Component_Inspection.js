import React, { Fragment } from 'react';
export default function  Component_Inspection () {
    return (
        <Fragment>
            {/* 페이지 점검 */}
            <div className="board-top-style">
                <ul className="board-title-box">
                    <li className="board-title">페이지 점검</li>
                    <li className="board-title-box-line"></li>
                    <li className="board-text">MIBALANCE</li>
                </ul>
                </div>
                <div className="border-container">
                <div className="board-list">
                    <div style={{borderTop: '3px solid #594544'}}/>
                    <div className="condition-box">
                    <span className="condition-text">
                        페이지 준비중입니다.
                    </span>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}