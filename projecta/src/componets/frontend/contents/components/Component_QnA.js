import React, { Fragment } from 'react';
import parse from 'html-react-parser';
import { currentUsesDomain }  from 'constants/index'
import Image02 from 'assets/images/qna-arrow.png';

const imgUrl = currentUsesDomain.imgUrl;
// 이미지

export default function Component_QnA ({ datas, num }) {
    const { background_image, list, title } = datas;
    const backgroundImage = `url(${imgUrl + background_image})`
    return (
        <Fragment>
            {/* 시술 Q&A */}
        <div className="qna-background img_basics" style={background_image && { backgroundImage }}>
          <div className="contents-container">
            <div className="container-wrap">
              <p className="contents-number">{ num }</p>
                <h3 className="contents-title">{ typeof title === 'string' ? parse(title) : title }</h3>
                  <ul className="qna-card">
                      {
                          list && list.length > 0 && list.map((cur, ind) => {
                              const count = ind + 1;
                              return (
                                <li className="qna-card-list" key={ind + cur.question}>
                                  <div className="q-desc-wrap">
                                    <p className="qna-num">{ 'Q' + count }</p>
                                    <p className="qna-title">{ cur.question }</p>
                                  </div>
                                  <div className="a-desc-wrap">
                                    <div>
                                      <figure className="qna-icon-img" style={{backgroundImage: `url(${Image02})`}}>
                                          <figcaption className="ir">댓글 아이콘</figcaption>
                                      </figure>
                                    </div>
                                    
                                    <p className="qna-text">
                                      { cur.answer }
                                    </p>
                                  </div>
                                </li>
                              )
                          })
                      }
                  </ul>
                </div>
              </div>
        </div>
        </Fragment>
    )
}