import React, { Fragment } from 'react';
import parse from 'html-react-parser';
import { currentUsesDomain }  from 'constants/index'
import { addZeroStringIfLessThan10 } from 'common/Utils';

import Image02 from 'assets/images/precautions.png';

const imgUrl = currentUsesDomain.imgUrl;

export default function Component_Precautions ({ datas, num }) {
    const { background_color, background_image, list, text_color, title } = datas;
    const backgroundImage = `url(${imgUrl + background_image})`
    return (
        <Fragment>
            {/* 유의사항 */}
            <div className="img_basics" style={background_image && {backgroundImage}} >
          <div className="contents-container">
            <div className="container-wrap">
                  <div className="precautions-card">
                    <div className="precautions-desc background-08A572" style={background_color && {backgroundColor: background_color}}>
                      <figure className="precautions-icon-img img_contain" style={{backgroundImage: `url(${Image02})`}}>
                          <figcaption className="ir">유의사항</figcaption>
                      </figure>
                      <p className="precautions-title">{ typeof title === 'string' ? parse(title) : title }</p>
                    </div>

                    <ul className="precautions-desc-wrap color-08A572" style={text_color && {color: text_color}}>
                        {
                            list && list.length > 0 && list.map((cur, ind) => {
                                let count = ind + 1;
                                count = addZeroStringIfLessThan10(count);
                                return (
                                  <li className="precautions-desc-list" key={ind}>
                                    <p className="precautions-num">{ count }</p>
                                    <div>
                                      <p className="precautions-ponit-text"><strong>{ cur.title }</strong></p>
                                      <p className="precautions-text">{ cur.sub_title }</p>
                                    </div>
                                  </li>
                                )
                            })
                        }
                    </ul>
                  </div>
                </div>
              </div>
        </div>
        </Fragment>
    )
}