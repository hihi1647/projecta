import React, { Fragment } from 'react';
import { addZeroStringIfLessThan10 } from 'common/Utils';
import parse from 'html-react-parser';

export default function Component_SquareNumber ({ datas, num }) {
    const { list, title } = datas;
    return (
        <Fragment>
            {/* 숫자 네모카드 */}
            <div className="contents-container">
            <div className="container-wrap">
                <div className="introduction-content">
                  <h3 className="contents-title">{ typeof title === 'string' ? parse(title) : title }</h3>
             
                  <ul className="squareNumber-card">
                      {
                          list && list.length > 0 && list.map((cur, ind) => {
                              const color = cur.background_color;
                              let count = ind + 1;
                              count = addZeroStringIfLessThan10(count);
                              const curDetails = cur.details;
                              return (
                                <li className="squareNumber-card-list border-CC6944" key={cur.title + ind} style={{borderColor: color}}>
                                <span className="round-num color-background-CC6944" style={{backgroundColor: color}}>{ count }</span>
                                <div className="squareNumber-card-desc">
                                  <p className="squareNumber-card-sub-title">{ cur.title }</p>
                                  <p className="squareNumber-card-title">{ cur.sub_title } </p>
                                  <span className="squareNumber-list-CC6944"> 
                                  <span className="inline-border border-color-AD8200" style={{borderColor: color}}></span> 
                                    <p className="squareNumber-card-text">{ typeof curDetails === 'string' ? parse(curDetails) : curDetails }</p>
                                  </span>
                                </div>
                              </li>
                              )
                          })
                      }
                  </ul>
                </div>
            </div>
          </div>
        </Fragment>
    )
}