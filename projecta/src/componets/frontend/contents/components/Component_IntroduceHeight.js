import React, { Fragment } from 'react';
import parse from 'html-react-parser';
import { currentUsesDomain }  from 'constants/index'

const imgUrl = currentUsesDomain.imgUrl;

export default function Component_IntroduceHeight ({ datas, num }) {
    const { background_color, details, image, list, title } = datas;

    const backgroundImage = `url(${imgUrl + image})`
    return (
        <Fragment>
            {/* 소개 세로 형태 */}
            <div className="contents-container introduction-background">
            <div className="container-wrap">
                <div className="introduction-content">
                  <p className="contents-number">{ num }</p>
                  <h3 className="contents-title">{ typeof title === 'string' ? parse(title) : title }</h3>
                  <p className="contnets-desc">
                      { typeof details === 'string' ? parse(details) : details }
                  </p>
                  {
                    image && (
                      <figure className="introduction-card-height-img img_contain" style={image && { backgroundImage }} />
                    )
                  }
                  <ul className="introduction-card-height">
                      {
                          list && list.length > 0 && list.map((cur, ind) => {
                              const listImage = `url(${imgUrl + cur.image})`
                              const curDetails = cur.details;
                              return (
                                <li className="introduction-card-height-list" key={ind + cur.text_color}>
                                  <figure className="introduction-card-height-cardImg img_basics" style={{backgroundImage: listImage }} />
                                  <div className="introduction-card-height-desc">
                                    <p className="introduction-card-height-title i-color" style={{ color: cur.text_color }} >{ cur.title }</p>
                                    <p className="introduction-card-height-sub-title i-color" style={{ color: cur.text_color }} >{ cur.sub_title }</p>
                                    <span className="i-list">  
                                      <span className="inline-border border-color-AD8200" style={{ borderColor: cur.text_color }}></span>
                                      <p className="introduction-card-height-text">{ typeof curDetails === 'string' ? parse(curDetails) : curDetails }</p>
                                    </span>
                                    </div>
                                </li>
                              )
                          })
                      }
                  </ul>
                </div>
            </div>
          </div>
        </Fragment>
    )
}