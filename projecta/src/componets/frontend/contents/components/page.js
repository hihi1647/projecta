const comp = [
    'Component_A',
    'Component_B',
    'Component_C',
    'Component_D',
    'Component_E',
    'Component_F',
    'Component_G',
    'Component_ImgOneWithNum',
    'Component_ImgOneTitle',
    'Component_Inspection',
    'Component_IntroduceHeight',
    'Component_Like',
    'Component_NumOneImg',
    'Component_Precautions',
    'Component_QnA',
    'Component_SquareNumber',
    'Component_Tab',
    'Component_Tab01',
    'Component_Tab02',
    'Component_TreatmentArea',
    'Component_TreatmentAreaOne',
    'Component_TreatmentAreaTwo',
];


export { default as Component_A } from './Component_A';
export { default as Component_B } from './Component_B';
export { default as Component_C } from './Component_C';
export { default as Component_D } from './Component_D';
export { default as Component_E } from './Component_E';
export { default as Component_F } from './Component_F';
export { default as Component_G } from './Component_G';
export { default as Component_ImgOneWithNum } from './Component_ImgOneWithNum';
export { default as Component_ImgOneTitle } from './Component_ImgOneTitle.';
export { default as Component_Inspection } from './Component_Inspection';
export { default as Component_IntroduceHeight } from './Component_IntroduceHeight';
export { default as Component_Like } from './Component_Like';
export { default as Component_NumOneImg } from './Component_NumOneImg';
export { default as Component_Precautions } from './Component_Precautions';
export { default as Component_QnA } from './Component_QnA';
export { default as Component_SquareNumber } from './Component_SquareNumber';
export { default as Component_Tab } from './Component_Tab';
export { default as Component_Tab01 } from './Component_Tab01';
export { default as Component_Tab02 } from './Component_Tab02';
export { default as Component_TreatmentArea } from './Component_TreatmentArea';
export { default as Component_TreatmentAreaOne } from './Component_TreatmentAreaOne';
export { default as Component_TreatmentAreaTwo } from './Component_TreatmentAreaTwo';