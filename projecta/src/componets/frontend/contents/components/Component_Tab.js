import React, { Fragment } from 'react';
import { Tabs } from 'element-react';

export default function Component_Tab () {
    const tabs = [
        {
            label: '바디 보톡스',
            comp: '바디 보톡스',
            name: '1'
        },
        {
            label: '바디 스킨보톡스',
            comp: '바디 스킨보톡스',
            name: '2'
        },
        {
            label: '다한증 보톡스',
            comp: '다한증 보톡스',
            name: '3'
        }
    ];
    return (
        <Fragment>
            <div className="contents-container color-background-E1CBA9">
                <div className="container-wrap">
                    {/* 여기에 tab 작업 해주세요 :) */}
                    <Tabs type="border-card" activeName="1">
                        {
                            tabs.map((tab,ind) => {
                                return (
                                    <Tabs.Pane label={tab.label} key={ind} name={tab.name}>
                                        {tab.comp}
                                    </Tabs.Pane>
                                )
                            })
                        }
                    </Tabs>
                </div>
            </div>
        </Fragment>
    )
}