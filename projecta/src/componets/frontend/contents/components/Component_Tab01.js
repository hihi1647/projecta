import React, { Fragment } from 'react';
import { Tabs } from 'element-react';


import img1 from 'assets/images/a-09.png';
import img2 from 'assets/images/a-10.png';
import img3 from 'assets/images/a-11.png';
import img4 from 'assets/images/a-12.png';
import img5 from 'assets/images/a-05.png';
import img6 from 'assets/images/a-06.png';
import img7 from 'assets/images/a-07.png';
import img8 from 'assets/images/a-08.png';
import img9 from 'assets/images/a-13.png';


export default function Component_Tab01 () {
    return (
        <Fragment>
            <div className="contents-container tab-container">
                <div className="container-wrap">
                    {/* 여기에 tab 작업 해주세요 :) */}
                    <Tabs type="border-card" activeName="1">
                         {/* Tabs.Pane이 각 탭 아이템, label은 탭 이름, name(중요)은 각 탭 아이디- 순차적으로 1,2,이렇게 :D */}
                        <Tabs.Pane label='바디 보톡스' name="1">
                            <p className="contents-number">02</p>
                            <h3 className="contents-title"><strong>바디 보톡스</strong> 소개</h3>
                            <p className="contnets-desc">
                                바디 보톡스는 보툴리눔 독소를 근육에 주사하는 시술입니다.<br/> 
                                보톡스의 주성분인 보튤리늄 성분은 근육의 움직임을 조절하는 신경 자극 물질을 막아<br/> 
                                울퉁불퉁 뭉쳐있는 근육을 부드럽게 풀어주고 과도하게 발달된 알, 근육을 축소시켜 줍니다.<br/>  
                                운동으로도 빠지지 않았던 알, 근육을 슬림하게 정리해줍니다.<br/> 
                            </p>
                        
                            <div className="effect-content">
                                <h3 className="contents-title"><strong>물방울 리프팅</strong> 효과</h3>
                                <ul className="effect-card"> 
                                    <li className="effect-card-item">
                                        <figure className="effect-card-img img_basics" style={{ backgroundImage:`url(${img1})` }}></figure>
                                        <p className="effect-card-text">
                                        승모근
                                        </p>
                                    </li>
                                    <li className="effect-card-item" >
                                        <figure className="effect-card-img img_basics" style={{ backgroundImage:`url(${img2})` }}></figure>
                                        <p className="effect-card-text">
                                        삼각근(팔뚝)
                                        </p>
                                    </li>
                                    <li className="effect-card-item" >
                                        <figure className="effect-card-img img_basics" style={{ backgroundImage:`url(${img3})` }}></figure>
                                        <p className="effect-card-text">
                                        삼두근
                                        </p>
                                    </li>
                                    <li className="effect-card-item" >
                                        <figure className="effect-card-img img_basics" style={{ backgroundImage:`url(${img4})` }} ></figure>
                                        <p className="effect-card-text">
                                        허벅지
                                        </p>
                                    </li>
                                    <li className="effect-card-item" >
                                        <figure className="effect-card-img img_basics" style={{ backgroundImage:`url(${img9})` }} ></figure>
                                        <p className="effect-card-text">
                                        종아리
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </Tabs.Pane>
                        <Tabs.Pane label='바디 스킨보톡스' name="2">
                        <p className="contents-number">02</p>
                            <h3 className="contents-title"><strong>바디 스킨 보톡스</strong> 소개</h3>
                            <p className="contnets-desc">
                            보톡스의 보툴리눔 독신를 비타민, 미네랄 등을 믹스한 용액을 피부 진피층과<br/> 
                            피하지방층에 소량씩 나누어 주사하는 시술합니다. 피부 탄력을 끌어 올려 리프팅해 주고<br/> 
                            미세 주름을 개선해 탱글탱글한 피부로 만들어 줍니다.<br/>  
                            </p>
                        
                            <div className="effect-content">
                                <h3 className="contents-title"><strong>바디 스킨 보톡스</strong> 시술부위</h3>
                                <ul className="effect-card"> 
                                    <li className="effect-card-item">
                                        <figure className="effect-card-img img_basics" style={{ backgroundImage:`url(${img5})` }}></figure>
                                        <p className="effect-card-text">
                                        전신
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </Tabs.Pane>

                        <Tabs.Pane label='다한증 보톡스' name="3">
                        <p className="contents-number">02</p>
                            <h3 className="contents-title"><strong>다한증 보톡스</strong> 소개</h3>
                            <p className="contnets-desc">
                            다한증이란 열이나 감정적인 자극에 반응하여 비정상적으로 많은 양의<br/> 
                            땀을 흘리는 질환으로 겨드랑이, 손바닥, 발바닥에 국소적으로 나타납니다.<br/> 
                            땀이 발생하는 부위에 보톡스 보툴리눔 독소를  10-30 포인트씩 얕게 주사하면<br/>  
                            땀과 땀 냄새의 원인이 되는 아세틸콜린의 분비가 억제되어 뽀송뽀송하고 쾌적함을 느낄 수 있습니다.<br/>  
                            </p>
                        
                            <div className="effect-content">
                                <h3 className="contents-title"><strong>다한증 보톡스</strong> 시술부위</h3>
                                <ul className="effect-card"> 
                                    <li className="effect-card-item">
                                        <figure className="effect-card-img img_basics" style={{ backgroundImage:`url(${img6})` }}></figure>
                                        <p className="effect-card-text">
                                        겨드랑이
                                        </p>
                                    </li>
                                    <li className="effect-card-item">
                                        <figure className="effect-card-img img_basics" style={{ backgroundImage:`url(${img7})` }}></figure>
                                        <p className="effect-card-text">
                                        손바닥
                                        </p>
                                    </li>
                                    <li className="effect-card-item">
                                        <figure className="effect-card-img img_basics" style={{ backgroundImage:`url(${img8})` }}></figure>
                                        <p className="effect-card-text">
                                        발바닥
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </Tabs.Pane>
                    </Tabs>
                </div>
            </div>
        </Fragment>
    )
}