import React, { Fragment } from 'react';
import parse from 'html-react-parser';
import { currentUsesDomain }  from 'constants/index'

const imgUrl = currentUsesDomain.imgUrl;

export default function Component_TreatmentArea ({ datas, num }) {
    const { background_color, list, title } = datas;
    return (
        <Fragment>
            {/* 시술 부위 이미지 2개 */}
            <div className="contents-container color-background-DEEDF2" style={background_color ? {backgroundColor: background_color} : {}}>
                <div className="container-wrap">
                <h3 className="contents-title">{ typeof title === 'string' ? parse(title) : title }</h3>
                    <ul className="treatmentAreaTwo-card">
                        {
                            list && list.length > 0 && list.map((cur, ind) => {
                                const img = `url(${imgUrl + cur.image})`;
                                const imgClassName = list.length > 1 ? "treatmentAreaTwo-img" : "treatmentAreaOne-img";

                                const imgClassName2 = list.length > 1 ? "treatmentAreaTwo-card-list" : "treatmentAreaOne-card-list";
                                return (
                                    // <li className={imgClassName} key={ind}>
                                    //     <figure className="treatmentAreaOne-img img_contain" style={{backgroundImage: img}}>
                                    //         <figcaption className="ir">face-옆광대,앞광대,코,볼살,v라인,이중턱</figcaption>
                                    //     </figure>
                                    // </li>

                                    <li className={imgClassName2} key={ind}>
                                        <figure className={imgClassName + " img_contain"} style={{backgroundImage: img}}>
                                            <figcaption className="ir">face-옆광대,앞광대,코,볼살,v라인,이중턱</figcaption>
                                        </figure>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
          </div>
        </Fragment>
    )
}