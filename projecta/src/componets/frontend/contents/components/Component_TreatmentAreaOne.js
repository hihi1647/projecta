import React, { Fragment } from 'react';

// 시술부위 사진1개
import Image01 from 'assets/images/treatmentArea-one-01.jpg';
export default function Component_TreatmentAreaOne () {
    return (
        <Fragment>
            {/* 시술 부위 이미지 1개 */}
            <div className="contents-container color-background-E1CBA9">
                <div className="container-wrap">
                    <h3 className="contents-title">실리프팅 시술부위</h3>
                        <ul className="treatmentAreaOne-card">
                            <li className="treatmentAreaOne-card-list">
                            <figure className="treatmentAreaOne-img img_contain" style={{backgroundImage: `url(${Image01})`}}>
                                <figcaption className="ir">face-눈썹 : 코그, 상안감 : 캐번, 눈밑 : 캐번, 팔자 : 회오리. 캐번, 코그,인디언밴드 (눈아래 앞광대주름) : 캐번,볼 : 캐번,심부볼 : 회오리. 코그, ,인중 : 캐번, 코그,마리오네트 : 회오리. 캐번,턱끝 : 코그,이중턱 : 회오리. 코그</figcaption>
                            </figure>
                            </li>
                        </ul>
                </div>
            </div>
        </Fragment>
    )
}