import React from 'react';
import parse from 'html-react-parser';
import { currentUsesDomain }  from 'constants/index'

const imgUrl = currentUsesDomain.imgUrl;

const Component_C = ({ datas }) => {
    const { list, title } = datas;
    return (
        <div className="contents-container">
            <div className="container-wrap">

                <div className="effect-content">
                    {/* this is for html parse */}
                    {/* <h3 className="contents-title"><strong>스킨 부스터</strong> 효과</h3> */}
                    <h3 className="contents-title">{typeof title === 'string' ? parse(title) : title }</h3>
                    <ul className="effect-card">
                        {
                            list && list.length > 0 && list.map((cur, ind) => {
                                const img = `url(${imgUrl + cur.image})`;

                                return (
                                    <li className="effect-card-item" key={cur.details + ind}>
                                        <figure className="effect-card-img img_basics" style={{backgroundImage: img}}></figure>
                                        <p 
                                            className="effect-card-text" 
                                            style={
                                                (cur.text_color || cur.text_background_color) ? 
                                                {color: cur.text_color, backgroundColor: cur.text_background_color} : {}
                                            }>
                                                {cur.details}
                                        </p>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Component_C;