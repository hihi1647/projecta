import React from 'react';
import parse from 'html-react-parser';
import { currentUsesDomain }  from 'constants/index'

const imgUrl = currentUsesDomain.imgUrl;

const Component_B = ({ datas, num }) => {
    const { background_image, details_1, details_2, title } = datas;
    const backgroundImage = `url(${imgUrl + background_image})`;
    return (
        <div className="summary-img img_basics" style={{backgroundImage: backgroundImage}}>
            <div className="contents-container">
                <div className="container-wrap">

                    <ul className="summary-content">
                        <li><p className="contents-number">{num}</p></li>
                        <li><h2 className="summary-title">{title}</h2></li>
                        {/* this is for html parse */}
                        {/* <li><span><p className="summary-text">피부를 건강하고 탄력 있게 해주는 성분을<br/>피부 안쪽에 직접적으로 주사하는 시술</p></span></li>
                        <li><p className="summary-sub-text">피부가 민감하고 예민해 레이저 시술이 어려운 분들도 시술 받으실 수 있으며<br/>단 시간내 피부 속부터 시작되는 피부 변화를 확인할 수 있습니다.</p></li> */}
                        <li><span><p className="summary-text">{ typeof details_1 === 'string' ? parse(details_1) : details_1 }</p></span></li>
                        <li><p className="summary-sub-text">{ typeof details_2 === 'string' ? parse(details_2) : details_2 }</p></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Component_B;