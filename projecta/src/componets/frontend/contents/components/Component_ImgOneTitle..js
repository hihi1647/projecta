import React, { Fragment } from 'react';
import parse from 'html-react-parser';
import { currentUsesDomain }  from 'constants/index'

const imgUrl = currentUsesDomain.imgUrl;

export default function Component_ImgOneTitle({ datas, num }) {
    const { background_color, image, title } = datas;
    const backgroundImage = `url(${imgUrl + image})`
    return (
        <Fragment>
           <div className="contents-container color-background-F2EBDA" style={background_color && {backgroundColor: background_color}}>
                <div className="container-wrap">
                  <p className="contents-number">{ num }</p>
                  <h3 className="contents-title">{ typeof title === 'string' ? parse(title) : title }</h3>
                    <figure className="imgOneTitle-img img_contain" style={image && { backgroundImage }}>
                    <figcaption className="ir">
                        실리프팅 시술부위
                    </figcaption>
                    </figure> 
                </div>
              </div>
        </Fragment>
    )
}