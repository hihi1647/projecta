import React from 'react';
import parse from 'html-react-parser';
import { currentUsesDomain }  from 'constants/index'

const imgUrl = currentUsesDomain.imgUrl;

const Component_E = ({ datas, num }) => {
    const { title, details, list } = datas;
    return (
        <div className="contents-container">
            <div className="container-wrap">

                <div className="information-content">
                    <p className="contents-number">{num}</p>
                    {/* this is for html parse */}
                    {/* <h3 className="contents-title"><strong>스킨 부스터</strong> 시술정보</h3> */}
                    <h3 className="contents-title">{ typeof title === 'string' ? parse(title) : title }</h3>
                    <p className="contnets-desc">{ typeof details === 'string' ? parse(details) : details }</p>
                    <ul className="information-card">
                        {
                            list && list.length > 0 &&  list.map((cur, ind) => {
                                const svg = `url(${imgUrl + cur.image})`;
                                return (
                                    <li className="information-card-item" key={cur.title + ind}>
                                        <div>
                                            <figure className="information-card-icon img_contain" style={{backgroundImage: svg}}/>
                                            <p className="information-card-title">{cur.title}</p>
                                            <p className="information-card-text">{cur.details}</p>
                                        </div>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Component_E;