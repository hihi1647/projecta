import React from 'react';
import parse from 'html-react-parser';
import { currentUsesDomain }  from 'constants/index'
import { addZeroStringIfLessThan10 } from 'common/Utils';

const imgUrl = currentUsesDomain.imgUrl;

const Component_F = ({ datas, num }) => {
    const { image, list, title } = datas;
    const backgroundImage = `url(${imgUrl + image})`;
    return (
        <div style={{backgroundColor:'#EBD2D2', position:'relative'}}>
            <div className="special-img img_basics" style={{backgroundImage: backgroundImage}}/>
            <div className="contents-container">
                <div className="container-wrap">
                    <div className="special-content">

                        <p className="contents-number">{num}</p>
                        {/* this is for html parse */}
                        {/* <h3 className="contents-title">미밸런스 <strong>스킨 부스터</strong>의 <strong>특별함</strong></h3> */}
                        <h3 className="contents-title">{ typeof title === 'string' ? parse(title) : title }</h3>
                        <ul className="special-card">
                            {
                                list && list.length > 0 &&  list.map((cur, ind) => {
                                    let count = ind + 1;
                                    count = addZeroStringIfLessThan10(count);

                                    return (
                                        <li className="special-card-item" key={cur.title + ind}>
                                            <p className="special-card-number">{count}</p>
                                            <p className="special-card-title">{cur.title}</p>
                                            <p className="special-card-text">{cur.details}</p>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Component_F;