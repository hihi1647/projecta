import React, { Fragment } from 'react';

// 좋아요 이미지
import Image01 from 'assets/images/table-01.jpg';

export default function Component_NumOneImg () {
    return (
        <Fragment>
            {/* 실리프팅 시술부위 - 02번 테이블이미지*/}
            <div className="contents-container introduction-background">
                <div className="container-wrap">
                    <p className="contents-number">02</p>
                    <h3 className="contents-title">실리프팅 시술부위</h3>
                    <p className="contnets-desc">
                    미밸런스는 표정에 따른 얼굴 근육의 움직임과 얼굴 전체의 균형과 대칭을 고려한 자연스러운 리프팅을 추구합니다. 
                    10년 이상의 경력을 가진 의료진이 고민 부위가 어디인지 처짐과 꺼짐의 정도는 어느 정도인지 꼼꼼하게 살핀 뒤 
                    알맞은 실 종류를 꼭 필요한 만큼만 사용하여 자연스럽게 당신의 나이를 되돌립니다.
                    </p>
                        <ul className="treatmentAreaOne-card">
                            <li className="treatmentAreaOne-card-list">
                            <figure className="treatmentAreaOne-img img_contain" style={{backgroundImage: `url(${Image01})`}}>
                                <figcaption className="ir">
                                    1. 코그 리프팅 1)실모양: 가시돌기모양 2) 타입: 녹는실, 3) 특징: 강한 리프팅, 잔주름 개선,콜라겐 생성, 탄력향상 4)추천시술부위:눈썹, 턱끝, 인중, 이중턱
                                    2. 회오리 리프팅 1)실모양: 나선형 모양 2) 타입: 녹는실, 3) 특징: 자연스럽고 강한 리프팅, 우수한 탄성력으로 자연스러운 표정변화, 콜라겐 생성, 탄력 향상, 적은 이물감 4)추천시술부위:처진 심부볼, 마리오네트
                                    3. 캐번 리프팅 1)실모양: 빈틈없는 스프링 모양 2) 타입: 녹는실, 3) 특징: 빈틈 없이 강력한 리프팅, 파워풀한 볼륨감, 얼굴 주름개선 탁월, 콜라겐 생성률 6배 UP!, 2년 이상의 유지기간 4)추천시술부위: 윤곽 리프팅 (볼살,이중턱,턱선) 볼륨이 필요한 눈가, 팔자주름
                                    </figcaption>
                            </figure>
                            </li>
                        </ul>
                    </div>
            </div>

        </Fragment>
    )
}