import React, { Fragment } from 'react';
import parse from 'html-react-parser';
import { currentUsesDomain }  from 'constants/index'


// 이미지
import Image01 from 'assets/images/img-02.jpg';

const imgUrl = currentUsesDomain.imgUrl;

export default function Component_ImgOneWithNum ({ datas, num }) {
    const { image } = datas;
    const backgroundImage = `url(${imgUrl + image})`
    return (
        <Fragment>
            <div className="img_basics" style={{backgroundImage: `url(${Image01})`}}>
                <div className="contents-container">
                    <div className="container-wrap">
                        
                        <figure className="ImgOne-img img_contain" style={image && { backgroundImage }}>
                        <figcaption className="ir">
                            슈링크 리프팅의 검증된 기술력-식약처 허가를 취득함은 물론 유럽 의료기기인증 CE MDD 획득 및 브라질 ANVISA 인증 획득, JEADV에 임상을 게재하였습니다.
                        </figcaption>
                        </figure>
                            
                    </div>
                </div>
            </div>
        </Fragment>
    )
}