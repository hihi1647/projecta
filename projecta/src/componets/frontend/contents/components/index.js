import React from 'react';

import {
    Component_A,
    Component_B,
    Component_C,
    Component_D,
    Component_E,
    Component_F,
    Component_G,
    Component_ImgOneWithNum,
    Component_ImgOneTitle,
    Component_Inspection,
    Component_IntroduceHeight,
    Component_Like,
    Component_NumOneImg,
    Component_Precautions,
    Component_QnA,
    Component_SquareNumber,
    Component_Tab,
    Component_Tab01,
    Component_Tab02,
    Component_TreatmentArea,
    Component_TreatmentAreaOne,
    Component_TreatmentAreaTwo,
} from './page';

// import Component_A from './Component_A';
// import Component_B from './Component_B';
// import Component_C from './Component_C';
// import Component_D from './Component_D';
// import Component_E from './Component_E';
// import Component_F from './Component_F';
// import Component_G from './Component_G';

const DETAIL_COMPONENTs = {
    Component_A,
    Component_B,
    Component_C,
    Component_D,
    Component_E,
    Component_F,
    Component_G,
    Component_ImgOneWithNum,
    Component_ImgOneTitle,
    Component_Inspection,
    Component_IntroduceHeight,
    Component_Like,
    Component_NumOneImg,
    Component_Precautions,
    Component_QnA,
    Component_SquareNumber,
    Component_Tab,
    Component_Tab01,
    Component_Tab02,
    Component_TreatmentArea,
    Component_TreatmentAreaOne,
    Component_TreatmentAreaTwo
};

const MyComponent = ({ tag, datas, num }) => {
    const TagName = DETAIL_COMPONENTs[tag];
    if (typeof DETAIL_COMPONENTs[tag] !== 'undefined') {
        return <TagName datas={datas} num={num} />
    }

    /* const TagName = tag;
    if (typeof tag !== 'undefined') {
        return <TagName datas={datas} num={num} />
    }
 */
    return (
        <div>
            The component {tag} has not been created yet
        </div>
    )
}

export default MyComponent;