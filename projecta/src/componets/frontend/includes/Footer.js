import React from 'react';
import api from 'common/api';
import { Select, Message } from 'element-react';
import { Link } from 'react-router-dom';

const required = ['writer', 'phone', 'main_category_id', 'isAgreementChecked', 'details'];
const validationCheck = {
    writer: {
        regex: 'required',
        msg: '이름은 필수 입력항목입니다.',
        required: true
    },
    phone: {
        regex: /^\d{3}-\d{3,4}-\d{4}$/,
        msg: '휴대폰 번호는 필수 입력항목입니다. (000-0000-0000)',
        required: true
    },
    main_category_id: {
        regex: 'required',
        msg: '치료과목을 선택해 주세요.',
        required: true
    },
    isAgreementChecked: {
        regex: 'required',
        msg: '개인정보 수집에 동의해 주세요.',
        required: true
    },
    details: {
        regex: 'required',
        msg: '문의 내용은 필수입니다.',
        required: true
    }
}

class Frontend_Footer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            writer: '',
            phone: '',
            main_category_id: '',
            isAgreementChecked: false,
            details: ''
        }

        
    }

    handleChange = (e) => {

        const target = e.target;
        const name = target.name;

        if (name === 'isAgreementChecked') {
            this.setState({
                [name]: target.checked
            })

        } else {
            this.setState({
                [name]: target.value
            });
        }

    }

    makeExpenseGuide = async (e) => {
        e.preventDefault();
        // input validation form check
        let validationCheck = true;

        const validationCheckMsg = this.validationCheck();

        if (validationCheckMsg !== 'success') {
            Message({
                message: validationCheckMsg,
                type: 'error'
            });
            return ;
        }

        const { writer, phone, main_category_id, details} = this.state;

        if ( validationCheck ) {
            const payload = {
                writer,
                phone,
                main_category_id,
                details
            };
    
            console.log('this is payload', payload);
    
            await api.createExpenseGuide(payload)
                .then(res => {
                    if (res) {
                        Message({
                            message: '빠른 상담 신청이 완료되었습니다.',
                            type: 'success'
                        });
                    }
                    console.log('successfully created', res);
                })
                .then(() => {
                    this.clean();
                })
                .catch((err) => {
                    console.log('something went wrong..', err);
                })

        }
        
    }

    clean = () => {
        this.setState({
            writer: '',
            phone: '',
            main_category_id: '',
            isAgreementChecked: false,
            details: ''
        })
    }

    isRequiredFilled = () => {
        return required.length === required.filter(cur => this.state[cur]).length
    }

    validationCheck = () => {

        for (let i = 0; i < required.length; i += 1) {
            const current = required[i];
            const regex = validationCheck[current].regex;
            let isValid = true;
            if (regex === 'required') {
                isValid = this.state[current];
            } else {
                isValid = regex.test(this.state[current]);
            }

            if (!isValid) {
                return validationCheck[current].msg;
            }
        }

        return 'success';
    }



  render(){
    let enters = "All\nI\nwant\nis\nenter";
    const {writer, isAgreementChecked, phone, details, main_category_id} = this.state;
    const expenseGuideCategories = this.props.expenseGuideCategories;

    console.log('this is footer', this.state, this.props);
    return (
          <footer className="footer">
            <h2 className="ir">footer</h2>
            <div className="footer-sns">
                <ul className="footer-sns-container">
                    <li className="sns-list sns-01">
                        <a href="http://pf.kakao.com/_jXxmxaxb" className="sns-btn" style={{backgroundColor:"#FAE300"}}>
                            <svg className="sns-icon" xmlns="http://www.w3.org/2000/svg" width="57.526" height="52.849" viewBox="0 0 57.526 52.849">
                                <g transform="translate(-489.567 -2534.852)">
                                    <path d="M83.861,77.391c-15.886,0-28.763,10.18-28.763,22.739,0,8.174,5.456,15.338,13.645,19.346-.6,2.246-2.178,8.138-2.495,9.4-.389,1.564.574,1.543,1.2,1.122.494-.329,7.882-5.351,11.069-7.519a36.424,36.424,0,0,0,5.339.392c15.885,0,28.763-10.182,28.763-22.74S99.746,77.391,83.861,77.391" transform="translate(434.469 2457.461)" fill="#391b1b"/>
                                    <g transform="translate(496.913 2550.692)">
                                    <path d="M114.242,185.048h-8.411a1.431,1.431,0,1,0,0,2.861h2.734v10.076a1.4,1.4,0,0,0,1.4,1.4h.306a1.4,1.4,0,0,0,1.393-1.4V187.909h2.583a1.431,1.431,0,1,0,0-2.861Z" transform="translate(-104.4 -184.846)" fill="#fae300"/>
                                    <path d="M266.047,195.538h-3.911V185.247a1.552,1.552,0,1,0-3.1,0V196.34a1.507,1.507,0,0,0,.024.248,1.325,1.325,0,0,0-.024.248,1.3,1.3,0,0,0,1.3,1.3h5.716a1.3,1.3,0,0,0,0-2.6Z" transform="translate(-235.992 -183.694)" fill="#fae300"/>
                                    <path d="M330.982,196.01l-4.359-5.725,3.938-3.94a1.366,1.366,0,1,0-1.932-1.931l-4.875,4.876v-4.042a1.552,1.552,0,1,0-3.1,0V196.68a1.552,1.552,0,1,0,3.1,0v-3.528l.886-.885,4.123,5.411a1.388,1.388,0,1,0,2.219-1.668Z" transform="translate(-288.427 -183.694)" fill="#fae300"/>
                                    <path  d="M179.912,196.382l-4.192-11.467-.014-.013a2.06,2.06,0,0,0-1.964-1.208,2,2,0,0,0-2.038,1.444,1.908,1.908,0,0,0-.08.189L167.5,196.382a1.39,1.39,0,1,0,2.623.922l.713-2.036h5.737l.713,2.036a1.39,1.39,0,1,0,2.623-.922Zm-8.163-3.713,1.928-5.5c.022,0,.041.006.062.006l1.926,5.5Z" transform="translate(-158.032 -183.694)" fill="#fae300"/>
                                    </g>
                                </g>
                                </svg>
                            <p className="sns-title">카카오톡 ID</p>
                            <p className="sns-text">미밸런스</p>
                        </a>
                    </li>
                    <li className="sns-list sns-02">
                        <a href="https://blog.naver.com/mibalance11" className="sns-btn font-color-white" style={{backgroundColor:"#3FA636"}}>
                            <svg className="sns-icon" xmlns="http://www.w3.org/2000/svg" width="59.896" height="55.556" viewBox="0 0 59.896 55.556">
                                <g transform="translate(-502.894 -174.752)">
                                  <path d="M530.975,223.2a2.062,2.062,0,1,0,2.063,2.062A2.062,2.062,0,0,0,530.975,223.2Z" transform="translate(-15.01 -27.951)" fill="#fff"/>
                                  <path d="M578.907,222.694a2.221,2.221,0,1,0,2.221,2.221A2.221,2.221,0,0,0,578.907,222.694Z" transform="translate(-42.57 -27.657)" fill="#fff"/>
                                  <path d="M554.778,174.752H510.907a8.013,8.013,0,0,0-8.013,8.013V208.8a8.012,8.012,0,0,0,8.013,8.013h15.5l5.27,12.417s.363,1.076,1.241,1.076h0c.879,0,1.241-1.076,1.241-1.076l5.27-12.417h15.353a8.013,8.013,0,0,0,8.013-8.013V182.765A8.013,8.013,0,0,0,554.778,174.752Zm-33.066,23.017c-.025,4.759-4.946,4.9-4.946,4.9a4.112,4.112,0,0,1-2.978-1.262v.858H510.2V187.48c-.014,0-.016-.005,0-.008v.008c.222.036,3.584-.008,3.584-.008v5.653c.843-1.437,3.28-1.414,3.28-1.414C522.375,192.206,521.712,197.769,521.712,197.769Zm6.309-5.6v10.12h-3.508V192.267c0-1.363-1.691-1.742-1.691-1.742v-3.533C528.425,187.219,528.021,192.166,528.021,192.166Zm8.316,10.505a5.432,5.432,0,1,1,5.88-5.414A5.662,5.662,0,0,1,536.337,202.671Zm19.218,0s.051,5.142-5.173,5.142h-1.59v-3.332h.959s2.2.2,2.17-3.18c0,0-.429,1.369-3.508,1.369,0,0-4.366-.315-4.366-4.575v-1.615a4.989,4.989,0,0,1,4.9-4.77A3.571,3.571,0,0,1,552,193.075v-1.085h3.559Z" transform="translate(0 0)" fill="#fff"/>
                                  <path d="M611.216,222.814a2.17,2.17,0,1,0,2.171,2.17A2.17,2.17,0,0,0,611.216,222.814Z" transform="translate(-61.238 -27.727)" fill="#fff"/>
                                </g>
                              </svg>
                            <p className="sns-title">네이버 블로그</p>
                            <p className="sns-text">미밸런스</p>
                        </a>
                    </li>
                    <li className="sns-list sns-03">
                        <a href="https://instagram.com/mibalance1?igshid=npoq186d9gdu" className="sns-btn" style={{backgroundColor:"#fff"}}>
                            <svg className="sns-icon" xmlns="http://www.w3.org/2000/svg" width="52.454" height="52.454" viewBox="0 0 52.454 52.454">
                                <defs>
                                  <linearGradient id="linear-gradient" x1="0.931" x2="0.09" y2="1" gradientUnits="objectBoundingBox">
                                    <stop offset="0" stopColor="#a702ff"/>
                                    <stop offset="0.673" stopColor="#ed1144"/>
                                    <stop offset="1" stopColor="#ffff10"/>
                                  </linearGradient>
                                </defs>
                                <g transform="translate(-104.541 -345.753)">
                                  <path d="M141.855,345.753H119.681a15.141,15.141,0,0,0-15.14,15.14v22.174a15.14,15.14,0,0,0,15.14,15.14h22.174A15.14,15.14,0,0,0,157,383.067V360.893A15.14,15.14,0,0,0,141.855,345.753Zm10.31,37.7a10.05,10.05,0,0,1-10.05,10.05H119.422a10.05,10.05,0,0,1-10.05-10.05V360.764a10.05,10.05,0,0,1,10.05-10.05h22.693a10.05,10.05,0,0,1,10.05,10.05Z" transform="translate(0 0)" fill="url(#linear-gradient)"/>
                                  <path d="M152.122,380.074a13.61,13.61,0,1,0,13.61,13.61A13.61,13.61,0,0,0,152.122,380.074Zm0,22.433a8.823,8.823,0,1,1,8.823-8.823A8.823,8.823,0,0,1,152.122,402.507Z" transform="translate(-21.354 -21.574)" fill="url(#linear-gradient)"/>
                                  <path d="M207.655,370.144a3.171,3.171,0,1,0,3.171,3.171A3.171,3.171,0,0,0,207.655,370.144Z" transform="translate(-62.823 -15.332)" fill="url(#linear-gradient)"/>
                                </g>
                              </svg>
                              
                            <p className="sns-title">인스타그램 ID</p>
                            <p className="sns-text">Mibalance</p>
                        </a>
                    </li>
                    <li className="sns-list sns-04">
                        <Link to={`/location`} className="sns-btn font-color-white" style={{backgroundColor:"#E38A88"}}>
                            <svg className="sns-icon" xmlns="http://www.w3.org/2000/svg" width="38.352" height="54.204" viewBox="0 0 38.352 54.204">
                                <path d="M282.575,247.965A19.177,19.177,0,0,0,263.4,267.141c0,10.591,19.689,35.029,19.689,35.029s18.663-24.438,18.663-35.029A19.174,19.174,0,0,0,282.575,247.965Zm0,28.382a9.333,9.333,0,1,1,9.333-9.334A9.333,9.333,0,0,1,282.575,276.347Z" transform="translate(-263.398 -247.965)" fill="#fff"/>
                              </svg>
                            <p className="sns-title">오시는 길</p>
                            <p className="sns-text">미밸런스</p>
                        </Link>
                    </li>
                </ul>
            </div>
            <div className="footer-counseling">
                <div className="footer-counseling-container">
                    <img  className="counseling-tel counseling-01" src={ require('../../../assets/images/footer-tel.png') } alt="미밸런스 비용상담신청 031-962-7781"/>
                    <img  className="m-counseling m-counseling-01" src={ require('../../../assets/images/m-tel.png') } alt="미밸런스 비용상담신청 031-962-7781"/>
                    <img  className="counseling-time counseling-03" src={ require('../../../assets/images/footer-time.png') } alt="진료시간 안내 월~금 오전 10시30분 부터 오후 8시30분까지 토요일 오전 10시30분 부터 오후 4시30분까지 일요일과 공휴일은 휴진일 입니다"/>
                    <img  className="m-counseling m-counseling-02" src={ require('../../../assets/images/m-counseling.png') } alt="미밸런스 비용문의"/>
                    <ul className="counseling-wrap counseling-02">
                        <li><input className="input-counseling form-input" value={writer} onChange={this.handleChange} type="text" name="writer" placeholder="* 이름"/></li>
                        <li><input className="input-counseling form-input" value={phone} onChange={this.handleChange} type="tel" name="phone" placeholder="* 휴대폰 번호를 입력해주세요 (000-0000-0000)" /></li> 
                        <li className="counseling-select-li">

                            {/* select 바꿔주세요 */}
                            <select className="counseling-select input-counseling form-input" value={main_category_id} id="counseling_select" name="main_category_id" onChange={this.handleChange}>
                                <option value="" disabled>* 치료과목을 선택해 주세요</option>
                                {
                                    (expenseGuideCategories && expenseGuideCategories.main_categories && expenseGuideCategories.main_categories.length > 0) ? expenseGuideCategories.main_categories.map(el => {
                                        return <option key={el.id} label={el.main_category_name} value={el.id} />
                                    }) : null
                                }
                            </select>
                            <span className="select-icon">
                                <i className="material-icons">expand_more</i>
                            </span>    
                        </li>
                        <li className="btn-couxsnseling-wrap">
                          
                                <input type="checkbox" id="cb1" checked={isAgreementChecked} name="isAgreementChecked" onChange={this.handleChange}/>
                                <label htmlFor="cb1"> 개인정보취급방침 동의</label> 
                                <Link to={`/condition/personal_information`} className="btn-click">자세히보기</Link>
                            
                            
                        </li>
                        <li><textarea className="form-input" name="details" placeholder="* 문의내용" value={details} onChange={this.handleChange} ></textarea></li> 
                        <li>
                            <a href="#" className="btn-counseling" onClick={(e) => this.makeExpenseGuide(e)}>
                                상담신청 확인
                                <svg className="icon-arrow" xmlns="http://www.w3.org/2000/svg" width="17.007" height="4.804" viewBox="0 0 17.007 4.804">
                                    <path className="path-arrow"data-name="path-arrow" d="M5216.949-13199.223h15.019l-4.759-3.6" transform="translate(-5216.449 13203.526)" fill="none" stroke="#3E2626" strokeLinecap="round" strokeWidth="1"/>
                                </svg>
                            </a>
                        </li> 
                    </ul>
                    
                </div>
            </div>
            <div className="footer-address">
                <div className="footer-address-container">
                    <ul className="footer-address-top footer-address-01">
                        <li><Link to={`/infor`}  className="sns-title">병원소개</Link></li>
                        <li><Link to={`/location`}  className="sns-title">오시는 길</Link></li>
                        <li><Link to={`/condition/personal_information`}>개인정보 취급방침</Link></li>
                        <li><Link to={`/condition/email_collection`}>이메일주소 무단수집 거부</Link></li>
                        <li><Link to={`/condition/provision`}>이용약관</Link></li>
                    </ul>
                    <ul className="footer-address-bottom footer-address-02">
                        <li><a href="#" className="footer-logo"></a></li>
                        <li>미밸런스의원<br/><span className="address">경기도 고양시 덕양구 권율대로 685, 501~505호 (원흥동, 에스아이비타워)<br/>상호명: 미밸런스의원   대표자: 윤정후, 이형진   사업자 등록번호: 735-74-00219<br/>COPYRIGHT© 2020 MI BALANCE ALL RIGHT RESERVED.</span></li>
                        <li className="btn-container"><a href="http://pf.kakao.com/_jXxmxaxb" className="footer-kakao"></a> </li>
                        <li className="btn-container"><a href="https://blog.naver.com/mibalance11" className="footer-blog"></a></li>
                        <li className="btn-container"><a href="https://instagram.com/mibalance1?igshid=npoq186d9gdu" className="footer-insta"></a></li>
                    </ul>
                </div>
            </div>
        </footer>
    );
  }
}

export default Frontend_Footer;