import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import myCookie from 'common/MyCookie';
import { isAuthenticated } from 'common/auth';
import { Menu } from 'element-react';
import parse from 'html-react-parser';
class Frontend_Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isNavMenuVisible: false
        }
    }

    componentDidMount() {
        this.setState({
            isNavMenuVisible: false
        })
    }

    componentDidUpdate(prevProps) {
        if (
          this.props.location.pathname !== prevProps.location.pathname
        ) {
          this.setState({
              isNavMenuVisible: false
          })
        }
    }

    toggleNavigationMenu = () => {
        this.setState(prev => ({
            isNavMenuVisible: !prev.isNavMenuVisible
        }))
    }


    signOut = () => {
        // need to be improve
        myCookie.clearUser();
        window.$user = null;

        const newState = {
            user: null,
            isAuthenticated: isAuthenticated()
        }

        this.props.changeState(newState);

        this.props.history.push('/');
    }

    render() {         
        const { 
            location, $user, 
            isAuthenticated, Headerlink,
        } = this.props;

        const { isNavMenuVisible } = this.state;
        const toggleNavigationMenu = this.toggleNavigationMenu

        console.log('this is from header', this.props, this.state.isNavMenuVisible);
        return (
            <Fragment>
            <header>
                <div className="header">
                    <Link to="/" className="header-logo-container">
                        <span className="logo"><h1 className="ir">미밸런스 의원</h1></span>
                    </Link>            
                    <div className="header--top"> 
                        <ul>
                            <li>
                                {
                                    isAuthenticated ? (
                                        <a className="header--menu-link" onClick={this.signOut}>로그아웃</a>
                                    ) : (
                                        <Link to={{ pathname: "/login", state: { from: location } }} className="header--menu-link">로그인</Link>
                                    )
                                }
                            </li>
                            <li>
                                {
                                    isAuthenticated ? (
                                        <Link to="/edit_profile">회원정보 수정</Link>
                                    ) : (
                                        <Link to="/register">회원가입</Link>
                                    )
                                }
                            </li>
                            {
                                ( $user && $user.isAdmin ) ? (
                                    <li><Link to="/backend">관리자 페이지</Link></li>
                                ): null
                            }
                        </ul>
                    </div>
                    <nav className="nav">
                        <h2 className="ir">네비게이션</h2>
                        <ul className="menu">
                            <li className="menu-item">
                                <a href="#" className="menu-link">미밸런스</a>
                                <ul className="menu-list">
                                    <li><Link to={`/infor`} className="menu-list-item">미밸런스 소개</Link></li>
                                    <li><Link to={`/doctor_infor`} className="menu-list-item">의료진 소개</Link></li>
                                    {/* <li><Link to={`/look_around`} className="menu-list-item">둘러보기</Link></li> */}
                                    <li><Link to={`/medical_treatment_guide`} className="menu-list-item">진료안내</Link></li>
                                    <li><Link to={`/location`} className="menu-list-item">오시는길</Link></li>
                                </ul>
                            </li>
                            {
                                Headerlink.map((main, ind) => (
                                    <Fragment>
                                        {
                                            (main.id === 5 || main.main_menu_order === 5) && (
                                                <li><Link to="/" className="none-box"/></li>
                                            )
                                        }
                                        <li className="menu-item" key={main.main_menu_name + ind} >
                                            <a href="#" className="menu-link">{main.main_menu_name}</a>
                                            <ul className="menu-list">
                                                {
                                                    main.sub_menus.map((sub, index) => (
                                                        <li key={index + sub.sub_menu_name}>
                                                            <Link to={sub.link} className="menu-list-item">
                                                                {
                                                                    (sub.id === 11 && typeof sub.sub_menu_name === 'string') ? 
                                                                        parse(sub.sub_menu_name) : sub.sub_menu_name 
                                                                }
                                                            </Link>
                                                        </li>
                                                    ))
                                                }
                                            </ul>
                                        </li>
                                    </Fragment>
                                    
                                ))
                            }
                            <li className="menu-item">
                                <a href="#" className="menu-link">상담/예약</a>
                                <ul className="menu-list">
                                    <li><Link to={`/consultings`} className="menu-list-item">온라인 상담</Link></li>
                                </ul>
                            </li>
                            <li className="menu-item">
                                <a href="#" className="menu-link">커뮤니티</a>
                                <ul className="menu-list">
                                    <li><Link to={`/before_afters`} className="menu-list-item">전후사진</Link></li>
                                    <li><Link to={`/notices`} className="menu-list-item">공지사항</Link></li>
                                </ul>
                            </li>
                        
                            <li className="menu-item btn-container">
                            <Link to={`/events`} className="event-link">
                                <p className="btn-event">
                                    <span className="icon-event"></span>
                                </p>
                            </Link>
                            </li>
                        </ul>
                    </nav>
                </div>
            {/*---- 모바일 네비게이션 부분 ----*/}
            {
                <div className="m-header">
                    <nav className="m-nav">
                        <Link to="/" className="m-logo"/>
                        <div className="m-nav-menu">
                            <div className="m-menu" onClick={toggleNavigationMenu}></div>
                            <Menu defaultActive="2" style={isNavMenuVisible ? {backgroundColor:'#CCBFB9', display: 'block'} : {backgroundColor:'#CCBFB9', display: 'none'}} uniqueOpened={true}>
                                <Menu.SubMenu index="1" className="sub-menu-title" title="미밸런스">
                                    <Menu.Item><Link to={`/infor`} className="menu-list-item">미밸런스 소개</Link></Menu.Item>
                                    <Menu.Item><Link to={`/doctor_infor`} className="menu-list-item">의료진 소개</Link></Menu.Item>
                                    <Menu.Item><Link to={`/look_around`} className="menu-list-item">둘러보기</Link></Menu.Item>
                                    <Menu.Item><Link to={`/medical_treatment_guide`} className="menu-list-item">진료안내</Link></Menu.Item>
                                    <Menu.Item><Link to={`/location`} className="menu-list-item">오시는길</Link></Menu.Item>
                                </Menu.SubMenu>
                                {/* 서버로부터 받는 데이터*/}
                                {
                                    Headerlink.map((main, ind) => (
                                        <Menu.SubMenu key={main.main_menu_name + ind} index={ind + 2} className="sub-menu-title" title={main.main_menu_name}>
                                            {
                                                main.sub_menus.map((sub, index) => (
                                                    <Menu.Item key={index + sub.sub_menu_name}>
                                                        <Link to={sub.link} className="menu-list-item">{sub.sub_menu_name}</Link>
                                                    </Menu.Item>
                                                ))
                                            }
                                        </Menu.SubMenu>
                                    ))
                                }
                                {/* 따로 관리하는 내비게이션 */}

                                <Menu.SubMenu index={Headerlink && Headerlink.length + 2} className="sub-menu-title" title="상담/예약">
                                    <Menu.Item><Link to={`/consultings`} className="menu-list-item">온라인 상담</Link></Menu.Item>
                                </Menu.SubMenu>
                                <Menu.SubMenu index={Headerlink && Headerlink.length + 3} className="sub-menu-title" title="커뮤니티">
                                    <Menu.Item><Link to={`/before_afters`} className="menu-list-item">전후사진</Link></Menu.Item>
                                    <Menu.Item><Link to={`/notices`} className="menu-list-item">공지사항</Link></Menu.Item>
                                </Menu.SubMenu>
                                <Menu.Item index={Headerlink && Headerlink.length + 4}><Link to={`/events`} className="menu-list-item">이벤트</Link></Menu.Item>
                            </Menu>
                        </div>
                    </nav>
                </div>
            }
        </header>
        </Fragment>
            );
        }
    }

export default Frontend_Header;