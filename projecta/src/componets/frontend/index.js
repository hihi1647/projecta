import React, { Component, Fragment } from 'react';
import { Route, Switch } from 'react-router-dom';
import QuickBar from 'widget/quickBar/Quick_Bar';

import Main from './main/Main'
import { 
  Frontend_Header,
  Frontend_Footer,
  Auth_Register,
  Auth_Login,
  Edit_Profile,
  Forgotten_Password,
  Reset_Password,
  Dynamic_Component,
  Frontend_RouterError
} from '../../pages';

import BeforeAfter_Container from './before_afters/BeforeAfter_Container';
import Consulting_Container from './consultings/Consulting_Container';
import Event_Container from './events/Event_Container';
import Notice_Container from './notices/Notice_Container'

import Infor from './intro/Infor';
import Doctor_infor from './intro/Doctor_infor';
import Look_around from './intro/Look_around';
import Medical_treatment_guide from './intro/Medical_treatment_guide';
import Location from './intro/Location';

import Email_collection from './condition/Email_collection';
import Personal_information from './condition/Personal_information';
import Provision from './condition/Provision';

import api from 'common/api';

const ROUTES = [
  {
    label: '/before_afters',
    children: BeforeAfter_Container
  },
  {
    label: '/consultings',
    children: Consulting_Container
  },
  {
    label: '/events',
    children: Event_Container
  },
  {
    label: '/notices',
    children: Notice_Container
  },
  {
    label: '/login',
    children: Auth_Login
  },
  {
    label: '/register',
    children: Auth_Register
  },
  {
    label: '/edit_profile',
    children: Edit_Profile
  },
  {
    label: '/forgotten_password',
    children: Forgotten_Password
  },
  {
    label: '/reset_password/:id',
    children: Reset_Password
  },
  {
    label: '/infor',
    children: Infor
  },
  {
    label: '/doctor_infor',
    children: Doctor_infor
  },
  {
    label: '/look_around',
    children: Look_around
  },
  {
    label: '/medical_treatment_guide',
    children: Medical_treatment_guide
  },
  {
    label: '/location',
    children: Location
  },
  {
    label: '/condition/email_collection',
    children: Email_collection
  },
  {
    label: '/condition/personal_information',
    children: Personal_information
  },
  {
    label: '/condition/provision',
    children: Provision
  }
];

class Frontend extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLowerThanHeader: false,
        expenseGuideCategories: {},
        headerLinksFromServer: [],
        headerLinksFromServer_only_subLinks: [],
    }
    // this.listenToScroll();
  }
  
  componentDidMount() {
      this.getDatas();

      window.addEventListener('scroll', this.listenToScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.listenToScroll);
  }

  listenToScroll = () => {
    const winScroll = document.body.scrollTop || document.documentElement.scrollTop

    const isLowerThanHeader = winScroll > 200;

    const beforeIsLowerThanHeader = this.state.isLowerThanHeader;

    if (beforeIsLowerThanHeader !== isLowerThanHeader) {
      this.setState({
        isLowerThanHeader
      })
    }
  }

  getDatas = async () => {
      await api.getExpenseGuideCategories(true)
          .then(res => {
              this.setState({
                expenseGuideCategories: res.data.data
              })
          })
          .catch(error => {
            console.log('something went wrong with expense guide category', error);
          });

      await api.getFrontendLinks()
          .then(res => {

            const resData = res.data.data;
            if (resData) {

              let headerLinksFromServer_only_subLinks = [];

              let headerLinksFromServer = resData.map(main => {
                let sub_menus = main.sub_menus;
                let new_sub_menus = sub_menus;
                if (sub_menus && sub_menus.length > 0) {
                  new_sub_menus = sub_menus.map(sub => {
                    return {
                      ...sub,
                      link: '/' + main.main_menu_url + '/' + sub.sub_menu_url
                    }
                  })
                }

                headerLinksFromServer_only_subLinks = [
                  ...headerLinksFromServer_only_subLinks,
                  ...new_sub_menus
                ]

                return {
                  ...main,
                  sub_menus: new_sub_menus
                }
              })

              this.setState({
                headerLinksFromServer,
                headerLinksFromServer_only_subLinks
              })
            }
          })
          .catch(error => {
            console.log('something went wrong with event category', error);
          });
  }

  render() {
    const path = this.props.match.path;
    const { $user, isAuthenticated, changeState } = this.props;
    const parentProps = {
      $user,
      isAuthenticated,
      changeState
    };
    const {
      isLowerThanHeader, 
      expenseGuideCategories,
      headerLinksFromServer,
      headerLinksFromServer_only_subLinks,
    } = this.state;

    console.log('this is frontend', this.state, this.props, ROUTES);
      return (
        <Fragment>
          <QuickBar isLowerThanHeader={isLowerThanHeader} />
          <Frontend_Header {...this.props} Headerlink={headerLinksFromServer}/>

          <Switch>

            <Route path={path} exact render={props => <Main {...props} {...parentProps} />} />
            {/* Consultings - answer - nurse, admin인지 확인, consulting - password, Nurse, Admin확인 edit - password, Nurse, Admin, answer인지 검사해야함*/}
            {
              ROUTES.map((route, index) => {
                const TagName = route.children;
                
                if (typeof route.children !== 'undefined') {
                  return (
                    <Route path={route.label} key={index} render={props => <TagName {...props} {...parentProps} />} />
                  )
                }

                {/* 에러페이지 */}
                return (
                  <Frontend_RouterError />
                )
              })
            }

            {/* 서버로 부터 받는 상세페이지들 */}
            {
              headerLinksFromServer_only_subLinks.map(headerLink => (
                <Route key={headerLink.id} path={headerLink.link} render={props => <Dynamic_Component {...props} {...parentProps} specific_links={headerLinksFromServer_only_subLinks} />} />
              ))
            }

            {/* 에러페이지 */}
            <Route path="*" render={props => <Frontend_RouterError />} />
          </Switch>
          <Frontend_Footer {...this.props} expenseGuideCategories={expenseGuideCategories} />
        </Fragment>
      )
    }
}

export default Frontend;
