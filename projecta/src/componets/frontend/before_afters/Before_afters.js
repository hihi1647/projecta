import React, { Component, Fragment } from 'react';
import api from 'common/api';
import { Link } from 'react-router-dom';

import { Message, Radio, Carousel, Select, Pagination } from 'element-react';
import { currentUsesDomain, NO_FIXED_ID } from 'constants/index';

import program_05 from 'assets/images/program_05.png'
import radio_button_01 from 'assets/images/radio_button_01.png'
import radio_button_02 from 'assets/images/radio_button_02.png'
import radio_button_03 from 'assets/images/radio_button_03.png'

const imgUrl = currentUsesDomain.imgUrl;


let rows = [];

function getIndexedRows(datas) {
  let numFixedIndex = 0;
  let numNotFixedIndex = 0;
  let rows = [];

  datas.forEach(data => {
    // 고정된 노티스일 때
    if (data.is_fix) {
      const beforeAfterArr = data.beforeAfters.map(fixedBeforeAfter => {
        const mainCarousel = formatCarouselImage(fixedBeforeAfter);
        return {
          ...fixedBeforeAfter,
          mainCarousel,
          is_fix: data.is_fix
        };
      });
      rows = [...rows, ...beforeAfterArr];

    } else {
      // 고정된 노티스가 아닐 때
      // index값은 페이지당 고정되지 않은 노티스 개수 * 현재 페이지 + 고정된 노티스부터 시작 
      numNotFixedIndex = data.beforeAfters.per_page * (data.beforeAfters.current_page - 1) + numFixedIndex;

      const beforeAfterArr = data.beforeAfters.data.map(beforeAfter => {
        numNotFixedIndex += 1;
        const mainCarousel = formatCarouselImage(beforeAfter);
        return {
          numIndex: numNotFixedIndex,
          ...beforeAfter,
          mainCarousel,
          is_fix: data.is_fix
        };
      });
      rows = [...rows, ...beforeAfterArr];
    }
  });

  return rows;
}

function formatCarouselImage(beforeAfter) {
  const arrangeOrder = {
    'front': ['before_photo_file_front', 'after_photo_file_front'],
    '45': ['before_photo_file_45', 'after_photo_file_45'],
    'side': ['before_photo_file_side', 'after_photo_file_side'] 
  };

  function checkCarouselImage(selectedArray) {

    if (selectedArray && selectedArray.length) {
      let before = beforeAfter[selectedArray[0]];
      let after = beforeAfter[selectedArray[1]];
      const DEFAULT_IMG = program_05;
      if (before || after) {
        if (!before) {
          before = DEFAULT_IMG;
        } else {
          before = imgUrl + before;
        }

        if (!after) {
          after = DEFAULT_IMG;
        } else {
          after = imgUrl + after;
        }

        return [before, after];
      }

      return false;
    }
    return false;
  }

  let results = {};

  Object.keys(arrangeOrder).forEach(key => {
    const child = checkCarouselImage(arrangeOrder[key]);

    if (child) {
      results = {
        ...results,
        [key]: {
          name: key,
          child
        }
      }
    }
  });

  return results;
}

class Frontend_BeforeAfters extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      total: 0,
      perPage: 0,
      checkboxObj: [],
      searchData: {
        categ: 0,
        searchOption: 0,
        query: ''
      },
      isSearched: false,
      checkedRadio: 'all',
      selectedBeforeAfter: {}
    }
  }

  componentDidMount() {
    this.getDatas();

    document.addEventListener('contextmenu', this.disabledRightClick)
  }

  componentWillUnmount() {
    document.removeEventListener('contextmenu', this.disabledRightClick)
  }

  disabledRightClick = (e) => {
    e.preventDefault();
    Message({
      message: '오른쪽 마우스를 클릭할 수 없습니다.',
      type: 'warning'
    });
  }

  getDatas = async (page) => {
    await api.getBeforeAfters(page)
      .then(res => {
        console.log('this is total beforeafters data', res);
        rows = getIndexedRows(res.data.data);

        const selectedBeforeAfter = this.getSelectedCarousel(rows);

        console.log('this is total rows', rows);

        const notFixedBeforeAfter = res.data.data.filter(cur => !cur.is_fix)[0].beforeAfters;

        this.setState({
          page: notFixedBeforeAfter.current_page,
          total: notFixedBeforeAfter.last_page,
          perPage: notFixedBeforeAfter.per_page,
          checkboxObj: rows,
          isSearched: false,
          selectedBeforeAfter
        });
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  getSearchedDatas = async (page) => {
    const { searchData } = this.state;
    const payload = {
      search_word: searchData.query,
      main_category_id: searchData.categ,
      search_option_id: searchData.searchOption
    };

    await api.getSearchBeforeAfter(payload, page, true)
      .then(res => {
        const resData = res.data.data;

        rows = getIndexedRows(resData);
        const selectedBeforeAfter = this.getSelectedCarousel(rows);

        const notFixedBeforeAfter = resData.filter(cur => !cur.is_fix)[0].beforeAfters;

        this.setState({
          page: notFixedBeforeAfter.current_page,
          total: notFixedBeforeAfter.last_page,
          perPage: notFixedBeforeAfter.per_page,
          checkboxObj: rows,
          isSearched: true,
          checkedRadio: searchData.categ,
          selectedBeforeAfter
        });
      })
      .catch(error => {
        console.log(error);
      })
  }

  handleRadioChange = async (value) => {
    // this.setState({
    //   checkedRadio: value
    // });

    let resp;

    if (value === 'all') {
      await api.getBeforeAfters()
        .then(res => {
          resp = res;
        })
        .catch(error => {
          console.log('something went wrong', error);
        })
    } else {
      const filtered_category_id = value;
  
      await api.getFilteredBeforeAfter(filtered_category_id, true)
        .then(res => {
          resp = res;
        })
        .catch(error => {
          console.log('something went wrong', error);
        })
    }

    if ( resp ) {
      rows = getIndexedRows(resp.data.data);
      const selectedBeforeAfter = this.getSelectedCarousel(rows);

      const notFixedBeforeAfter = resp.data.data.filter(cur => !cur.is_fix)[0].beforeAfters;
      

      this.setState({
        page: notFixedBeforeAfter.current_page,
        total: notFixedBeforeAfter.last_page,
        perPage: notFixedBeforeAfter.per_page,
        checkboxObj: rows,
        checkedRadio: value,
        selectedBeforeAfter
      });
    }
  }

  handleChangePage = (selectedPage) => {
    const { isSearched } = this.state;
    if (isSearched) {
      this.getSearchedDatas(selectedPage);
    } else {
      this.getDatas(selectedPage);
    }

    // window.scrollTo(0, 0);
  };

  searchClickHanlder = async () => {
    console.log('clicking...', this.state.searchData);
    const { searchData, isSearched } = this.state;

    if (searchData.searchOption === 0 && searchData.categ === 0 && searchData.query === '') {
      if (isSearched) {
        this.getDatas();
      }
    } else {
      this.getSearchedDatas();
    }
  }

  categChangeHandler = (value) => {
    this.searchChangeHandler(value,'categ');
  }
  searchOptionChangeHandler = (value) => {
    this.searchChangeHandler(value,'searchOption');
  }

  searchChangeHandler = (target, type) => {
    let newSearchData = {
      ...this.state.searchData
    };
    if (type === 'query') {
      newSearchData = {
        ...newSearchData,
        [type]: target.target.value
      }
    } else {
      newSearchData = {
        ...newSearchData,
        [type]: target
      };
    }

    this.setState({
      searchData: newSearchData
    });
  }

  searchClear = () => {
    this.setState({
      searchData: {
        categ: 0,
        searchOption: 0,
        query: ''
      }
    })
  }

  handleLink = (id) => {

    const selectedBeforeAfter = this.getSelectedCarousel(this.state.checkboxObj, id);
    
    this.setState({
      selectedBeforeAfter
    })

    // this.props.history.push(`${this.props.match.url}/before_after/${id}`);
  }

  // need to be improve
  getSelectedCarousel = (beforeAfters, currentId) => {
    let targetBeforeAfter;
    if (currentId) {
      targetBeforeAfter = beforeAfters.filter(cur => cur.id === currentId)[0];
    } else {
      targetBeforeAfter = beforeAfters[0];
    }

    console.log('this is selected data', targetBeforeAfter);
    return targetBeforeAfter;
  }

  makingDefaultImgElement = (beforeAfter) => {
    const mainCarousel = beforeAfter.mainCarousel;
    const priority = ['front', 'side', '45'];

    for (let i = 0; i < priority.length; i += 1) {
      const currentOrder = priority[i];
      if (mainCarousel[currentOrder]) {
        return (
          <Fragment>
            <img style={{width: '50%'}} src={mainCarousel[currentOrder].child[0]} />
            <img style={{width: '50%'}} src={mainCarousel[currentOrder].child[1]} />
          </Fragment>
        )
      }
    }
  }

  carouselIndicator = (beforeAfter) => {

    const btnImage = {
      'front': radio_button_01,
      '45': radio_button_02,
      'side': radio_button_03
    }

    const arrangeOrder = ['front', '45', 'side'];

    let carouselIndicatorStyle = ``;
    const mainCarousel = beforeAfter.mainCarousel;

    if (mainCarousel) {
      let count = 1;
      for (let i = 0; i < arrangeOrder.length; i += 1) {
        const targetImage = arrangeOrder[i];
        if (mainCarousel[targetImage]) {
          carouselIndicatorStyle += `.board-list .el-carousel__indicators li:nth-child(${count++}) { background-image: url(${btnImage[targetImage]}); margin:0 0.25rem;}`
        }
      }
    }

    return carouselIndicatorStyle;
  }


  render(){
    const { $user, location } = this.props;
    console.log('this is frontend before afters', this.state, this.props);
    const beforeAfterCategories = this.props.beforeAfterCategories;
    const { searchData, checkboxObj, selectedBeforeAfter } = this.state;
    const { categ, searchOption, query } = searchData;
    return (
      <Fragment>
      <div className="board-top-style">
        <ul className="board-title-box">
          <li className="board-title">전후사진</li>
          <li className="board-title-box-line"></li>
          <li className="board-text">MIBALANCE BEFORE&AFTER</li>
        </ul>
      </div>
       
      <div className="border-container">
        <div className="board-list">
          <Radio.Group value={this.state.checkedRadio} onChange={this.handleRadioChange}>
            <Radio.Button value="all">전체</Radio.Button>
            {
              beforeAfterCategories.main_categories ? beforeAfterCategories.main_categories.map((categ, index) => (
                <Radio.Button value={categ.id} key={index}>{categ.main_category_name}</Radio.Button>
              )) : null
            }
          </Radio.Group>
          
          <div className="beaf-Body">
          <div style={{position: "relative"}}>
            {
              ($user && $user.role) ? null : (   
                  
                  <div className="blur-box">
                    <p className="blur-box-en"> BEFORE &amp; AFTER </p>
                    <span className="blur-box-desc">
                      <svg className="blur-icon-lock" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#fff"><path d="M18 10v-4c0-3.313-2.687-6-6-6s-6 2.687-6 6v4h-3v14h18v-14h-3zm-5 7.723v2.277h-2v-2.277c-.595-.347-1-.984-1-1.723 0-1.104.896-2 2-2s2 .896 2 2c0 .738-.404 1.376-1 1.723zm-5-7.723v-4c0-2.206 1.794-4 4-4 2.205 0 4 1.794 4 4v4h-8z"/></svg>
                      <p className="blur-text">회원 로그인을 하시면 다양한 사진의 전후사진을 확인하실 수 있습니다.</p>
                      <Link to={{ pathname: "/login", state: { from: location } }} className="btn-blur-login">
                        로그인하기
                        <svg className="icon-arrow" xmlns="http://www.w3.org/2000/svg" width="17.007" height="4.804" viewBox="0 0 17.007 4.804"><path className="banner-arrow" d="M5216.949-13199.223h15.019l-4.759-3.6" transform="translate(-5216.449 13203.526)" fill="none" stroke="#fff" strokeLinecap="round" strokeWidth="1"></path></svg>
                      </Link>
                    </span>
                  </div>  
              )
            }
            <Carousel indicatorPosition="outside">
              {
                selectedBeforeAfter.mainCarousel && Object.keys(selectedBeforeAfter.mainCarousel).map((curImageArrKey, index) => {
                  return (
                    <Carousel.Item key={index}>
                      <li style={index % 2 === 1 ? {width: '100%'} : {width: '100%'}}>
                        {/* <div className="before-stapm stapm-position-right"><p>AFTER</p></div>
                        <div className="before-stapm stapm-position-left"><p>BEFORE</p></div> */}
                        {
                          selectedBeforeAfter.mainCarousel[curImageArrKey].child.map((curImage, ind) => (
                            <img key={curImage + ind} style={{flex: '1', width: '50%'}} src={curImage} />
                          ))
                        }
                      </li>
                    </Carousel.Item>
                  )
                })
              }
              
            </Carousel>
            <style dangerouslySetInnerHTML={{__html: this.carouselIndicator(selectedBeforeAfter)}} />
          </div>
          <div className="card-desc card-desc-main">
            {(selectedBeforeAfter.fix_option_id !== NO_FIXED_ID && selectedBeforeAfter.fix_option_id !== null) && <span className="beforeAfter-fix-text">{selectedBeforeAfter.fix_option_name}</span>}
            <span>{selectedBeforeAfter.title}</span>
          </div>
          <ul className="card-list">
              {
                checkboxObj.map((cur, ind) => (

                  <li className="card-item" key={ind} onClick={() => this.handleLink(cur.id)}>
                 
                      {/* <div className="before-stapm stapm-position-right"><p>AFTER</p></div>
                      <div className="before-stapm stapm-position-left"><p>BEFORE</p></div> */}
                      <figure className="card-image">
                        { ($user && $user.role) ? null : <div className="blur-box"><svg className="card-blur-icon-lock" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18 10v-4c0-3.313-2.687-6-6-6s-6 2.687-6 6v4h-3v14h18v-14h-3zm-5 7.723v2.277h-2v-2.277c-.595-.347-1-.984-1-1.723 0-1.104.896-2 2-2s2 .896 2 2c0 .738-.404 1.376-1 1.723zm-5-7.723v-4c0-2.206 1.794-4 4-4 2.205 0 4 1.794 4 4v4h-8z"/></svg></div> }
                        {
                          this.makingDefaultImgElement(cur)
                        }
                      </figure>
                      <div className="card-desc">
                        {(cur.fix_option_id !== NO_FIXED_ID && cur.fix_option_id !== null) && <span className="beforeAfter-fix-text">{cur.fix_option_name}</span>}
                        <span>{cur.title}</span>
                      </div>
              
                  </li>
                ))
              }
            </ul>
          </div>
        </div>
 

        <Pagination 
          style={{display:'flex',justifyContent:'center'}} 
          layout="prev, pager, next" 
          currentPage={this.state.page} 
          pageCount={this.state.total} 
          pageSize={this.state.perPage} 
          small={true}
          onCurrentChange={this.handleChangePage}
        />
        <div className="board-search">
          <div className="search-box">
            <Select value={searchData.categ} placeholder="분야 분류" onChange={this.categChangeHandler}>
              {beforeAfterCategories.main_categories ? beforeAfterCategories.main_categories.map(el => {
                return <Select.Option key={el.id} label={el.main_category_name} value={el.id} />
              }) : null}
            </Select>
          </div>
          <div className="search-box">
            <Select value={searchData.searchOption} placeholder="글제목" onChange={this.searchOptionChangeHandler}>
              {beforeAfterCategories.search_options ? beforeAfterCategories.search_options.map(el => {
                return <Select.Option key={el.id} label={el.search_option_name} value={el.id} />
              }) : null}
            </Select>
          </div>
          <span>
            <input className="board-input search-box" type="text" value={query} onChange={(e) => this.searchChangeHandler(e, 'query')} />
            {
              (categ || searchOption || query) && <i className="el-icon-circle-cross" onClick={this.searchClear}></i> 
            }
          </span>
          <a className="btn-frontend btn-search search-box" onClick={this.searchClickHanlder}>검색</a>
        </div>
      </div>
      </Fragment>
    );
  }
}

export default Frontend_BeforeAfters;