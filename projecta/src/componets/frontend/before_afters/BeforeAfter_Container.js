import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Before_afters from './Before_afters';

import api from 'common/api';

class BeforeAfter_Container extends React.Component {
    state = {
        beforeAfterCategories: {},
    };
    

    componentDidMount() {
        this.getDatas();
    }

    getDatas = async () => {
        await api.getBeforeAftersCategories(true)
          .then(res => {
              this.setState({
                  beforeAfterCategories: res.data.data
              })
          })
          .catch(error => {
              console.log('something went wrong with category', error);
          })
    }

    render() {
        const { beforeAfterCategories } = this.state
        const { path } = this.props.match;

        const { $user, isAuthenticated, changeState } = this.props;
        return (
            <Switch>
                <Route path={path} render={(props) => <Before_afters {...props} $user={$user} beforeAfterCategories={beforeAfterCategories} />} />
            </Switch>
        )
    }
}

export default BeforeAfter_Container;