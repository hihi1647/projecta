import React, { Fragment } from 'react';
import './styleIntro.css';

class Doctor_infor extends React.Component { 
    render() {
      return (
          <Fragment>
          <div className="board-top-style">
            <ul className="board-title-box">
                <li><h2 className="board-title">미밸런스 의료진 소개</h2></li>
                <li className="board-title-box-line"></li>
                <li className="board-text">MIBALANCE DOCTOR INFOR</li>
            </ul>
          </div>
          <div className="doctor-inforPage-background">
            <div className="contents-container">
              <div className="container-wrap doctor-inforPage-wrap">
              <div className="doctor-inforPage-textWrap">
                <ul className="doctor-text-list">
                  <li className="doctor-inforPage-title">
                    <img className="icon_mark_01" src={ require('../../../assets/images/icon_mark_01.png') } />
                    <strong>수준 높은 진료와 체계적인 관리</strong>로 어제보다 아름다운 나를 만나게 해드리겠습니다.
                    <img className="icon_mark_02" src={ require('../../../assets/images/icon_mark_02.png') } />
                  </li>
                  <li className="doctor-inforPage-text">피부 / 리프팅 / 쁘띠 / 비만</li>
                  <li><h3 className="doctor-inforPage-name"><strong>윤정후</strong> 대표원장</h3><svg className="icon-doctor-petal" xmlns="http://www.w3.org/2000/svg" width="23.513" height="24.909" viewBox="0 0 23.513 24.909"><g transform="translate(-3559.337 17839.749)"><path d="M526.563,1685.929a18.97,18.97,0,0,0-17.928,17.928A18.968,18.968,0,0,0,526.563,1685.929Z" transform="translate(3050.702 -19525.678)" fill="#e5c5b9" opacity="0.5"></path><path d="M17.928,0A18.97,18.97,0,0,0,0,17.928,18.968,18.968,0,0,0,17.928,0Z" transform="matrix(0.921, 0.391, -0.391, 0.921, 3566.347, -17838.348)" fill="#e5c5b9"></path></g></svg></li>
                </ul>
                <div className="doctor-inforPage-imgWrap">
                  <img className="doctor-inforPage-img" src={ require('../../../assets/images/profile_01.png') } alt="윤정후 대표원장 사진" />
                </div>
              </div>

              <div className="doctor-list-wrap">
                  <ul className="doctor-list doctor-list-one">
                    <li>· <strong>현 미밸런스의원 대표원장</strong></li>
                    <li>· 전 명동 유앤아이의원 원장</li>
                    <li>· 전 톡스앤필 원장</li>
                    <li>· 전 수클리닉의원 원장</li>
                    <li>· 전 미하이의원 원장</li>
                    <li>· 카톨릭대학교 성모병원 전문의</li>
                    <li>· 대한리프팅연구회 정회원</li>
                    <li>· 대한미용외과학회 회원</li>
                  </ul>
                  <ul className="doctor-list doctor-list-two">
                    <li>· 대한미용성형레이저학회 회원</li>
                    <li>· 대한레이저피부모발학회 회원</li>
                    <li>· 대한비만학회 회원</li>
                    <li>· 대한비만미용체형학회 회원</li>
                    <li>· 대한근골격초음파학회 회원</li>
                    <li>· 대한마취통증의학회 정회원</li>
                    <li>· 대한통증학회 정회원</li>
                    <li>· 전국미용의사총연합회 정회원</li>
                  </ul>
              </div>
            </div>
            </div>
          </div>

          <div className="doctor-inforPage-background">
            <div className="contents-container">
              <div className="container-wrap doctor-inforPage-container">
              <div className="doctor-inforPage-textWrap">
                <ul className="doctor-text-list">
                  <li className="doctor-inforPage-title">
                    <img className="icon_mark_01" src={ require('../../../assets/images/icon_mark_01.png') } />
                    <strong>정밀한 진단과 정교한 진료</strong>로 어떤 순간에도 무너지지 않는 아름다움을 선사해 드리겠습니다.
                    <img className="icon_mark_02" src={ require('../../../assets/images/icon_mark_02.png') } />
                  </li>
                  <li className="doctor-inforPage-text">피부 / 리프팅 / 쁘띠 / 비만 / 체형</li>
                  <li><h3 className="doctor-inforPage-name"><strong>이형진</strong> 대표원장</h3><svg className="icon-doctor-petal" xmlns="http://www.w3.org/2000/svg" width="23.513" height="24.909" viewBox="0 0 23.513 24.909"><g transform="translate(-3559.337 17839.749)"><path d="M526.563,1685.929a18.97,18.97,0,0,0-17.928,17.928A18.968,18.968,0,0,0,526.563,1685.929Z" transform="translate(3050.702 -19525.678)" fill="#e5c5b9" opacity="0.5"></path><path d="M17.928,0A18.97,18.97,0,0,0,0,17.928,18.968,18.968,0,0,0,17.928,0Z" transform="matrix(0.921, 0.391, -0.391, 0.921, 3566.347, -17838.348)" fill="#e5c5b9"></path></g></svg></li>
                </ul>
                <div className="doctor-inforPage-imgWrap">
                  <img className="doctor-inforPage-img" src={ require('../../../assets/images/profile_02.png') } alt="윤정후 대표원장 사진" />
                </div>
              </div>

              <div className="doctor-list-wrap">
                  <ul className="doctor-list doctor-list-one">
                    <li>· <strong>현 미밸런스의원 대표원장</strong></li>
                    <li>· 전 강남 프레쉬의원 원장</li>
                    <li>· 전 아비쥬의원 원장</li>
                    <li>· 인하대학교병원 전문의</li>
                    <li>· 대한미용성형레이저의학회 정회원</li>
                    <li>· 대한리프팅연구회 정회원</li>
                    <li>· 대한가정의학회 정회원</li>
                    <li>· 대한지방흡입학회 회원</li>
                  </ul>
                  <ul className="doctor-list doctor-list-two">
                    <li>· 대한비만학회 회원</li>
                    <li>· 대한레이저피부모발학회 회원</li>
                    <li>· 대한비만미용체형학회 회원</li>
                    <li>· 한국피부비만성형학회 회원</li>
                    <li>· 한국미용외과의학회 회원</li>
                    <li>· 대한여드름학회 회원</li>
                    <li>· 전국미용의사총연합회 정회원</li>
                  </ul>
              </div>
            </div>
            </div>
          </div>
          
          </Fragment>
      )
    } 
  }
  export default Doctor_infor;