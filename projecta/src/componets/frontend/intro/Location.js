import React, { Fragment } from 'react';

export default function Location () {
    return (
        <Fragment>
            <div className="board-top-style">
                <ul className="board-title-box">
                    <li><h2 className="board-title">미밸런스 오시는길</h2></li>
                    <li className="board-title-box-line"></li>
                    <li className="board-text">LOCATION OF MIBALANCE</li>
                </ul>
            </div>

            
                <div className="contents-container">
                    <div className="container-wrap">
                        <div className="locationPage-background">
                            <div className="location-box"> 
                                <ul className="location-card">
                                    <li><img className="location_icon" src={ require('../../../assets/icon/location-mi.svg') } alt="미밸런스" /><h3 className="ir">미밸런스</h3></li>
                                    <li><p className="location-add">경기도 고양시 덕양구 권율대로 685 SIB타워 5층</p></li>
                                    <li><a className="location-btn" href="https://map.naver.com/v5/search/%EB%AF%B8%EB%B0%B8%EB%9F%B0%EC%8A%A4%EC%9D%98%EC%9B%90/place/1491536807?c=14123090.5669669,4530270.9867344,15,0,0,0,dh" target="_blank">네이버 지도로 보기</a></li>
                                    <li className="location-sub-item">
                                        <p>월 - 금 AM 10:30 - PM 20:30</p>
                                        <p>토요일 AM 10:30 - PM 16:30</p>
                                        <p>일요일은 휴진일 입니다.</p>
                                        <p className="location-tel">031.962.7781</p>
                                    </li>
                                </ul>
                            </div>
                    </div>
                    <div className="sub-card-wrap">
                        <ul className="location-sub-card location-sub-card-top">
                            <li className="location-sub-card-item">
                                <p className="location-sub-topText location-sub-twoText">P</p>
                                <p className="location-sub-twoText">주차 안내</p>
                            </li>
                            <li className="location-sub-card-item"><p className="location-sub-text">건물 내 <strong>지하 주차장 무료 이용</strong>이 가능합니다.</p></li>
                        </ul>
                        <ul className="location-sub-card location-sub-card-two">
                            <li className="location-sub-card-item">
                                <p className="location-sub-twoText"><strong>병원 정문</strong></p>
                                <p className="location-sub-twoText">위치 안내</p>
                            </li>
                            <li className="location-sub-card-item"><p className="location-sub-text">미밸런스를 방문해주신 고객님께서는<br/><strong>건물의 정면에서 오른쪽 입구로 들어오시면 됩니다.</strong></p></li>
                        </ul>
                    </div>
                    <div className="bus-img"></div>
                </div>
               
            </div>
            
        </Fragment>
    )
}