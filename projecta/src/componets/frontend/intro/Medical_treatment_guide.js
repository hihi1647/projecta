import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import './styleIntro.css';

class Medical_treatment_guide extends React.Component { 
    render() {
      return (
          <Fragment>
            <div className="board-top-style">
            <ul className="board-title-box">
                <li><h2 className="board-title">미밸런스 진료안내</h2></li>
                <li className="board-title-box-line"></li>
                <li className="board-text">MEDICAL TREATMENT GUIDE</li>
            </ul>
            </div>

            <div className="contents-container">
                <div className="container-wrap">
                    <h3 className="medical-guide-title">미밸런스 <strong>진료과목</strong> 안내</h3>
                    <ul className="guide-img-card"> 
                        <li className="guide-img-card-item"><img className="guide_img" src={ require('../../../assets/images/guide_01.jpg') } alt="쁘띠" /></li>
                        <li className="guide-img-card-item"><img className="guide_img" src={ require('../../../assets/images/guide_02.jpg') } alt="리프팅" /></li>
                        <li className="guide-img-card-item"><img className="guide_img" src={ require('../../../assets/images/guide_03.jpg') } alt="레이저" /></li>
                        <li className="guide-img-card-item"><img className="guide_img" src={ require('../../../assets/images/guide_04.jpg') } alt="바디" /></li>
                        <li className="guide-img-card-item"><img className="guide_img" src={ require('../../../assets/images/guide_05.jpg') } alt="스킨케어" /></li>
                    </ul>
                </div>
            </div>
            <div className="medical-guidePage-background">
                <div className="contents-container">
                    <div className="container-wrap">
                        <div className="time-guide-box">
                            <h3 className="medical-guide-title-left">미밸런스<br/><strong>진료시간</strong> 안내</h3>
                            <div className="guid-day-time-wrap">
                                <ul className="guide-day">
                                    <li>월-금</li>
                                    <li>토요일</li>
                                    <li>점심시간</li>
                                </ul>
                                <ul className="guide-time">
                                    <li>AM 10:30 - PM 20:30</li>
                                    <li>AM 10:30 - PM 16:30</li>
                                    <li>PM 14:00 - PM 15:00</li>
                                </ul>
                            </div>
                            
                            <ul className="guide-infomation">
                                <li><svg className="icon-infor" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm1 18h-2v-6h-2v-2h4v8zm-1-9.75c-.69 0-1.25-.56-1.25-1.25s.56-1.25 1.25-1.25 1.25.56 1.25 1.25-.56 1.25-1.25 1.25z"/></svg>토요일은 점심시간 없이 진료합니다.</li>
                                <li><svg className="icon-infor" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm1 18h-2v-6h-2v-2h4v8zm-1-9.75c-.69 0-1.25-.56-1.25-1.25s.56-1.25 1.25-1.25 1.25.56 1.25 1.25-.56 1.25-1.25 1.25z"/></svg>일요일은 휴진일 입니다.</li>
                            </ul>
                            <ul className="guide-number">
                                <li>상담문의</li>
                                <li>031.962.7781</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="contents-container">
                <div className="container-wrap">
                    
                    <ul className="guide-card"> 
                        <li className="guide-card-item">
                            <h3 className="medical-guide-title-left">미밸런스<br/><strong>예약 진료제</strong> 운영</h3>
                            <p className="medical-guide-desc">미밸런스의원은 예약제로 운영됩니다.<br/><br/>내원 전 미리 전화, 카카오톡으로 예약하시면 대기 시간 없이 바로 진료 받아보실 수 있습니다.</p>
                        </li>
                        <li className="guide-card-item">
                            <Link to={`location`} className="guide-box">
                                <img className="guide-box-icon-01" src={ require('../../../assets/icon/quick-04.svg')} alt="오시는 길"/>
                                <p>오시는 길</p>
                            </Link>
                        </li>
                        <li className="guide-card-item">
                            <Link to={`consultings`} className="guide-box">
                                <img className="guide-box-icon-02" src={ require('../../../assets/icon/quick-02.svg')} alt="온라인 상담"/>
                                <p>온라인 상담</p>
                            </Link>
                        </li>
                        <li className="guide-card-item">
                            <a href="https://pf.kakao.com/_jXxmxaxb" className="guide-box guide-card-item-kakao">
                                <img className="guide-box-icon-03" src={ require('../../../assets/icon/guide-kakao.svg')} alt="카카오톡 상담"/>
                                <p>카카오톡 상담</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
          
          </Fragment>
      )
    } 
  }
  export default Medical_treatment_guide;