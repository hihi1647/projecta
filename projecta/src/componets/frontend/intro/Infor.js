import React, { Fragment } from 'react';
import './styleIntro.css';
export default function Infor () {
    return (
        <Fragment>
        <div className="board-top-style">
        <ul className="board-title-box">
            <li><h2 className="board-title">미밸런스 소개</h2></li>
            <li className="board-title-box-line"></li>
            <li className="board-text">MIBALANCE INFOR</li>
        </ul>
        </div>
            <div className="contents-container">
                <div className="container-wrap">
                    <ul className="infor-img-card"> 
                        <li className="infor-img-card-item"><img className="infor_img" src={ require('../../../assets/images/mi_01.png') } alt="아름답던 mi" /></li>
                        <li className="infor-img-card-item"><img className="infor_img" src={ require('../../../assets/images/mi_02.png') } alt="아름다운 mi" /></li>
                        <li className="infor-img-card-item"><img className="infor_img" src={ require('../../../assets/images/mi_03.png') } alt="아름다울 mi" /></li>
                        <li className="infor-img-card-item"><img className="infor_img" src={ require('../../../assets/images/mi_04.png') } alt="어떤 순간에도 아름다울 수 있도록, 아름다움의 균형을 만들어 드리겠습니다." /></li>
                    </ul>
                </div>
            </div>
            <div className="infor-background">
                <div className="contents-container">
                    <div className="container-wrap infor-desc-wrap">
                        <div  className="doctor_img-box" ><img className="doctor_img  infor-desc-list" src={ require('../../../assets/images/doctor-two.png') } alt="윤정후 대포원장님, 이형진 대표원장님" /></div>
                        <ul className="infor-desc-card infor-desc-list">
                            <li className="infor-sub-text">미밸런스가 생각하는 아름다움의 비밀,</li>
                            <li className="infor-sub-title"><strong>균형과 조화</strong> 입니다.</li>
                            <li>
                                <p className="infor-sub-desc">
                                서로 다른 두음이 동시에 울려서 생기는 소리를 화음이라고 합니다. 서로 다른 두음이 어떤 균형을 가지고 있는지에 따라 불협화음이 되기도 하고 듣기에 아름다운 화음이 되는데 아름다움 화음 사이에는 “완전음정”이라고하는 공식이 존재합니다.
                                우리가 예쁘다, 아름답다라고 느끼는 피부와 체형에도 이러한 공식이 존재합니다. <br/><br/>
                                바로 균형과 조화라는 공식입니다.  얼굴과 체형의 비율, 눈, 코, 입과 얼굴형의 배치, 피부 탄력도와 수분감, 피부 톤의 균형이 조화롭게 맞아 떨어질 때 아름다움은 자연스럽게 뿜어져 나오게 됩니다.<br/><br/>
                                미밸런스의 의료진은 13년 이상의 임상경험을 통해 깨달음 균형과 조화의 공식으로 아름다움을 만듭니다. 미밸런스만의 균형과 조화의 공식으로 어떤 순간에도 무너지지 않는 아름다움을 만들어 드리겠습니다.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="contents-container">
                <div className="container-wrap">

                    <ul className="infor-care-title-box">
                        <li><svg className= "icon-doctor-petal" xmlns="http://www.w3.org/2000/svg" width="23.513" height="24.909" viewBox="0 0 23.513 24.909"><g transform="translate(-3559.337 17839.749)"><path d="M526.563,1685.929a18.97,18.97,0,0,0-17.928,17.928A18.968,18.968,0,0,0,526.563,1685.929Z" transform="translate(3050.702 -19525.678)" fill="#e5c5b9" opacity="0.5"></path><path d="M17.928,0A18.97,18.97,0,0,0,0,17.928,18.968,18.968,0,0,0,17.928,0Z" transform="matrix(0.921, 0.391, -0.391, 0.921, 3566.347, -17838.348)" fill="#e5c5b9"></path></g></svg></li>
                        <li className="infor-care-title"> 미밸런스는 <strong>원칙을 지켜 올바르게 진료</strong>합니다. </li>
                        <li className="infor-care-text">MIBALANCE PRINCIPLE OF CARE</li>
                    </ul>
                    <ul className="infor-care-card">
                        <li className="infor-care-list">
                            <div className="infor-care-img-box"><img className="infor-care-img" src={ require('../../../assets/images/infor_care_01.png') } alt="13년 경력 의료진" /></div> 
                            <ul className="infor-care-card-desc">
                                <li className="infor-care-card-desc-list"><p className="infor-care-card-num">01</p></li>
                                <li className="infor-care-card-desc-list"><p className="infor-care-card-title">13년 이상 경력의 전문의 진료</p></li>
                                <li className="infor-care-card-desc-list"><p className="infor-care-card-text">미밸런스는 13년 이상의 임상 경험을 가지고 있는 전문의와 3년 이상의 관리 경험을 가지고 있는 스텝들이 함께하는 만큼 수준 높은 진료를 자부합니다.</p></li>
                            </ul>
                        </li>
                        <li className="infor-care-list">
                        <div className="infor-care-img-box"><img className="infor-care-img" src={ require('../../../assets/images/infor_care_02.png') } alt="정밀한 진단, 체계적인 진료" /></div>
                            <ul className="infor-care-card-desc">
                                <li className="infor-care-card-desc-list"><p className="infor-care-card-num">02</p></li>
                                <li className="infor-care-card-desc-list"><p className="infor-care-card-title">정밀한 진단, 체계적인 진료</p></li>
                                <li className="infor-care-card-desc-list"><p className="infor-care-card-text">신뢰도 높은 장비를 사용하여 현재 상태를 정밀하게 진단합니다. 문제점을 정확히 파악하는 것은 만족도 높은 진료 시작입니다.</p></li>
                            </ul>
                        </li>
                        <li className="infor-care-list">
                        <div className="infor-care-img-box"><img className="infor-care-img" src={ require('../../../assets/images/infor_care_03.png') } alt="정밀한 진단, 체계적인 진료" /></div>
                            <ul className="infor-care-card-desc">
                                <li className="infor-care-card-desc-list"><p className="infor-care-card-num">03</p></li>
                                <li className="infor-care-card-desc-list"><p className="infor-care-card-title">1:1 커스터마이징 진료</p></li>
                                <li className="infor-care-card-desc-list"><p className="infor-care-card-text">미밸런스는 규격화된 진료를 거부합니다. 개인의 상태와 특성이 다르기 때문입니다.<br/><br/>개인적인 특성을 고려하여 1:1 커스터마이징 진료하며 진료 수립 과정에 있어 환자의 선택권을 철저하게 존중합니다.</p></li>
                            </ul>
                        </li>    
                        <li className="infor-care-list">
                        <div className="infor-care-img-box"><img className="infor-care-img" src={ require('../../../assets/images/infor_care_04.png') } alt="정밀한 진단, 체계적인 진료" /></div>
                            <ul className="infor-care-card-desc">
                                <li className="infor-care-card-desc-list"><p className="infor-care-card-num">04</p></li>
                                <li className="infor-care-card-desc-list"><p className="infor-care-card-title">정품·정량·정직의 원칙 진료</p></li>
                                <li className="infor-care-card-desc-list"><p className="infor-care-card-text">미밸런스는 정식으로 인증 받은 정품만을 사용하며 정량을 준수하여 진료합니다.</p></li>
                            </ul>
                        </li>                       
                    </ul>

                </div>
            </div>
        </Fragment>
    )
}