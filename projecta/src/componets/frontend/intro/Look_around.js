import React, { Fragment } from 'react';
import { Carousel } from 'element-react';

import m1 from 'assets/images/01.jpg';
import m2 from 'assets/images/02.jpg';
import m3 from 'assets/images/03.jpg';
import m4 from 'assets/images/04.jpg';
import m5 from 'assets/images/05.jpg';
import m6 from 'assets/images/06.jpg';
import m7 from 'assets/images/07.jpg';

const imgs = [
    m1, m2, m3, m4, m5, m6, m7
]



class Look_around extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: 0
        }

        this.carouselSelector = React.createRef();
        this.carouselFrame = React.createRef();
    }

    clickHandler = (e) => {
        this.carouselFrame.current.setActiveItem(e);
    }

    changeHandler = (e) => {
        this.centeralize(e);

        this.setState({
            selected: e
        });
    }

    centeralize = (target) => {
        const contWidth = this.carouselSelector.current.offsetWidth;
        const imgWidth = this.carouselSelector.current.children[0].offsetWidth;

        const contPosition = target * imgWidth - imgWidth / 2;

        this.carouselSelector.current.scrollLeft = contPosition;
    }
    
    render() {





        const tabs = [
            {
                label: '바디 보톡스',
                comp: '바디 보톡스',
                name: '1'
            },
            {
                label: '바디 스킨보톡스',
                comp: '바디 스킨보톡스',
                name: '2'
            },
            {
                label: '다한증 보톡스',
                comp: '다한증 보톡스',
                name: '3'
            }
        ];

        return (
            <Fragment>
                <div className="board-top-style">
                    <ul className="board-title-box">
                        <li><h2 className="board-title">미밸런스 둘러보기</h2></li>
                        <li className="board-title-box-line"></li>
                        <li className="board-text">LOOK AROUND MIBALANCE</li>
                    </ul>
                </div>
            
                <div className="contents-container">
                    <div className="container-wrap">

                        <div className="lookAroundCont" style={{width: '100%', height: '400px', overflow: 'auto', display: 'flex', flexWrap: 'nowrap', transition: '0.3s'}} ref={this.carouselSelector}>
                            {
                                imgs.map((item, index) => {
                                    return (
                                        <img src={item} key={index} className={this.state.selected === index ? "selected" : ""} style={{maxWidth: '450px', width: '100%', height: '100%', objectFit: 'cover'}} onClick={() => this.clickHandler(index)}/>
                                    )
                                })
                            }
                        </div>
                        <div>
                            <Carousel indicatorPosition="none" arrow="always" autoplay={false} onChange={this.changeHandler} ref={this.carouselFrame}>
                                {
                                imgs.map((item, index) => {
                                    return (
                                    <Carousel.Item key={index} name={index} >
                                        <img src={item} style={{width: '100%', height: '100%', objectFit: 'cover'}} />
                                    </Carousel.Item>
                                    )
                                })
                                }
                            </Carousel>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default Look_around