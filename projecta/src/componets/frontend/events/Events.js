import React, { Component, Fragment } from 'react';
import api from 'common/api';

import { Select, Pagination, Message } from 'element-react';
import { currentUsesDomain } from 'constants/index';

const imgUrl = currentUsesDomain.imgUrl;

let rows = [];

function getIndexedRows(datas) {
  let numFixedIndex = 0;
  let numNotFixedIndex = 0;
  let rows = [];

  datas.forEach(data => {
    // 고정된 노티스일 때
    if (data.is_fix) {
      const fixedArr = data.events.map(fixedData => {

        let { created_at, start_date, end_date } = fixedData;

        created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;
        start_date = start_date ? ( typeof start_date === 'string' ? start_date.slice(0, 10) : start_date.toJSON().slice(0,10) ) : start_date;
        end_date = end_date ? ( typeof end_date === 'string' ? end_date.slice(0, 10) : end_date.toJSON().slice(0,10) ) : end_date;
        
        const event_status = calcEventStatus(fixedData.start_date, fixedData.end_date);
        const is_fix = data.is_fix && data.is_fix;

        return {
          ...fixedData,
          created_at,
          start_date,
          end_date,
          event_status,
          is_fix
        };
      });
      rows = [...rows, ...fixedArr];

    } else {
      // 고정된 노티스가 아닐 때
      // index값은 페이지당 고정되지 않은 노티스 개수 * 현재 페이지 + 고정된 노티스부터 시작 
      numNotFixedIndex = data.events.per_page * (data.events.current_page - 1) + numFixedIndex;

      const notFixedArr = data.events.data.map(notFixedData => {
        numNotFixedIndex += 1;

        let { created_at, start_date, end_date } = notFixedData;

        created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;
        start_date = start_date ? ( typeof start_date === 'string' ? start_date.slice(0, 10) : start_date.toJSON().slice(0,10) ) : start_date;
        end_date = end_date ? ( typeof end_date === 'string' ? end_date.slice(0, 10) : end_date.toJSON().slice(0,10) ) : end_date;

        const event_status = calcEventStatus(notFixedData.start_date, notFixedData.end_date);
        const is_fix = notFixedData.is_fix && notFixedData.is_fix;

        return {
          numIndex: numNotFixedIndex,
          ...notFixedData,
          created_at,
          start_date,
          end_date,
          event_status,
          is_fix
        };
      });
      rows = [...rows, ...notFixedArr];
    }
  });

  return rows;
}

function getSearchedIndexedRows(datas) {
  let numNotFixedIndex = 0;

  numNotFixedIndex = datas.per_page * (datas.current_page - 1);

  return datas.data.map(data => {

    let { created_at, start_date, end_date } = data;

    created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;
    start_date = start_date ? ( typeof start_date === 'string' ? start_date.slice(0, 10) : start_date.toJSON().slice(0,10) ) : start_date;
    end_date = end_date ? ( typeof end_date === 'string' ? end_date.slice(0, 10) : end_date.toJSON().slice(0,10) ) : end_date;

    const event_status = calcEventStatus(data.start_date, data.end_date);
    const is_fix = !!data.fix_option_id;

    return {
      ...data,
      created_at,
      start_date,
      end_date,
      event_status,
      is_fix
    }
  })

}

// need to improve(handling only in backend ? or both front, backend)
// if when we set start_date, end_date, we set like this way in backend
function calcEventStatus(startDate, endDate) {
  // this should be deleted later..
  let startDateTime, endDateTime;

  const startTime = " 00:00:00";
  const endTime = " 23:59:59";

  if (startDate) {
    if (typeof startDate === 'string') {
      startDateTime = startDate.slice(0, 10) + startTime;
    } else {
      startDateTime = startDate.toJSON().slice(0, 10) + endTime;
    }

    startDateTime = new Date(startDateTime).getTime();
  }

  if (endDate) {
    if (typeof endDate === 'string') {
      endDateTime = endDate.slice(0, 10) + endTime;
    } else {
      endDateTime = endDate.toJSON().slice(0, 10) + endTime;
    }

    endDateTime = new Date(endDateTime).getTime();
  }
  


  let currentTime = new Date().getTime();

  let msg = '이벤트 진행중';

  if (startDateTime > currentTime) {
    msg = '이벤트 준비중';
  } else if (currentTime > endDateTime) {
    msg = '이벤트 종료';
  }

  return msg;
}

class Frontend_Events extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      total: 0,
      perPage: 0,
      checkboxObj: [],
      searchData: {
        searchOption: 0,
        query: ''
      },
      isSearched: false
    }
  }

  componentDidMount() {
    this.getDatas();
  }

  getDatas = async (page) => {
    await api.getEvents(page)
      .then(res => {
        const resData = res.data.data;

        if (resData) {
          rows = getIndexedRows(resData);

          const notFixedDatas = res.data.data.filter(cur => !cur.is_fix)[0].events;

          this.setState({
            page: notFixedDatas.current_page,
            total: notFixedDatas.last_page,
            perPage: notFixedDatas.per_page,
            checkboxObj: rows,
            isSearched: false
          });
        }
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  getSearchedDatas = async (page) => {
    const { searchData } = this.state;
    const payload = {
      search_word: searchData.query,
      search_option_id: searchData.searchOption
    };

    await api.getSearchEvent(payload, page, true)
      .then(res => {
        const resData = res.data.data;

        if (resData) {
          rows = getSearchedIndexedRows(resData);

          this.setState({
            checkboxObj: rows,
            total: resData.last_page,
            page: resData.current_page,
            perPage: resData.per_page,
            isSearched: true
          })
          console.log('successfully searched..', res, rows);
        }
      
      })
      .catch(error => {
        console.log(error);
      })
  }

  handleChangePage = (selectedPage) => {
    const { isSearched } = this.state;
    if (isSearched) {
      this.getSearchedDatas(selectedPage);
    } else {
      this.getDatas(selectedPage);
    }
  };

  searchClickHanlder = async () => {
    console.log('clicking...', this.state.searchData);
    const { searchData, isSearched } = this.state;
    
    if (searchData.searchOption === 0 && searchData.query === '') {
      if (isSearched) {
        this.getDatas();
      }
    } else {
      this.getSearchedDatas();
    }    
  }

  searchOptionChangeHandler = (value) => {
    this.searchChangeHandler(value,'searchOption');
  }

  searchChangeHandler = (target, type) => {
    let newSearchData = {
      ...this.state.searchData
    };
    if (type === 'query') {
      newSearchData = {
        ...newSearchData,
        [type]: target.target.value
      }
    } else {
      newSearchData = {
        ...newSearchData,
        [type]: target
      };
    }

    this.setState({
      searchData: newSearchData
    });
  }

  searchClear = () => {
    this.setState({
      searchData: {
        searchOption: 0,
        query: ''
      }
    })
  }

  handleLink = (eventData) => {
    if (eventData.event_status === '이벤트 종료') {
      Message({
        type: 'info',
        message: "죄송합니다. 해당 이벤트는 종료되었습니다."
      });
    } else {
      this.props.history.push(`${this.props.match.url}/event/${eventData.id}`);
    }
  }

  render() {
    const eventCategories = this.props.eventCategories;
    const { searchData, checkboxObj } = this.state;
    const { searchOption, query } = searchData;
    console.log('this is front Events', eventCategories, this.state, this.props);
    
    return (
      <Fragment>
      <div className="board-top-style">
        <ul className="board-title-box">
            <li className="board-title">이벤트</li>
            <li className="board-title-box-line"></li>
            <li className="board-text">MIBALANCE EVENT</li>
        </ul>
      </div>
      <div className="border-container">
        <div className="board-list">
          <ul className="event-list">
          {
            checkboxObj.map((row, index) => (
              <li className="event-item" key={index} onClick={(e) => this.handleLink(row)}>
                {
                  row.is_fix && <div className="event-fixation"style={row.fix_option_name !== 'NEW' ? {backgroundColor:'#E89B96'} : {backgroundColor:'#FFD2B1'}}>{row.fix_option_name}</div>
                }
                <figure className="event-img">
                  {
                    row.photo_file && <img src={imgUrl + row.photo_file}/>
                  }
                </figure>
                <div className="event-desc">
                <ul className="event-desc-wrap">
                  <li><p className="event-title">{row.title}</p></li>
                  <li>
                    {/* [sub_title] 삭제하지 말아주세요 ! */}
                    {/* <p className="event-sub-title">{row.sub_title}</p>  */}
                  {
                    row.start_date || row.end_date ? (
                    <p className="event-date">기간 : { row.start_date + " ~ " + row.end_date }</p>
                    ) : null
                  }
                  </li>
                </ul>
                 
                  <div className="btn-event-page" style={row.event_status !== '이벤트 종료' ? {color: '#E38A88', border: '1px solid #E38A88',backgroundColor:'#FFF4F4' } : {color: '#fff', border: '1px solid #555555',backgroundColor:'#555555', padding: '0.8em 1.5em'}} >
                    {/* 조건부 스타일링 -> 인라인 스타일, 클래스 사용
                      className = {row.event_status !== '종료' ? 'bottomLineThroughText' : 'middleLineThroughText'}
                      style={row.event_status !== '종료' ? {textDecoration: 'underline' } : {textDecoration: 'line-through'}}
                    */}
                    {row.event_status}
                  </div>
                </div>
              </li>
            ))
          }
          </ul>
        </div>

        <br />

        <Pagination 
          style={{display:'flex',justifyContent:'center'}} 
          layout="prev, pager, next" 
          currentPage={this.state.page} 
          pageCount={this.state.total} 
          pageSize={this.state.perPage} 
          small={true}
          onCurrentChange={this.handleChangePage}
        />
        <div className="board-search">
          <div className="search-box">
            <Select value={searchData.searchOption} placeholder="글제목" onChange={this.searchOptionChangeHandler}>
              {eventCategories.search_options ? eventCategories.search_options.map(el => {
                return <Select.Option key={el.id} label={el.search_option_name} value={el.id} />
              }) : null}
            </Select>
          </div>
          <span>
            <input className="board-input search-box" type="text" value={query} onChange={(e) => this.searchChangeHandler(e, 'query')} />
            {
              (searchOption || query) && <i className="el-icon-circle-cross" onClick={this.searchClear}></i> 
            }
          </span>
          <a className="btn-frontend btn-search search-box" onClick={this.searchClickHanlder}>검색</a>
        </div>

      </div>
      </Fragment>
    )
  }
}
export default Frontend_Events;