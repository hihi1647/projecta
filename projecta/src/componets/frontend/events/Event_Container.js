import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Event from './Event';
import Events from './Events';

import api from 'common/api';

class Event_Container extends React.Component {
    state = {
        eventCategories: {},
    };
    

    componentDidMount() {
        this.getDatas();
    }

    getDatas = async () => {
        await api.getEventCategories(true)
          .then(res => {
              this.setState({
                  eventCategories: res.data.data
              })
          })
          .catch(error => {
            console.log('something went wrong with event category', error);
          });
    }

    render() {
        const { eventCategories } = this.state
        const { path } = this.props.match;

        const { $user, isAuthenticated, changeState } = this.props;
        return (
            <Switch>
                <Route path={`${path}/event/:id`} render={(props) => <Event {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={path} render={(props) => <Events {...props} $user={$user} eventCategories={eventCategories} />} />
            </Switch>
        )
    }
}

export default Event_Container;