import React, { Fragment } from 'react';
import api from 'common/api';
import { Link } from 'react-router-dom';
import parse from 'html-react-parser';

const columnsObj = {
  'title': '제목',
  'writer': '작성자',
  'created_at': '작성일',
  'views': '조회수',
  'start_date': '노출 시작일',
  'end_date': '노출 종료일',
  'sub_title': '본문요약',
  'fix_option_id': '고정',
  'photo_file': '대표 사진',
  'event_status': '노출 상태',
  'details': '내용'
}

const columns = [
  'writer',
  'created_at',
  'title',
  'details'
];
class Frontend_Event extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      event: {}
    }

    this.history = this.props.history;
    this.match = this.props.match;
    this.currentEventID = this.match.params.id;
  }

  componentDidMount() {
    this.getDatas(this.currentEventID);
  }

  getDatas = async (currentEventID) => {
    await api.getEvent(currentEventID, true)
      .then(res => {

        const resData = res.data.data;

        if (resData) {
          let { created_at } = resData;

          created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;

          const event = {
            ...resData,
            created_at
          }

          this.setState({
            event
          })
        }
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  goBack = () => {
    this.history.push('/events');
  }

  handleLink = (targetPage) => {
    if (targetPage) {
      const newURL = this.match.url.replace(':id', targetPage.id);
      this.history.push(newURL);
    }

    console.log('clicking Links...', targetPage);
  }

  render() {
    const event = this.state.event;
    console.log('this is event', event);
    return (
      <div>
        {
          event.id ? (
            <Fragment>
              <div className="board-top-style">
                <ul className="board-title-box">
                  <li className="board-title">이벤트</li>
                  <li className="board-title-box-line"></li>
                  <li className="board-text">MIBALANCE EVENT</li>
                </ul>
              </div>
              <div className="border-container">
                <div className="board-list">
                  <div className="border-header">
                    <h3 className="event-border-header-title">{event.title}</h3>
                  </div>

                  <div className="border-header-bottom">
                    <span className="border-view-name">{columnsObj.writer}</span>
                    <span className="border-view-body">{event.writer}</span>
                    <span className="border-view-name">{columnsObj.created_at}</span>
                    <span className="border-view-body">{event.created_at}</span>
                  </div>

                  <div className="border-body"> {typeof event.details === 'string' ? parse(event.details) : event.details}</div>
                  <div className="border-btn-wrap"><a className="btn-borderList" onClick={this.goBack}>목록</a></div>
                  <div className="prev-contianer">
                  {
                    event.previous ? (
                      <Fragment>
                      <div className="prev-wrap prev-wrap-boder">
                        <span className="border-arrow">
                          <Link to={`${this.match.path.replace(':id', event.previous.id)}`}>
                          <svg  className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><path d="M0 16.67l2.829 2.83 9.175-9.339 9.167 9.339 2.829-2.83-11.996-12.17z" fill="##615454"/></svg>
                            이전글
                          </Link>
                        </span>
                        <span className="border-prev-body">{event.previous.title}</span>
                      </div>
                      </Fragment>
                    ) : null
                  }
                  {
                    event.next ? (
                      <Fragment>
                        <div className="prev-wrap">
                            <div className="border-arrow">
                          <Link to={`${this.match.path.replace(':id', event.next.id)}`}>
                          <svg className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z" fill="##615454"/></svg>
                            다음글
                          </Link>
                          </div>
                          <div className="border-prev-body">{event.next.title}</div>
                        </div>
                      </Fragment>
                    ) : null
                  }
                </div>
              </div>
            </div>
            </Fragment>
          ) : (
            <div>
              something went wrong..
            </div>
          )
        }
      </div>
    )
  }
}
export default Frontend_Event;