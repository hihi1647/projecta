import React from 'react';
import api from 'common/api';

import { currentUsesDomain } from 'constants/index';

import { Radio, Dialog, Tooltip } from 'element-react';
import parse from 'html-react-parser';

import imageResize from './RichEditorLogic';
import RichEditorSVG from './RichEditorSVG';
import './RichEditor.css';
//https://www.youtube.com/watch?v=7ymnoz8bBqQ


const imgUrl = currentUsesDomain.imgUrl;


const RichEditorFnc = {
    undo: {
        label: 'undo',
        icon: RichEditorSVG.undo
    },
    redo: {
        label: 'redo',
        icon: RichEditorSVG.redo
    },
    bold: {
        label: 'bold',
        icon: RichEditorSVG.bold
    }, 
    italic: {
        label: 'italic',
        icon: RichEditorSVG.italic
    }, 
    underline: {
        label: 'underline',
        icon: RichEditorSVG.underline
    }, 
    strikeThrough: {
        label: 'strikeThrough',
        icon: RichEditorSVG.strike
    }, 
    justifyLeft: {
        label: 'justifyLeft',
        icon: RichEditorSVG.align_left
    }, 
    justifyCenter: {
        label: 'justifyCenter',
        icon: RichEditorSVG.align_center
    }, 
    justifyRight: {
        label: 'justifyRight',
        icon: RichEditorSVG.align_right
    }, 
    justifyFull: {
        label: 'justifyFull',
        icon: RichEditorSVG.align_justify
    }, 
    indent: {
        label: 'indent',
        icon: RichEditorSVG.indent
    }, 
    outdent: {
        label: 'outdent',
        icon: RichEditorSVG.outdent
    }, 
    subscript: {
        label: 'subscript',
        icon: RichEditorSVG.subscript
    }, 
    superscript: {
        label: 'superscript',
        icon: RichEditorSVG.superscript
    }, 
    insertUnorderedList: {
        label: 'insertUnorderedList',
        icon: RichEditorSVG.list_bullets
    }, 
    insertOrderedList: {
        label: 'insertOrderedList',
        icon: RichEditorSVG.list_number
    }, 
    insertParagraph: {
        label: 'insertParagraph',
        icon: '<div>insertParagraph</div>'
    }, 
    insertHorizontalRule: {
        label: 'insertHorizontalRule',
        icon: RichEditorSVG.horizontal_rule
    }, 
    
    formatBlock: {
        label: 'formatBlock',
        icon: RichEditorSVG.paragraph_style,
        onChange: [
            'H1', 'H2', 'H3', 'H4', 'H5', 'H6'
        ]
    }, 
    fontName: {
        label: 'fontName',
        styleName: 'fontFamily',
        icon: RichEditorSVG.text_style,
        onChange: [
            'Arial', 'Comic Sans MS', 'Courier', 'Georgia', 'Tahoma', 'Times New Roman', 'Verdana'
        ]
    }, 
    fontSize: {
        label: 'fontSize',
        styleName: 'fontSize',
        icon: '',
        onChange: [
            8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 28, 36, 48, 72 
        ]
    }, 
    foreColor: {
        label: 'foreColor',
        icon: RichEditorSVG.font_color,
        onChange: true,
        type: 'color',
        withArg: {
            status: true
        }
    }, 
    hiliteColor: {
        label: 'hiliteColor',
        icon: RichEditorSVG.highlight_color,
        onChange: true,
        type: 'color',
        withArg: {
            status: true
        }
    }, 

    createLink: {
        label: 'createLink',
        icon: RichEditorSVG.link,
    }, 
    unlink: {
        label: 'unlink',
        icon: RichEditorSVG.unlink
    }, 
    insertImage: {
        label: 'insertImage',
        icon: RichEditorSVG.image
    }, 
    selectAll: {
        label: 'selectAll',
        icon: '<div>SelectAll</div>'
    },

    toggleCode: {
        label: 'toggleCode',
        icon: RichEditorSVG.code_view
    }
}

const columns = [
    'undo',
    'redo',
    'bold',
    'italic',
    'underline',
    'strikeThrough',
    'justifyLeft',
    'justifyCenter',
    'justifyRight',
    'justifyFull',
    'indent',
    'outdent',
    'subscript',
    'superscript',
    'insertUnorderedList',
    'insertOrderedList',
    'insertParagraph',
    'insertHorizontalRule',


    'createLink',
    'unlink',
    'fontName',
    'fontSize',
    'foreColor',
    'hiliteColor',
    'insertImage',
    'selectAll',
    'toggleCode'
]

const test = [
    'undo',
    'redo',
    'bold',
    'italic',
    'underline',
    'strikeThrough',
    'justifyLeft',
    'justifyCenter',
    'justifyRight',
    'justifyFull',
    'indent',
    'outdent',
    'subscript',
    'superscript',
    'insertUnorderedList',
    'insertOrderedList',
    'insertHorizontalRule',
    'formatBlock',
    'fontName',
    'fontSize',
    'foreColor',
    'hiliteColor',
]

class RichEditor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            designMode: 'On',
            dialogVisible: false,
            imgServerLink: '',
            imgLocalFile: '',
            imgLink: '',
            imgRadio: 'link',
            selectOptions: {
                formatBlock: '',
                fontName: 'Arial',
                fontSize: 10,
                hiliteColor: '',
                foreColor: ''
            },
            isSelectingImage: false
        }

        this.showingSourceCode = false;
        this.isInEditMode = true;
        this.richTextField = React.createRef();
        this.selectedImage = null;

        console.log('helllo', this.richTextField);

    }

    componentDidMount() {
        this.enabledEditMode();

        this.init();
    }

    enabledEditMode = () => {
        this.richTextField.current.contentDocument.designMode = 'On';
    }

    init = () => {
        const targetDocument = this.richTextField.current.contentDocument;
        const targetFrame = targetDocument.getElementsByTagName('body')[0];
        // const targetHead = targetDocument.getElementsByTagName('head')[0];
        // targetHead.innerHTML = '<style>.selectedImage { border: 1px solid #aaa; } img { cursor: pointer }</style>'
        if (targetFrame) {
            targetFrame.addEventListener('mouseup', this.mouseDown);
            // targetFrame.addEventListener('touchstart', this.touchDown);
        }
    }

    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;

        let selectOptions = this.state.selectOptions;
        selectOptions = {
            ...selectOptions,
            [name]: value
        }

        this.setState({
            selectOptions
        });

        this.execCommandWithArg(name, value);
    }

    handleRadioChange = (e, input) => {
        this.setState({
            [input]: e,
            imgServerLink: '',
            imgLocalFile: '',
            imgLink: '',
        })
    }

    execCmd = (command) => {
        this.richTextField.current.contentDocument.execCommand(command, false, null);
    }

    execCommandWithArg = (command, arg) => {
        this.richTextField.current.contentDocument.execCommand(command, false, arg);
    }

    checkExecCommand = (input, command) => {
        if (input === 'execCmd') {
            this.execCmd(command)
        } else {
            this.execCommandWithArg(command)
        }
    }

    handleImgChange = (e, input) => {
        let value, imgLink;
        if (input === 'imgLocalFile') {
            value = e.target.files[0];
            imgLink = window.URL.createObjectURL(value);
        } else {
            value = e.target.value;
            imgLink = value;
        }
        
        this.setState({
            [input]: value,
            imgLink
        })
    }

    cleanImgUpload = () => {
        this.setState({
            dialogVisible: false,
            imgServerLink: '',
            imgLocalFile: '',
            imgLink: '',
            imgRadio: 'link'
        })
    }

    imgUpload = async () => {

        const { imgServerLink, imgLocalFile, imgLink } = this.state;
        let inputImage;
        // validation check!;
        if (imgServerLink) {
            inputImage = imgServerLink;
        } else if (imgLocalFile) {
            const formData = new FormData();
            formData.append('file', imgLocalFile);

            await api.uploadImage(formData, true)
                .then((res) => {
                    if (res && res.data) {
                        inputImage = imgUrl + res.data;
                    }
                })
                .catch((err) => {
                    console.log('something went wrong...', err);
                });
        }

        this.execCommandWithArg('insertImage', inputImage);
        this.cleanImgUpload();
    }

    toggleEdit = () => {
        if (this.isInEditMode) {
            this.richTextField.current.contentDocument.designMode = 'Off';
            this.isInEditMode = false;
        } else {
            this.isInEditMode = true;
        }
    }
    
    toggleSource = () => {
        const targetFrame = this.richTextField.current.contentDocument.getElementsByTagName('body')[0];

        if (this.showingSourceCode) {
            targetFrame.innerHTML = targetFrame.textContent;
            this.showingSourceCode = false;
        } else {
            targetFrame.textContent = targetFrame.innerHTML;
            this.showingSourceCode = true;
        }
    }

    mouseDown = (e) => {
        this.onDown(e);
        e.preventDefault();
    }

    touchDown = (e) => {
        this.onDown(e.touches[0]);
        e.preventDefault();
    }

    onDown = (e) => {
        const target = e.target;
        const targetDocument = this.richTextField.current.contentDocument;
        if (target) {
            const nodeName = target.nodeName;
            if (nodeName && nodeName.toLowerCase() === 'img') {
                this.setState({
                    isSelectingImage: true
                })

                imageResize(target, targetDocument, true);

                /* if (this.selectedImage) {
                    this.selectedImage.classList.remove('selectedImage');
                }

                this.selectedImage = target;
                this.selectedImage.classList.add('selectedImage');

                console.log('you are clicking img'); */
            } else {
                this.setState({
                    isSelectingImage: false
                })

                /* if (this.selectedImage) {
                    this.selectedImage.classList.remove('selectedImage');
                }

                this.selectedImage = null;
                console.log('you are clicking not img'); */
            }
        }
    }

    render() {
        const { imgServerLink, imgLocalFile, imgLink, selectOptions, isSelectingImage } = this.state;

        return (
            <div className="rich-container">
                <div>
                    {
                        test.map((cur, index) => {
                            const { onChange, label, styleName, icon } = RichEditorFnc[cur];
                            const isLabelJustify = label === 'justifyLeft' || label === 'justifyCenter' || label === 'justifyRight' || label === 'justifyFull'
                            return (
                                onChange ? (
                                    onChange.length > 0 ? (
                                        <select className="rich-select" key={index} value={selectOptions[label]} name={label} onChange={this.handleChange}>
                                            {
                                                onChange.map((children, ind) => {
                                                    return <option key={children + ind} label={children} value={children} style={{[styleName]: children}}/>
                                                }) 
                                            }
                                        </select>
                                    ) : (
                                        <Tooltip content={label} placement="bottom" >
                                            <input key={index} type="color" name={label} onInput={this.handleChange} />
                                        </Tooltip>
                                    )
                                ) : (
                                    <Tooltip content={label} placement="bottom" >
                                        <button 
                                            disabled={!isLabelJustify && isSelectingImage} 
                                            className="rich-btn" 
                                            key={index} 
                                            onClick={() => this.execCmd(label)}
                                        >
                                            {parse(icon)}
                                        </button>
                                    </Tooltip>
                                )
                            )
                        })
                    }

                    <Tooltip content={RichEditorFnc.createLink.label} placement="bottom" >
                        <button disabled={isSelectingImage} className="rich-btn" onClick={() => this.execCommandWithArg(RichEditorFnc.createLink.label, prompt('Enter a URL'))}>
                            {parse(RichEditorFnc.createLink.icon)}
                        </button>
                    </Tooltip>
                    <Tooltip content={RichEditorFnc.toggleCode.label} placement="bottom" >
                        <button disabled={isSelectingImage} className="rich-btn" onClick={this.toggleSource}>
                            {parse(RichEditorFnc.toggleCode.icon)}
                        </button>
                    </Tooltip>
                    <Tooltip content={RichEditorFnc.insertImage.label} placement="bottom" >
                        <button disabled={isSelectingImage} className="rich-btn" onClick={() => this.setState({ dialogVisible: true })}>
                            {parse(RichEditorFnc.insertImage.icon)}
                        </button>
                    </Tooltip>
                    {/* <button disabled={isSelectingImage} className="rich-btn" onClick={() => this.execCommandWithArg(RichEditorFnc.createLink.label, prompt('Enter a URL'))}>
                        {parse(RichEditorFnc.createLink.icon)}
                    </button>
                    <button disabled={isSelectingImage} className="rich-btn" onClick={this.toggleSource}>
                        {parse(RichEditorFnc.toggleCode.icon)}
                    </button>
                    <button disabled={isSelectingImage} className="rich-btn" onClick={() => this.setState({ dialogVisible: true })}>
                        {parse(RichEditorFnc.insertImage.icon)}
                    </button> */}
                
                </div>

                <div>
                    <iframe className="rich-frame" name="richTextField" ref={this.richTextField} style={{ marginTop: '15px', width: '1000px', height: '500px', boxShadow: '1px 1px 10px #000', border: 'none'}}>

                    </iframe>
                </div>
                <Dialog
                    title="이미지 업로드"
                    visible={ this.state.dialogVisible }
                    onCancel={this.cleanImgUpload}
                >
                    <Dialog.Body>
                        <Radio.Group value={this.state.imgRadio} onChange={(e) => this.handleRadioChange(e, 'imgRadio')}>
                            <Radio.Button value="link" >URL</Radio.Button>
                            <Radio.Button value="file" >File Upload</Radio.Button>
                        </Radio.Group>
                        {
                            this.state.imgRadio === 'link' ? (
                                <div className="image__upload-link">
                                    <input value={imgServerLink} onChange={(e) => this.handleImgChange(e, 'imgServerLink') }/>
                                </div>
                            ) : (
                                <div>
                                    <button style={{width: '100%', position: 'relative'}}>
                                        <div className="image__upload-file">
                                            <input type="file" name="file" onChange={(e) => this.handleImgChange(e, 'imgLocalFile')} />
                                            Click to Upload
                                        </div>
                                    </button>
                                </div>
                            )
                        }
                        {
                            imgLink && (
                                <div style={{width: '200px', height: '200px', border: '1px solid black', margin: 'auto'}}>
                                    <img src={imgLink} style={{width: '100%', height: '100%', maxWidth: '100%', maxHeight: '100%', objectFit: 'cover'}} />
                                </div>
                            )
                        }
                        
                    </Dialog.Body>

                    <Dialog.Footer className="dialog-footer">
                        <button onClick={this.imgUpload}>확인</button>
                    </Dialog.Footer>
                </Dialog>

                
            </div>
        )
    }
}

export default RichEditor