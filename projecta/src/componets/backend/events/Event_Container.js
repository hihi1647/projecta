import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Event from './Event';
import Events from './Events';
import Event_Create from './Create';
import Event_Edit from './Edit';

import api from 'common/api';

import { NO_FIXED_ID } from 'constants/index';

class Event_Container extends React.Component {
    state = {
        eventCategories: {},
    };
    
    componentDidMount() {
        this.getDatas();
    }

    getDatas = async () => {
        await api.getEventCategories(true)
            .then(res => {
                const resData = res.data.data;

                if (resData) {
                    let fix_options = resData.fix_options;

                    if (fix_options) {
                        fix_options = [
                            {
                                id: NO_FIXED_ID, 
                                fix_option_name: "고정없음"
                            },
                            ...fix_options
                        ]
                    }

                    const eventCategories = {
                        ...resData,
                        fix_options
                    };

                    this.setState({
                        eventCategories
                    })
                }
            })
            .catch(error => {
              console.log('something went wrong with event category', error);
            });
    }

    render() {
        const { eventCategories } = this.state
        const { path } = this.props.match;

        const { $user, isAuthenticated, changeState } = this.props;
        return (
            <Switch>
                <Route path={`${path}/event/:id/edit`} render={(props) => <Event_Edit {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={`${path}/event/create`} render={(props) => <Event_Create {...props} $user={$user} eventCategories={eventCategories} />} />
                <Route path={`${path}/event/:id`} render={(props) => <Event {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={path} render={(props) => <Events {...props} $user={$user} eventCategories={eventCategories} />} />
            </Switch>
        )
    }
}

export default Event_Container;