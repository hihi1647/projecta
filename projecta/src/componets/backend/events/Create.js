import React, { Fragment } from 'react';
import api from 'common/api';
import { Select, Message } from 'element-react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import dateFnsFormat from 'date-fns/format';

import { parseDate, formatDate, formatDateMySQL } from 'common/Utils';
import { DATE_FORMAT, currentUsesDomain, NO_FIXED_ID } from 'constants/index';

import Custom_Image_Font_Quill from 'componets/quill/Custom_Image_Font_Quill';

const imgUrl = currentUsesDomain.imgUrl;


const columnsObj = {
    'title': '제목',
    'writer': '작성자',
    'created_at': '작성일',
    'views': '조회수',
    'start_date': '노출 시작일',
    'end_date': '노출 종료일',
    'sub_title': '본문요약',
    'fix_option_id': '고정',
    'photo_file': '대표 이미지',
    'event_status': '노출 상태',
    'details': '내용'
}
  
const columns = [
    'writer',
    'created_at',
    'views',
    'fix_option_id',
    'event_status',
    'start_date',
    'end_date',
    'title',
    'sub_title',
    'details',
    'photo_file'
];

class Backend_Event_Create extends React.Component {
    constructor(props) {
        super(props);
    
        this.state = {
            event: {
                writer: '',
                created_at: new Date(),
                views: 0,
                fix_option_id: NO_FIXED_ID,
                event_status_id: 0,
                start_date: new Date(),
                end_date: new Date(),
                title: '',
                sub_title: '',
                details: '',
                photo_file: '',
                endDateValue: ''
            },
            event_status_menu: [
                {id: 0, label: '진행중'},
                {id: 1, label: '종료'}
            ],
            photo_file: ''
        }
    
        this.history = this.props.history;
        this.match = this.props.match;
    }
    
    createEvent = async () => {

        // input validation form check
        let validationCheck = true;

        const formData = new FormData();
        const eventData = this.state.event;
        const payloadKey = [
            'writer',
            'created_at',
            'views',
            'fix_option_id',
            'start_date',
            'end_date',
            'title',
            'sub_title',
            'details',
            'photo_file'
        ];
        payloadKey.forEach(cur => {
            if (cur === 'created_at') {
                if (typeof eventData[cur] !== 'string') {
                    formData.append(cur, formatDateMySQL(eventData[cur]));
                    // formData.append(cur, eventData[cur].toUTCString());
                }
            } else if (cur === 'start_date' && eventData[cur]) {
                let startDate = eventData[cur];
                startDate = typeof startDate !== 'string' ? startDate.toJSON() : startDate;
                startDate = startDate.slice(0, 10) + " 00:00:00";

                formData.append(cur, startDate);
            } else if (cur === 'end_date' && eventData[cur]) {
                let endDate = eventData[cur];
                endDate = typeof endDate !== 'string' ? endDate.toJSON() : endDate;
                endDate = endDate.slice(0, 10) + " 23:59:59";

                formData.append(cur, endDate);
            } else if (cur === 'fix_option_id' && eventData[cur]) {
                let fix_option_id = eventData[cur];
                if (fix_option_id !== NO_FIXED_ID) {
                    formData.append(cur, eventData[cur]);
                }
            } else {
                formData.append(cur, eventData[cur]);
            }
        
        })

        if ( validationCheck ) {
            await api.createEvent(formData, true)
                .then(res => {
                    if (res) {
                        Message({
                            message: '성공적으로 생성되었습니다.',
                            type: 'success'
                        });
                    }

                    console.log('successfully created', res);
                })
                .then(() => {
                    this.goBack();
                })
                .catch(error => {
                    console.log('something went wrong', error);
                })
        }
    }


    cancel = () => {
    }
    
    goBack = () => {
        this.history.push('/backend/events');
    }

    handleChange = (event, input) => {

        let newData = {
            ...this.state.event
        };
        
        if (
            input === 'created_at' || 
            input === 'fix_option_id' ||
            input === 'start_date' ||
            input === 'details'
        ) {
            newData = {
                ...newData,
                [input]: event
            }
        } else if (input === 'event_status_id') {
            if (event === 1) {
              const end_date = new Date();
              newData = {
                ...newData,
                [input]: event,
                end_date,
                endDateValue: end_date
              }
            } else {
              newData = {
                ...newData,
                [input]: event
              }
            }          
  
        } else if (input === 'end_date') {
            if (event) {
                let selectedEndDateTime = event.toJSON().slice(0, 10) + " 23:59:59";
                console.log('this is it,', selectedEndDateTime, event, event.toJSON(), event.toJSON().slice(0, 10));
                selectedEndDateTime = new Date(selectedEndDateTime).getTime();
                const currentTime = new Date().getTime();
    
                const event_status_id = selectedEndDateTime >= currentTime ? 0 : 1;
                newData = {
                ...newData,
                [input]: event,
                event_status_id
                }
            }
        } else {
            newData = {
            ...newData,
            [input]: event.target.value
            };
        }

        this.setState({
            event: newData
        })
    }

    handleImgChange = (e, input) => {

        let newData = {
            ...this.state.event
        };

        newData = {
            ...newData,
            [input]: e.target.files[0]
        };

        this.setState({
            event: newData,
            [input]: window.URL.createObjectURL(e.target.files[0])
        })
    }

    render() {
        const { event, event_status_menu, photo_file } = this.state;
        const eventCategories = this.props.eventCategories;
        console.log('this is create event', event, this.props, this.state);
        
        return (
            <Fragment>
                <div className="event-create-body">
                    <div className="backend-boardTop-style">
                        <h2 className="backend-boardTop-title">이벤트</h2>
                        <p className="backend-boardTop-text">관리자 글쓰기 페이지 입니다.</p>
                    </div>
                    <ul className="backend-board-create-wrap">
                    {
                    columns.slice(0,3).map(column => (
                        <Fragment key={column} >
                            <li>
                                <p className="backend-board-title">{columnsObj[column]}</p>
                                <span className="backend-board-text">
                                    {
                                        column === 'created_at' ? (
                                            <DayPickerInput
                                                formatDate={formatDate}
                                                format={DATE_FORMAT}
                                                parseDate={parseDate}
                                                placeholder={(!event.created_at || typeof event.created_at === 'string') ? `${dateFnsFormat(new Date(), DATE_FORMAT)}` : `${dateFnsFormat(event.created_at, DATE_FORMAT)}`}
                                                onDayChange={(e) => this.handleChange(e, 'created_at')}
                                            />
                                        ) : (
                                            <input className="backend-board-create-input" placeholder="작성자 이름을 입력하세요." type="text" value={event[column]} onChange={(e) => this.handleChange(e, column)} />
                                        )
                                    }
                                </span>
                            </li>
                        </Fragment>
                    ))
                    }
                
                
                    <li>
                        <p className="backend-board-title">{columnsObj.fix_option_id}</p>
                        <span className="backend-board-text">
                            <Select value={event.fix_option_id} placeholder="고정 선택" onChange={(e) => this.handleChange(e, 'fix_option_id')}>
                                {(eventCategories && eventCategories.fix_options && eventCategories.fix_options.length > 0) ? eventCategories.fix_options.map(el => (
                                    <Select.Option key={el.id} label={el.fix_option_name} value={el.id} />
                                )) : null}
                            </Select>
                        </span>
                    </li>

                    <li>
                        <p className="backend-board-title">{columnsObj.event_status}</p>
        
                        <span className="backend-board-text">
                            <Select value={event.event_status_id} placeholder="노출 상태" onChange={(e) => this.handleChange(e, 'event_status_id')}>
                                {(event_status_menu && event_status_menu.length > 0) ? event_status_menu.map(el => (
                                <Select.Option key={el.id} label={el.label} value={el.id} />
                                )) : null}
                            </Select>
                        </span>
                    </li>
           

                    <li>
                        <p className="backend-board-title">{columnsObj.start_date}</p>
                    
                        <span className="backend-board-text">
            
                        <DayPickerInput
                            formatDate={formatDate}
                            format={DATE_FORMAT}
                            parseDate={parseDate}
                            placeholder={(!event.start_date || typeof event.start_date === 'string') ? `${dateFnsFormat(new Date(), DATE_FORMAT)}` : `${dateFnsFormat(event.start_date, DATE_FORMAT)}`}
                            onDayChange={(e) => this.handleChange(e, 'start_date')}
                        />
                        </span>
                    </li>

                    <li>
                        <p className="backend-board-title">{columnsObj.end_date}</p>
                        <span className="backend-board-text">
                        <DayPickerInput
                            formatDate={formatDate}
                            format={DATE_FORMAT}
                            parseDate={parseDate}
                            value={event.endDateValue}
                            placeholder={(!event.end_date || typeof event.end_date === 'string') ? `${dateFnsFormat(new Date(), DATE_FORMAT)}` : `${dateFnsFormat(event.end_date, DATE_FORMAT)}`}
                            onDayChange={(e) => this.handleChange(e, 'end_date')}
                        />
                        </span>
                    </li>
              

                <li>
                  <p className="backend-board-title">{columnsObj.title}</p>
                  <span className="backend-board-text">
                    <input className="backend-board-create-input" placeholder="제목을 입력하세요." type="text" value={event.title} onChange={(e) => this.handleChange(e, 'title')} />
                  </span>
                </li>

                {/* 본문요약부분 주석
                <li>
                  <p className="backend-board-title">{columnsObj.sub_title}</p>
                  <p className="backend-board-text">
                    <input className="backend-board-create-input" type="text" value={event.sub_title} onChange={(e) => this.handleChange(e, 'sub_title')} />
                  </p>
                </li> */}

                <li>
                  <p className="backend-board-title">{columnsObj.details}</p>
                  <p className="backend-board-text">
                    <Custom_Image_Font_Quill setContents={event.details} onChange={(e) => this.handleChange(e, 'details')} />
                  </p>
                </li>
                <li>
                  <p className="backend-board-title">{columnsObj.photo_file}</p>
                    <span className="backend-create-photo">
                        {photo_file && <img src={photo_file} style={{width: '200px'}} />}
                       <input className="backend-file-input" type="file" name="file" onChange={(e) => this.handleImgChange(e, 'photo_file')} />
                       <p style={{fontSize:'12px',paddingTop:'4px',color:'#f77a99' }}>* 대표 이미지 사이즈는 <b>566 X 283</b>를 권장합니다.</p>
                    </span>
                </li>
                </ul>  
            
    
            <div style={{textAlign: 'right'}} className="backend-topBtn-wrap">
               
                <button className="btn-backbend btn-edit" onClick={this.createEvent}>글쓰기</button>
                <button className="btn-backbend btn-warning" onClick={this.goBack}>삭제하기</button>
                <button className="btn-backbend btn-check" onClick={this.goBack}>목록으로</button>
            </div>
            </div>  
          </Fragment>
        )
    }
}

export default Backend_Event_Create;