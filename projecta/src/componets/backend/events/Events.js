import React from 'react';
import api from 'common/api';

import { Select, Pagination, Message } from 'element-react';

const columns = [
  { prop: 'checkbox', label: '선택', width: 50 },
  { prop: 'title', label: '제목', width: 170 },
  { prop: 'created_at', label: '작성일', width: 170 },
  { prop: 'views', label: '조회수', width: 170 }
];

let rows = [];

function getIndexedRows(datas) {
  let numFixedIndex = 0;
  let numNotFixedIndex = 0;
  let rows = [];

  datas.forEach(data => {
    // 고정된 노티스일 때
    if (data.is_fix) {
      const fixedArr = data.events.map(fixedData => {

        let created_at = fixedData.created_at;
        created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;

        return {
          ...fixedData,
          created_at,
          checkbox: false,
          is_fix: data.is_fix
        };
      });
      rows = [...rows, ...fixedArr];

    } else {
      // 고정된 노티스가 아닐 때
      // index값은 페이지당 고정되지 않은 노티스 개수 * 현재 페이지 + 고정된 노티스부터 시작 
      numNotFixedIndex = data.events.per_page * (data.events.current_page - 1) + numFixedIndex;

      const notFixedArr = data.events.data.map(notFixedData => {
        numNotFixedIndex += 1;

        let created_at = notFixedData.created_at;
        created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;

        return {
          numIndex: numNotFixedIndex,
          ...notFixedData,
          created_at,
          checkbox: false,
          is_fix: data.is_fix
        };
      });
      rows = [...rows, ...notFixedArr];
    }
  });

  return rows;
}

function getSearchedIndexedRows(datas) {
  let numNotFixedIndex = 0;

  numNotFixedIndex = datas.per_page * (datas.current_page - 1);

  return datas.data.map(data => {

    let created_at = data.created_at;
    created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;
    const is_fix = !!data.fix_option_id;
    return {
      ...data,
      numIndex: ++numNotFixedIndex,
      created_at,
      checkbox: false,
      is_fix
    }
  })

}

class Backend_Events extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      total: 0,
      perPage: 0,
      checkboxObj: [],
      searchData: {
        searchOption: 0,
        query: ''
      },
      isSearched: false,
      checkAll: false
    }
  }

  componentDidMount() {
    this.getDatas();
  }

  getDatas = async (page) => {
    await api.getEvents(page)
      .then(res => {
        const resData = res.data.data;

        if (resData) {
          rows = getIndexedRows(resData);

          const notFixedDatas = res.data.data.filter(cur => !cur.is_fix)[0].events;

          this.setState({
            page: notFixedDatas.current_page,
            total: notFixedDatas.last_page,
            perPage: notFixedDatas.per_page,
            checkboxObj: rows,
            isSearched: false
          });
        }
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  getSearchedDatas = async (page) => {
    const { searchData } = this.state;
    const payload = {
      search_word: searchData.query,
      search_option_id: searchData.searchOption
    };

    await api.getSearchEvent(payload, page, true)
      .then(res => {
        const resData = res.data.data;

        if (resData) {
          rows = getSearchedIndexedRows(resData);

          this.setState({
            checkboxObj: rows,
            total: resData.last_page,
            page: resData.current_page,
            perPage: resData.per_page,
            isSearched: true
          })
          console.log('successfully searched..', res, rows);
        }
      
      })
      .catch(error => {
        console.log(error);
      })
  }

  handleChangePage = (selectedPage) => {
    const { isSearched } = this.state;
    if (isSearched) {
      this.getSearchedDatas(selectedPage);
    } else {
      this.getDatas(selectedPage);
    }
  };

  handleCheckBox = (event, id) => {
    const ind = this.state.checkboxObj.findIndex(cur => cur.id === id);
    const newTarget = {
      ...this.state.checkboxObj[ind],
      checkbox: event.target.checked
    }
    let newCheckboxObj = [
      ...this.state.checkboxObj.slice(0, ind),
      newTarget,
      ...this.state.checkboxObj.slice(ind + 1),
    ]
    
    const checkAll = newCheckboxObj.length === newCheckboxObj.filter(cur => cur.checkbox).length;
    this.setState({
      checkboxObj: newCheckboxObj,
      checkAll
    })
  } 

  handleSelectAll = () => {
    let checkAll = !this.state.checkAll;

    let newCheckboxObj = this.state.checkboxObj.map(cur => ({
      ...cur,
      checkbox: checkAll
    }));

    this.setState({
      checkboxObj: newCheckboxObj,
      checkAll
    })
  }

  searchClickHanlder = async () => {
    console.log('clicking...', this.state.searchData);
    const { searchData, isSearched } = this.state;

    if (searchData.searchOption === 0 && searchData.query === '') {
      if (isSearched) {
        this.getDatas();
      }
    } else {
      this.getSearchedDatas();
    }
  }

  searchOptionChangeHandler = (value) => {
    this.searchChangeHandler(value,'searchOption');
  }

  searchChangeHandler = (target, type) => {
    let newSearchData = {
      ...this.state.searchData
    };
    if (type === 'query') {
      newSearchData = {
        ...newSearchData,
        [type]: target.target.value
      }
    } else {
      newSearchData = {
        ...newSearchData,
        [type]: target
      };
    }

    this.setState({
      searchData: newSearchData
    });
  }

  searchClear = () => {
    this.setState({
      searchData: {
        searchOption: 0,
        query: ''
      }
    })
  }

  handleLink = (id) => {
    this.props.history.push(`${this.props.match.url}/event/${id}`);
  }

  createEvent = () => {
    this.props.history.push(`${this.props.match.url}/event/create`);
  }

  deleteEvent = async () => {

    const checkedArr = this.state.checkboxObj.filter(data => data.checkbox);
    const checkedIDs = checkedArr.map(data => data.id); 
    if (checkedIDs.length > 0) {
       await api.deleteEventsAll({ids: checkedIDs})
        .then(res => {
          Message({
            message: '성공적으로 삭제되었습니다.',
            type: 'success'
          });
        })
        .then(() => {
          this.getDatas();
        })
        .catch(err => {
          console.log('something went wrong', err);
        });
    }
  }

  render() {
    console.log('this is backend Events', this.state, this.props);
    const { searchData, checkboxObj } = this.state;
    const { searchOption, query } = searchData;
    const eventCategories = this.props.eventCategories;
    return (
      <div className="backend-body">
        <div className="backend-table-wrap">
          <div className="backend-boardTop-style">
            <h2 className="backend-boardTop-title">이벤트</h2>
            <p className="backend-boardTop-text">이벤트 관리자 페이지 입니다.</p>
          </div>

        <div className="backend-topBtn-wrap">
          <button className="btn-backbend btn-check" onClick={this.handleSelectAll}>{this.state.checkAll ? '전체해제' : '전체선택'}</button>
          <button className="btn-backbend btn-warning" onClick={this.deleteEvent}>선택삭제</button>
        </div>

        <table>
          <colgroup>
            <col className="col-100"/>
            <col/>
            <col className="col-100"/>
            <col className="col-100"/>
          </colgroup>
          <thead>
            <tr>
              {
                columns.map((column, index) => (
                  <td key={index} >
                    {column.label}
                  </td>
                ))
              }
            </tr>
          </thead>
          <tbody className="backend-tbody col-03">
          {
            checkboxObj.map((row, index) => (
              <tr key={index} >
                {
                  columns.map((column, colIndex) => {
                    return column.prop === 'checkbox' ? (
                      <td key={index + column.prop + colIndex}>
                        <input type="checkbox" checked={row[column.prop]} onChange={(e) => this.handleCheckBox(e, row.id)} />
                      </td>
                    ) : (
                      column.prop === 'title' ? (
                        <td key={index+ column.prop + colIndex} onClick={() => this.handleLink(row.id)}>
                          {
                            row.is_fix && <span>{row.fix_option_name}</span>
                          }
                          {row[column.prop]}
                        </td>
                      ) : (
                      <td key={index + column.prop + colIndex} onClick={() => this.handleLink(row.id)}>
                        {row[column.prop]}
                      </td>
                      )
                    )
                  })
                }
              </tr>
            ))
          }
          </tbody>
        </table>
        </div>
        <br />
        
        <button className="btn-backbend btn-backend-writing" onClick={this.createEvent}>글쓰기</button>
        <Pagination 
          style={{display:'flex',justifyContent:'center'}} 
          layout="prev, pager, next" 
          currentPage={this.state.page} 
          pageCount={this.state.total} 
          pageSize={this.state.perPage} 
          small={true}
          onCurrentChange={this.handleChangePage}
        />
        <div className="board-search">
        
          <div className="search-box">
              <Select value={searchData.searchOption} placeholder="글제목" onChange={this.searchOptionChangeHandler}>
                {eventCategories.search_options ? eventCategories.search_options.map(el => {
                  return <Select.Option key={el.id} label={el.search_option_name} value={el.id} />
                }) : null}
              </Select>
            </div>
            <span>
              <input className="board-input search-box" type="text" value={query} onChange={(e) => this.searchChangeHandler(e, 'query')} />
              {
                (searchOption || query) && <i className="el-icon-circle-cross" onClick={this.searchClear}></i> 
              }
            </span>
            <button className="btn-search search-box" onClick={this.searchClickHanlder}>검색</button>
          </div>

        </div>
      
    )
  }

}

export default Backend_Events;
