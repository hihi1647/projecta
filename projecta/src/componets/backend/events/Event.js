import React, { Fragment } from 'react';
import api from 'common/api';
import { Link } from 'react-router-dom';
import parse from 'html-react-parser';
import { Message } from 'element-react';

import { currentUsesDomain } from 'constants/index';

const imgUrl = currentUsesDomain.imgUrl;

const columnsObj = {
  'title': '제목',
  'writer': '작성자',
  'created_at': '작성일',
  'views': '조회수',
  'start_date': '노출 시작일',
  'end_date': '노출 종료일',
  'sub_title': '본문요약',
  'fix_option_name': '고정',
  'photo_file': '대표 사진',
  'event_status': '노출 상태',
  'details': '내용'
}

const columns = [
  'writer',
  'created_at',
  'views',
  'fix_option_name',
  'event_status',
  'start_date',
  'end_date',
  'title',
  'sub_title',
  'details',
  'photo_file'
];

class Backend_Event extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      event: {}
    }

    this.history = this.props.history;
    this.match = this.props.match;
    this.currentEventID = this.match.params.id;
  }

  componentDidMount() {
    this.getDatas(this.currentEventID);
  }

  getDatas = async (currentEventID) => {
    await api.getEvent(currentEventID, true)
      .then(res => {

        const resData = res.data.data;

        if (resData) {
          let { created_at, start_date, end_date } = resData;

          const end_date_time = end_date ? ( typeof end_date === 'string' ? new Date(end_date).getTime() : end_date.getTime() ) : end_date;

          created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;
          start_date = start_date ? ( typeof start_date === 'string' ? start_date.slice(0, 10) : start_date.toJSON().slice(0,10) ) : start_date;
          end_date = end_date ? ( typeof end_date === 'string' ? end_date.slice(0, 10) : end_date.toJSON().slice(0,10) ) : end_date;
          
          let event_status = '진행중';

          const currentTime = new Date().getTime();

          if (end_date_time < currentTime) {
            event_status = '종료'
          }

          const event = {
            ...resData,
            created_at,
            start_date,
            end_date,
            event_status
          }

          this.setState({
            event
          })
        }
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  editLink = () => {
    this.history.push(`${this.match.url}/edit`);
  }

  deleteEvent = async () => {

    /* handleOpen('snackbar');
    handleClose('dialog'); */

    await api.deleteEvent(this.currentEventID, true)
      .then((res)=> {
        Message({
          message: '성공적으로 삭제되었습니다.',
          type: 'success'
        });
      })
      .then(() => {
        this.goBack();
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  goBack = () => {
    this.history.push('/backend/events');
  }

  handleLink = (targetPage) => {
    if (targetPage) {
      const newURL = this.match.url.replace(':id', targetPage.id);
      this.history.push(newURL);
    }

    console.log('clicking Links...', targetPage);
  }

  render() {
    const event = this.state.event;
    console.log('this is event', event);
    return (
      <div className="backend-body">
        <div className="backend-boardTop-style">
          <h2 className="backend-boardTop-title">이벤트</h2>
          <span className="backend-boardTop-text">이벤트 관리자 페이지 입니다.</span>
        </div>
        {
          event.id ? (
            <Fragment>
              <ul className="backend-board-wrap">
                  {
                    columns.slice(0,3).map(column => (
                      <Fragment key={column} >
                        <li>
                          <span className="backend-board-title">{columnsObj[column]}</span>
                          <span className="backend-board-text">{event[column]}</span>
                        </li>
                      </Fragment>
                    ))
                  }
               
                  {
                    columns.slice(3,5).map(column => (
                      <Fragment key={column} >       
                        <li> 
                          <span className="backend-board-title">{columnsObj[column]}</span> 
                          <span className="backend-board-text">{event[column]}</span>
                        </li>
                      </Fragment>
                    ))
                  }
              
              
                  {
                    columns.slice(5,7).map(column => (
                      <Fragment key={column} >
                        <li>
                          <span className="backend-board-title">{columnsObj[column]}</span>
                          <span className="backend-board-text">{event[column]}</span>
                        </li>
                      </Fragment>
                    ))
                  }

                <li>
                  <span className="backend-board-title">{columnsObj.title}</span>
                  <span className="backend-board-text">{event.title}</span>
                </li>
                <li>
                  <span className="backend-board-title">{columnsObj.sub_title}</span>
                  <span className="backend-board-text">{event.sub_title}</span>
                </li>

                <li>
                  <span className="backend-board-title">{columnsObj.details}</span>
                  <span className="backend-board-text">
                    {typeof event.details === 'string' ? parse(event.details) : event.details}
                  </span>
                </li>
                <li>
                  <span className="backend-board-title">{columnsObj.photo_file}</span>
                  <span className="backend-board-text">
                    <img className="beforeAfter-img" src={imgUrl + event.photo_file} style={{width: '520px'}}/>
                  </span>
                </li>
              </ul>

              <div style={{textAlign: 'right'}} className="backend-topBtn-wrap">
                
                <button className="btn-backbend btn-edit" onClick={this.editLink}>수정하기</button>
                <button className="btn-backbend btn-warning" onClick={this.deleteEvent}>삭제하기</button>
                <button className="btn-backbend btn-check" onClick={this.goBack}>목록으로</button>
              </div>

              <div className="prev-contianer">
                {
                  event.previous ? (
                    <Fragment>
                      <div className="prev-wrap prev-wrap-boder">
                        <span className="border-arrow">
                          <Link to={`${this.match.path.replace(':id', event.previous.id)}`}>
                            <svg  className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><spanath d="M0 16.67l2.829 2.83 9.175-9.339 9.167 9.339 2.829-2.83-11.996-12.17z" fill="##615454"/></svg>
                            이전글
                          </Link>
                        </span>
                      <span className="border-prev-body">{event.previous.title}</span>
                    </div>
                    </Fragment>
                  ) : null
                }
                {
                  event.next ? (
                    <Fragment>
                      <div className="prev-wrap">
                        <div className="border-arrow">
                          <Link to={`${this.match.path.replace(':id', event.next.id)}`}>
                          <svg className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><spanath d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z" fill="##615454"/></svg>
                            다음글
                          </Link>
                      </div>
                        <span className="border-prev-body">{event.next.title}</span>
                      </div>
                    </Fragment>
                  ) : null
                }
              </div>
            </Fragment>
          ) : (
            <div>
              something went wrong..
            </div>
          )
        }
      </div>
    )
  }

}

export default Backend_Event;