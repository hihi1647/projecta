import React, { Fragment } from 'react';
import api from 'common/api';
import { Link } from 'react-router-dom';
import parse from 'html-react-parser';

const columnsObj = {
  'title': '제목',
  'writer': '작성자',
  'created_at': '작성일',
  'views': '조회수'
}

const columns = [
  'title',
  'writer',
  'created_at',
  'views'
];

class Backend_Notice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notice: {}
    }

    this.history = this.props.history;
    this.match = this.props.match;
    this.currentNoticeID = this.match.params.id;
  }

  componentDidMount() {
    this.getNotice(this.currentNoticeID);
  }

  getNotice = async (currentNoticeID) => {
    await api.getNotice(currentNoticeID, true)
      .then(res => {
        this.setState({
          notice: res.data.data
        });
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  editNotice = () => {
    this.history.push(`${this.match.url}/edit`);
  }

  deleteNotice = async () => {

    /* handleOpen('snackbar');
    handleClose('dialog'); */

    await api.deleteNotice(this.currentNoticeID, true)
      .then((res)=> {
        alert('successfully removed');
      })
      .then(() => {
        this.goBack();
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  goBack = () => {
    this.history.push('/backend/notices');
  }

  handleLink = (targetPage) => {
    if (targetPage) {
      const newURL = this.match.url.replace(':id', targetPage.id);
      this.history.push(newURL);
    }

    console.log('clicking Links...', targetPage);
  }

  render() {
    const notice = this.state.notice;
    console.log('this is notice', notice);
    return (
      <div className="backend-body">
          <div className="backend-boardTop-style">
            <h2 className="backend-boardTop-title">공지사항</h2>
            <span className="backend-boardTop-text">공지사항 관리자 페이지 입니다.</span>
          </div>
        {
        notice.id ? (
          <Fragment>
            <ul className="backend-board-wrap">
            {
              columns.slice(1).map(column => (
                <Fragment key={column} >
                  <li>
                    <span className="backend-board-title">{columnsObj[column]}</span>
                    <span className="backend-board-text">{notice[column]}</span>
                  </li>
                </Fragment>
              ))
            }

            <li>
              <span className="backend-board-title">구분</span>
              <span className="backend-board-text">{notice.notice_category_name}</span>
            </li>

            <li>
              <span className="backend-board-title">고정</span>
              <span className="backend-board-text">{notice.is_fix ? 'O' : 'X'}</span>
            </li>

            <li>
              <span className="backend-board-title">{columnsObj[columns[0]]}</span>
              <span className="backend-board-text">{notice[columns[0]]}</span>
            </li>
            <li>
              <span className="backend-board-title">내용</span>
              <span className="backend-board-text">{parse(notice.details)}</span>
            </li>
          </ul>
          
          <div style={{textAlign: 'right'}} className="backend-topBtn-wrap">
            
            <button className="btn-backbend btn-edit" onClick={this.editNotice}>수정하기</button>
            <button className="btn-backbend btn-warning" onClick={this.deleteNotice}>삭제하기</button>
            <button className="btn-backbend btn-check" onClick={this.goBack}>목록으로</button>
          </div>

          <div className="prev-contianer">
            {
              notice.previous ? (
                <Fragment>
                  <div className="prev-wrap prev-wrap-boder">
                    <span className="border-arrow">
                      <Link to={`${this.match.path.replace(':id', notice.previous.id)}`}>
                      <svg  className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><spanath d="M0 16.67l2.829 2.83 9.175-9.339 9.167 9.339 2.829-2.83-11.996-12.17z" fill="##615454"/></svg>
                        이전글
                      </Link>
                    </span>
                    <span className="border-prev-body">{notice.previous.title}</span>
                  </div>
                </Fragment>
              ) : null
            }
            {
              notice.next ? (
                <Fragment>
                  <div className="prev-wrap">
                    <span className="border-arrow">
                    <Link to={`${this.match.path.replace(':id', notice.next.id)}`}>
                    <svg className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><spanath d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z" fill="##615454"/></svg>
                      다음글
                    </Link>
                  </span>
                  <span className="border-prev-body">{notice.next.title}</span>
                  </div>
                </Fragment>
              ) : null
            }
          </div>

          

        </Fragment>
        ) : (
          <div>
            something went wrong..
          </div>
        )
      }

      </div>
    )
  }
}

export default Backend_Notice;