import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Notice from './Notice';
import Notices from './Notices';
import Notice_Create from './Create';
import Notice_Edit from './Edit';

import api from 'common/api';

class Notice_Container extends React.Component {
    state = {
        noticeCategories: {},
    };
    
    componentDidMount() {
        this.getDatas();
    }

    getDatas = async () => {
        await api.getNoticeCategories(true)
            .then(res => {
                const resData = res.data.data;
                if (resData) {
                    let noticeCategories = resData;
                    let fix_options = [
                        {
                          id: 0,
                          label: '고정 없음'
                        },
                        {
                          id: 1,
                          label: '고정'
                        }
                    ];

                    noticeCategories = {
                        ...noticeCategories,
                        fix_options
                    }

                    this.setState({
                        noticeCategories
                    })
                }
            })
            .catch(error => {
                console.log('something went wrong with category', error);
            })
    }

    render() {
        const { noticeCategories } = this.state
        const { path } = this.props.match;

        const { $user, isAuthenticated, changeState } = this.props;
        return (
            <Switch>
                <Route path={`${path}/notice/:id/edit`} render={(props) => <Notice_Edit {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={`${path}/notice/create`} render={(props) => <Notice_Create {...props} $user={$user} noticeCategories={noticeCategories} />} />
                <Route path={`${path}/notice/:id`} render={(props) => <Notice {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={path} render={(props) => <Notices {...props} $user={$user} noticeCategories={noticeCategories} />} />
            </Switch>
        )
    }
}

export default Notice_Container;