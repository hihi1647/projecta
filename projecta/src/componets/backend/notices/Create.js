import React, { Fragment } from 'react';
import api from 'common/api';
import { Select, Message } from 'element-react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import dateFnsFormat from 'date-fns/format';

import { parseDate, formatDate } from 'common/Utils';
import { DATE_FORMAT } from 'constants/index';

import Custom_Image_Font_Quill from 'componets/quill/Custom_Image_Font_Quill';

const columnsObj = {
  'title': '제목',
  'writer': '작성자',
  'created_at': '작성일',
  'views': '조회수'
}

const columns = [
  'title',
  'writer',
  'created_at',
  'views'
];

class Backend_Notices_Create extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      notice: {
        title: '',
        writer: '',
        created_at: new Date(),
        views: 0,
        details: '',
        notice_category_id: '',
        is_fix: 0
      }
    }

    this.history = this.props.history;
    this.match = this.props.match;
  }

  createNotice = async () => {

    // input validation form check
    let validationCheck = true;
    let payload = { ...this.state.notice };

    if ( validationCheck ) {
      await api.createNotice(payload, true)
      .then(res => {
        console.log('successfully created', res);
        Message({
          message: '성공적으로 생성되었습니다.',
          type: 'success'
        });
        this.goBack();
      })
      .catch(error => {
        console.log('something went wrong', error);
      })
    }
  }


  cancel = () => {
  }

  goBack = () => {
    this.history.push('/backend/notices');
  }

  handleChange = (event, input) => {

    let newNotice = {
      ...this.state.notice
    };

    if (input === 'created_at' || input === 'is_fix' || input === 'notice_category_id' || input === 'details') {
      newNotice = {
        ...newNotice,
        [input]: event
      }
    } else {
      newNotice = {
        ...newNotice,
        [input]: event.target.value
      };
    }

    this.setState({
      notice: newNotice
    })
  }

  render() {
    const notice = this.state.notice;
    const noticeCategories = this.props.noticeCategories;
    console.log('this is create notice', noticeCategories, notice, this.props);

    return (
      <Fragment>
        <div className="event-create-body">
          <div className="backend-boardTop-style">
              <h2 className="backend-boardTop-title">공지사항</h2>
              <p className="backend-boardTop-text">관리자 글쓰기 페이지 입니다.</p>
          </div>
          <ul className="backend-board-create-wrap">
          {
            columns.slice(1,2).map(column => (
              <li key={column} >
                  <p className="backend-board-title">{columnsObj[column]}</p>
                  <span className="backend-board-text">
                    <input className="backend-board-create-input" placeholder="작성자 이름을 입력하세요." type="text" value={notice[column]} onChange={(e) => this.handleChange(e, column)} />
                  </span>
              </li>
            ))
          }

          <li>
            <p className="backend-board-title">{columnsObj['created_at']}</p>
            <span className="backend-board-text">

            <DayPickerInput
              formatDate={formatDate}
              format={DATE_FORMAT}
              parseDate={parseDate}
              placeholder={`${dateFnsFormat(new Date(), DATE_FORMAT)}`}
              onDayChange={(e) => this.handleChange(e, 'created_at')}
            />
            
            </span>
          </li>

          <li>
            <p className="backend-board-title">구분</p>
            <span className="backend-board-text">
              <Select value={notice.notice_category_id} placeholder="구분 선택" onChange={(e) => this.handleChange(e, 'notice_category_id')}>
                {(noticeCategories && noticeCategories.notice_categories && noticeCategories.notice_categories.length > 0) ? noticeCategories.notice_categories.map(el => (
                  <Select.Option key={el.id} label={el.notice_category_name} value={el.id} />
                )) : null}
              </Select>
            </span>
          </li>

          <li>
            <p className="backend-board-title">고정</p>
            <span className="backend-board-text">
              <Select value={notice.is_fix} placeholder="고정 선택" onChange={(e) => this.handleChange(e, 'is_fix')}>
                {(noticeCategories && noticeCategories.fix_options && noticeCategories.fix_options.length > 0) ? noticeCategories.fix_options.map(el => (
                  <Select.Option key={el.id} label={el.label} value={el.id} />
                )) : null}
              </Select>
            </span>
          </li>

          <li>
            <p className="backend-board-title">{columnsObj[columns[0]]}</p>
            <span className="backend-board-text">
              <input className="backend-board-create-input" placeholder="제목을 입력하세요." type="text" value={notice.title} onChange={(e) => this.handleChange(e, columns[0])} />
            </span>
          </li>
          <li>
            <p className="backend-board-title">내용</p>
            <span className="backend-board-text">
            <Custom_Image_Font_Quill setContents={notice.details} onChange={(e) => this.handleChange(e, 'details')} />
            </span>
          </li>
        </ul>
       

        <div style={{textAlign: 'right'}} className="backend-topBtn-wrap">
          
          <button className="btn-backbend btn-edit" onClick={this.createNotice}>글쓰기</button>
          <button className="btn-backbend btn-warning" onClick={this.goBack}>삭제하기</button>
          <button className="btn-backbend btn-check" onClick={this.goBack}>목록으로</button>
      
        </div>
        </div>
      </Fragment>
    )
  }
}

export default Backend_Notices_Create;