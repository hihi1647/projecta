import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Before_after from './Before_after';
import Before_afters from './Before_afters';
import Before_after_Create from './Create';
import Before_after_Edit from './Edit';

import api from 'common/api';

import { NO_FIXED_ID } from 'constants/index';

class BeforeAfter_Container extends React.Component {
    state = {
        beforeAfterCategories: {},
    };
    
    componentDidMount() {
        this.getDatas();
    }

    getDatas = async () => {
        await api.getBeforeAftersCategories(true)
            .then(res => {
                const resData = res.data.data;

                if (resData) {
                    let fix_options = resData.fix_options;

                    if (fix_options) {
                        fix_options = [
                            {
                                id: NO_FIXED_ID, 
                                fix_option_name: "고정없음"
                            },
                            ...fix_options
                        ]
                    }

                    const beforeAfterCategories = {
                        ...resData,
                        fix_options
                    };

                    this.setState({
                        beforeAfterCategories
                    })
                }
                
            })
            .catch(error => {
                console.log('something went wrong with category', error);
            })
    }

    render() {
        const { beforeAfterCategories } = this.state
        const { path } = this.props.match;

        const { $user, isAuthenticated, changeState } = this.props;
        return (
            <Switch>
                <Route path={`${path}/before_after/:id/edit`} render={(props) => <Before_after_Edit {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={`${path}/before_after/create`} render={(props) => <Before_after_Create {...props} $user={$user} beforeAfterCategories={beforeAfterCategories} />} />
                <Route path={`${path}/before_after/:id`} render={(props) => <Before_after {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={path} render={(props) => <Before_afters {...props} $user={$user} beforeAfterCategories={beforeAfterCategories} />} />
            </Switch>
        )
    }
}

export default BeforeAfter_Container;