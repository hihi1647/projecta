import React, { Fragment } from 'react';
import api from 'common/api';
import { currentUsesDomain }  from 'constants/index'

import { Link } from 'react-router-dom';
import parse from 'html-react-parser';

const imgUrl = currentUsesDomain.imgUrl;

const columnsObj = {
  'title': '제목',
  'writer': '작성자',
  'created_at': '작성일',
  'views': '조회수'
}

const columns = [
  'title',
  'writer',
  'created_at',
  'views'
];

const imageArr = [{
  before: {
    name: '정면 이미지 전',
    title: 'before_photo_file_front'
  },
  after: {
    name: '정면 이미지 후',
    title: 'after_photo_file_front'
  }
},
{
  before: {
    name: '45 이미지 전',
    title: 'before_photo_file_45'
  },
  after: {
    name: '45 이미지 후',
    title: 'after_photo_file_45'
  }
},
{
  before: {
    name: '측면 이미지 전',
    title: 'before_photo_file_side'
  },
  after: {
    name: '측면 이미지 후',
    title: 'after_photo_file_side'
  }
}
];

class Backend_BeforeAfter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      beforeAfter: {}
    }

    this.history = this.props.history;
    this.match = this.props.match;
    this.currentBeforeAfterID = this.match.params.id;

  }

  componentDidMount() {
    this.getBeforeAfter(this.currentBeforeAfterID);
  }

  getBeforeAfter = async (currentBeforeAfterID) => {
    await api.getBeforeAfter(currentBeforeAfterID, true)
      .then(res => {
        this.setState({
          beforeAfter: res.data.data
        });
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  editBeforeAfter = () => {
    this.history.push(`${this.match.url}/edit`);
  }

  deleteBeforeAfter = async () => {

    await api.deleteBeforeAfter(this.currentBeforeAfterID, true)
      .then((res)=> {
        alert('successfully removed');
      })
      .then(() => {
        this.goBack();
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  goBack = () => {
    this.history.push('/backend/before_afters');
  }
  
  render() {
    const beforeAfter = this.state.beforeAfter;
    console.log('this is beforeAfter', beforeAfter, this.props);
    return (
      <div className="backend-body">
        <div className="backend-boardTop-style">
          <h2 className="backend-boardTop-title">전후사진</h2>
          <span className="backend-boardTop-text">전후사진 관리자 페이지 입니다.</span>
        </div>
    
        {
        beforeAfter.id ? (
          <Fragment>
            <ul className="backend-board-wrap">
            {
              columns.slice(1).map(column => (
                <Fragment key={column} >
                  <li>
                    <span className="backend-board-title">{columnsObj[column]}</span>
                    <span className="backend-board-text">{beforeAfter[column]}</span>
                  </li>
                 
                </Fragment>
              ))
            }
            <li>
              <span className="backend-board-title">분야분류</span>
              <span className="backend-board-text">{beforeAfter.main_category_name}</span>
            </li>
            <li>
              <span className="backend-board-title">고정</span>
              <span className="backend-board-text">{beforeAfter.fix_option_name}</span>
            </li>
            <li>
              <span className="backend-board-title">{columnsObj[columns[0]]}</span>
              <span className="backend-board-text">{beforeAfter[columns[0]]}</span>
            </li>
            <li>
              <span className="backend-board-title">내용</span>
              <span className="backend-board-text">{typeof beforeAfter.details === 'string' ? parse(beforeAfter.details) : beforeAfter.details}</span>
            </li>
            <li>
              <span className="backend-board-title">이미지</span>
              <ul>
              {
                // need to be improve
                imageArr.map((curImage, ind) => (
                  <li className="beforeAfter-img-card" key={ind} > 
                    {
                      beforeAfter[curImage.before.title] && (
                        <span className="beforeAfter-img-card-list">
                          <span className="beforeAfter-img-title">{curImage.before.name}</span>
                          <img className="beforeAfter-img" src={imgUrl + beforeAfter[curImage.before.title]} style={{width: '520px'}} />
                        </span>
                      )
                    }
                    {
                      beforeAfter[curImage.after.title] && (
                        <span className="beforeAfter-img-card-list">
                          <span className="beforeAfter-img-title">{curImage.after.name}</span>                
                          <img className="beforeAfter-img" src={imgUrl + beforeAfter[curImage.after.title]} style={{width: '520px'}} />
                        </span>
                      )
                    }
                  </li>
                ))
              }
              </ul>
            </li>
          </ul>    

          <div style={{textAlign: 'right'}} className="backend-topBtn-wrap">
            
            <button className="btn-backbend btn-edit" onClick={this.editBeforeAfter}>수정하기</button>
            <button className="btn-backbend btn-warning" onClick={this.deleteBeforeAfter}>삭제하기</button>
            <button className="btn-backbend btn-check" onClick={this.goBack}>목록으로</button>
          </div>
          <div className="prev-contianer">
            {
              beforeAfter.previous ? (
                <Fragment>
                  <div className="prev-wrap prev-wrap-boder">
                    <span className="border-arrow">
                      <Link to={`${this.match.path.replace(':id', beforeAfter.previous.id)}`}>
                        <svg  className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><path d="M0 16.67l2.829 2.83 9.175-9.339 9.167 9.339 2.829-2.83-11.996-12.17z" fill="##615454"/></svg>
                        이전글
                      </Link>
                    </span>
                    <span className="border-prev-body">{beforeAfter.previous.title}</span>
                  </div>
                  
                </Fragment>
              ) : null
            }
            {
              beforeAfter.next ? (
                <Fragment>
                  <div className="prev-wrap">
                    <div className="border-arrow">
                      <Link to={`${this.match.path.replace(':id', beforeAfter.next.id)}`}>
                        <svg className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z" fill="##615454"/></svg>
                        다음글
                      </Link>
                  </div>
                  <span className="border-prev-body">{beforeAfter.next.title}</span>
                </div>
                </Fragment>
              ) : null
            }
          </div>
        
        </Fragment>
        ) : (
          <div>
            something went wrong..
          </div>
        )
      }
          
      </div>
    )
  }
}

export default Backend_BeforeAfter;