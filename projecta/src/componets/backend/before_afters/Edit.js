import React, { Fragment } from 'react';
import api from 'common/api';
import { Select, Message } from 'element-react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import dateFnsFormat from 'date-fns/format';

import { parseDate, formatDate } from 'common/Utils';
import { DATE_FORMAT } from 'constants/index';
import { currentUsesDomain, NO_FIXED_ID }  from 'constants/index'

import Custom_Image_Font_Quill from 'componets/quill/Custom_Image_Font_Quill';

const imgUrl = currentUsesDomain.imgUrl;

const columnsObj = {
  'title': '제목',
  'writer': '작성자',
  'created_at': '작성일',
  'views': '조회수'
}

const columns = [
  'title',
  'writer',
  'created_at',
  'views'
];

const imageArr = [{
    before: {
      name: '정면 이미지 전',
      title: 'before_photo_file_front'
    },
    after: {
      name: '정면 이미지 후',
      title: 'after_photo_file_front'
    }
  },
  {
    before: {
      name: '45 이미지 전',
      title: 'before_photo_file_45'
    },
    after: {
      name: '45 이미지 후',
      title: 'after_photo_file_45'
    }
  },
  {
    before: {
      name: '측면 이미지 전',
      title: 'before_photo_file_side'
    },
    after: {
      name: '측면 이미지 후',
      title: 'after_photo_file_side'
    }
  }
];
class Backend_BeforeAfters_Edit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      beforeAfter: {
        title: '',
        writer: '',
        created_at: new Date(),
        views: 0,
        details: '',
        main_category_id: '',
        fix_option_id: '',
        before_photo_file_front: '',
        after_photo_file_front: ''
      },
      beforeAfterCategories: null,
      before_photo_file_front: '',
      after_photo_file_front: '',
      before_photo_file_45: '',
      after_photo_file_45: '',
      before_photo_file_side: '',
      after_photo_file_side: ''
    }

    this.history = this.props.history;
    this.match = this.props.match;
    this.currentBeforeAfterID = this.match.params.id;
  }

  componentDidMount() {
    this.getBeforeAfter(this.currentBeforeAfterID);
  }

  getBeforeAfter = async (currentBeforeAfterID) => {
    await api.getUpdateBeforeAfter(currentBeforeAfterID, true)
      .then(res => {
        const resData = res.data.data;

        if (resData) {
          let beforeAfter = resData.beforeAfter;

          // need to be improve

          let before_photo_file_45 = beforeAfter.before_photo_file_45;
          let after_photo_file_45 = beforeAfter.after_photo_file_45;
          let before_photo_file_front = beforeAfter.before_photo_file_front;
          let after_photo_file_front = beforeAfter.after_photo_file_front;
          let before_photo_file_side = beforeAfter.before_photo_file_side;
          let after_photo_file_side = beforeAfter.after_photo_file_side;
          
          if ( before_photo_file_45 ) {
            before_photo_file_45 = imgUrl + before_photo_file_45;
          }

          if ( after_photo_file_45 ) {
            after_photo_file_45 = imgUrl + after_photo_file_45;
          }

          if ( before_photo_file_front ) {
            before_photo_file_front = imgUrl + before_photo_file_front;
          }

          if ( after_photo_file_front ) {
            after_photo_file_front = imgUrl + after_photo_file_front;
          }

          if ( before_photo_file_side ) {
            before_photo_file_side = imgUrl + before_photo_file_side;
          }

          if ( after_photo_file_side ) {
            after_photo_file_side = imgUrl + after_photo_file_side;
          }

          let fix_options = resData.setUp.fix_options;

          if (fix_options) {
            fix_options = [
                {
                    id: NO_FIXED_ID, 
                    fix_option_name: "고정없음"
                },
                ...fix_options
            ]
          }

          const beforeAfterCategories = {
            ...resData.setUp,
            fix_options
          };

          let fix_option_id = beforeAfter.fix_option_id;

          if (fix_option_id === null) {
            fix_option_id = NO_FIXED_ID;
          }

          let details = beforeAfter.details;
          if (details === null) {
            details = '';
          }

          beforeAfter = {
            ...beforeAfter,
            fix_option_id,
            details
          }
          
          this.setState({
            beforeAfterCategories,
            beforeAfter,
            before_photo_file_front,
            after_photo_file_front,
            before_photo_file_45,
            after_photo_file_45,
            before_photo_file_side,
            after_photo_file_side
          })

        }
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }


  editBeforeAfter = async () => {

    // input validation form check
    let validationCheck = true;

    const formData = new FormData();
    Object.keys(this.state.beforeAfter).forEach(cur => {
      if (cur === 'created_at' && typeof this.state.beforeAfter[cur] !== 'string') {
        formData.append(cur, this.state.beforeAfter[cur].toUTCString());
      } else if (cur === 'fix_option_id' && this.state.beforeAfter[cur]) {
        let fix_option_id = this.state.beforeAfter[cur];
        if (fix_option_id !== NO_FIXED_ID) {
            formData.append(cur, this.state.beforeAfter[cur]);
        }
      } else {
        formData.append(cur, this.state.beforeAfter[cur]);
      }
      
    })

    // const formData = {...this.state.beforeAfter};
    
    console.log('this is formData', formData);

    if ( validationCheck ) {
      await api.updateBeforeAfter(this.currentBeforeAfterID, formData, true)
      .then(res => {
        Message({
          message: '성공적으로 수정되었습니다.',
          type: 'success'
        });
        console.log('successfully edit', res);
      
      })
      .then(() => {
          this.goBack();
      })
      .catch(error => {
        console.log('something went wrong', error);
      })
    }
  }

  deleteBeforeAfter = async () => {

    await api.deleteBeforeAfter(this.currentBeforeAfterID, true)
      .then((res)=> {
        Message({
          message: '성공적으로 삭제되었습니다.',
          type: 'success'
        });
      })
      .then(() => {
        this.goBack();
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }


  cancel = () => {
  }

  goBack = () => {
    this.history.push('/backend/before_afters');
  }

  handleChange = (event, input) => {

    let newBeforeAfter = {
      ...this.state.beforeAfter
    };

    if (input === 'created_at' || input === 'fix_option_id' || input === 'main_category_id' || input === 'details') {
      newBeforeAfter = {
        ...newBeforeAfter,
        [input]: event
      }
    } else {
      newBeforeAfter = {
        ...newBeforeAfter,
        [input]: event.target.value
      };
    }

    this.setState({
      beforeAfter: newBeforeAfter
    })
  }

  handleImgChange = (event, input) => {

    let newBeforeAfter = {
      ...this.state.beforeAfter
    };

    newBeforeAfter = {
      ...newBeforeAfter,
      [input]: event.target.files[0]
    };

    this.setState({
      beforeAfter: newBeforeAfter,
      [input]: window.URL.createObjectURL(event.target.files[0])
    })
  }

  render() {
    const { beforeAfter, beforeAfterCategories } = this.state;
    console.log('this is edit beforeAfter', beforeAfterCategories, beforeAfter);
    return (
        <Fragment>
          <div className="event-create-body">
            <div className="backend-boardTop-style">
                <h2 className="backend-boardTop-title">전후사진</h2>
                <span className="backend-boardTop-text">수정 페이지 입니다.</span>
            </div>

            <ul className="backend-board-create-wrap">
            <li>
              <span className="backend-board-title">{columnsObj['writer']}</span>
              <span className="backend-board-text">
                <input className="backend-board-create-input" type="text" value={beforeAfter['writer']} onChange={(e) => this.handleChange(e, 'writer')} />
              </span>
            </li>

            <li>
              <span className="backend-board-title">{columnsObj['created_at']}</span>
              <span className="backend-board-text">
                <DayPickerInput
                  formatDate={formatDate}
                  format={DATE_FORMAT}
                  parseDate={parseDate}
                  placeholder={`${dateFnsFormat(new Date(beforeAfter.created_at), DATE_FORMAT)}`}
                  onDayChange={(e) => this.handleChange(e, 'created_at')}
                />
              </span>
            </li>

            <li>
              <span className="backend-board-title">{columnsObj['views']}</span>
              <span className="backend-board-text">
                <input className="backend-board-create-input" type="text" value={beforeAfter['views']} onChange={(e) => this.handleChange(e, 'views')} />
              </span>
            </li>

            <li>
              <span className="backend-board-title">분야분류</span>
              <span className="backend-board-text">
                <Select value={beforeAfter.main_category_id} placeholder="분야 분류" onChange={(e) => this.handleChange(e, 'main_category_id')}>
                  {(beforeAfterCategories && beforeAfterCategories.main_categories && beforeAfterCategories.main_categories.length > 0) ? beforeAfterCategories.main_categories.map(el => (
                    <Select.Option key={el.id} label={el.main_category_name} value={el.id} />
                  )) : null}
                </Select>
              </span>
            </li>

            <li>
              <span className="backend-board-title">고정</span>
              <span className="backend-board-text">
                <Select value={beforeAfter.fix_option_id} placeholder="고정 옵션" onChange={(e) => this.handleChange(e, 'fix_option_id')}>
                  {(beforeAfterCategories && beforeAfterCategories.fix_options && beforeAfterCategories.fix_options.length > 0) ? beforeAfterCategories.fix_options.map(el => (
                    <Select.Option key={el.id} label={el.fix_option_name} value={el.id} />
                  )) : null}
                </Select>
              </span>
            </li>

            <li>
              <span className="backend-board-title">{columnsObj[columns[0]]}</span>
              <span className="backend-board-text">
                <input className="backend-board-create-input" type="text" value={beforeAfter.title} onChange={(e) => this.handleChange(e, columns[0])} />
              </span>
            </li>
            <li>
              <span className="backend-board-title">내용</span>
              <span className="backend-board-text">
                <Custom_Image_Font_Quill setContents={beforeAfter.details} onChange={(e) => this.handleChange(e, 'details')} />
              </span>
            </li>

            <ul>
              {
                imageArr.map((curImage, ind) => (
                  <li key={ind} > 
                    <div className="backend-beforeAfters-create-photo-wrap">
                        <span className="backend-create-photo">
                        <span className="backend-board-title">{curImage.before.name}</span>
                          { this.state[curImage.before.title] && <img src={this.state[curImage.before.title]} style={{width: '200px'}} /> }
                          <input className="backend-file-input" type="file" name="file" onChange={(e) => this.handleImgChange(e, curImage.before.title)} />
                       </span>
                    
                       <span className="backend-create-photo">
                        <span className="backend-board-title">{curImage.after.name}</span>
                        { this.state[curImage.after.title] && <img src={this.state[curImage.after.title]} style={{width: '200px'}} /> }
                          <input className="backend-file-input" type="file" name="file" onChange={(e) => this.handleImgChange(e, curImage.after.title)} />
                      </span>
                    </div>
                  </li>
                ))
              }
            </ul>
          </ul>
          

          <div style={{textAlign: 'right'}} className="backend-topBtn-wrap">
            
            <button className="btn-backbend btn-edit" onClick={this.editBeforeAfter}>수정완료</button>
            <button className="btn-backbend btn-warning" onClick={this.deleteBeforeAfter}>삭제하기</button>
            <button className="btn-backbend btn-check" onClick={this.goBack}>목록으로</button>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default Backend_BeforeAfters_Edit;