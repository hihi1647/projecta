import React from 'react';
import api from 'common/api';

import { Select, Pagination } from 'element-react';

// https://material-ui.com/components/tables/

const columns = [
  { prop: 'checkbox', label: '선택', width: 50 },
  { prop: 'main_category_name', label: '분야', width: 100 },
  { prop: 'title', label: '제목', width: 170 },
  { prop: 'created_at', label: '작성일', width: 170 },
  { prop: 'views', label: '조회수', width: 170 }
];

let rows = [];

function getIndexedRows(datas) {
  let numFixedIndex = 0;
  let numNotFixedIndex = 0;
  let rows = [];

  datas.forEach(data => {
    // 고정된 노티스일 때
    if (data.is_fix) {
      const beforeAfterArr = data.beforeAfters.map(fixedBeforeAfter => {

        let created_at = fixedBeforeAfter.created_at;
        created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;

        return {
          ...fixedBeforeAfter,
          created_at,
          is_fix: data.is_fix
        };
      });
      rows = [...rows, ...beforeAfterArr];

    } else {
      // 고정된 노티스가 아닐 때
      // index값은 페이지당 고정되지 않은 노티스 개수 * 현재 페이지 + 고정된 노티스부터 시작 
      numNotFixedIndex = data.beforeAfters.per_page * (data.beforeAfters.current_page - 1) + numFixedIndex;

      const beforeAfterArr = data.beforeAfters.data.map(beforeAfter => {
        let created_at = beforeAfter.created_at;
        created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;

        numNotFixedIndex += 1;
        return {
          numIndex: numNotFixedIndex,
          ...beforeAfter,
          created_at,
          is_fix: data.is_fix
        };
      });
      rows = [...rows, ...beforeAfterArr];
    }
  });

  return rows;
}
class Backend_BeforeAfters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      total: 0,
      perPage: 0,
      checkboxObj: [],
      searchData: {
        categ: 0,
        searchOption: 0,
        query: ''
      },
      isSearched: false,
      checkAll: false
    }
  }

  componentDidMount() {
    this.getDatas();
  }

  getDatas = async (page) => {
    await api.getBeforeAfters(page)
      .then(res => {
        rows = getIndexedRows(res.data.data);

        const newRow = rows.map(cur => ({
          ...cur,
          checkbox: false
        }));

        const notFixedBeforeAfter = res.data.data.filter(cur => !cur.is_fix)[0].beforeAfters;

        this.setState({
          page: notFixedBeforeAfter.current_page,
          total: notFixedBeforeAfter.last_page,
          perPage: notFixedBeforeAfter.per_page,
          checkboxObj: newRow,
          isSearched: false
        });
      })
      .catch(error => {
        console.log('something went wrong', error);
      });
  }

  getSearchedDatas = async (page) => {
    const { searchData } = this.state;
    const payload = {
      search_word: searchData.query,
      main_category_id: searchData.categ,
      search_option_id: searchData.searchOption
    };

    await api.getSearchBeforeAfter(payload, page, true)
      .then(res => {
        const resData = res.data.data;

        rows = getIndexedRows(resData);

        const notFixedBeforeAfter = resData.filter(cur => !cur.is_fix)[0].beforeAfters;

        this.setState({
          page: notFixedBeforeAfter.current_page,
          total: notFixedBeforeAfter.last_page,
          perPage: notFixedBeforeAfter.per_page,
          checkboxObj: rows
        });
        console.log('successfully searched..', res);
      })
      .catch(error => {
        console.log(error);
      })
  }

  handleChangePage = (selectedPage) => {
    const { isSearched } = this.state;
    if (isSearched) {
      this.getSearchedDatas(selectedPage);
    } else {
      this.getDatas(selectedPage);
    }
  };

  handleCheckBox = (event, id) => {
    const ind = this.state.checkboxObj.findIndex(cur => cur.id === id);
    const newTarget = {
      ...this.state.checkboxObj[ind],
      checkbox: event.target.checked
    }
    let newCheckboxObj = [
      ...this.state.checkboxObj.slice(0, ind),
      newTarget,
      ...this.state.checkboxObj.slice(ind + 1),
    ]

    const checkAll = newCheckboxObj.length === newCheckboxObj.filter(cur => cur.checkbox).length;
    this.setState({
      checkboxObj: newCheckboxObj,
      checkAll
    })
  } 

  handleSelectAll = () => {
    let checkAll = !this.state.checkAll;

    let newCheckboxObj = this.state.checkboxObj.map(cur => ({
      ...cur,
      checkbox: checkAll
    }));

    this.setState({
      checkboxObj: newCheckboxObj,
      checkAll
    })
  }

  searchClickHanlder = async () => {
    console.log('clicking...', this.state.searchData);
    const { searchData, isSearched } = this.state;

    if (searchData.searchOption === 0 && searchData.categ === 0 && searchData.query === '') {
      if (isSearched) {
        this.getDatas();
      }
    } else {
      this.getSearchedDatas();
    }
  }

  categChangeHandler = (value) => {
    this.searchChangeHandler(value,'categ');
  }
  searchOptionChangeHandler = (value) => {
    this.searchChangeHandler(value,'searchOption');
  }

  searchChangeHandler = (target, type) => {
    let newSearchData = {
      ...this.state.searchData
    };
    if (type === 'query') {
      newSearchData = {
        ...newSearchData,
        [type]: target.target.value
      }
    } else {
      newSearchData = {
        ...newSearchData,
        [type]: target
      };
    }

    this.setState({
      searchData: newSearchData
    });
  }

  searchClear = () => {
    this.setState({
      searchData: {
        categ: 0,
        searchOption: 0,
        query: ''
      }
    })
  }

  handleLink = (id) => {
    this.props.history.push(`${this.props.match.url}/before_after/${id}`);
  }

  createBeforeAfter = () => {
    this.props.history.push(`${this.props.match.url}/before_after/create`);
  }

  deleteBeforeAfter = async () => {

    const checkedArr = this.state.checkboxObj.filter(beforeAfter => beforeAfter.checkbox);
    const checkedIDs = checkedArr.map(beforeAfter => beforeAfter.id); 
    if (checkedIDs.length > 0) {
       await api.deleteBeforeAftersAll({ids: checkedIDs})
        .then(res => {
          alert('successfully deleted all');
          this.getDatas();
        })
        .catch(err => {
          console.log('something went wrong', err);
        });
    }
  }


  render() {
    console.log('this is backend before afters', this.state, this.props);
    const beforeAfterCategories = this.props.beforeAfterCategories;
    const { searchData, checkboxObj } = this.state;
    const { categ, searchOption, query } = searchData;
    return (
   
      <div className="backend-body">
        <div className="backend-table-wrap">
            <div className="backend-boardTop-style">
              <h2 className="backend-boardTop-title">전후사진</h2>
              <p className="backend-boardTop-text">전후사진 관리자 페이지 입니다.</p>
            </div>

            <div className="backend-topBtn-wrap">
                <button className="btn-backbend btn-check" onClick={this.handleSelectAll}>{this.state.checkAll ? '전체해제' : '전체선택'}</button>
                <button className="btn-backbend btn-warning" onClick={this.deleteBeforeAfter}>선택삭제</button>
            </div>
          <table>
            <colgroup>
              <col className="col-150"/>
              <col className="col-150"/>
              <col/>
              <col className="col-150"/>
              <col className="col-150"/>
            </colgroup>
            <thead>
              <tr>
                {
                  columns.map((column, index) => (
                    <td key={index} >
                      {column.label}
                    </td>
                  ))
                }
              </tr>
            </thead>
            <tbody className="backend-tbody col-04">
            {
              checkboxObj.map((row, index) => (
                <tr key={index} >
                  {
                    columns.map((column, colIndex) => {
                      return column.prop === 'checkbox' ? (
                        <td key={colIndex}>
                          <input type="checkbox" checked={row[column.prop]} onChange={(e) => this.handleCheckBox(e, row.id)} />
                        </td>
                      ) : (                        
                        column.prop === 'title' ? (
                          <td key={colIndex} onClick={() => this.handleLink(row.id)}>
                            {
                              row.is_fix && <span>{row.fix_option_name}</span>
                            }
                            {row[column.prop]}
                          </td>
                        ) : (
                        <td key={colIndex} onClick={() => this.handleLink(row.id)}>
                          {row[column.prop]}
                        </td>
                        )
                      )
                    })
                  }
                </tr>
              ))
            }
            </tbody>
          </table>
        </div>
        <br />
        <button className="btn-backbend btn-backend-writing" onClick={this.createBeforeAfter}>글쓰기</button>
        {/*
        <ul className="m-board">
          {
            this.state.checkboxObj.map((row, index) => (
              <li key={index} >
                {
                  columns.map((column, colIndex) => {
                    return column.prop === 'checkbox' ? (
                      <span key={colIndex}>
                        <input type="checkbox" checked={row[column.prop]} onChange={(e) => this.handleCheckBox(e, row.id)} />
                      </span>
                    ) : (
                      column.prop === 'numIndex' ? (
                        <span key={colIndex} onClick={() => this.handleLink(row.id)}>
                          {row[column.prop] ? row[column.prop] : 'ㅁ'}
                        </span>
                      ) : (
                      <span key={colIndex} onClick={() => this.handleLink(row.id)}>
                        {row[column.prop]}
                      </span>
                      )
                    )
                  })
                }
              </li>
            ))
          }
          </ul>
        */}
        <Pagination 
          style={{display:'flex',justifyContent:'center'}} 
          layout="prev, pager, next" 
          currentPage={this.state.page} 
          pageCount={this.state.total} 
          pageSize={this.state.perPage} 
          small={true}
          onCurrentChange={this.handleChangePage}
        />
        <div className="board-search">
              <div className="search-box">
            <Select value={searchData.categ} placeholder="분야 분류" onChange={this.categChangeHandler}>
              {beforeAfterCategories.main_categories ? beforeAfterCategories.main_categories.map(el => {
                return <Select.Option key={el.id} label={el.main_category_name} value={el.id} />
              }) : null}
            </Select>
          </div>
          <div className="search-box">
            <Select value={searchData.searchOption} placeholder="글제목" onChange={this.searchOptionChangeHandler}>
              {beforeAfterCategories.search_options ? beforeAfterCategories.search_options.map(el => {
                return <Select.Option key={el.id} label={el.search_option_name} value={el.id} />
              }) : null}
            </Select>
          </div>
          <span>
            <input className="board-input search-box" type="text" value={query} onChange={(e) => this.searchChangeHandler(e, 'query')} />
            {
              (categ || searchOption || query) && <i className="el-icon-circle-cross" onClick={this.searchClear}></i> 
            }
          </span>
          <button className="btn-search search-box" onClick={this.searchClickHanlder}>검색</button>
        </div>
      </div>

    )
  }
}

export default Backend_BeforeAfters;