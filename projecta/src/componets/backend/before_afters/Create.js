import React, { Fragment } from 'react';
import api from 'common/api';
import { Select, Message } from 'element-react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import dateFnsFormat from 'date-fns/format';

import { parseDate, formatDate } from 'common/Utils';
import { DATE_FORMAT, NO_FIXED_ID } from 'constants/index';

import Custom_Image_Font_Quill from 'componets/quill/Custom_Image_Font_Quill';

const columnsObj = {
  'title': '제목',
  'writer': '작성자',
  'created_at': '작성일',
  'views': '조회수'
}

const columns = [
  'title',
  'writer',
  'created_at',
  'views'
];

const imageArr = [{
  before: {
    name: '정면 이미지 전',
    title: 'before_photo_file_front'
  },
  after: {
    name: '정면 이미지 후',
    title: 'after_photo_file_front'
  }
},
{
  before: {
    name: '45 이미지 전',
    title: 'before_photo_file_45'
  },
  after: {
    name: '45 이미지 후',
    title: 'after_photo_file_45'
  }
},
{
  before: {
    name: '측면 이미지 전',
    title: 'before_photo_file_side'
  },
  after: {
    name: '측면 이미지 후',
    title: 'after_photo_file_side'
  }
}
];


class Backend_BeforeAfters_Create extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      beforeAfter: {
        title: '',
        writer: '',
        created_at: new Date(),
        views: 0,
        details: '',
        main_category_id: '',
        fix_option_id: NO_FIXED_ID,
        before_photo_file_front: '',
        after_photo_file_front: ''
      },
      beforeAfterCategories: null,
      before_photo_file_front: '',
      after_photo_file_front: '',
      before_photo_file_45: '',
      after_photo_file_45: '',
      before_photo_file_side: '',
      after_photo_file_side: ''
    }

    this.history = this.props.history;
    this.match = this.props.match;
  }

  createBeforeAfter = async () => {

    // input validation form check
    let validationCheck = true;

    const formData = new FormData();
    Object.keys(this.state.beforeAfter).forEach(cur => {
      if (cur === 'created_at' && typeof this.state.beforeAfter[cur] !== 'string') {
        formData.append(cur, this.state.beforeAfter[cur].toUTCString());
      } else if (cur === 'fix_option_id' && this.state.beforeAfter[cur]) {
        let fix_option_id = this.state.beforeAfter[cur];
        if (fix_option_id !== NO_FIXED_ID) {
            formData.append(cur, this.state.beforeAfter[cur]);
        }
      } else {
        formData.append(cur, this.state.beforeAfter[cur]);
      }
      
    })

    // const formData = {...this.state.beforeAfter};
    
    console.log('this is formData', formData);

    if ( validationCheck ) {
      await api.createBeforeAfter(formData, true)
        .then(res => {
            if (res) {
                Message({
                    message: '성공적으로 생성되었습니다.',
                    type: 'success'
                });
            }

            console.log('successfully created', res);
        })
        .then(() => {
            this.goBack();
        })
        .catch(error => {
          console.log('something went wrong', error);
        })
    }
  }


  cancel = () => {
  }

  goBack = () => {
    this.history.push('/backend/before_afters');
  }

  handleChange = (event, input) => {

    let newBeforeAfter = {
      ...this.state.beforeAfter
    };

    if (input === 'created_at' || input === 'fix_option_id' || input === 'main_category_id' || input === 'details') {
      newBeforeAfter = {
        ...newBeforeAfter,
        [input]: event
      }
    } else {
      newBeforeAfter = {
        ...newBeforeAfter,
        [input]: event.target.value
      };
    }

    this.setState({
      beforeAfter: newBeforeAfter
    })
  }

  handleImgChange = (event, input) => {

    let newBeforeAfter = {
      ...this.state.beforeAfter
    };

    newBeforeAfter = {
      ...newBeforeAfter,
      [input]: event.target.files[0]
    };

    this.setState({
      beforeAfter: newBeforeAfter,
      [input]: window.URL.createObjectURL(event.target.files[0])
    })
  }

  render() {
    const beforeAfter = this.state.beforeAfter;
    const beforeAfterCategories = this.props.beforeAfterCategories;
    console.log('this is create beforeAfter', beforeAfterCategories, beforeAfter);

    return (
      <Fragment>
        <div className="event-create-body">
          <div className="backend-boardTop-style">
              <h2 className="backend-boardTop-title">전후사진</h2>
              <p className="backend-boardTop-text">관리자 글쓰기 페이지 입니다.</p>
          </div>
          <ul className="backend-board-create-wrap">
            <li>
              <p className="backend-board-title">{columnsObj['writer']}</p>
              <span className="backend-board-text">
                <input className="backend-board-create-input" placeholder="작성자 이름을 입력하세요." type="text" value={beforeAfter['writer']} onChange={(e) => this.handleChange(e, 'writer')} />
              </span>
            </li>

          <li>
            <p className="backend-board-title">{columnsObj['created_at']}</p>
            <span className="backend-board-text">
              <DayPickerInput
                formatDate={formatDate}
                format={DATE_FORMAT}
                parseDate={parseDate}
                placeholder={`${dateFnsFormat(new Date(), DATE_FORMAT)}`}
                onDayChange={(e) => this.handleChange(e, 'created_at')}
              />
              
              {/* <DayPickerInput selectedDay={beforeAfter.created_at} onDayChange={(e) => this.handleChange(e, 'created_at')} /> */}
            </span>
          </li>
          
          <li>
            <p className="backend-board-title">{columnsObj['views']}</p>
            <span className="backend-board-text">
              <input type="text" value={beforeAfter['views']} onChange={(e) => this.handleChange(e, 'views')} />
            </span>
          </li>

          <li>
            <p className="backend-board-title">분야분류</p>
            <span className="backend-board-text">
              <Select value={beforeAfter.notice_category_id} placeholder="분야 분류" onChange={(e) => this.handleChange(e, 'main_category_id')}>
                {(beforeAfterCategories && beforeAfterCategories.main_categories && beforeAfterCategories.main_categories.length > 0) ? beforeAfterCategories.main_categories.map(el => (
                  <Select.Option key={el.id} label={el.main_category_name} value={el.id} />
                )) : null}
              </Select>
            </span>
          </li>

          <li>
            <p className="backend-board-title">고정</p>
            <span className="backend-board-text">
              <Select value={beforeAfter.fix_option_id} placeholder="고정 옵션" onChange={(e) => this.handleChange(e, 'fix_option_id')}>
                {(beforeAfterCategories && beforeAfterCategories.fix_options && beforeAfterCategories.fix_options.length > 0) ? beforeAfterCategories.fix_options.map(el => (
                  <Select.Option key={el.id} label={el.fix_option_name} value={el.id} />
                )) : null}
              </Select>
            </span>
          </li>

          <li>
            <p className="backend-board-title">{columnsObj[columns[0]]}</p>
            <span className="backend-board-text">
              <input className="backend-board-create-input" placeholder="제목을 입력하세요." type="text" value={beforeAfter.title} onChange={(e) => this.handleChange(e, columns[0])} />
            </span>
          </li>

          <li>
            <p className="backend-board-title">내용</p>
              <span className="backend-board-text">
                <Custom_Image_Font_Quill setContents={beforeAfter.details} onChange={(e) => this.handleChange(e, 'details')} />
              </span>
          </li>
       
          <ul>
            {
              imageArr.map((curImage, ind) => (
                <li key={ind} > 
                    <div className="backend-beforeAfters-create-photo-wrap">

                      <span className="backend-create-photo">
                        <p className="backend-board-title">{curImage.before.name}</p>
                        { this.state[curImage.before.title] && <img src={this.state[curImage.before.title]} style={{width: '200px'}} /> }
                        <input className="backend-file-input" type="file" name="file" onChange={(e) => this.handleImgChange(e, curImage.before.title)} />
                        <p style={{fontSize:'12px',paddingTop:'4px',color:'#f77a99' }}>* 전후사진 이미지 사이즈는 <b>590 X 472</b>를 권장합니다.</p>
                      </span>
                      
                      <span className="backend-create-photo">
                        <p className="backend-board-title">{curImage.after.name}</p>
                        { this.state[curImage.after.title] && <img src={this.state[curImage.after.title]} style={{width: '200px'}} /> }
                        <input className="backend-file-input" type="file" name="file" onChange={(e) => this.handleImgChange(e, curImage.after.title)} />
                      </span>
                    </div>

                  </li>
      
              ))
            }
          </ul>
        </ul> 

        <div style={{textAlign: 'right'}} className="backend-topBtn-wrap">
          
          <button className="btn-backbend btn-edit" onClick={this.createBeforeAfter}>글쓰기</button>
          <button className="btn-backbend btn-warning" onClick={this.goBack}>삭제하기</button>
          <button className="btn-backbend btn-check" onClick={this.goBack}>목록으로</button>
        </div>
      
      </div>
      </Fragment>
    )
  }
}

export default Backend_BeforeAfters_Create;