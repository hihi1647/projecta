import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Consultings from './Consultings';
import Consulting_Answer from './Answer';

import api from 'common/api';

class Consulting_Container extends React.Component {
    state = {
        consultingCategories: {},
    };
    
    componentDidMount() {
        this.getDatas();
    }

    getDatas = async () => {
        await api.getConsultingCategories(true)
            .then(res => {
                this.setState({
                    consultingCategories: res.data.data
                })
            })
            .catch(error => {
              console.log('something went wrong with consulting category', error);
            });
    }

    render() {
        const { consultingCategories } = this.state
        const { path } = this.props.match;

        const { $user, isAuthenticated, changeState } = this.props;
        return (
            <Switch>
                <Route path={`${path}/consulting/:id/answer`} render={(props) => <Consulting_Answer {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={path} render={(props) => <Consultings {...props} $user={$user} consultingCategories={consultingCategories} />} />
            </Switch>
        )
    }
}

export default Consulting_Container;