import React, { Fragment } from 'react';
import api from 'common/api';
import { Select, Message } from 'element-react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import dateFnsFormat from 'date-fns/format';

import { parseDate, formatDate, formatDateMySQL } from 'common/Utils';
import { DATE_FORMAT } from 'constants/index';

import parse from 'html-react-parser';

import Custom_Image_Font_Quill from 'componets/quill/Custom_Image_Font_Quill';
import Custom_Image_Quill from 'componets/quill/Custom_Image_Quill';

const columnsObj = {
    'title': '제목',
    'phone': '휴대폰',
    'writer': '작성자',
    'created_at': '작성일',
    'views': '조회수',
    'email': '이메일',
    'details': '내용',
    'password': '비밀번호',
    'main_category_name': '상담과목(1분류)',
    'sub_category_name': '상담과목(2분류)',
    'answer_status_id': '상담결과',
    'answer_date': '답변일시',
    'answer': '답변',
    'manager_id': '담당자',
    'consulting_status_id': '2차 상담결과',
    'memo': '관리자 메모'
  }
  
  const columns = [
    'writer',
    'created_at',
    'phone',
    'email',
    'title',
    'main_category_name',
    'sub_category_name',
    'details'
  ];

class Backend_Consulting_Answer extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            consulting: {},
            setUp: {}
        }

        this.history = this.props.history;
        this.match = this.props.match;
        this.currentConsultingID = this.match.params.id;
    }

    componentDidMount() {
        this.getDatas(this.currentConsultingID);
    }

    getDatas = async (currentConsultingID) => {
        await api.getUpdateConsulting(currentConsultingID, true)
            .then(res => {
                const resData = res.data.data;

                if (resData) {
                    let { created_at, answer_date, reservation_date, memo, answer } = resData.consulting;
                    created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;
                    if (answer_date) {
                        answer_date = new Date(answer_date);
                    } else {
                        answer_date = new Date();
                    }

                    if (reservation_date) {
                        reservation_date = new Date(reservation_date);
                    } else {
                        reservation_date = new Date()
                    }

                    if (answer === null) {
                        answer = '';
                    }

                    if (memo === null) {
                        memo = '';
                    }

                    const consulting = {
                        ...resData.consulting,
                        created_at,
                        answer_date,
                        reservation_date,
                        memo,
                        answer
                    }

                    this.setState({
                        setUp: resData.setUp,
                        consulting
                    })
                }
            })
            .catch(error => {
            console.log('something went wrong', error);
            });
    }


    editConsulting = async () => {

        // input validation form check
        let validationCheck = true;

        const {
            answer_status_id,
            answer_date,
            answer,
            manager_id,
            consulting_status_id,
            memo,
            reservation_date
        } = this.state.consulting;

        let payload = {
            answer_status_id,
            answer_date,
            answer,
            manager_id,
            consulting_status_id,
            memo,
            reservation_date
        }

        if ( !payload.answer_date ) {
            payload.answer_date = formatDateMySQL(new Date());
        } else {
            payload.answer_date = formatDateMySQL(payload.answer_date);
        }

        if ( !payload.reservation_date ) {
            payload.reservation_date = formatDateMySQL(new Date());
        } else {
            payload.reservation_date = formatDateMySQL(payload.reservation_date);
        }


        if ( validationCheck ) {
            await api.updateConsulting(this.currentConsultingID, payload, true)
                .then(res => {
                    Message({
                        message: '성공적으로 수정되었습니다.',
                        type: 'success'
                    });
                    console.log('successfully created', res);
                })
                .then(() => {
                    this.goBack();
                  })
                .catch(error => {
                    console.log('something went wrong', error);
                })
        }
    }

    deleteConsulting = async () => {

        await api.deleteConsulting(this.currentConsultingID, true)
            .then((res)=> {
                Message({
                    message: '성공적으로 삭제되었습니다.',
                    type: 'success'
                });
            })
            .then(() => {
                this.goBack();
            })
            .catch(error => {
                console.log('something went wrong', error);
            });
    }
        
    goBack = () => {
        this.history.push('/backend/consultings');
    }

    handleChange = (event, input) => {

        let newConsulting = {
            ...this.state.consulting
        };

        if (
            input === 'answer_status_id' || 
            input === 'answer_date' || 
            input === 'answer' || 
            input === 'manager_id' || 
            input === 'consulting_status_id' || 
            input === 'reservation_date' || 
            input === 'memo'
        ) {
            newConsulting = {
                ...newConsulting,
                [input]: event
            }
        } else {
            newConsulting = {
                ...newConsulting,
                [input]: event.target.value
            };
        }

        this.setState({
            consulting: newConsulting
        })
    }

    render() {
        const consulting = this.state.consulting;
        const { managers, consulting_statuses, answer_statuses } = this.state.setUp;
        const $user = this.props.$user;
        const higherAccess =  $user && ($user.isAdmin || $user.isNurse);
        console.log('this is backend answer consulting', consulting, this.props);
        return (
            <Fragment>
                <div className="backend-body">
                    <div className="backend-boardTop-style">
                        <h2 className="backend-boardTop-title">온라인 상담</h2>
                        <span className="backend-boardTop-text">온라인 상담 관리자 페이지 입니다.</span>
                    </div>

                    <ul className="backend-board-wrap">
                    {
                        columns.map((column, ind) => (
                            <Fragment key={ind}>
                                <li>
                                    <span className="backend-board-title">{columnsObj[column]}</span>
                                    <span className="backend-board-text">{column === 'details' ? ( typeof consulting[column] === 'string' ? parse(consulting[column]) : consulting[column] ) : consulting[column]}</span>
                                </li>
                            </Fragment>
                        ))
                    }
                        <li>
                            <span className="backend-board-title">{columnsObj.answer_status_id}</span>
                            <span className="backend-board-text">
                                <Select value={consulting.answer_status_id} placeholder="구분 선택" onChange={(e) => this.handleChange(e, 'answer_status_id')}>
                                    {(answer_statuses && answer_statuses.length > 0) ? answer_statuses.map(el => (
                                    <Select.Option key={el.id} label={el.answer_status_name} value={el.id} />
                                    )) : null}
                                </Select>
                            </span>
                        </li>
                        <li>
                            <span className="backend-board-title">{columnsObj.answer_date}</span>
                            <span className="backend-board-text">
                                <DayPickerInput
                                    formatDate={formatDate}
                                    format={DATE_FORMAT}
                                    parseDate={parseDate}
                                    placeholder={(!consulting.answer_date || typeof consulting.answer_date === 'string') ? `${dateFnsFormat(new Date(), DATE_FORMAT)}` : `${dateFnsFormat(consulting.answer_date, DATE_FORMAT)}`}
                                    onDayChange={(e) => this.handleChange(e, 'answer_date')}
                                />
                            </span>
                        </li>
                        <li>
                            <span className="backend-board-title">{columnsObj.answer}</span>
                            <span className="backend-board-text">
                                <Custom_Image_Quill setContents={consulting.answer} onChange={(e) => this.handleChange(e, 'answer')} />
                                {/* <Custom_Image_Font_Quill setContents={consulting.answer} onChange={(e) => this.handleChange(e, 'answer')} /> */}
                            </span>
                        </li>
                    </ul>
                </div>
               
            <div className="answer-body">    
                <div className="backend-boardTop-style">
                    <h2 className="backend-boardTop-title">관리자 메모</h2>
                    <span className="backend-boardTop-text">관리자 메모를 남겨주세요</span>
                </div>
                <ul className="backend-board-wrap">
                    <li>
                        <span className="backend-board-title">{columnsObj.manager_id}</span>
                        <span className="backend-board-text">
                            <Select value={consulting.manager_id} placeholder="담당자 선택" onChange={(e) => this.handleChange(e, 'manager_id')}>
                                {(managers && managers.length > 0) ? managers.map(el => (
                                <Select.Option key={el.id} label={el.manager_name} value={el.id} />
                                )) : null}
                            </Select>
                        </span>
                    </li>

                    <li>
                        <span className="backend-board-title">{columnsObj.consulting_status_id}</span>
                        <span className="backend-board-text">
                            <Select value={consulting.consulting_status_id} placeholder="상담결과 선택" onChange={(e) => this.handleChange(e, 'consulting_status_id')}>
                                {(consulting_statuses && consulting_statuses.length > 0) ? consulting_statuses.map(el => (
                                <Select.Option key={el.id} label={el.consulting_status_name} value={el.id} />
                                )) : null}
                            </Select>
                        </span>
                    </li>

                    <li>
                        <span className="backend-board-title">{columnsObj.reservation_date}</span>
                        <span className="backend-board-text">
                            <DayPickerInput
                                formatDate={formatDate}
                                format={DATE_FORMAT}
                                parseDate={parseDate}
                                placeholder={(!consulting.reservation_date || typeof consulting.reservation_date === 'string') ? `${dateFnsFormat(new Date(), DATE_FORMAT)}` : `${dateFnsFormat(consulting.reservation_date, DATE_FORMAT)}`}
                                onDayChange={(e) => this.handleChange(e, 'reservation_date')}
                            />
                        </span>
                    </li>

                    <li>
                        <span className="backend-board-title">{columnsObj.memo}</span>
                        <span className="backend-board-text">
                            <Custom_Image_Quill setContents={consulting.memo} onChange={(e) => this.handleChange(e, 'memo')} />
                            {/* <Custom_Image_Font_Quill setContents={consulting.memo} onChange={(e) => this.handleChange(e, 'memo')} /> */}
                        </span>
                    </li>
                </ul>
            

            <div style={{textAlign: 'right'}} className="backend-topBtn-wrap">
                
                <button className="btn-backbend btn-edit" onClick={this.editConsulting}>답변하기</button>
                <button className="btn-backbend btn-warning" onClick={this.deleteConsulting}>삭제하기</button>
                <button className="btn-backbend btn-check" onClick={this.goBack}>목록으로</button>
            </div>
            </div>
            </Fragment>
        )
    }

}

export default Backend_Consulting_Answer;