import React, { Fragment } from 'react';
import api from 'common/api';
import { Message } from 'element-react';
import { currentUsesDomain } from 'constants/index';
import { Link } from 'react-router-dom';

const imgUrl = currentUsesDomain.imgUrl;

const columnsObj = {
  'device': '접속 기기',
  're_pop_up_time': '재노출 시간',
  'start_date': '노출 시작일시',
  'end_date': '노출 종료일시',
  'layer_width': '이미지 넓이',
  'layer_height': '이미지 높이',
  'layer_x': '이미지 좌측 위치',
  'layer_y': '이미지 상단 위치',
  'title': '팝업 제목',
  'photo_file': '내용'
}

const columns = [
  'title',
  'writer',
  'created_at',
  'views'
];


class Backend_Popup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          popup: {}
        }
    
        this.history = this.props.history;
        this.match = this.props.match;
        this.currentPopupID = this.match.params.id;
    
    }
    
    componentDidMount() {
        this.getDatas(this.currentPopupID);
    }
    
    getDatas = async (currentPopupID) => {
        await api.getPopup(currentPopupID, true)
          .then(res => {

            const resData = res.data.data;
            if (resData) {
                let { start_date, end_date, mobile, pc } = resData;

                start_date = start_date ? ( typeof start_date === 'string' ? start_date.slice(0, 10) : start_date.toJSON().slice(0,10) ) : start_date;
                end_date = end_date ? ( typeof end_date === 'string' ? end_date.slice(0, 10) : end_date.toJSON().slice(0,10) ) : end_date;
            
                let device = '해당없음';
                if (mobile && pc) {
                  device = '모두';
                } else if (mobile) {
                  device = 'mobile';
                } else if (pc) {
                  device = 'PC';
                }

                let popup = {
                    ...resData,
                    start_date,
                    end_date,
                    device
                }

                this.setState({
                    popup
                });
            }
            
          })
          .catch(error => {
            console.log('something went wrong', error);
          });
    }
    
    editPopup = () => {
        this.history.push(`${this.match.url}/edit`);
    }
    
    deletePopup = async () => {
    
        await api.deletePopup(this.currentPopupID, true)
          .then((res)=> {
            Message({
              message: '성공적으로 삭제되었습니다.',
              type: 'success'
            });
          })
          .then(() => {
            this.goBack();
          })
          .catch(error => {
            console.log('something went wrong', error);
          });
    }
    
    goBack = () => {
        this.history.push('/backend/popups');
    }

    render() {
        const popup = this.state.popup;
        console.log('this is popup', popup, this.props);
        const px = 'px';

        return (
          <div className="backend-body">
            <div className="backend-boardTop-style">
              <h2 className="backend-boardTop-title">팝업관리</h2>
              <p className="backend-boardTop-text">팝업 관리자 페이지 입니다.</p>
            </div>
              {
              popup.id ? (
                <Fragment>
                    <ul className="backend-board-wrap">
                        <li>
                            <p className="backend-board-title">{columnsObj.device}</p>
                            <p className="backend-board-text">{popup.device}</p>
                        </li>

                        <li>
                            <p className="backend-board-title">{columnsObj.re_pop_up_time}</p>
                            <p className="backend-board-text">{popup.re_pop_up_time}시간</p>
                        </li>

                        <li>
                            <p className="backend-board-title">{columnsObj.start_date}</p>
                            <p className="backend-board-text">{popup.start_date}</p>
                        </li>

                        <li>
                            <p className="backend-board-title">{columnsObj.end_date}</p>
                            <p className="backend-board-text">{popup.end_date}</p>
                        </li>

                        <li>
                            <p className="backend-board-title">{columnsObj.layer_x}</p>
                            <p className="backend-board-text">{popup.layer_x + px}</p>
                        </li>

                        <li>
                            <p className="backend-board-title">{columnsObj.layer_y}</p>
                            <p className="backend-board-text">{popup.layer_y + px}</p>
                        </li>

                        <li>
                            <p className="backend-board-title">{columnsObj.layer_width}</p>
                            <p className="backend-board-text">{popup.layer_width + px}</p>
                        </li>

                        <li>
                            <p className="backend-board-title">{columnsObj.layer_height}</p>
                            <p className="backend-board-text">{popup.layer_height + px}</p>
                        </li>

                        <li>
                            <p className="backend-board-title">{columnsObj.title}</p>
                            <p className="backend-board-text">{popup.title}</p>
                        </li>
                        {
                            popup.photo_file && (
                                <Fragment>
                                    <li>
                                        <p className="backend-board-title">{columnsObj.photo_file}</p>
                                        <p className="backend-board-text">
                                            <img className="beforeAfter-img" src={imgUrl + popup.photo_file}/>
                                        </p>
                                    </li>
                                </Fragment>  
                            )
                        }
                    </ul>
                  
                  <div style={{textAlign: 'right'}} className="backend-topBtn-wrap">
                   
                    <button className="btn-backbend btn-edit" onClick={this.editPopup}>수정하기</button>
                    <button className="btn-backbend btn-warning" onClick={this.deletePopup}>삭제하기</button>
                    <button className="btn-backbend btn-check" onClick={this.goBack}>목록으로</button>
                  </div>

                  <div className="prev-contianer">
                  {
                    popup.previous ? (
                      <Fragment>
                        <div className="prev-wrap prev-wrap-boder">
                          <span className="border-arrow">
                            <Link to={`${this.match.path.replace(':id', popup.previous.id)}`}>
                            <svg  className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><path d="M0 16.67l2.829 2.83 9.175-9.339 9.167 9.339 2.829-2.83-11.996-12.17z" fill="##615454"/></svg> 
                              이전글
                            </Link>
                          </span>
                          <span className="border-prev-body">{popup.previous.title}</span>
                        </div>
                      </Fragment>
                    ) : null
                  }
                  {
                    popup.next ? (
                      <Fragment>
                        <div className="prev-wrap">
                          <span className="border-arrow">
                          <Link to={`${this.match.path.replace(':id', popup.next.id)}`}>
                          <svg className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z" fill="##615454"/></svg>
                            다음글
                          </Link>
                        </span>
                        <span className="border-prev-body">{popup.next.title}</span>
                        </div>
                      </Fragment>
                    ) : null
                  }
                </div>
              </Fragment>
              ) : (
                <div>
                  something went wrong..
                </div>
              )
            }
      
            </div>
        )
    }
}

export default Backend_Popup;