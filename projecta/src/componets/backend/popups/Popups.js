import React from 'react';
import api from 'common/api';

import { Select, Pagination, Message } from 'element-react';

const columns = [
    { prop: 'checkbox', label: '선택' },
    { prop: 'numIndex', label: '번호'},
    { prop: 'device', label: '접속기기' },
    { prop: 'title', label: '팝업 제목'},
    { prop: 'start_date', label: '시작일' },
    { prop: 'end_date', label: '종료일' }
  ];
  
  let rows = [];
  
  function getIndexedRows(datas) {
    let numIndex = datas.per_page * (datas.current_page - 1);  
  
    return datas.data.map(data => {
      numIndex = data.id;

      let { start_date, end_date, mobile, pc } = data;

      start_date = start_date ? ( typeof start_date === 'string' ? start_date.slice(0, 10) : start_date.toJSON().slice(0,10) ) : start_date;
      end_date = end_date ? ( typeof end_date === 'string' ? end_date.slice(0, 10) : end_date.toJSON().slice(0,10) ) : end_date;
  
      let device = '해당없음';
      if (mobile && pc) {
        device = '모두';
      } else if (mobile) {
        device = 'mobile';
      } else if (pc) {
        device = 'PC';
      }
      return {
        ...data,
        numIndex,
        checkbox: false,
        start_date,
        end_date,
        device
      }
    });
  }

class Backend_Popups extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          page: 0,
          total: 0,
          perPage: 0,
          checkboxObj: [],
          searchData: {
            categ: 0,
            searchOption: 0,
            query: ''
          },
          isSearched: false,
          checkAll: false
        }
    }
    
    componentDidMount() {
        this.getDatas();
    }
    
    getDatas = async (page) => {
        await api.getPopups(page)
            .then(res => {
                const resData = res.data.data;

                if (resData) {
                    rows = getIndexedRows(resData);

                    this.setState({
                        page: resData.current_page,
                        total: resData.last_page,
                        perPage: resData.per_page,
                        checkboxObj: rows,
                        isSearched: false
                    });
                }
            })
            .catch(error => {
                console.log('something went wrong', error);
            });
    }

    getSearchedDatas = async (page) => {
      const { searchData } = this.state;
      const payload = {
        search_word: searchData.query
      };
  
      await api.getSearchPopup(payload, page)
          .then(res => {
              const resData = res.data.data;

              if (resData) {
                  rows = getIndexedRows(resData);

                  this.setState({
                      page: resData.current_page,
                      total: resData.last_page,
                      perPage: resData.per_page,
                      checkboxObj: rows,
                      isSearched: true
                  });
                  console.log('successfully searched..', res, rows);
              }
          })
          .catch(error => {
              console.log(error);
          })
    }
    
    handleChangePage = (selectedPage) => {
      const { isSearched } = this.state;
      if (isSearched) {
        this.getSearchedDatas(selectedPage);
      } else {
        this.getDatas(selectedPage);
      }
    };
    
    handleCheckBox = (event, id) => {
        const ind = this.state.checkboxObj.findIndex(cur => cur.id === id);
        const newTarget = {
          ...this.state.checkboxObj[ind],
          checkbox: event.target.checked
        }
        let newCheckboxObj = [
          ...this.state.checkboxObj.slice(0, ind),
          newTarget,
          ...this.state.checkboxObj.slice(ind + 1),
        ]
        
        const checkAll = newCheckboxObj.length === newCheckboxObj.filter(cur => cur.checkbox).length;
        this.setState({
          checkboxObj: newCheckboxObj,
          checkAll
        })
    }
    
    handleSelectAll = () => {
      let checkAll = !this.state.checkAll;
  
      let newCheckboxObj = this.state.checkboxObj.map(cur => ({
        ...cur,
        checkbox: checkAll
      }));
  
      this.setState({
        checkboxObj: newCheckboxObj,
        checkAll
      })
    }
    
    deletePopupAll = async () => {
    
        const checkedArr = this.state.checkboxObj.filter(popup => popup.checkbox);
        const checkedIDs = checkedArr.map(popup => popup.id); 
        if (checkedIDs.length > 0) {
            await api.deletePopupsAll({ids: checkedIDs})
                .then(res => {
                    Message({
                        message: '성공적으로 삭제되었습니다.',
                        type: 'success'
                    });
                })
                .then(() => {
                    this.getDatas();
                })
                .catch(err => {
                    console.log('something went wrong', err);
                });
        }
    }

    deletePopup = async (ID) => {
    
        if (ID) {
            await api.deletePopup(ID)
                .then(res => {
                    Message({
                    message: '성공적으로 삭제되었습니다.',
                    type: 'success'
                    });
                })
                .then(() => {
                    this.getDatas();
                })
                .catch(err => {
                    console.log('something went wrong', err);
                });
        }
    }
    
    searchClickHanlder = async () => {
        console.log('clicking...', this.state.searchData);
        const { searchData, isSearched } = this.state;

        if (searchData.query === '') {
          if (isSearched) {
            this.getDatas();
          }
        } else {
          this.getSearchedDatas();
        }
    }
    
    categChangeHandler = (value) => {
        this.searchChangeHandler(value,'categ');
    }
    searchOptionChangeHandler = (value) => {
        this.searchChangeHandler(value,'searchOption');
    }
    
    searchChangeHandler = (target, type) => {
        let newSearchData = {
          ...this.state.searchData
        };
        if (type === 'query') {
          newSearchData = {
            ...newSearchData,
            [type]: target.target.value
          }
        } else {
          newSearchData = {
            ...newSearchData,
            [type]: target
          };
        }
    
        this.setState({
          searchData: newSearchData
        });
    }

    searchClear = () => {
      this.setState({
        searchData: {
          query: ''
        }
      })
    }
    
    handleLink = (id, toWhere) => {
        let linkURLs = `${this.props.match.url}/popup/${id}`;
        if (toWhere === 'edit') {
            linkURLs = `${this.props.match.url}/popup/${id}/edit`;
        } else if (toWhere === 'create') {
            linkURLs = `${this.props.match.url}/popup/create`;
        }
        
        this.props.history.push(linkURLs);
    }

    render() {
      console.log('this is backend popups', this.state, this.props);
      const { searchData, checkboxObj } = this.state;
      const { categ, searchOption, query } = searchData;
      const consultingCategories = this.props.consultingCategories;
      const $user = this.props.$user;
      const higherAccess =  $user && ($user.isAdmin || $user.isNurse);

        return (
         
        <div className="backend-body">
          <div className="backend-table-wrap">
            <div className="backend-boardTop-style">
              <h2 className="backend-boardTop-title">팝업관리</h2>
              <p className="backend-boardTop-text">팝업 관리자 페이지 입니다.</p>
            </div>

            <div className="backend-topBtn-wrap">
              <button className="btn-backbend btn-check" onClick={this.handleSelectAll}>{this.state.checkAll ? '전체해제' : '전체선택'}</button>
              <button className="btn-backbend btn-warning" onClick={this.deletePopupAll}>선택삭제</button> 
            </div>
            <table>
              <colgroup>
                <col className="col-100"/>
                <col className="col-100"/>
                <col className="col-100"/>
                <col/>
                <col className="col-100"/>
                <col className="col-100"/>
              </colgroup>
            <thead>
              <tr>
                {
                  columns.map((column, index) => (
                    <td key={index} >
                      {column.label}
                    </td>
                  ))
                }
              </tr>
            </thead>
            <tbody className="backend-tbody">
                {
                  checkboxObj.map((row, index) => (
                    <tr key={index} >
                      {
                        (
                          columns.map((column, colIndex) => {
                            return column.prop === 'checkbox' ? (
                              <td key={colIndex}>
                                <input type="checkbox" checked={row[column.prop]} onChange={(e) => this.handleCheckBox(e, row.id)} />
                              </td>
                            ) : (
                              <td style={{cursor:'pointer'}} key={colIndex} onClick={() => this.handleLink(row.id)}>
                                {row[column.prop]}
                              </td>
                            )
                          })
                        )
                      }
                    </tr>
                  ))
                }
              </tbody>
            </table>
          </div>
          
            <br />
            <button className="btn-backbend btn-backend-writing" onClick={() => this.handleLink('', 'create')}>추가하기</button>
            
            <Pagination 
              style={{display:'flex',justifyContent:'center'}} 
              layout="prev, pager, next" 
              currentPage={this.state.page} 
              pageCount={this.state.total} 
              pageSize={this.state.perPage} 
              small={true}
              onCurrentChange={this.handleChangePage}
            />
            <div className="board-search">
              <span>
                <input className="board-input search-box" type="text" value={query} onChange={(e) => this.searchChangeHandler(e, 'query')} />
                {
                  (query) && <i className="el-icon-circle-cross" onClick={this.searchClear}></i> 
                }
              </span>
              <a className="btn-search search-box" onClick={this.searchClickHanlder}>검색</a>
            </div>
           
          </div>
        )
    }
}

export default Backend_Popups;
