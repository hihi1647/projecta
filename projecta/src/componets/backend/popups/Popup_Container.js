import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Popup from './Popup';
import Popups from './Popups';
import Popup_Create from './Create';
import Popup_Edit from './Edit';

class Popup_Container extends React.Component {
    render() {
        const { path } = this.props.match;

        const { $user, isAuthenticated, changeState } = this.props;
        return (
            <Switch>
                <Route path={`${path}/popup/:id/edit`} render={(props) => <Popup_Edit {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={`${path}/popup/create`} render={(props) => <Popup_Create {...props} $user={$user} />} />
                <Route path={`${path}/popup/:id`} render={(props) => <Popup {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={path} render={(props) => <Popups {...props} $user={$user} />} />
            </Switch>
        )
    }
}

export default Popup_Container;