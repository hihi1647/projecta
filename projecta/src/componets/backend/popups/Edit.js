import React, { Fragment } from 'react';
import api from 'common/api';
import { Message } from 'element-react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import dateFnsFormat from 'date-fns/format';

import { parseDate, formatDate, formatDateMySQL, toDateObject } from 'common/Utils';
import { DATE_FORMAT, currentUsesDomain } from 'constants/index';

const imgUrl = currentUsesDomain.imgUrl;

const columnsObj = {
    'device': '접속 기기',
    're_pop_up_time': '재노출 시간',
    'start_date': '노출 시작일시',
    'end_date': '노출 종료일시',
    'layer_width': '팝업 넓이',
    'layer_height': '팝업 높이',
    'layer_x': '팝업 좌측 위치',
    'layer_y': '팝업 상단 위치',
    'title': '팝업 제목',
    'photo_file': '내용'
}

const columns = [
    'title',
    'writer',
    'created_at',
    'views'
];

class Backend_Popup_Edit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            popup: {},
            photo_file: '',
            start_date_helper: false,
            end_date_helper: false,
            device: {
                all: false,
                pc: false,
                mobile: false
            }
        }

        this.history = this.props.history;
        this.match = this.props.match;
        this.currentPopupID = this.match.params.id;
    }

    componentDidMount() {
        this.getDatas(this.currentPopupID);
    }

    getDatas = async (currentPopupID) => {
        await api.getUpdatePopup(currentPopupID, true)
            .then(res => {
                const resData = res.data.data;

                if (resData) {
                    let { start_date, end_date, mobile, pc, photo_file } = resData;

                    start_date = toDateObject(start_date);
                    end_date = toDateObject(end_date);

                    let popup = {
                        ...resData,
                        start_date,
                        end_date
                    };

                    let device = {
                        all: false,
                        pc: false,
                        mobile: false
                    };

                    if (mobile && pc) {
                        device.all = true;
                        device.pc = true;
                        device.mobile = true;
                    } else if (mobile) {
                        device.mobile = true;
                    } else if (pc) {
                        device.pc = true;
                    }

                    photo_file = imgUrl + photo_file;

                    this.setState({
                        popup,
                        photo_file,
                        device
                    });
                }
            })
            .catch(error => {
                console.log('something went wrong', error);
            });
    }


    editPopup = async () => {

        // input validation form check
        let validationCheck = true;

        const formData = new FormData();
        const popupData = this.state.popup;
        const device = this.state.device;
        const payloadKey = [
            'mobile',
            'pc',
            're_pop_up_time',
            'start_date',
            'end_date',
            'layer_width',
            'layer_height',
            'layer_x',
            'layer_y',
            'title',
            'photo_file'
        ];

        payloadKey.forEach(cur => {
            if (cur === 'start_date' && popupData[cur]) {
                let startDate = popupData[cur];
                startDate = typeof startDate !== 'string' ? startDate.toJSON() : startDate;
                startDate = startDate.slice(0, 10) + " 00:00:00";

                formData.append(cur, startDate);
            } else if (cur === 'end_date' && popupData[cur]) {
                let endDate = popupData[cur];
                endDate = typeof endDate !== 'string' ? endDate.toJSON() : endDate;
                endDate = endDate.slice(0, 10) + " 23:59:59";

                formData.append(cur, endDate);
            } else if (cur === 'pc' || cur === 'mobile') {
                let value;
                device[cur] ? value = 1 : value = 0;
                formData.append(cur, value);
            } else {
                formData.append(cur, popupData[cur]);
            }
        })

        if (validationCheck) {
            await api.updatePopup(this.currentPopupID, formData, true)
                .then(res => {
                    Message({
                        message: '성공적으로 수정되었습니다.',
                        type: 'success'
                    });
                    console.log('successfully edit', res);

                })
                .then(() => {
                    this.goBack();
                })
                .catch(error => {
                    console.log('something went wrong', error);
                })
        }
    }

    deletePopup = async () => {

        await api.deletePopup(this.currentPopupID, true)
            .then((res) => {
                Message({
                    message: '성공적으로 삭제되었습니다.',
                    type: 'success'
                });
            })
            .then(() => {
                this.goBack();
            })
            .catch(error => {
                console.log('something went wrong', error);
            });
    }


    cancel = () => {
    }

    goBack = () => {
        this.history.push('/backend/popups');
    }

    handleCheckBox = (event, input) => {

        let device = this.state.device;

        let newDevice = {};

        if (input === 'all') {
            if (device.all) {
                newDevice = {
                    all: false,
                    pc: false,
                    mobile: false
                }
            } else {
                newDevice = {
                    all: true,
                    pc: true,
                    mobile: true
                }
            }
        } else {
            newDevice = {
                ...device,
                [input]: event.target.checked
            };
            if (newDevice.pc && newDevice.mobile) {
                newDevice = {
                    ...newDevice,
                    all: true
                }
            } else {
                newDevice = {
                    ...newDevice,
                    all: false
                }
            }

        }
        this.setState({
            device: newDevice
        })
    }

    handleChange = (event, input) => {

        let newPopup = {
            ...this.state.popup
        };

        if (input === 'start_date' || input === 'end_date') {
            newPopup = {
                ...newPopup,
                [input]: event
            }
        } else {
            newPopup = {
                ...newPopup,
                [input]: event.target.value
            };
        }

        this.setState({
            popup: newPopup
        })
    }

    handleImgChange = (event, input) => {

        let newPopup = {
            ...this.state.popup
        };

        newPopup = {
            ...newPopup,
            [input]: event.target.files[0]
        };

        this.setState({
            popup: newPopup,
            [input]: window.URL.createObjectURL(event.target.files[0])
        })
    }

    render() {
        console.log('this is edit popup', this.props, this.state);
        const { device, popup, photo_file } = this.state;
        return (
            <div className="event-create-body">
                <div className="backend-boardTop-style">
                    <h2 className="backend-boardTop-title">팝업관리</h2>
                    <p className="backend-boardTop-text">수정 페이지 입니다.</p>
                </div>
                {
                    popup.id ? (
                        <Fragment>
                            <ul className="backend-board-create-wrap">
                                <li>
                                    <p className="backend-board-title">{columnsObj.device}</p>
                                    <span className="backend-board-text">
                                        <input type="checkbox" checked={device.all} onChange={(e) => this.handleCheckBox(e, 'all')} /> 모두
                                    <input type="checkbox" checked={device.pc} onChange={(e) => this.handleCheckBox(e, 'pc')} /> Pc
                                    <input type="checkbox" checked={device.mobile} onChange={(e) => this.handleCheckBox(e, 'mobile')} /> Mobile
                                </span>
                                </li>
                                <li>
                                    <p className="backend-board-title">{columnsObj.re_pop_up_time}</p>
                                    <span className="backend-board-text">
                                        <input className="backend-popup-input" type="number" value={popup.re_pop_up_time} onChange={(e) => this.handleChange(e, 're_pop_up_time')} />시간
                                    </span>
                                </li>

                                <li>
                                    <p className="backend-board-title">{columnsObj.start_date}</p>
                                    <span className="backend-board-text">
                                        <DayPickerInput
                                            formatDate={formatDate}
                                            format={DATE_FORMAT}
                                            parseDate={parseDate}
                                            value={popup.start_date}
                                            placeholder={(!popup.start_date || typeof popup.start_date === 'string') ? `${dateFnsFormat(new Date(), DATE_FORMAT)}` : `${dateFnsFormat(popup.start_date, DATE_FORMAT)}`}
                                            onDayChange={(e) => this.handleChange(e, 'start_date')}
                                        />
                                    </span>
                                </li>

                                <li>
                                    <p className="backend-board-title">{columnsObj.end_date}</p>
                                    <span className="backend-board-text">
                                        <DayPickerInput
                                            formatDate={formatDate}
                                            format={DATE_FORMAT}
                                            parseDate={parseDate}
                                            value={popup.end_date}
                                            placeholder={(!popup.end_date || typeof popup.end_date === 'string') ? `${dateFnsFormat(new Date(), DATE_FORMAT)}` : `${dateFnsFormat(popup.end_date, DATE_FORMAT)}`}
                                            onDayChange={(e) => this.handleChange(e, 'end_date')}
                                        />
                                    </span>
                                </li>

                                <li>
                                    <p className="backend-board-title">{columnsObj.layer_x}</p>
                                    <span className="backend-board-text">
                                        <input className="backend-popup-input" type="number" value={popup.layer_x} onChange={(e) => this.handleChange(e, 'layer_x')} />px
                                </span>
                                </li>

                                <li>
                                    <p className="backend-board-title">{columnsObj.layer_y}</p>
                                    <span className="backend-board-text">
                                        <input className="backend-popup-input" type="number" value={popup.layer_y} onChange={(e) => this.handleChange(e, 'layer_y')} />px
                                </span>
                                </li>

                                <li>
                                    <p className="backend-board-title">{columnsObj.layer_width}</p>
                                    <span className="backend-board-text">
                                        <input className="backend-popup-input" type="number" value={popup.layer_width} onChange={(e) => this.handleChange(e, 'layer_width')} />px
                                </span>
                                </li>

                                <li>
                                    <p className="backend-board-title">{columnsObj.layer_height}</p>
                                    <span className="backend-board-text">
                                        <input className="backend-popup-input" type="number" value={popup.layer_height} onChange={(e) => this.handleChange(e, 'layer_height')} />px
                                </span>
                                </li>

                                <li>
                                    <p className="backend-board-title">{columnsObj.title}</p>
                                    <span className="backend-board-text">
                                        <input className="backend-board-create-input" type="text" value={popup.title} onChange={(e) => this.handleChange(e, 'title')} />
                                    </span>
                                </li>

                                <li>
                                    <p className="backend-board-title">{columnsObj.photo_file}</p>
                                    <span className="backend-create-photo">
                                        {popup.photo_file && <img src={photo_file} style={{ width: '200px' }} />}
                                        <input className="backend-file-input" type="file" name="file" onChange={(e) => this.handleImgChange(e, 'photo_file')} />
                                    </span>
                                </li>
                            </ul>

                            <div style={{ textAlign: 'right' }} className="backend-topBtn-wrap">
                                
                                <button className="btn-backbend btn-edit" onClick={this.editPopup}>수정완료</button>
                                <button className="btn-backbend btn-warning" onClick={this.deletePopup}>삭제하기</button>
                                <button className="btn-backbend btn-check" onClick={this.goBack}>목록으로</button>
                            </div>

                        </Fragment>
                    ) : (
                            <div>
                                something went wrong..
                            </div>
                        )
                }
            </div>
        )
    }
}

export default Backend_Popup_Edit;