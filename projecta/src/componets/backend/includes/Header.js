import React from 'react';
// import '../../../assets/css/backend.css'
import { Link } from 'react-router-dom';

const router = [
  'before_afters',
  'consultings',
  'events',
  'expense_guides',
  'notices',
  'reservations',
  'popups'
];

const ROUTER_NAME = {
  before_afters: '전후사진',
  consultings: '온라인 상담',
  events: '이벤트',
  expense_guides: '비용상담',
  notices: '공지사항',
  reservations: '온라인 예약',
  popups: '팝업'
};

class Backend_Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: 'before_afters'
    };
  }

  setSelectHandler = (event, link) => {
    this.setState({
      selected: link
    })
  }

  render() {
    return (
      <div className="backend-aside-menu">
        <ul className="backend-menu">
          <li className="backend-menu-logo"><Link to="/"><span className="icon-home"/></Link></li>
          {
            router.map(link => (
              <li className="backend-menu-list"
                key={link}
                selected={this.state.selected === link}
                onClick={(event) => this.setSelectHandler(event, link)}
              >
                <Link to={`${this.props.url}/${link}`}>
                  {ROUTER_NAME[link]}
                </Link>
              </li>
            ))
          }
        </ul>
      </div>
        
    )
  }
}

export default Backend_Header;

