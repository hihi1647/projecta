import React from 'react';
import '../../App.css'; 
import { Backend_Header } from 'pages';
import Container from './Container';


class Backend extends React.Component {
  render() {
      const { url } = this.props.match;
    return (
      <div className="backend-container">
        <Backend_Header url={url} />
        <Container {...this.props} />
      </div>
    )
  }
}

export default Backend;
