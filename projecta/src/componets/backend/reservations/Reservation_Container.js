import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Reservation from './Reservation';
import Reservations from './Reservations';

class Reservation_Container extends React.Component {
    render() {
        const { path } = this.props.match;

        const { $user, isAuthenticated, changeState } = this.props;
        return (
            <Switch>
                <Route path={`${path}/reservation`} render={(props) => <Reservation {...props} $user={$user} />} />
                <Route path={path} render={(props) => <Reservations {...props} $user={$user} />} />
            </Switch>
        )
    }
}

export default Reservation_Container;