import React from 'react';
import { Route, Switch } from 'react-router-dom';

import BeforeAfter_Container from './before_afters/BeforeAfter_Container';
import Consulting_Container from './consultings/Consulting_Container';
import Event_Container from './events/Event_Container';
import ExpenseGuide_Container from './expense_guides/ExpenseGuide_Container';
import Notice_Container from './notices/Notice_Container'
import Popup_Container from './popups/Popup_Container'
import Reservation_Container from './reservations/Reservation_Container'

const ROUTES = [
    {
      label: '/backend/before_afters',
      children: BeforeAfter_Container
    },
    {
      label: '/backend/consultings',
      children: Consulting_Container
    },
    {
      label: '/backend/events',
      children: Event_Container
    },
    {
        label: '/backend/expense_guides',
        children: ExpenseGuide_Container
    },
    {
      label: '/backend/notices',
      children: Notice_Container
    },
    {
        label: '/backend/popups',
        children: Popup_Container
    },
    {
        label: '/backend/reservations',
        children: Reservation_Container
    }
  ];

class Container extends React.Component {
    render() {
        console.log('this is from container', this.props);

        const path = this.props.match.path;
        const { $user } = this.props;
        const parentProps = { $user };

        return (
            <div className="backend-body-container">
                <Switch>
                    <Route exact path={path} render={()=>(<h3 className="backend-body">관리자 페이지 메인</h3>)}/>
                    {
                        ROUTES.map((route, index) => {
                            const TagName = route.children;
                            
                            if (typeof route.children !== 'undefined') {
                                return (
                                    <Route path={route.label} key={index} render={props => <TagName {...props} {...parentProps} />} />
                                )
                            }

                            {/* 에러페이지 */}
                            return (
                                <h3 className="backend-body">에러페이지</h3>
                            )
                        })
                    }
                    {/* 에러페이지 */}
                    <Route path="*" render={()=>(<h3 className="backend-body">에러페이지</h3>)} />
                </Switch>
            </div>
        )
    }
}

export default Container;
