import React from 'react';
import { Route, Switch } from 'react-router-dom';
import ExpenseGuide from './ExpenseGuide';
import ExpenseGuides from './ExpenseGuides';

import api from 'common/api';

class ExpenseGuide_Container extends React.Component {
    state = {
        expenseGuideCategories: {},
    };
    
    componentDidMount() {
        this.getDatas();
    }

    getDatas = async () => {
        await api.getExpenseGuideCategories(true)
            .then(res => {
                this.setState({
                  expenseGuideCategories: res.data.data
                })
            })
            .catch(error => {
              console.log('something went wrong with expense guide category', error);
            });
    }

    render() {
        const { expenseGuideCategories } = this.state
        const { path } = this.props.match;

        const { $user, isAuthenticated, changeState } = this.props;
        return (
            <Switch>
                <Route path={`${path}/expense_guide/:id`} render={(props) => <ExpenseGuide {...props} $user={$user} key={props.match.params.id} />} />
                <Route path={path} render={(props) => <ExpenseGuides {...props} $user={$user} expenseGuideCategories={expenseGuideCategories} />} />
            </Switch>
        )
    }
}

export default ExpenseGuide_Container;