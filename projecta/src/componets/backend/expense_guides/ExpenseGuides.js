import React from 'react';
import api from 'common/api';

import { Select, Pagination, Message } from 'element-react';

const columns = [
    { prop: 'checkbox', label: '선택' },
    { prop: 'numIndex', label: '번호'},
    { prop: 'main_category_name', label: '구분'},
    { prop: 'title', label: '제목'},
    { prop: 'created_at', label: '작성일'},
    { prop: 'phone', label: '전화번호'},
    { prop: 'manager_name', label: '담당자'},
    { prop: 'consulting_status_name', label: '처리결과'}
];
  
let rows = [];
  
function getIndexedRows(datas) {
    let numIndex = datas.per_page * (datas.current_page - 1);  
  
    return datas.data.map(data => {
      numIndex = data.id;
  
      let created_at = data.created_at;
      created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;
  
      return {
        ...data,
        numIndex,
        checkbox: false,
        created_at
      }
    });
}


class ExpenseGuides extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          page: 0,
          total: 0,
          perPage: 0,
          checkboxObj: [],
          searchData: {
            categ: 0,
            searchOption: 0,
            query: ''
          },
          isSearched: false,
          checkAll: false
        }
    }
    
    componentDidMount() {
        this.getDatas();
    }
    
    getDatas = async (page) => {
        await api.getExpenseGuides(page)
            .then(res => {
                const resData = res.data.data;

                if (resData) {
                    rows = getIndexedRows(resData);

                    this.setState({
                      page: resData.current_page,
                      total: resData.last_page,
                      perPage: resData.per_page,
                      checkboxObj: rows,
                      isSearched: false
                    });
                }
            
            })
            .catch(error => {
                console.log('something went wrong', error);
            });
    }

    getSearchedDatas = async (page) => {
      const { searchData } = this.state;
      const payload = {
        search_word: searchData.query,
        main_category_id: searchData.categ,
        search_option_id: searchData.searchOption
      };
  
      await api.getSearchExpenseGuide(payload, page)
        .then(res => {
          const resData = res.data.data;
  
          if (resData) {
            rows = getIndexedRows(resData);
  
            this.setState({
              page: resData.current_page,
              total: resData.last_page,
              perPage: resData.per_page,
              checkboxObj: rows,
              isSearched: true
            });
            console.log('successfully searched..', res, rows);
          }
          
        })
        .catch(error => {
          console.log(error);
        })
    }
    
    handleChangePage = (event, selectedPage) => {
      const { isSearched } = this.state;
      if (isSearched) {
        this.getSearchedDatas(selectedPage);
      } else {
        this.getDatas(selectedPage);
      }
    };
    
    handleCheckBox = (event, id) => {
        const ind = this.state.checkboxObj.findIndex(cur => cur.id === id);
        const newTarget = {
          ...this.state.checkboxObj[ind],
          checkbox: event.target.checked
        }
        let newCheckboxObj = [
          ...this.state.checkboxObj.slice(0, ind),
          newTarget,
          ...this.state.checkboxObj.slice(ind + 1),
        ]
        
        const checkAll = newCheckboxObj.length === newCheckboxObj.filter(cur => cur.checkbox).length;
        this.setState({
          checkboxObj: newCheckboxObj,
          checkAll
        })
    }
    
    handleSelectAll = () => {
      let checkAll = !this.state.checkAll;
  
      let newCheckboxObj = this.state.checkboxObj.map(cur => ({
        ...cur,
        checkbox: checkAll
      }));
  
      this.setState({
        checkboxObj: newCheckboxObj,
        checkAll
      })
    }
    
    deleteExpenseGuide = async () => {
    
        const checkedArr = this.state.checkboxObj.filter(expense => expense.checkbox);
        const checkedIDs = checkedArr.map(expense => expense.id); 
        if (checkedIDs.length > 0) {
           await api.deleteExpenseGuidesAll({ids: checkedIDs})
            .then(res => {
              Message({
                message: '성공적으로 삭제되었습니다.',
                type: 'success'
              });
            })
            .then(() => {
              this.getDatas();
            })
            .catch(err => {
              console.log('something went wrong', err);
            });
        }
    }
    
    searchClickHanlder = async () => {
        console.log('clicking...', this.state.searchData);
        const { searchData, isSearched } = this.state;

        if (searchData.searchOption === 0 && searchData.categ === 0 && searchData.query === '') {
          if (isSearched) {
            this.getDatas();
          }
        } else {
          this.getSearchedDatas();
        }
    }
    
    categChangeHandler = (value) => {
        this.searchChangeHandler(value,'categ');
    }
    searchOptionChangeHandler = (value) => {
        this.searchChangeHandler(value,'searchOption');
    }
    
    searchChangeHandler = (target, type) => {
        let newSearchData = {
          ...this.state.searchData
        };
        if (type === 'query') {
          newSearchData = {
            ...newSearchData,
            [type]: target.target.value
          }
        } else {
          newSearchData = {
            ...newSearchData,
            [type]: target
          };
        }
    
        this.setState({
          searchData: newSearchData
        });
    }

    searchClear = () => {
      this.setState({
        searchData: {
          categ: 0,
          searchOption: 0,
          query: ''
        }
      })
    }
    
    handleLink = (id) => {
        this.props.history.push(`${this.props.match.url}/expense_guide/${id}`);
    }


    render() {
        console.log('this is backend expenseGuides', this.state, this.props);
        const { searchData, checkboxObj } = this.state;
        const { categ, searchOption, query } = searchData;
        const expenseGuideCategories = this.props.expenseGuideCategories;
        const $user = this.props.$user;
        const higherAccess =  $user && ($user.isAdmin || $user.isNurse);

        return (
             
        <div className="backend-body">
          <div className="backend-table-wrap">
              <div className="backend-boardTop-style">
                <h2 className="backend-boardTop-title">비용안내</h2>
                <p className="backend-boardTop-text">비용안내 관리자 페이지 입니다.</p>
              </div>
  
              <div className="backend-topBtn-wrap">
                <button className="btn-backbend btn-check" onClick={this.handleSelectAll}>{this.state.checkAll ? '전체해제' : '전체선택'}</button>
                <button className="btn-backbend btn-warning" onClick={this.deleteExpenseGuide}>선택삭제</button>
              </div>
      
                 
      
              <table>
              {/* <colgroup>
                <col className="col-100"/>
                <col className="col-100"/>
                <col className="col-100"/>
                <col/>
                <col className="col-100"/>
                <col className="col-100"/>
                <col className="col-100"/>
                <col className="col-100"/>
              </colgroup> */}
                      <thead>
                        <tr>
                          {
                            columns.map((column, index) => (
                              <td key={index} >
                                {column.label}
                              </td>
                            ))
                          }
                        </tr>
                      </thead>
                      <tbody className="backend-tbody">
                        {
                          checkboxObj.map((row, index) => (
                            <tr key={index} >
                              {
                                (
                                  columns.map((column, colIndex) => {
                                    return column.prop === 'checkbox' ? (
                                      <td key={colIndex}>
                                        <input type="checkbox" checked={row[column.prop]} onChange={(e) => this.handleCheckBox(e, row.id)} />
                                      </td>
                                    ) : (
                                      column.prop === 'title' ? (
                                        <td style={{cursor:'pointer'}} key={colIndex} onClick={() => this.handleLink(row.id)}>
                                          {row.writer + "님의 상담이 접수되었습니다."}
                                        </td>
                                      ) : (
                                        <td style={{cursor:'pointer'}} key={colIndex} onClick={() => this.handleLink(row.id)}>
                                          {row[column.prop]}
                                        </td>
                                      )
                                    )
                                  })
                                )
                              }
                            </tr>
                          ))
                        }
                      </tbody>
                    </table>
                    </div>
                    <br />
                    
                    <Pagination 
                      style={{display:'flex',justifyContent:'center'}} 
                      layout="prev, pager, next" 
                      currentPage={this.state.page} 
                      pageCount={this.state.total} 
                      pageSize={this.state.perPage} 
                      small={true}
                      onCurrentChange={this.handleChangePage}
                    />
                    <div className="board-search">
                      <div className="search-box">
                        <Select value={searchData.categ} placeholder="구분없음" onChange={this.categChangeHandler}>
                          {(expenseGuideCategories && expenseGuideCategories.main_categories && expenseGuideCategories.main_categories.length > 0 ) ? expenseGuideCategories.main_categories.map(el => {
                            return <Select.Option key={el.id} label={el.main_category_name} value={el.id} />
                          }) : null}
                        </Select>
                      </div>
                      <div className="search-box">
                        <Select value={searchData.searchOption} placeholder="글제목" onChange={this.searchOptionChangeHandler}>
                          {(expenseGuideCategories && expenseGuideCategories.search_options && expenseGuideCategories.search_options.length > 0 ) ? expenseGuideCategories.search_options.map(el => {
                            return <Select.Option key={el.id} label={el.search_option_name} value={el.id} />
                          }) : null}
                        </Select>
                      </div>
                      <span>
                        <input className="board-input search-box" type="text" value={query} onChange={(e) => this.searchChangeHandler(e, 'query')} />
                        {
                          (categ || searchOption || query) && <i className="el-icon-circle-cross" onClick={this.searchClear}></i> 
                        }
                      </span>
                      <a className="btn-search search-box" onClick={this.searchClickHanlder}>검색</a>
                    </div>
                  </div>
          
        )
    }
}

export default ExpenseGuides