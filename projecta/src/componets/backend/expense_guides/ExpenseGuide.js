import React, { Fragment } from 'react';
import api from 'common/api';
import { Link } from 'react-router-dom';
import { Select, Message } from 'element-react';

const columnsObj = {
    'phone': '휴대폰',
    'title': '제목',
    'writer': '작성자',
    'created_at': '작성일',
    'details': '내용',
    'main_category_id': '구분',
    'manager_id': '담당자',
    'consulting_status_id': '처리결과',
    'memo': '담당자 메모'
  }
  
  const columns = [
    'writer',
    'created_at',
    'phone',
    'main_category_id',
    'details'
  ];


class ExpenseGuide extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            expenseGuide: {},
            setUp: {}
        }

        this.history = this.props.history;
        this.match = this.props.match;
        this.currentExpenseGuideID = this.match.params.id;
    }

    componentDidMount() {
        this.getDatas(this.currentExpenseGuideID);
    }

    getDatas = async (currentExpenseGuideID) => {
        await api.getUpdateExpenseGuide(currentExpenseGuideID, true)
            .then(res => {
                const resData = res.data.data;

                if (resData) {
                  let expenseGuide = resData.expenseGuide;
                  let { created_at } = expenseGuide;
                  created_at = created_at ? ( typeof created_at === 'string' ? created_at.slice(0, 10) : created_at.toJSON().slice(0,10) ) : created_at;

                  expenseGuide = {
                      ...expenseGuide,
                      created_at
                  }

                  this.setState({
                      setUp: resData.setUp,
                      expenseGuide
                  })
                }
            })
            .catch(error => {
                console.log('something went wrong', error);
            });
    }

    editExpenseGuide = async () => {
    
      // input validation form check
      let validationCheck = true;

      const expenseGuideData = this.state.expenseGuide;
      const payloadKey = [
        'manager_id',
        'consulting_status_id',
        'memo'
      ];

      let payload = {};
      payloadKey.forEach(cur => {
        payload[cur] = expenseGuideData[cur];
      })

      if ( validationCheck ) {
          await api.updateExpenseGuide(this.currentExpenseGuideID, payload, true)
              .then(res => {
                Message({
                  message: '성공적으로 수정되었습니다.',
                  type: 'success'
                });
                console.log('successfully edit', res);
              
              })
              .then(() => {
                  this.goBack();
              })
              .catch(error => {
                  console.log('something went wrong', error);
              })
      }
    }

    deleteExpenseGuide = async () => {

        await api.deleteExpenseGuide(this.currentExpenseGuideID, true)
            .then((res)=> {
                Message({
                    message: '성공적으로 삭제되었습니다.',
                    type: 'success'
                });
            })
            .then(() => {
                this.goBack();
            })
            .catch(error => {
                console.log('something went wrong', error);
            });
    }
        
    goBack = () => {
        this.history.push('/backend/expense_guides');
    }

    handleChange = (event, input) => {
    
      let newData = {
        ...this.state.expenseGuide
      };
      
      if (input === 'manager_id' || input === 'consulting_status_id') {
        newData = {
          ...newData,
          [input]: event
        }
      } else {
        newData = {
          ...newData,
          [input]: event.target.value
        };
      }
  
      this.setState({
        expenseGuide: newData
      })
    }

    handleLink = (targetPage) => {
        if (targetPage) {
          const newURL = this.match.url.replace(':id', targetPage.id);
          this.history.push(newURL);
        }
    
        console.log('clicking Links...', targetPage);
    }


    render() {
        const { expenseGuide, setUp } = this.state;
        console.log('this is expenseGuide', expenseGuide);
        
        return (
          <div className="backend-body">
            <div className="backend-boardTop-style">
              <h2 className="backend-boardTop-title">비용안내</h2>
              <span className="backend-boardTop-text">비용안내 관리자 페이지 입니다.</span>
            </div>
              {
                expenseGuide.id ? (
                  <Fragment>
                    <ul className="backend-board-wrap">
                      <li>
                          <span className="backend-board-title">{columnsObj.writer}</span>
                          <span className="backend-board-text">{expenseGuide.writer}</span>
                      </li>

                      <li>
                          <span className="backend-board-title">{columnsObj.created_at}</span>
                          <span className="backend-board-text">{expenseGuide.created_at}</span>
                      </li>
                      <li>
                        <span className="backend-board-title">{columnsObj.main_category_id}</span>
                        <span className="backend-board-text">{expenseGuide.main_category_name}</span>
                      </li>
                      <li>
                        <span className="backend-board-title">{columnsObj.phone}</span>
                        <span className="backend-board-text">{expenseGuide.phone}</span>
                      </li>
                      <li>
                        <span className="backend-board-title">{columnsObj.title}</span>
                        <span className="backend-board-text">{expenseGuide.writer + '님의 상담이 접수되었습니다.'}</span>
                      </li>

                      <li>
                          <span className="backend-board-title">{columnsObj.details}</span>
                          <span className="backend-board-text">
                              {expenseGuide.details}
                          </span>
                      </li>
                      
                      <li>
                          <span className="backend-board-title">{columnsObj.manager_id}</span>
                          <span className="backend-board-text">
                              <Select value={expenseGuide.manager_id} placeholder="담당자 선택" onChange={(e) => this.handleChange(e, 'manager_id')}>
                                  {(setUp && setUp.managers && setUp.managers.length > 0) ? setUp.managers.map(el => (
                                      <Select.Option key={el.id} label={el.manager_name} value={el.id} />
                                  )) : null}
                              </Select>
                          </span>
                      </li>

                      <li>
                          <span className="backend-board-title">{columnsObj.consulting_status_id}</span>
                          <span className="backend-board-text">
                              <Select value={expenseGuide.consulting_status_id} placeholder="처리결과 선택" onChange={(e) => this.handleChange(e, 'consulting_status_id')}>
                                  {(setUp && setUp.consulting_statuses && setUp.consulting_statuses.length > 0) ? setUp.consulting_statuses.map(el => (
                                      <Select.Option key={el.id} label={el.consulting_status_name} value={el.id} />
                                  )) : null}
                              </Select>
                          </span>
                      </li>

                      <li>
                        <span className="backend-board-title">{columnsObj.memo}</span>
                        <span className="backend-board-text">
                          <input type="text" value={expenseGuide.memo} onChange={(e) => this.handleChange(e, 'memo')} />
                        </span>
                      </li>
                    </ul>     

                    <div style={{textAlign: 'right'}} className="backend-topBtn-wrap">
                      
                      <button className="btn-backbend btn-edit" onClick={this.editExpenseGuide}>수정하기</button>
                      <button className="btn-backbend btn-warning" onClick={this.deleteExpenseGuide}>삭제하기</button>
                      <button className="btn-backbend btn-check" onClick={this.goBack}>목록으로</button>
                    </div>

                    <div className="prev-contianer">
                      {
                        expenseGuide.previous ? (
                        <Fragment>
                         <div className="prev-wrap prev-wrap-boder">
                            <span className="border-arrow">
                              <Link to={`${this.match.path.replace(':id', expenseGuide.previous.id)}`}>
                              <svg  className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><spanath d="M0 16.67l2.829 2.83 9.175-9.339 9.167 9.339 2.829-2.83-11.996-12.17z" fill="##615454"/></svg>
                                이전글
                              </Link>
                            </span>
                            <span className="border-prev-body">{expenseGuide.previous.title}</span>
                          </div>
                        </Fragment>
                        ) : null
                      }
                      {
                        expenseGuide.next ? (
                        <Fragment>
                          <div className="prev-wrap">
                            <div className="border-arrow">
                              <Link to={`${this.match.path.replace(':id', expenseGuide.next.id)}`}>
                              <svg className="border-arrowSVG" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><spanath d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z" fill="##615454"/></svg>
                                다음글
                              </Link>
                            </div>
                            <span className="border-prev-body">{expenseGuide.next.title}</span>
                          </div>
                        </Fragment>
                        ) : null
                      }
                    </div>
                  </Fragment>
                  ) : (
                    <div>
                      something went wrong..
                    </div>
                )
              }
          </div>
        )
    }
}

export default ExpenseGuide