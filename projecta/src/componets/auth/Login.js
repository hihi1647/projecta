import React, { Component } from 'react';
import api from 'common/api';
import { Link } from 'react-router-dom';
import { registerToken } from 'constants/index';
import { isAuthenticated } from 'common/auth';
import User from 'common/User';

import { Message } from 'element-react';

const required = ['username', 'password'];

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        }

        if (isAuthenticated()) {
            this.props.history.go(-1);
        }
    }

    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;
    
        this.setState({
            [name]: value
        });
    }

    signIn = async (e) => {
        e.preventDefault();
        const data = {
            username: this.state.username,
            password: this.state.password,
            ...registerToken
        }

        await api.signIn(data)
            .then((res) => {
                console.log('successfully login..', res);
                // Login, cookie, redirect to frontend
                // need to be improve
                const user = new User();
                const $user = user.copy(res.data.data);
                window.$user = $user.deepCopy();

                const newState = {
                    user: $user,
                    isAuthenticated: isAuthenticated()
                }

                this.props.changeState(newState);
            })
            .then(() => {
                let { from } = this.props.location.state || { from: { pathname: "/" }};

                this.props.history.replace(from);
            })
            .catch(error => {
                const resData = error.response.data;
                const { success, message } = resData;
                if (!success) {
                    Message({
                        message: '로그인 정보가 틀렸습니다. 이메일과 비밀번호를 확인해 주세요.',
                        type: 'error'
                    });
                } else {
                    console.log(error.data);
                }
            })
    }

    isRequiredFilled = () => {
        return required.length === required.filter(cur => this.state[cur]).length
    }


    render() {
       const {username, password} = this.state;
       const disabled = !this.isRequiredFilled();
       console.log('this is login', this.props);
        return (
            <div>
                <div className="board-top-style">
                    <ul className="board-title-box">
                    <li className="board-title">로그인</li>
                    <li className="board-title-box-line"></li>
                    <li className="board-text">MIBALANCE LOGIN</li>
                    </ul>
                </div>
                <div className="auth-container">
                    <div className="register--container">
                    <form className="register--form">
                        <div>
                            <label className="register--form-text">이메일</label>
                            <input className="register--input" type="email" name="username" value={username} onChange={this.handleChange} />
                        </div>
                        <div>
                            <label className="register--form-text">비밀번호</label>
                            <input className="register--input" type="password" name="password" value={password} onChange={this.handleChange} />
                        </div>   
                        <button className="btn-signin" disabled={disabled} onClick={this.signIn}>로그인</button>
                    </form>
                    </div>
                    <ul className="login-chekbox-container">
                            <li className="login-find">
                                <p className="login-find-text">· 비밀번호를 잊으셨나요? 비밀번호를 찾아주세요.</p>
                                <Link to={'/forgotten_password'} className="btn-find">비밀번호 찾기</Link>
                            </li>
                            <li className="login-find">
                                <p className="login-find-text">· 아직 회원이 아니시라면 회원 가입 후 이용해 주세요.</p>
                                <Link to={`/register`} className="btn-find">회원가입</Link>
                            </li>
                        </ul>
                </div>
            </div>
        )
    }
}

export default Login;