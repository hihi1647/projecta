import React, { Component } from 'react';
import api from 'common/api';
import { isAuthenticated } from 'common/auth';
import User from 'common/User';
import { Message } from 'element-react';
import { Link } from 'react-router-dom';

const required = ['email', 'password', 'passwordCheck', 'name', 'phone', 'condition1_required', 'condition2_required'];
const validationCheck = {
    email: {
        regex: /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i,
        msg: '이메일이 정확하지 않습니다.',
        required: true
    },
    password: {
        regex: /^[A-Za-z0-9]{6,12}$/,
        msg: '비밀번호를 숫자와 영문자 조합으로 6~12자 이내로 적어주세요',
        required: true
    },
    passwordCheck: {
        regex: 'passwordCheck',
        msg: '비밀번호가 일치하지 않습니다.',
        required: true
    },
    name: {
        regex: 'required',
        msg: '이름은 필수 입력항목입니다.',
        required: true
    },
    phone: {
        regex: /^\d{3}-\d{3,4}-\d{4}$/,
        msg: '휴대폰 번호는 필수 입력항목입니다.(000-0000-0000)',
        required: true
    },
    condition1_required: {
        regex: 'required',
        msg: '회원가입 약관에 동의해 주세요.',
        required: true
    },
    condition2_required: {
        regex: 'required',
        msg: '개인정보 수집에 동의해 주세요.',
        required: true
    }
}

// phone = /^\d{3}-\d{3,4}-\d{4}$/; (핸드폰번호)
// phone = /^\d{2,3}-\d{3,4}-\d{4}$/; (일반전화)
        
class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            name: '',
            password: '',
            passwordCheck: '',
            phone: '',
            checkbox_All: false,
            condition1_required: false,
            condition2_required: false,
            condition3: false,
            isEmailNotDuplicated: false
        }

        if (isAuthenticated()) {
            this.props.history.go(-1);
        }
    }

    handleCheckBox = (event, input) => {
        let { condition1_required, condition2_required, condition3 } = this.state;

        let checkbox = {
            condition1_required,
            condition2_required,
            condition3
        };

        checkbox = {
            ...checkbox,
            [input]: event.target.checked
        };

        let checkbox_All = Object.keys(checkbox).length === Object.keys(checkbox).filter(cur => checkbox[cur]).length;

        this.setState({
            checkbox_All,
            [input]: event.target.checked
        })
    }

    handleSelectAll = () => {
        let checkbox_All = !this.state.checkbox_All;

        this.setState({
            checkbox_All,
            condition1_required: checkbox_All,
            condition2_required: checkbox_All,
            condition3: checkbox_All
        })
    }

    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;

        this.setState({
            [name]: value
        });
    }

    checkEmailDuplicated = async () => {
        const { email } = this.state;
        const isValid = validationCheck.email.regex.test(email);
        if (email && isValid) {

            const data = {
                email
            };
    
            await api.checkEmailDuplicated(data)
                .then((res) => {
                    console.log('successfully registered..', res);
                    const resData = res.data.data;
                    if (resData) {
                        this.setState({
                            isEmailNotDuplicated: true
                        });
                    } else {
                        Message({
                            message: '중복된 이메일입니다.',
                            type: 'error'
                        });
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        } else {
            Message({
                message: '이메일이 정확하지 않습니다.',
                type: 'error'
            })
        }
    }

    signUp = async (e) => {
        e.preventDefault();
        const validationCheckMsg = this.validationCheck();

        if (!this.state.isEmailNotDuplicated) {
            Message({
                message: '중복된 이메일입니다.',
                type: 'error'
            });
            return ;
        }
        if (validationCheckMsg !== 'success') {
            Message({
                message: validationCheckMsg,
                type: 'error'
            });
            return ;
        }

        const data = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            phone: this.state.phone
        }

        await api.registerUser(data)
            .then((res) => {
                console.log('successfully registered..', res);
                // Login, cookie, redirect to frontend

                // if (res.data.result)

                const user = new User();
                const $user = user.copy(res.data.data);
                window.$user = $user.deepCopy();

                const newState = {
                    user: $user,
                    isAuthenticated: isAuthenticated()
                }

                this.props.changeState(newState);
            })
            .then(() => {
                let { from } = this.props.location.state || { from: { pathname: "/" }};

                this.props.history.replace(from);
            })
            .catch(error => {
                console.log(error);
            })
    }

    goBack = () => {
        this.props.history.go(-1);
    }

    isRequiredFilled = () => {
        return required.length === required.filter(cur => this.state[cur]).length
    }

    validationCheck = () => {

        for (let i = 0; i < required.length; i += 1) {
            const current = required[i];
            const regex = validationCheck[current].regex;
            let isValid = true;
            if (regex === 'required') {
                isValid = this.state[current];
            } else if (regex === 'passwordCheck') {
                isValid = this.state.password === this.state.passwordCheck;
                
            } else {
                isValid = regex.test(this.state[current]);
            }

            if (!isValid) {
                return validationCheck[current].msg;
            }
        }

        return 'success';
    }

    render() {
        const { 
            name, email, 
            password, phone, passwordCheck, checkbox_All, 
            condition1_required, condition2_required, condition3 
        } = this.state;
        const disabled = !this.isRequiredFilled();

        return (
            <div>
            <div className="board-top-style">
                <ul className="board-title-box">
                <li className="board-title">회원가입</li>
                <li className="board-title-box-line"></li>
                <li className="board-text">MIBALANCE REGISTER</li>
                </ul>
            </div>
            <div className="auth-container">
                <div className="register--container">
                    <form className="register--form"> 
                        <div>
                            <label className="register--form-text">이메일<span style={{color: 'red'}}>*</span></label>
                            <input className="register--input" type="email" name="email" value={email} onChange={this.handleChange} onBlur={this.checkEmailDuplicated} placeholder="@까지 정확하게 입력해주세요." />
                            <p className="error_email">아이디는 5자 이상으로 입력해 주세요.</p>
                        </div>
                        <div>
                            <label className="register--form-text">비밀번호<span style={{color: 'red'}}>*</span></label>
                            <input className="register--input" type="password" name="password" value={password} onChange={this.handleChange} placeholder="영문+숫자 조합 6~12자리"/>
                        </div>
                        <div>
                            <label className="register--form-text">비밀번호 확인<span style={{color: 'red'}}>*</span></label>
                            <input className="register--input"  type="password" name="passwordCheck" value={passwordCheck} onChange={this.handleChange}/>
                        </div>
                        <div>
                            <label className="register--form-text">이름<span style={{color: 'red'}}>*</span></label>
                            <input className="register--input" type="text" name="name" value={name} onChange={this.handleChange} />
                        </div>
                        <div>
                            <label className="register--form-text">휴대폰 번호<span style={{color: 'red'}}>*</span></label>
                            <input className="register--input"  type="text" name="phone" value={phone} onChange={this.handleChange} placeholder="000-0000-0000"/>
                        </div>
                        
                    </form>
                </div>

                <ul className="register__chekbox--container">
                    <li className="register--checkbox-all">
                        <input className="chk-box-none" type="checkbox" checked={checkbox_All} onChange={this.handleSelectAll} id="chk1" />
                        
                        <label for="chk1"><span className="chk-icon"></span><span className="chk--all">전체 동의합니다.</span></label>
                        <p className="chk--text">전체동의는 필수 및 선택정보에 대한 동의도 포함되어 있으며, 개별적으로도 동의를 선택하실 수 있습니다. 선택항목에 대한 동의를 거부하는 경우에도 회원가입 서비스는 이용 가능합니다.</p>
                    </li>
                    <li className="register--checkbox">
                   
                        <input className="chk-box-none" type="checkbox" checked={condition1_required} onChange={(e) => this.handleCheckBox(e, 'condition1_required')} id="chk2" />
                       
                        <label for="chk2"><span className="chk-icon"></span><span className="chk--title">회원가입 약관 (필수)</span></label>
                        <Link to={`condition/provision`} className="register-link-btn">자세히 보기</Link>
                        
                    </li>
                    <li className="register--checkbox">
                        <input className="chk-box-none" type="checkbox" checked={condition2_required} onChange={(e) => this.handleCheckBox(e, 'condition2_required')} id="chk3" />
                        <label for="chk3"><span className="chk-icon"></span><span className="chk--title">개인정보처리방침 (필수)</span></label>
                        <Link to={`condition/personal_information`} className="register-link-btn">자세히 보기</Link>
                    </li>
                    {/* <li className="register--checkbox">
                        <input className="chk-box-none" type="checkbox" checked={condition3} onChange={(e) => this.handleCheckBox(e, 'condition3')} id="chk4" />
                        <label for="chk4"><span className="chk-icon"></span><a href="https://www.naver.com"className="chk--title">예약확인 및 이벤트 정보 수신 동의(선택)</a></label>
                        <Link to={`condition/personal_information`} className="register-link-btn">자세히 보기</Link>
                    </li> */}
                    <li className="register--btn">
                        <button className="register--cbtn" onClick={this.goBack}>가입취소</button>
                        <button className="register--jbtn" disabled={disabled} onClick={this.signUp}>회원가입</button>
                    </li>
                </ul>
                </div>
            </div>
        )
    }
}

export default Register;