import React from 'react';
import { Route, Redirect } from "react-router-dom";

export const AuthenticatedRoute = ({
    component: Component,
    exact,
    path,
    authSuccess,
    $user
  }) => (
    <Route
      exact={exact}
      path={path}
      render={props =>
        authSuccess ? (
          <Component {...props} $user={$user} />
        ) : (
          <Redirect to={{ pathname: "/login", state: { from: props.location }}} />
        )
      }
    />
  )