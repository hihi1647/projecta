import React, { Fragment } from 'react';
import { isAuthenticated } from 'common/auth';
import api from 'common/api';
import { Message } from 'element-react';

class Forgotten_Password extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: ''
        }

        if (isAuthenticated()) {
            this.props.history.go(-1);
        }
    }

    changeHandler = (e) => {
        this.setState({
            email: e.target.value
        })
    }

    goBack = () => {
        //this.props.history.push('/backend/before_afters');
        this.props.history.go(-1);
    }

    sendEmail = async () => {
        // do email validation check;
        const validationCheckMsg = true;

        const payload = {
            email: this.state.email,
        }

        await api.sendResetPasswordEmail(payload)
            .then((res) => {
                const resData = res.data.data;
                if (resData) {
                    Message({
                        message: '성공적으로 보냈습니다.',
                        type: 'success'
                    });
                    this.goBack();
                } else {
                    Message({
                        message: '없는 이메일입니다.',
                        type: 'error'
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })
    }

    render() {
        return (
            <Fragment>
            <div className="board-top-style">
                <ul className="board-title-box">
                    <li className="board-title">비밀번호 찾기</li>
                    <li className="board-title-box-line"></li>
                    <li className="board-text">MIBALANCE</li>
                </ul>
            </div>
            <div className="auth-container">
                <div className="register--container forgotten-password-container">
                <form className="register--form "> 
                    <div>
                        <label className="register--form-text">이메일 주소를 입력해주세요.<span style={{color: 'red'}}>*</span></label>
                        <input className="register--input forgotten_password-input" type="text" placeholder="@까지 정확하게 입력해주세요." value={this.state.email} onChange={this.changeHandler} />

                        {/* <button className="btn-forgotten_password btn-forgotten_password-c " onClick={this.goBack}>취소</button> */}
                        <button className="btn-forgotten_password btn-forgotten_password-s " onClick={this.sendEmail}>보내기</button>
                    </div>
                </form>
                </div>
            </div>
            </Fragment>
        )
    }
}

export default Forgotten_Password;