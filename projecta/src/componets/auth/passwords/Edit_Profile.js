import React from 'react';
import { isAuthenticated } from 'common/auth';
import myCookie from 'common/MyCookie';
import api from 'common/api';
import { MessageBox, Message } from 'element-react';


// 여기서는 회원이 로그인 되어있는지 확인하고 유저 아이디를 보낸다. 수정하고 나선 로그아웃하고 다시 로그인 페이지로
// reset_password는 링크타고 들어와서 code가 유효한지 파악하고 유효하면 바꿀 수 있게 한다.
// register는 로그인이 안 되어 있을 때
// forgotten페이지도 로그인이 안 되어 있을 때

const required = ['password', 'passwordCheck', 'phone'];
const validationCheck = {
    password: {
        regex: /^[A-Za-z0-9]{6,12}$/,
        msg: '비밀번호를 숫자와 영문자 조합으로 6~12자 이내로 적어주세요',
        required: true
    },
    passwordCheck: {
        regex: 'passwordCheck',
        msg: '비밀번호가 일치하지 않습니다.',
        required: true
    },
    phone: {
        regex: 'required',
        msg: '휴대폰 번호는 필수 입력항목입니다.',
        required: true
    }
}
class Edit_Profile extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            data: {}
        }

        this.userID = '';

        if (isAuthenticated() && this.props.$user) {
            this.userID = this.props.$user.id;
        } else {
            this.props.history.go(-1);
        }
    }

    componentDidMount() {
        this.getDatas();
    }

    getDatas = async () => {
        const payload = {id: this.userID};
        await api.getUserData(payload, true)
          .then(res => {
            const resData = res.data.data;
            let phone = res.data.data.phone;
            if (!phone) {
                phone = '';
            }

            if (resData) {
                const data = {
                    ...resData,
                    password: '',
                    passwordCheck: '',
                    phone
                }
                this.setState({
                    data
                })
            }
          })
          .catch(error => {
            console.log('something went wrong', error);
          });
    }
    
    
    updateUser = async () => {
        // input validation form check
        const validationCheckMsg = this.validationCheck();
        if (validationCheckMsg !== 'success') {
            Message({
                message: validationCheckMsg,
                type: 'error'
            });
            return ;
        }

        const payloadKey = [
            'id',
            'phone',
            'password'
        ];

        const { phone, password } = this.state.data;

        const payload = {
            id: this.userID,
            phone,
            password
        }


        await api.updateUser(payload, true)
            .then(res => {
                Message({
                    message: '성공적으로 수정되었습니다.',
                    type: 'success'
                });
            })
            .then(() => {
                this.signOut();
                this.props.history.push('/login');
            })
            .catch(error => {
                console.log('something went wrong', error);
            })
    }

    deleteUser = () => {
        MessageBox.prompt('비밀번호를 입력해주세요.', '사용자 확인', {
            confirmButtonText: '확인',
            cancelButtonText: '취소',
          }).then(async ({ value }) => {
            const payload = {
                password: value,
                id: this.userID
            };
            return await api.deleteUser(payload, true);
          }).then((res)=>{
            if (res && res.data.data) {
              Message({
                type: 'success',
                message: '탈퇴가 완료되었습니다.'
              });
      
              this.props.history.push('/');
            } else {
              Message({
                type: 'info',
                message: "비밀번호가 일치하지 않습니다."
              });
            }
          }).catch((err) => {
            console.log('something went wrong', err);
          });
    }
    
    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;

        const data = {
            ...this.state.data,
            [name]: value
        }

        this.setState({
            data
        });
    }

    signOut = () => {
        // need to be improve
        myCookie.clearUser();
        window.$user = null;

        const newState = {
            user: null,
            isAuthenticated: isAuthenticated()
        }

        this.props.changeState(newState);
    }

    goBack = () => {
        //this.props.history.push('/backend/before_afters');
        this.props.history.go(-1);
    }

    isRequiredFilled = () => {
        return required.length === required.filter(cur => this.state.data[cur]).length
    }

    validationCheck = () => {

        for (let i = 0; i < required.length; i += 1) {
            const current = required[i];
            const regex = validationCheck[current].regex;
            let isValid = true;
            if (regex === 'required') {
                isValid = this.state.data[current];
            } else if (regex === 'passwordCheck') {
                isValid = this.state.data.password === this.state.data.passwordCheck;
                
            } else {
                isValid = regex.test(this.state.data[current]);
            }

            if (!isValid) {
                return validationCheck[current].msg;
            }
        }

        return 'success';
    }

    render() {
        const { email, name, phone, password, passwordCheck } = this.state.data;
        const disabled = !this.isRequiredFilled();
        console.log('this is edit profile page', this.state);
        return (
            <div>
                <div className="board-top-style">
                    <ul className="board-title-box">
                        <li className="board-title">회원정보 수정</li>
                        <li className="board-title-box-line"></li>
                        <li className="board-text">MIBALANCE EDIT PROFILE</li>
                    </ul>
                </div>
                <div className="auth-container">
                    <div className="register--container ptofile--container">
                        <form className="register--form"> 
                            <div>
                                <span className="register--form-text edit-profile-box">이메일</span>
                                <span className="register--input edit-profile-box edit-profile-text">{email}</span>
                            </div>
                            <div>
                                <span className="register--form-text edit-profile-box">이름</span>
                                <span className="register--input edit-profile-box edit-profile-text">{name}</span>
                            </div>

                            <div>
                                <label className="register--form-text">비밀번호<span style={{color: 'red'}}>*</span></label>
                                <input className="register--input" type="password" name="password" value={password} onChange={this.handleChange} placeholder="영문+숫자 조합 6~12자리"/>
                            </div>
                            <div>
                                <label className="register--form-text">비밀번호 확인<span style={{color: 'red'}}>*</span></label>
                                <input className="register--input"  type="password" name="passwordCheck" value={passwordCheck} onChange={this.handleChange}/>
                            </div>
                            
                            <div>
                                <label className="register--form-text">휴대폰 번호<span style={{color: 'red'}}>*</span></label>
                                <input className="register--input"  type="text"  name="phone" value={phone} onChange={this.handleChange} />
                            </div>     
                        </form>

                        <ul className=" btn-ptofile--container">
                            <li className="btn-ptofile">
                                <button className="register--cbtn btn-ptofile-02" onClick={this.deleteUser}>회원 탈퇴</button>
                                <button className="register--cbtn btn-ptofile-02" onClick={this.goBack}>취소</button>
                                <button className="register--jbtn btn-ptofile-01" disabled={disabled} onClick={this.updateUser}>회원정보 수정</button>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        )
    }
}

export default Edit_Profile;