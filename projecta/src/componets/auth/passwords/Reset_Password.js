import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { isAuthenticated } from 'common/auth';
import myCookie from 'common/MyCookie';
import api from 'common/api';
import { Message } from 'element-react';

const required = ['password', 'passwordCheck'];
const validationCheck = {
    password: {
        regex: /^[A-Za-z0-9]{6,12}$/,
        msg: '비밀번호를 숫자와 영문자 조합으로 6~12자 이내로 적어주세요',
        required: true
    },
    passwordCheck: {
        regex: 'passwordCheck',
        msg: '비밀번호가 일치하지 않습니다.',
        required: true
    }
}

// code 검사해서 유효하면 true, 아니면 false
// 유효해서 리셋성공하면 resetSuccess true
class Reset_Password extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            password: '',
            passwordCheck: '',
            code: false,
            resetSuccess: false
        }

        this.history = this.props.history;
        this.match = this.props.match;
        this.linkCode = this.match.params.id;
    }

    componentDidMount() {
        this.checkEmailLinkIsValid();
    }

    checkEmailLinkIsValid = async () => {
        const payload = { code: this.linkCode };

        await api.checkEmailLinkIsValid(payload)
            .then(res => {
                const resData = res.data.data; 

                /* 코드가 있을 시
                {
                    "success": true,
                    "data": true,
                    "message": "There is the code."
                }
                or 
                
                코드가 없을 시

                {
                    "success": true,
                    "data": false,
                    "message": "There is not the code."
                } */

                this.setState({
                    code: resData
                })
            })
            .catch(error => {
                console.log('something went wrong', error);
            });
    }

    resetPassword = async () => {
        // input validation form check
        const validationCheckMsg = this.validationCheck();
        if (validationCheckMsg !== 'success') {
            Message({
                message: validationCheckMsg,
                type: 'error'
            });
            return ;
        }

        const payloadKey = [
            'code',
            'password'
        ];

        const code = this.linkCode;

        const { password } = this.state;

        const payload = {
            code,
            password
        }

        await api.resetPassword(payload, true)
            .then(res => {
                this.setState({
                    resetSuccess: true
                })
            })
            .catch(error => {
                console.log('something went wrong', error);
            })

    }

    isRequiredFilled = () => {
        return required.length === required.filter(cur => this.state[cur]).length
    }

    validationCheck = () => {

        for (let i = 0; i < required.length; i += 1) {
            const current = required[i];
            const regex = validationCheck[current].regex;
            let isValid = true;
            if (regex === 'required') {
                isValid = this.state[current];
            } else if (regex === 'passwordCheck') {
                isValid = this.state.password === this.state.passwordCheck;
                
            } else {
                isValid = regex.test(this.state[current]);
            }

            if (!isValid) {
                return validationCheck[current].msg;
            }
        }

        return 'success';
    }

    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;

        this.setState({
            [name]: value
        });
    }

    signOut = () => {
        // 이메일 링크 타고 들어왔는데 라우터 먹힐건지..
        // need to be improve
        myCookie.clearUser();
        window.$user = null;

        const newState = {
            user: null,
            isAuthenticated: isAuthenticated()
        }

        this.props.changeState(newState);
    }

    render() {
        const { password, passwordCheck, code, resetSuccess } = this.state;
        const disabled = !this.isRequiredFilled();
        return (
            <Fragment>
            <div className="board-top-style">
                <ul className="board-title-box">
                    <li className="board-title">비밀번호 수정</li>
                    <li className="board-title-box-line"></li>
                    <li className="board-text">MIBALANCE</li>
                </ul>
            </div>
                {
                    resetSuccess ? (
                        
                        <div className="border-container">
                          <div className="board-list">
                              <div style={{borderTop: '3px solid #594544'}}/>
                              <div className="condition-box" style={{textAlign:'center'}}>
                                <span className="condition-text" style={{fontSize:'16px'}}>
                                성공적으로 변경되었습니다.
                                </span>
                              </div>
                            </div>
                            <div className="btn-home-wrap">
                                <Link to={`/`} className="btn-home">홈으로 가기</Link>
                            </div>
                        </div>
                        ) : (
                        code ? (
                            <div>
                                <div className="register--container reset-password-container">
                                    <form className="register--form"> 
                                        <div>
                                            <label className="register--form-text">비밀번호<span style={{color: 'red'}}>*</span></label>
                                            <input className="register--input" type="password" name="password" value={password} onChange={this.handleChange} placeholder="영문+숫자 조합 6~12자리"/>
                                        </div>
                                        <div>
                                            <label className="register--form-text">비밀번호 확인<span style={{color: 'red'}}>*</span></label>
                                            <input className="register--input" type="password" name="passwordCheck" value={passwordCheck} onChange={this.handleChange}/>
                                        </div>   
                                    </form>
                                    <div className="reset-password-btn-container">
                                        <button className="register--jbtn " disabled={disabled} onClick={this.resetPassword}>비밀번호 수정</button>
                                    </div>
                                </div>
                            </div>
                        ) : (
                            <div className="border-container">
                                <div className="board-list">
                                    <div style={{borderTop: '3px solid #594544'}}/>
                                    <div className="condition-box" style={{textAlign:'center'}}>
                                    <span className="condition-text" style={{fontSize:'16px'}}>
                                         유효하지 않는 링크입니다.
                                    </span>
                                    </div>
                                </div>
                                <div className="btn-home-wrap">
                                    <Link to={`/`} className="btn-home">홈으로 가기</Link>
                                </div>
                            
                            </div>
                            
                        )               
                    )
                }
            </Fragment>
        )
    }
}

export default Reset_Password;