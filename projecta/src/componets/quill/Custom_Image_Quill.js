// https://medium.com/@engross/react-quill%EC%97%90%EC%84%9C-image-upload%EC%99%80-ie-ios-%EB%8C%80%EC%9D%91%ED%95%98%EA%B8%B0-3a8a709ee4ae
// https://codesandbox.io/s/qv5m74l80w
import React from "react";
import ReactQuill, { Mixin, Toolbar, Quill } from "react-quill";
// import Dropzone, { ImageFile } from "react-dropzone";
import ImageResize from '@taoqf/quill-image-resize-module';
import { Radio, Dialog, Tooltip } from 'element-react';
import "react-quill/dist/quill.snow.css";
import "./style.css";

import api from 'common/api';
import { currentUsesDomain }  from 'constants/index'

const imgUrl = currentUsesDomain.imgUrl;

const __ISMSIE__ = navigator.userAgent.match(/Trident/i) ? true : false;
const __ISIOS__ = navigator.userAgent.match(/iPad|iPhone|iPod/i) ? true : false;

Quill.register('modules/ImageResize', ImageResize);

class Custom_Image_Quill extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contents: __ISMSIE__ ? "<p>&nbsp;</p>" : "",
      dialogVisible: false,
      imgServerLink: '',
      imgLocalFile: '',
      imgLink: '',
      imgRadio: 'file',
    };

    this.quillRef = null;
    this.dropzone = null;
    this.onKeyEvent = false;

    this.modules = {
      toolbar: {
        container: [
          ["bold", "italic", "underline", "strike", "blockquote", { color: [] }, { background: [] }],
          [{ font: [] }, { size: ["small", false, "large", "huge"] }],
          [
            { list: "ordered" },
            { list: "bullet" },
            { indent: "-1" },
            { indent: "+1" },
            { align: [] }
          ],
          ["link", "image", "video"],
          ["clean"]
        ],
        handlers: { image: this.imageHandler }
      },
      ImageResize: {
        displayStyles: {
          backgroundColor: 'black',
          border: 'none',
          color: 'white'
        },
        modules: [ 'Resize', 'DisplaySize', 'Toolbar' ]
      },
      clipboard: { matchVisual: false }
    };
  
    this.formats = [
      "header",
      "bold",
      "italic",
      "underline",
      "strike",
      "blockquote",
      "font",
      "size",
      "color",
      "background",
      "list",
      "bullet",
      "indent",
      "link",
      "image",
      "video",
      "align"
    ];
  }

  handleRadioChange = (e, input) => {
    this.setState({
        [input]: e,
        imgServerLink: '',
        imgLocalFile: '',
        imgLink: '',
    })
  }

  handleImgChange = (e, input) => {
    let value, imgLink;
    if (input === 'imgLocalFile') {
        value = e.target.files[0];
        imgLink = window.URL.createObjectURL(value);
    } else {
        value = e.target.value;
        imgLink = value;
    }
    
    this.setState({
        [input]: value,
        imgLink
    })
  }

  cleanImgUpload = () => {
      this.setState({
          dialogVisible: false,
          imgServerLink: '',
          imgLocalFile: '',
          imgLink: '',
          imgRadio: 'link'
      })
  }

  imgUpload = async () => {

      const { imgServerLink, imgLocalFile, imgLink } = this.state;
      let inputImage;
      // validation check!;
      if (imgServerLink) {
          inputImage = imgServerLink;

          this.updateQuillImageUpload(inputImage);
      } else if (imgLocalFile) {
          const formData = new FormData();
          formData.append('file', imgLocalFile);

          await api.uploadImage(formData, true)
              .then((res) => {
                  if (res && res.data) {
                      inputImage = imgUrl + res.data;

                      this.updateQuillImageUpload(inputImage);
                  }
              })
              .catch((err) => {
                  console.log('something went wrong...', err);
              });
      }

      this.cleanImgUpload();
  }

  updateQuillImageUpload = (url) => {
    const quill = this.quillRef.getEditor();
    quill.focus();
    const range = quill.getSelection();
    quill.insertEmbed(range.index, "image", url);
    quill.setSelection(range.index + 1);
    
  }

  imageHandler = () => {
    this.setState({ dialogVisible: true })
    // if (this.dropzone) this.dropzone.open();
  };

  
  onKeyUp = (event) => {
    if (!__ISIOS__) return;
    // enter
    if (event.keyCode === 13) {
      this.onKeyEvent = true;
      this.quillRef.blur();
      this.quillRef.focus();
      if (document.documentElement.className.indexOf("edit-focus") === -1) {
        document.documentElement.classList.toggle("edit-focus");
      }
      this.onKeyEvent = false;
    }
  };

  onFocus = () => {
    if (
      !this.onKeyEvent &&
      document.documentElement.className.indexOf("edit-focus") === -1
    ) {
      document.documentElement.classList.toggle("edit-focus");
      // window.scrollTo(0, 0);
    }
  };

  onBlur = () => {
    if (
      !this.onKeyEvent &&
      document.documentElement.className.indexOf("edit-focus") !== -1
    ) {
      document.documentElement.classList.toggle("edit-focus");
    }
  };

  doBlur = () => {
    this.onKeyEvent = false;
    this.quillRef.blur();
    // force clean
    if (document.documentElement.className.indexOf("edit-focus") !== -1) {
      document.documentElement.classList.toggle("edit-focus");
    }
  };

  onChangeContents = (contents) => {
    /* let _contents = null;
    if (__ISMSIE__) {
      if (contents.indexOf("<p><br></p>") > -1) {
        _contents = contents.replace(/<p><br><\/p>/gi, "<p>&nbsp;</p>");
      }
    }
    this.setState({ contents: _contents || contents }); */

    let _contents = '';
    if (__ISMSIE__) {
      if (contents.indexOf("<p><br></p>") > -1) {
        _contents = contents.replace(/<p><br><\/p>/gi, "<p>&nbsp;</p>");
      }
    }

    _contents = _contents || contents;

    this.props.onChange(_contents);
  };

  render() {
    const { imgServerLink, imgLocalFile, imgLink } = this.state;
    return (
      <div className="main-panel rich-container">
        <div className="main-content">
          <ReactQuill
            ref={(el) => (this.quillRef = el)}
            value={this.props.setContents || ''}
            onChange={this.onChangeContents}
            onKeyUp={this.onKeyUp}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            theme="snow"
            modules={this.modules}
            formats={this.formats}
          />

          <Dialog
              title="이미지 업로드"
              visible={ this.state.dialogVisible }
              onCancel={this.cleanImgUpload}
          >
              <Dialog.Body>
                  <Radio.Group value={this.state.imgRadio} onChange={(e) => this.handleRadioChange(e, 'imgRadio')}>
                  <Radio.Button value="file" >File Upload</Radio.Button>
                      <Radio.Button value="link" >URL</Radio.Button>
                  </Radio.Group>
                  {
                      this.state.imgRadio === 'link' ? (
                          <div className="image__upload-link">
                              <input value={imgServerLink} onChange={(e) => this.handleImgChange(e, 'imgServerLink') }/>
                          </div>
                      ) : (
                          <div>
                              <button style={{width: '100%', position: 'relative'}}>
                                  <div className="image__upload-file">
                                      <input type="file" name="file" onChange={(e) => this.handleImgChange(e, 'imgLocalFile')} />
                                      Click to Upload
                                  </div>
                              </button>
                          </div>
                      )
                  }
                  {
                      imgLink && (
                          <div style={{width: '200px', height: '200px', border: '1px solid black', margin: 'auto'}}>
                              <img src={imgLink} style={{width: '100%', height: '100%', maxWidth: '100%', maxHeight: '100%', objectFit: 'cover'}} />
                          </div>
                      )
                  }
                  
              </Dialog.Body>

              <Dialog.Footer className="dialog-footer">
                  <button onClick={this.imgUpload}>확인</button>
              </Dialog.Footer>
          </Dialog>
        </div>
      </div>
    );
  }
}

export default Custom_Image_Quill;