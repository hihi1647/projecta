
export const registerToken = {
    "grant_type" : 'password',
    "client_id" : 2,
    "client_secret" : 'doux4l9lifqPlTmlAd8aseSRxQCfZ9xPFNwaitoo'
}

const SERVER_LIST = {
    mibalance: {
        label: 'mibalance',
        imgUrl: 'http://mibalance.co.kr/mibalance_api/public/images/',
        api: 'mibalance_api/public/api'
    },
    bchyundai: {
        label: 'bchyundai',
        imgUrl: 'http://15.165.177.86/bchyundai/public/images/',
        api:'bchyundai/public/api'
    }
}

const defaultUsesDomainURL = 'bchyundai';
let currentUsesDomainURL = defaultUsesDomainURL;

function checkingCurrentUsesDomain() {
    const domainCases = ['mibalance'];
    const hostname = window.location.hostname;

    domainCases.forEach(domain => {
        if (hostname.includes(domain)) {
            currentUsesDomainURL = domain;
        }
    });
}

checkingCurrentUsesDomain();

export const currentUsesDomain = SERVER_LIST[currentUsesDomainURL];

export const imgUrl = "http://15.165.177.86/bchyundai/public/images/"

export const DATE_FORMAT = 'MM/dd/yyyy';

export const ROLES = {
    admin: {
        name: '관리자',
        roles: [1]
    },
    nurse: {
        name: '담당자',
        roles: [2]
    },
    user: {
        name: '일반회원',
        roles: [3]
    }
}

export const reservation_answer_status = {
    answer_pending: 1,
    answer_done: 2
}

export const NO_FIXED_ID = "no_fixed"

export const HAS_NUMS_DETAIL_PAGE_COMPS = [
    'Component_B', 'Component_D', 'Component_E', 'Component_F', 'Component_Tab01',
    'Component_QnA', 'Component_IntroduceHeight', 'Component_ImgOneTitle', 'Component_ImgOneWithNum'
];