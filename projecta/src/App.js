import React, { Component, Fragment, useEffect } from 'react';
import './App.css';
import { Route, Switch, BrowserRouter, useLocation } from 'react-router-dom';
import { Frontend, Backend } from './pages';
import { AuthenticatedRoute } from 'componets/auth/AuthenticatedRoute';
import User from 'common/User';
import myCookie from 'common/MyCookie';
import { isAuthenticated } from 'common/auth';

function ScrollToTop() {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
}

class App extends Component {
  constructor() {
    super();
    this.state = {
      user: User.fromCookie(),
      isAuthenticated: isAuthenticated()
    }
  }

  changeState = (newStateObj) => {
    this.setState(newStateObj);
  }

  render() {
    const $user = this.state.user;
    const isAuthenticated = this.state.isAuthenticated;
    return (
      <BrowserRouter>
        <ScrollToTop />
        <Switch>
          {/* 관리자 페이지 */}
          {/* <Route path="/backend" component={Backend} /> */}
          <AuthenticatedRoute authSuccess={$user && $user.isAdmin} component={Backend} $user={$user} path={"/backend"}/>
          {/* 프론트 페이지 */}
          <Route path="/" render={props => <Frontend {...props} $user={$user} isAuthenticated={isAuthenticated} changeState={this.changeState} /> } />
        </Switch>
      </BrowserRouter>
    );
  }

}

export default App;